-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:11
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:installLocation
		ADDED from AndroidManifest.xml:4:11
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:6:5
	android:name
		ADDED from AndroidManifest.xml:6:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:7:5
	android:name
		ADDED from AndroidManifest.xml:7:22
uses-permission#android.permission.READ_CONTACTS
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.RECEIVE_SMS
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.SEND_SMS
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.CAMERA
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.RECORD_AUDIO
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.FLASHLIGHT
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:18:5
	android:name
		ADDED from AndroidManifest.xml:18:22
uses-feature#android.hardware.camera
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:19
uses-feature#android.hardware.camera.autofocus
ADDED from AndroidManifest.xml:23:5
	android:required
		ADDED from AndroidManifest.xml:25:9
	android:name
		ADDED from AndroidManifest.xml:24:9
uses-feature#android.hardware.camera.flash
ADDED from AndroidManifest.xml:26:5
	android:required
		ADDED from AndroidManifest.xml:28:9
	android:name
		ADDED from AndroidManifest.xml:27:9
uses-feature#android.hardware.telephony
ADDED from AndroidManifest.xml:29:5
	android:required
		ADDED from AndroidManifest.xml:31:9
	android:name
		ADDED from AndroidManifest.xml:30:9
application
ADDED from AndroidManifest.xml:33:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:appcompat-v7:21.0.0:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:38:9
	android:allowBackup
		ADDED from AndroidManifest.xml:35:9
	android:icon
		ADDED from AndroidManifest.xml:37:9
	android:theme
		ADDED from AndroidManifest.xml:40:9
	android:hardwareAccelerated
		ADDED from AndroidManifest.xml:36:9
	android:largeHeap
		ADDED from AndroidManifest.xml:39:9
	android:name
		ADDED from AndroidManifest.xml:34:9
provider#com.clikclap.db.DataProvider
ADDED from AndroidManifest.xml:41:9
	android:exported
		ADDED from AndroidManifest.xml:44:13
	android:authorities
		ADDED from AndroidManifest.xml:43:13
	android:name
		ADDED from AndroidManifest.xml:42:13
activity#com.clikclap.activities.SplashActivity
ADDED from AndroidManifest.xml:46:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:50:13
	android:noHistory
		ADDED from AndroidManifest.xml:49:13
	android:label
		ADDED from AndroidManifest.xml:48:13
	android:theme
		ADDED from AndroidManifest.xml:51:13
	android:name
		ADDED from AndroidManifest.xml:47:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:52:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:53:17
	android:name
		ADDED from AndroidManifest.xml:53:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:55:17
	android:name
		ADDED from AndroidManifest.xml:55:27
activity#com.clikclap.activities.RegistrationActivity
ADDED from AndroidManifest.xml:58:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:63:13
	android:screenOrientation
		ADDED from AndroidManifest.xml:61:13
	android:label
		ADDED from AndroidManifest.xml:60:13
	android:theme
		ADDED from AndroidManifest.xml:62:13
	android:name
		ADDED from AndroidManifest.xml:59:13
activity#com.clikclap.activities.MainActivity
ADDED from AndroidManifest.xml:64:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:69:13
	android:screenOrientation
		ADDED from AndroidManifest.xml:68:13
	android:label
		ADDED from AndroidManifest.xml:66:13
	android:name
		ADDED from AndroidManifest.xml:65:13
	android:launchMode
		ADDED from AndroidManifest.xml:67:13
activity#com.clikclap.activities.NewVideoActivity
ADDED from AndroidManifest.xml:75:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:78:13
	android:label
		ADDED from AndroidManifest.xml:77:13
	android:theme
		ADDED from AndroidManifest.xml:79:13
	android:name
		ADDED from AndroidManifest.xml:76:13
activity#com.clikclap.activities.ContactsActivity
ADDED from AndroidManifest.xml:80:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:83:13
	android:label
		ADDED from AndroidManifest.xml:82:13
	android:theme
		ADDED from AndroidManifest.xml:84:13
	android:name
		ADDED from AndroidManifest.xml:81:13
activity#com.clikclap.activities.PlayFlowActivity
ADDED from AndroidManifest.xml:85:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:89:13
	android:label
		ADDED from AndroidManifest.xml:87:13
	android:theme
		ADDED from AndroidManifest.xml:90:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:88:13
	android:name
		ADDED from AndroidManifest.xml:86:13
meta-data#android.support.PARENT_ACTIVITY
ADDED from AndroidManifest.xml:91:13
	android:name
		ADDED from AndroidManifest.xml:92:17
	android:value
		ADDED from AndroidManifest.xml:93:17
activity#com.clikclap.activities.CommentsActivity
ADDED from AndroidManifest.xml:95:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:99:13
	android:label
		ADDED from AndroidManifest.xml:97:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:98:13
	android:name
		ADDED from AndroidManifest.xml:96:13
activity#com.clikclap.activities.GuessSmileActivity
ADDED from AndroidManifest.xml:104:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:107:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:106:13
	android:name
		ADDED from AndroidManifest.xml:105:13
activity#com.clikclap.activities.FillProfileActivity
ADDED from AndroidManifest.xml:112:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:115:13
	android:theme
		ADDED from AndroidManifest.xml:116:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:114:13
	android:name
		ADDED from AndroidManifest.xml:113:13
activity#com.clikclap.activities.AgreementActivity
ADDED from AndroidManifest.xml:121:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:124:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:123:13
	android:name
		ADDED from AndroidManifest.xml:122:13
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:appcompat-v7:21.0.0:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
