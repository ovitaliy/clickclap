package com.clickclap.api.requests;

import android.test.InstrumentationTestCase;

import com.clickclap.Const;
import com.clickclap.rest.service.AppRetrofitSpiceService;
import com.clickclap.util.PrefHelper;

import retrofit.RestAdapter;

/**
 * Created by ovi on 2/10/16.
 */
public abstract class BaseApiTestCase<API_SET> extends InstrumentationTestCase {

    private static final String API_END_POINT = "http:/";

    private Class<API_SET> mClass;

    public BaseApiTestCase(Class<API_SET> aClass) {
        mClass = aClass;
    }

    protected RestAdapter restAdapter;
    protected static String PHONE = "359878843471";
    protected static String USER_ID = "570";

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        PrefHelper.init(getInstrumentation().getContext());
        PrefHelper.setStringPref(Const.PREF_TOKEN, "1455783798MBr57iZiJkCeaoVaFHwl");

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Const.BASE_API_URL)
                .setConverter(new AppRetrofitSpiceService.TestConverter())
                .build();


    }

    public API_SET getApiSet() {
        return restAdapter.create(mClass);
    }

}
