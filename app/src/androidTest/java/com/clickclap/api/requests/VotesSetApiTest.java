package com.clickclap.api.requests;

import com.clickclap.enums.VoteListType;
import com.clickclap.model.Vote;
import com.clickclap.model.VoteOption;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VoteRestApi;
import com.clickclap.rest.model.AddVoteResponse;
import com.clickclap.rest.model.VoteListsResponse;
import com.clickclap.rest.model.VoteResponse;
import com.clickclap.rest.request.vote.AddVoteRequest;
import com.clickclap.rest.request.vote.GetVoteRequest;
import com.clickclap.rest.request.vote.GetVotesListRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovi on 2/12/16.
 */
public class VotesSetApiTest extends BaseApiTestCase<VoteRestApi> {

    private static final boolean ENABLED_ALL = false;

    private static final boolean ENABLED_ADD_VOTE = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_VOTE_LIST = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_MY_VOTE_LIST = ENABLED_ALL | false;


    public VotesSetApiTest() {
        super(VoteRestApi.class);
    }


    public void testGetVoteList() {
        if (!ENABLED_GET_VOTE_LIST) return;

        List<Vote> voteList = getVoteList();

        assertNotNull(voteList);

        for (Vote vote : voteList) {
            checkVote(vote);
        }
    }

    public void testGetMyVoteList() {
        if (!ENABLED_GET_MY_VOTE_LIST) return;

        VoteListsResponse voteListsResponse = getApiSet().getMyList(new ArgsMap());
        assertNotNull(voteListsResponse);
        assertNull(voteListsResponse.getErrorCodes());

        List<Vote> voteList = voteListsResponse.getVotes();

        assertNotNull(voteList);

        for (Vote vote : voteList) {
            checkVote(vote);
        }
    }

    public void testAddVote() {
        if (!ENABLED_ADD_VOTE) return;

        final String newVoteDescription = "Test vote" + (System.currentTimeMillis() / 1000);

        AddVoteResponse addVoteResponse = getApiSet().addVote(AddVoteRequest.getArgsMap(newVoteDescription, new String[]{"a", "b"}));

        assertNotNull(addVoteResponse);
        assertNull(addVoteResponse.getErrorCodes());


        int newVoteId = addVoteResponse.getId();
        assertTrue(newVoteId != 0);

        VoteResponse voteResponse = getApiSet().getVote(GetVoteRequest.getArgsMap(newVoteId));
        assertNotNull(voteResponse);
        assertNull(voteResponse.getErrorCodes());

        assertTrue(voteResponse.getVote().getOptions().size() == 2);
        assertEquals(voteResponse.getVote().getOptions().get(0).getTitle(), "a");
        assertEquals(voteResponse.getVote().getOptions().get(1).getTitle(), "b");
    }

    private void checkVote(Vote vote) {
        assertNotNull(vote);

        assertTrue(vote.getId() != 0);
        assertNotNull(vote.getTitle());

        VoteResponse voteResponse = getApiSet().getVote(GetVoteRequest.getArgsMap(vote.getId()));
        assertNotNull(voteResponse);
        assertNull(voteResponse.getErrorCodes());

        List<VoteOption> voteOptions = voteResponse.getVote().getOptions();
        assertNotNull(voteOptions);
        assertTrue(voteOptions.size() != 0);

        for (VoteOption voteOption : voteOptions) {
            assertNotNull(voteOption);
            assertTrue(voteOption.getId() != 0);
            assertNotNull(voteOption.getTitle());
        }
    }

    private List<Vote> getVoteList() {

        List<Vote> voteList = new ArrayList<>();

        VoteListType periods[] = {
                VoteListType.NOW, VoteListType.PAST
        };

        for (VoteListType period : periods) {

            VoteListsResponse voteListsResponse = getApiSet().getVoteList(GetVotesListRequest.getArgsMap(period));
            assertNotNull(voteListsResponse);
            assertNull(voteListsResponse.getErrorCodes());
            if (voteListsResponse.getVotes() != null)
                voteList.addAll(voteListsResponse.getVotes());

        }
        return voteList;

    }
}
