package com.clickclap.api.requests;

import com.clickclap.model.City;
import com.clickclap.model.Country;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.DatabaseRestApi;
import com.clickclap.rest.model.CitiesListResponse;
import com.clickclap.rest.model.CountriesListResponse;

import java.util.List;

/**
 * Created by ovi on 2/12/16.
 */
public class DatabaseSetApiTest extends BaseApiTestCase<DatabaseRestApi> {

    private static final boolean ENABLED_ALL = true;

    private static final boolean ENABLED_GET_COUNTRIES = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_CITY = ENABLED_ALL | false;

    public DatabaseSetApiTest() {
        super(DatabaseRestApi.class);
    }

    public void testGetCountries() {
        if (!ENABLED_GET_COUNTRIES) return;

        CountriesListResponse response = getApiSet().getCountries(new ArgsMap());

        assertNotNull(response);
        assertNull(response.getErrorCodes());

        List<Country> countries = response.getCountries();
        assertNotNull(countries);
        assertTrue(countries.size() > 0);
    }

    public void testGetCities() {
        if (!ENABLED_GET_CITY) return;

        final String checkingCityName = "Москв";

        ArgsMap args = new ArgsMap(true);
        args.put("id_country", String.valueOf(1));
        args.put("q", checkingCityName);
        args.put("lang", "ru");

        CitiesListResponse response = getApiSet().getCities(args);

        assertNotNull(response);
        assertNull(response.getErrorCodes());

        List<City> cities = response.getCities();
        assertNotNull(cities);
        boolean cityFound = false;

        for (City city : cities) {
            assertNotNull(city);
            assertTrue(city.getId() != 0);
            assertNotNull(city.getTitle());

            if (city.getTitle().contains(checkingCityName)) {
                cityFound = true;
            }
        }

        assertTrue("cities response does not contains city that starts with " + checkingCityName, cityFound);

    }

}
