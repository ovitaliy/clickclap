package com.clickclap.api.requests;

import com.clickclap.AppUser;
import com.clickclap.enums.VideoType;
import com.clickclap.model.Category;
import com.clickclap.model.Comment;
import com.clickclap.model.Video;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.FlowRestApi;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.CategoriesListResponse;
import com.clickclap.rest.model.CommentsResponse;
import com.clickclap.rest.model.FlowResponse;
import com.clickclap.rest.request.CategoriesRequestType;
import com.clickclap.rest.request.GetCategoriesRequest;
import com.clickclap.rest.request.video.GetFlowRequest;
import com.clickclap.rest.request.video.MarkVideoRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovi on 2/11/16.
 */
public class VideoSetApiTest extends BaseApiTestCase<VideoRestApi> {

    private static final boolean ENABLED_ALL = false;

    private static final boolean ENABLED_VIDEO_CATEGORIES = ENABLED_ALL | false;
    private static final boolean ENABLED_LIKES = ENABLED_ALL | false;
    private static final boolean ENABLED_COMMENTS = ENABLED_ALL | false;
    private static final boolean ENABLED_BUYING_IN_MARKET = ENABLED_ALL | false;

    public VideoSetApiTest() {
        super(VideoRestApi.class);
    }

    public void testVideoCategoriesApi() {
        if (!ENABLED_VIDEO_CATEGORIES) return;

        VideoType[] videoTypes = VideoType.values();

        for (VideoType videoType : videoTypes) {
            ArgsMap map = GetCategoriesRequest.getArgsMap(CategoriesRequestType.FULL, videoType);
            CategoriesListResponse response = restAdapter.create(FlowRestApi.class).getCategories(map);

            assertNotNull(response);
            assertNull(response.getErrorsMessage());

            List<Category> categories = response.getCategories();

            assertNotNull(categories);
            assertTrue(response.getCategories().size() != 0);

            for (Category category : categories) {

                assertNotNull(category.getTitle());
                assertNotNull(category.getParams());

                if (category.getVideosCount() > 0) {
                    assertNotNull(category.getImage());

                    map = GetFlowRequest.getArgsMap(category.getParams(), 0, 0);
                    FlowResponse flowResponse = restAdapter.create(FlowRestApi.class).getFlow(map);

                    assertNotNull(flowResponse);
                    assertNull(flowResponse.getErrorCodes());


                    ArrayList<Video> videos = flowResponse.getFlow();
                    assertNotNull(videos);
                    assertTrue(videos.size() != 0);

                    for (Video video : videos) {
                        assertNotNull(video.getAuthorName());

                        assertTrue(video.getVideoId() != 0);
                        assertNotNull(getVideoErrorMessage(category, "getAuthorId"), video.getAuthorId());
                        assertNotNull(getVideoErrorMessage(category, "getAuthorName"), video.getAuthorName());
                        assertNotNull(getVideoErrorMessage(category, "getThumbUrl"), video.getThumbUrl());
                        assertNotNull(getVideoErrorMessage(category, "getUrl"), video.getShortUrl());
                        assertNotNull(getVideoErrorMessage(category, "getShortUrl"), video.getShortUrl());
                        assertNotNull(getVideoErrorMessage(category, "getCity"), video.getCity());
                        assertNotNull(getVideoErrorMessage(category, "getCountry"), video.getCountry());
                    }
                }
            }
        }
    }

    public void testLikes() {
        if (!ENABLED_LIKES) return;

        Video video = getVideo();

        assertNotNull(video);

        // like video
        ArgsMap map = MarkVideoRequest.getArgsMap(video.getVideoId(), 1);
        BaseResponse baseResponse = getApiSet().markVideo(map);

        assertNotNull(baseResponse);
        assertNotNull(baseResponse.getErrorCodes());

        video = getVideo();
        assertNotNull(video);

        assertTrue(video.getMyMark() == 1);

        //unlike video
        map = MarkVideoRequest.getArgsMap(video.getVideoId(), 0);
        baseResponse = getApiSet().markVideo(map);

        assertNotNull(baseResponse);
        assertNotNull(baseResponse.getErrorCodes());

        video = getVideo();
        assertNotNull(video);

        assertTrue(video.getMyMark() == 0);

    }

    public void testComments() {
        if (!ENABLED_COMMENTS) return;


        Video video = getVideo();

        String newCommentText = "test comment " + System.currentTimeMillis();

        // add new comment
        ArgsMap map = new ArgsMap();
        map.put("id_video", video.getVideoId());
        map.put("comment", newCommentText);
        BaseResponse baseResponse = getApiSet().commentVideo(map);

        assertNotNull(baseResponse);
        assertNull(baseResponse.getErrorCodes());


        // check if comment list contains new added comment
        map = new ArgsMap();
        map.put("id_video", video.getVideoId());
        CommentsResponse commentsResponse = getApiSet().getComments(map);

        assertNotNull(commentsResponse);
        assertNull(commentsResponse.getErrorCodes());

        List<Comment> comments = commentsResponse.getComments();
        assertNotNull(comments);
        assertTrue("Must be at least one comment", comments.size() != 0);

        boolean commentFound = false;
        for (Comment comment : comments) {
            assertNotNull(comment);

            assertNotNull(comment.getAuthor());
            assertTrue(comment.getAuthorId() != 0);
            assertNotNull(comment.getAvatarUrl());
            assertNotNull(comment.getText());
            assertTrue(comment.getTime() != null);

            if (comment.getText().equals(newCommentText)) {
                commentFound = true;
            }
        }

        assertTrue("new added comment not found in comment list", commentFound);
    }

    public void testShare() {
        Video video = getVideo();

        ArgsMap map = new ArgsMap();
        map.put("numbers", "380997746073");
        map.put("id_video", video.getVideoId());

        BaseResponse baseResponse = getApiSet().shareVideo(map);

        assertNotNull(baseResponse);
        assertNull(baseResponse.getErrorCodes());
    }

    public Video getVideo() {
        ArgsMap map = new ArgsMap();
        map.put("type", "full");
        map.put("id_user", USER_ID);

        FlowResponse flowResponse = restAdapter.create(FlowRestApi.class).getFlow(map);

        return flowResponse.getFlow().get(0);
    }

    private String getVideoErrorMessage(Category category, String value) {
        return String.format("Video value '%s' in category '%s'", value, category.getTitle());
    }

}
