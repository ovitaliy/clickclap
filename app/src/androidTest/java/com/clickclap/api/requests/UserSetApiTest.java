package com.clickclap.api.requests;

import android.content.IntentFilter;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.enums.Education;
import com.clickclap.enums.Hobby;
import com.clickclap.enums.Interest;
import com.clickclap.enums.Job;
import com.clickclap.enums.Pet;
import com.clickclap.enums.Religion;
import com.clickclap.enums.Sport;
import com.clickclap.model.UserFinded;
import com.clickclap.model.UserInfo;
import com.clickclap.receivers.SmsReceiver;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.AuthResponse;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.CoinsHistoryListResponse;
import com.clickclap.rest.model.UserInfoResponse;
import com.clickclap.rest.model.UserSearchResponse;
import com.clickclap.rest.model.VerifyCodeResponse;
import com.clickclap.rest.request.user.EditUserInfoRequest;
import com.clickclap.util.PrefHelper;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by ovi on 2/10/16.
 */
public class UserSetApiTest extends BaseApiTestCase<UserRestApi> {

    private static final boolean ENABLED_ALL = true;

    private static final boolean ENABLED_CHECK_TOKEN = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_USER = ENABLED_ALL | false;
    private static final boolean ENABLED_EDIT = ENABLED_ALL | false;
    private static final boolean ENABLED_CHECK_BALANCE = ENABLED_ALL | false;
    private static final boolean ENABLED_SEARCH = ENABLED_ALL | false;
    private static final boolean ENABLED_FOLLOW_UNFOLLOW = ENABLED_ALL | false;
    private static final boolean ENABLED_AUTH = ENABLED_ALL | false;


    private String mTestName;

    public UserSetApiTest() {
        super(UserRestApi.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        mTestName = "testName" + System.currentTimeMillis();
    }

    public void testCheckToken() {
        if (!ENABLED_CHECK_TOKEN) return;

        String token = PrefHelper.getToken();
        assertNotNull(token);
    }

    public void testGetUser() throws Exception {
        if (!ENABLED_GET_USER) return;

        UserInfoResponse userResponse = getApiSet().getUserInfo(new ArgsMap());

        assertNotNull(userResponse);

        UserInfo userInfo = userResponse.getUser();
        assertNotNull(userInfo);

        assertNotNull(userInfo.getId() != 0);
        assertNotNull(userInfo.getPhoto());
        assertNotNull(userInfo.getCountry());
        assertNotNull(userInfo.getCity());
    }

    public void testEditUser() {
        if (!ENABLED_EDIT) return;

        final int INDEX = 2;
        final String QUESTIONNAIRE_ID = String.valueOf(INDEX);


        UserInfoResponse userResponse = restAdapter.create(UserRestApi.class).getUserInfo(new ArgsMap());

        UserInfo userInfo = userResponse.getUser();
        userInfo.setFName(mTestName);
        userInfo.setLName(mTestName);
        userInfo.setHobbie(Hobby.getById(QUESTIONNAIRE_ID));
        userInfo.setSport(Sport.getById(QUESTIONNAIRE_ID));
        userInfo.setPet(Pet.getById(QUESTIONNAIRE_ID));
        userInfo.setInterest(Interest.getById(QUESTIONNAIRE_ID));
        userInfo.setReligion(Religion.getById(QUESTIONNAIRE_ID));

        userInfo.setJobType(Job.getById(QUESTIONNAIRE_ID));
        userInfo.setDegree(Education.getById(QUESTIONNAIRE_ID));

        userInfo.setCountryId(1);
        userInfo.setCityId(1);

        userInfo.setJobTitle("craft");

        userInfo.setBirthDate(INDEX);

        //---
        BaseResponse editResponse = getApiSet().editUserInfo(EditUserInfoRequest.getArgsMap(userInfo));
        //---
        assertNotNull(editResponse);

        userResponse = getApiSet().getUserInfo(new ArgsMap());
        userInfo = userResponse.getUser();

        assertEquals(mTestName, userInfo.getFName());
        assertEquals(mTestName, userInfo.getLName());
        assertEquals(INDEX, userInfo.getBirthDate());

        assertEquals(userInfo.getHobbie().getId(), INDEX);
        assertEquals(userInfo.getSport().getId(), INDEX);
        assertEquals(userInfo.getPet().getId(), INDEX);
        assertEquals(userInfo.getInterest().getId(), INDEX);
        assertEquals(userInfo.getReligion().getId(), INDEX);

        assertEquals(userInfo.getJobType().getId(), INDEX);
        assertEquals(userInfo.getDegree().getId(), INDEX);

        assertNotSame(userInfo.getJobTitle(), "craft");

        assertEquals(userInfo.getCityId(), 1);
        assertEquals(userInfo.getCountryId(), 1);

    }


    public void testBalance() {
        if (!ENABLED_CHECK_BALANCE) return;

        UserInfoResponse userResponse = getApiSet().getUserInfo(new ArgsMap());
        UserInfo userInfo = userResponse.getUser();

        CoinsHistoryListResponse coinsHistoryListResponse = restAdapter.create(UserRestApi.class).getCoinsHistory(new ArgsMap(true));

        assertNotNull("coinsHistoryListResponse is null", coinsHistoryListResponse);

        assertEquals(userInfo.getBalance(), coinsHistoryListResponse.getBalance());

        if (userInfo.getBalance() != 0) {
            assertNotNull("coinsHistoryList con not be null", coinsHistoryListResponse.getHistory());
            assertNotSame("coinsHistoryList con not be empty", coinsHistoryListResponse.getHistory().size(), 0);
        }
    }

    public void testSearch() {
        if (!ENABLED_SEARCH) return;

        UserSearchResponse userSearchResponse = getApiSet().searchUser(new ArgsMap());

        assertNotNull(userSearchResponse);

        List<UserFinded> foundUsersList = userSearchResponse.getUsers();

        assertNotNull(foundUsersList);
        assertNotSame("must me at least one user", foundUsersList.size(), 0);

        for (UserFinded userFinded : foundUsersList) {

            assertNotNull(userFinded);
            assertTrue(userFinded.getId() != 0);
            assertNotNull(userFinded.getPhoto());
            assertNotNull(userFinded.getFirstName());
            assertNotNull(userFinded.getLastName());
            assertNotNull(userFinded.getLang());

            assertNotNull(userFinded.getCity());
            assertNotNull(userFinded.getCountry());
        }
    }


    public void testFollowUnFollow() {
        if (!ENABLED_FOLLOW_UNFOLLOW) return;


        UserSearchResponse userSearchResponse = getApiSet().searchUser(new ArgsMap());

        assertNotNull(userSearchResponse);

        List<UserFinded> foundUsersList = userSearchResponse.getUsers();

        for (UserFinded userFinded : foundUsersList) {
            int userId = userFinded.getId();
            if (userId == AppUser.getUid())
                continue;

            ArgsMap argsMap = new ArgsMap();
            argsMap.put("id_user", userId);

            // follow user
            BaseResponse response = getApiSet().followUser(argsMap);

            assertNotNull(response);

            // check relationship
            UserInfoResponse userResponse = getApiSet().getUserInfo(argsMap);

            assertTrue(userResponse.getUser().getRelationship() == 1);


            //unfollow user
            response = getApiSet().unfollowUser(argsMap);
            assertNotNull(response);

            // check relationship
            userResponse = getApiSet().getUserInfo(argsMap);

            assertTrue(userResponse.getUser().getRelationship() == 0);

            break;
        }
    }


    private String mSmsCode;

    private CountDownLatch mCountDownLatch;

    public void testAuth() {
        if (!ENABLED_AUTH) return;

        ArgsMap map = new ArgsMap(false);
        map.put("token", "");
        AuthResponse authResponse = getApiSet().auth(map);

        assertNotNull(authResponse);

        String token = authResponse.getToken();
        assertNotNull(token);

        PrefHelper.setStringPref(Const.PREF_TOKEN, token);

        App.getInstance().registerReceiver(new SmsReceiver(
                new SmsReceiver.SmsReceiverListener() {
                    @Override
                    public void onGetSmsCode(CharSequence code) {
                        mSmsCode = code.toString();

                        mCountDownLatch.countDown();
                    }
                }
        ), new IntentFilter(SmsReceiver.SMS_RECEIVED));

        map = new ArgsMap(true);
        map.put("phone", PHONE);

        BaseResponse baseResponse = getApiSet().getSmsCode(map);
        assertNotNull(baseResponse);

        mCountDownLatch = new CountDownLatch(1);

        try {
            mCountDownLatch.await(10000, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        assertNotNull("sms code can not be null", mSmsCode);

        map = new ArgsMap(true);
        map.put("code", mSmsCode);
        VerifyCodeResponse verifyCodeResponse = getApiSet().verifySmsCode(map);

        assertNull(verifyCodeResponse.getErrorCodes());
        assertTrue("userId can not be null", verifyCodeResponse.getUserId() != 0);
        assertTrue("there is existing user, status can not be 0", verifyCodeResponse.getStatus() != 0);
    }


}
