package com.clickclap.jobs;

import android.database.Cursor;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.UploadingMedia;
import com.clickclap.model.Video;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.request.video.VideoUploadRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by ovi on 4/25/16.
 */
public class UploadVideoJob extends Thread{

    public static void startUploader() {
        UploadVideoJob uploadVideoJob = new UploadVideoJob();
        uploadVideoJob.setPriority(MIN_PRIORITY);
        uploadVideoJob.run();
    }

    @Override
    public void run() {
        Cursor videoCursor = App.getInstance().getContentResolver().query(ContentDescriptor.Videos.URI,
                null,
                ContentDescriptor.Videos.Cols.UPLOADING_STATUS + " = " + Video.STATUS_UPLOAD_REQUIRED,
                null,
                ContentDescriptor.Videos.Cols.CREATED_AT + " ASC");

        if (videoCursor != null && videoCursor.moveToFirst()) {
            do {
                int id = videoCursor.getInt(videoCursor.getColumnIndex("_id"));
                VideoUploadRequest videoUploadRequest = new VideoUploadRequest(id);
                videoUploadRequest.setPriority(SpiceRequest.PRIORITY_LOW);
                App.getFileSpiceManager().execute(videoUploadRequest, String.valueOf(id), DurationInMillis.ALWAYS_EXPIRED, new BaseRequestListener<UploadingMedia>());
            } while (videoCursor.moveToNext());

            videoCursor.close();
        }
    }

}
