package com.clickclap.jobs;

import android.content.ContentValues;
import android.net.Uri;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.Const;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.UploadingMedia;
import com.clickclap.model.Video;
import com.clickclap.model.VideoInfo;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.request.video.VideoUploadRequest;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.NetworkUtil;
import com.octo.android.robospice.persistence.DurationInMillis;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by ovitali on 13.11.2015.
 */
public final class SaveVideoToUploadJob extends Thread {

    private VideoInfo mVideoInfo;

    public static void run(VideoInfo videoInfo) {
        new SaveVideoToUploadJob(videoInfo).start();
    }

    private SaveVideoToUploadJob(VideoInfo videoInfo) {
        mVideoInfo = videoInfo;
    }

    @Override
    public void run() {
        Video video = new Video();
        video.setCreatedAt(System.currentTimeMillis());
        video.setUploadingStatus(Video.STATUS_UPLOAD_REQUIRED);

        if (mVideoInfo.getVideoType() != null) {
            video.setType(mVideoInfo.getVideoType().getId());
        }

        if (mVideoInfo.getCategoryId() != 0)
            video.setCategoryId(mVideoInfo.getCategoryId());

        video.setCost(mVideoInfo.getCost());
        video.setReplyId(mVideoInfo.getReplyToVideoId());
        video.setSmileId(mVideoInfo.getSmileId());
        video.setTitle(mVideoInfo.getTags());

        File srcFileVideo = new File(mVideoInfo.getVideoPath());
        File srcFileImage = new File(mVideoInfo.getImagePath());

        File outFileVideo = new File(FilePathHelper.getUploadDirectory(), video.getCreatedAt() + "." + Const.VIDEO_EXTENTION);
        File outFileImage = new File(FilePathHelper.getUploadDirectory(), video.getCreatedAt() + "." + Const.IMAGE_EXTENTION);

        try {
            FileUtils.copyFile(srcFileVideo, outFileVideo);
            FileUtils.copyFile(srcFileImage, outFileImage);
        } catch (IOException e) {
            Log.e("SaveVideoToUploadJob", "IOException", e);
        }

        ContentValues contentValues = video.toContentValues();
        contentValues.put(ContentDescriptor.Videos.Cols.IS_AVATAR, mVideoInfo.isAvatar() ? 1 : 0);
        Uri uri = App.getInstance().getContentResolver().insert(ContentDescriptor.Videos.URI, contentValues);

        if (!NetworkUtil.isOfflineMode()) {
            int id = Integer.parseInt(uri.getLastPathSegment());
            App.getFileSpiceManager().execute(new VideoUploadRequest(id), String.valueOf(id), DurationInMillis.ONE_SECOND, new BaseRequestListener<UploadingMedia>());
        }
    }
}
