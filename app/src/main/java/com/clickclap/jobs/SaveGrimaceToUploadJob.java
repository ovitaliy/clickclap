package com.clickclap.jobs;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.Grimace;
import com.clickclap.model.VideoInfo;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.request.video.GrimaceUploadRequest;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.ImageCroper;
import com.clickclap.util.NetworkUtil;
import com.humanet.filters.FilterController;
import com.octo.android.robospice.persistence.DurationInMillis;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ovitali on 13.11.2015.
 */
public final class SaveGrimaceToUploadJob extends Thread {

    private VideoInfo mVideoInfo;

    public static void run(VideoInfo videoInfo) {
        new SaveGrimaceToUploadJob(videoInfo).start();
    }

    private SaveGrimaceToUploadJob(VideoInfo videoInfo) {
        mVideoInfo = videoInfo;
    }

    @Override
    public void run() {
        long createdAt = System.currentTimeMillis();
        Grimace grimace = new Grimace();
        grimace.setAuthorId(AppUser.getUid());

        grimace.setSmileId(mVideoInfo.getSmileId());
        grimace.setEffect(FilterController.getFilterId(mVideoInfo.getFilter()));

        grimace.setToUpload(1);

        File srcFileImage = new File(mVideoInfo.getImagePath());

        File outFileImage = new File(FilePathHelper.getUploadDirectory(), createdAt + "." + Const.IMAGE_EXTENTION);

        Bitmap bitmap = BitmapFactory.decodeFile(srcFileImage.getAbsolutePath());
        Context context = App.getInstance();

        bitmap = ImageCroper.getCircularBitmap(bitmap,
                context.getResources().getDimensionPixelSize(R.dimen.image_border_size),
                context.getResources().getColor(R.color.yellow)
        );

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outFileImage);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        } catch (IOException e) {
            Log.e("SaveVideoToUploadJob", "IOException", e);
        }

        grimace.setUrl(outFileImage.getAbsolutePath());

        ContentValues contentValues = grimace.toContentValues();
        contentValues.put(ContentDescriptor.Grimaces.Cols.TYPE, Grimace.TYPE_COLLECTION);
        Uri uri = App.getInstance().getContentResolver().insert(ContentDescriptor.Grimaces.URI, contentValues);

        if (!NetworkUtil.isOfflineMode()) {
            int id = Integer.parseInt(uri.getLastPathSegment());
            App.getFileSpiceManager().execute(new GrimaceUploadRequest(id), String.valueOf(id), DurationInMillis.ONE_SECOND, new BaseRequestListener());
        }
    }
}
