package com.clickclap.jobs;

import android.database.Cursor;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.AddGrimaceResponse;
import com.clickclap.rest.request.video.GrimaceUploadRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by ovi on 4/25/16.
 */
public class UploadGrimaceJob extends Thread {

    public static void startUploader() {
        UploadGrimaceJob uploadVideoJob = new UploadGrimaceJob();
        uploadVideoJob.setPriority(MIN_PRIORITY);
        uploadVideoJob.run();
    }

    @Override
    public void run() {
        Cursor cursor = App.getInstance().getContentResolver().query(ContentDescriptor.Grimaces.URI,
                null,
                ContentDescriptor.Grimaces.Cols.TO_UPLOAD + " = 1",
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                GrimaceUploadRequest uploadRequest = new GrimaceUploadRequest(id);
                uploadRequest.setPriority(SpiceRequest.PRIORITY_LOW);
                App.getFileSpiceManager().execute(uploadRequest, String.valueOf(id), DurationInMillis.ALWAYS_EXPIRED, new BaseRequestListener<AddGrimaceResponse>());
            } while (cursor.moveToNext());

            cursor.close();
        }
    }

}
