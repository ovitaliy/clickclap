package com.clickclap.enums;

import com.clickclap.rest.ServerRequestParams;

/**
 * Created by Deni on 27.07.2015.
 */
public enum VideoUpdateAction implements ServerRequestParams {
    END {
        @Override
        public String getRequestParam() {
            return "end";
        }
    }, WINNER {
        @Override

        public String getRequestParam() {
            return "winner";
        }
    }
}
