package com.clickclap.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.clickclap.App;
import com.clickclap.view.CirclePickerItem;

/**
 * Created by Denis on 12.02.2015.
 */
public enum Emotion implements CirclePickerItem {
    SMILE,
    FUN,
    SAD,
    THINK,
    SILENCE,
    SURPRISE,
    FACEPALM,
    WINK,
    TEASE,
    KISS;


    public int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String emotionName = "e_" + name().toLowerCase();
        return context.getResources().getIdentifier(emotionName, type, packageName);
    }

    @Override
    public Drawable getDrawable() {
        Drawable drawable = null;
        int resId = getResId(App.getInstance(), "drawable");
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(App.getInstance(), resId);
        }
        return drawable;
    }

    @Override
    public int getId() {
        return ordinal() + 1;
    }

    @Override
    public String getTitle() {
        String title = null;
        Context context = App.getInstance();
        int resId = getResId(context, "string");
        if (resId != 0) {
            title = context.getString(resId);
        }
        return title;
    }

    public static Emotion getById(int id) {
        if (id < 1) {
            id = 1;
        }
        for (Emotion emotion : values()) {
            if (emotion.getId() == id) {
                return emotion;
            }
        }
        return null;
    }

}
