package com.clickclap.enums;

import android.text.TextUtils;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.rest.ServerRequestParams;

import java.util.Locale;

/**
 * Created by Denis on 12.03.2015.
 */
public enum Language implements ServerRequestParams {
    RUS {
        @Override
        public String getRequestParam() {
            return "ru";
        }
    },
    ENG {
        @Override
        public String getRequestParam() {
            return "en";
        }
    },
    ESP {
        @Override
        public String getRequestParam() {
            return "es";
        }
    };

    public int getId() {
        return ordinal();
    }

    public String getTitle() {
        switch (this) {
            case ESP:
                return App.getInstance().getString(R.string.esp_lang);
            case RUS:
                return App.getInstance().getString(R.string.rus_lang);
            default:
                return App.getInstance().getString(R.string.eng_lang);
        }
    }

    public String getSuffix() {
        return "_" + name().toLowerCase();
    }

    public static Language getSystem() {
        String lang = Locale.getDefault().getLanguage();

        if ("ru".equalsIgnoreCase(lang)) {
            return RUS;
        } else if ("es".equalsIgnoreCase(lang)) {
            return ESP;
        } else {
            return ENG;
        }
    }

    public static Language getFromString(String string) {
        if (string != null) {
            string = string.replace("_", "");
            if (RUS.isEqual(string)) {
                return RUS;
            } else if (ESP.isEqual(string)) {
                return ESP;
            } else {
                return ENG;
            }
        } else {
            return getSystem();
        }
    }

    public boolean isEqual(String suffix) {
        if (!TextUtils.isEmpty(suffix) && suffix.length() > 1) {
            return name().toLowerCase().contains(suffix.substring(0, 2).toLowerCase());
        } else {
            return false;
        }
    }

    public static Language getById(int id) {
        for (Language lang : values()) {
            if (lang.getId() == id) {
                return lang;
            }
        }
        return null;
    }
}
