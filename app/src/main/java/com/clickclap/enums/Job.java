package com.clickclap.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.view.EditableCirclePickerItem;
import com.clickclap.rest.ServerRequestParams;
import com.clickclap.util.Utils;

/**
 * Created by Denis on 27.05.2015.
 */
public enum Job implements EditableCirclePickerItem, ServerRequestParams {

    UNEMPLOYED,
    WAGE_EARNER,
    SELFEMPLOYED,
    FREELANCE;

    private int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String resName = "";
        if (!isOtherOption()) {
            resName += "job_";
        }
        resName += name().toLowerCase();
        return context.getResources().getIdentifier(resName, type, packageName);
    }

    @Override
    public Drawable getDrawable() {
        Context context = App.getInstance();
        Drawable drawable;
        int resId = getResId(context, "drawable");
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(App.getInstance(), resId);
        } else {
            drawable = ContextCompat.getDrawable(App.getInstance(), R.drawable.beach);
        }
        return drawable;
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getTitle() {
        String title = null;
        Context context = App.getInstance();
        int resId = getResId(context, "string");
        if (resId != 0) {
            title = context.getString(resId);
        }
        return title;
    }

    @Override
    public boolean isOtherOption() {
        return false;
    }

    @Override
    public void setTitle(String title) {
    }

    public static Job getById(String id) {
        Job result = null;
        if (Utils.isInteger(id)) {
            for (Job item : values()) {
                if (item.getId() == Integer.parseInt(id)) {
                    result = item;
                }
            }
        }
        return result;
    }

    @Override
    public String getRequestParam() {
        return String.valueOf(getId());
    }
}
