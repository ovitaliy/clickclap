package com.clickclap.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.rest.ServerRequestParams;
import com.clickclap.util.Utils;
import com.clickclap.view.CirclePickerItem;

/**
 * Created by Denis on 27.05.2015.
 */
public enum Education implements CirclePickerItem, ServerRequestParams {

    STUDENT, SECONDARY, HIGHER;

    private int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String resName = "";
        resName += "education_";
        resName += name().toLowerCase();
        return context.getResources().getIdentifier(resName, type, packageName);
    }

    @Override
    public Drawable getDrawable() {
        Context context = App.getInstance();
        Drawable drawable;
        int resId = getResId(context, "drawable");
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(App.getInstance(), resId);
        } else {
            drawable = ContextCompat.getDrawable(App.getInstance(), R.drawable.beach);
        }
        return drawable;
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getTitle() {
        String title = null;
        Context context = App.getInstance();
        int resId = getResId(context, "string");
        if (resId != 0) {
            title = context.getString(resId);
        }
        return title;
    }

    public static Education getById(String id) {
        Education result = null;
        if (Utils.isInteger(id)) {
            for (Education item : values()) {
                if (item.getId() == Integer.parseInt(id)) {
                    result = item;
                }
            }
        }
        return result;
    }

    @Override
    public String getRequestParam() {
        return String.valueOf(getId());
    }
}
