package com.clickclap.enums;

import com.clickclap.rest.ServerRequestParams;

/**
 * Created by Denis on 28.04.2015.
 */
public enum VoteListType implements ServerRequestParams {
    NOW {
        @Override
        public String getRequestParam() {
            return "now";
        }
    }, PAST {
        @Override
        public String getRequestParam() {
            return "past";
        }
    }
}
