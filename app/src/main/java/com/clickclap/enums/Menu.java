package com.clickclap.enums;

import com.clickclap.R;

/**
 * Created by denisvasilenko on 08.12.15.
 */
public enum Menu {
    EMOTICONS {
        public int getTitle() {
            return R.string.menu_emoticons;
        }

        @Override
        public int getImage() {
            return R.drawable.category_emoticons;
        }
    }, GRIMACES {
        public int getTitle() {
            return R.string.menu_grimaces;
        }

        @Override
        public int getImage() {
            return R.drawable.ic_grimace;
        }
    }, VISTORY {
        public int getTitle() {
            return R.string.menu_vistory;
        }

        @Override
        public int getImage() {
            return R.drawable.vistory;
        }
    }, MARKETPLACE {
        public int getTitle() {
            return R.string.menu_marketplace;
        }

        @Override
        public int getImage() {
            return R.drawable.marketplace;
        }
    }, INFO {
        public int getTitle() {
            return R.string.menu_info;
        }

        @Override
        public int getImage() {
            return R.drawable.info;
        }
    };

    public abstract int getTitle();

    public abstract int getImage();
}
