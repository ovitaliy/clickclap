package com.clickclap.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.view.CirclePickerItem;
import com.clickclap.rest.ServerRequestParams;

/**
 * Created by Deni on 23.07.2015.
 */
public enum GameType implements ServerRequestParams, CirclePickerItem {
    WHAT_WHERE_WHEN, BURIME, ANTICS;

    @Override
    public String getRequestParam() {
        return String.valueOf(getId());
    }

    public int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String emotionName = "game_" + name().toLowerCase();
        return context.getResources().getIdentifier(emotionName, type, packageName);
    }

    @Override
    public Drawable getDrawable() {
        Drawable drawable;
        int resId = getResId(App.getInstance(), "drawable");
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(App.getInstance(), resId);
        } else {
            drawable = ContextCompat.getDrawable(App.getInstance(), R.drawable.beach);
        }
        return drawable;
    }

    @Override
    public int getId() {
        return ordinal() + 1;
    }

    @Override
    public String getTitle() {
        String title = null;
        Context context = App.getInstance();
        int resId = getResId(context, "string");
        if (resId != 0) {
            title = context.getString(resId);
        }
        return title;
    }

    public static GameType getById(int id) {
        for (GameType type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

    public static GameType getByRequestParam(String requestParam) {
        for (GameType type : values()) {
            if (type.getRequestParam().equals(requestParam)) {
                return type;
            }
        }
        return null;
    }

}