package com.clickclap.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.clickclap.App;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.view.EditableCirclePickerItem;
import com.clickclap.rest.ServerRequestParams;
import com.clickclap.util.Utils;

/**
 * Created by Denis on 05.03.2015.
 */
public enum Religion implements EditableCirclePickerItem, ServerRequestParams {
    CHRISTIANITY,
    ISLAM,
    JUDAISM,
    HINDUISM,
    BUDDHISM,
    ATHEISM,
    AGNOSTICISM,
    OTHER;

    private String mCustomTitle;

    private int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String resName = "";
        if (!isOtherOption()) {
            resName += "religion_";
        }
        resName += name().toLowerCase();
        return context.getResources().getIdentifier(resName, type, packageName);
    }

    @Override
    public Drawable getDrawable() {
        Context context = App.getInstance();
        Drawable drawable;
        int resId = getResId(context, Const.RES_TYPE_DRAWABLE);
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(App.getInstance(), resId);
        } else {
            drawable = ContextCompat.getDrawable(App.getInstance(), R.drawable.other_option);
        }
        return drawable;
    }

    @Override
    public int getId() {
        return ordinal() + 1;
    }

    @Override
    public String getTitle() {
        if (!TextUtils.isEmpty(mCustomTitle) || isOtherOption()) {
            return mCustomTitle;
        } else {
            String title = null;
            Context context = App.getInstance();
            int resId = getResId(context, Const.RES_TYPE_STRING);
            if (resId != 0) {
                title = context.getString(resId);
            }
            return title;
        }
    }

    @Override
    public boolean isOtherOption() {
        return false;
    }

    @Override
    public void setTitle(String title) {
        mCustomTitle = title;
    }

    public static Religion getById(String id) {
        Religion result = null;
        if (Utils.isInteger(id)) {
            for (Religion item : values()) {
                if (item.getId() == Integer.parseInt(id)) {
                    result = item;
                }
            }
        }
        if (result == null) {
            result = OTHER;
            result.setTitle(id);
        }
        return result;
    }

    @Override
    public String getRequestParam() {
        if (isOtherOption()) {
            return getTitle();
        }
        return String.valueOf(getId());
    }
}
