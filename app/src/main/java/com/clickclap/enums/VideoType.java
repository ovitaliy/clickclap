package com.clickclap.enums;

import com.clickclap.R;
import com.clickclap.rest.ServerRequestParams;

/**
 * Created by Denis on 06.04.2015.
 */
public enum VideoType implements ServerRequestParams {
    FLOW_ALL {
        public int getCode() {
            return 0;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "all";
        }

        public int getTitle() {
            return R.string.video_type_flows;
        }
    },

    FLOW_BEST {
        public int getCode() {
            return 0;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "best";
        }

        public int getTitle() {
            return -1;//shouldn't be showed
        }
    },

    FLOW_MY_CHOICE {
        public int getCode() {
            return 0;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "my";
        }

        public int getTitle() {
            return -1;//shouldn't be showed
        }
    },

    EMOTICON_ALL {
        public int getCode() {
            return 1;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "all";
        }

        public int getTitle() {
            return R.string.video_type_emoticons;
        }
    },

    EMOTICON_BEST {
        public int getCode() {
            return 1;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "best";
        }

        public int getTitle() {
            return -1;
        }
    },

    EMOTICON_MY_CHOICE {
        public int getCode() {
            return 1;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "my";
        }

        public int getTitle() {
            return -1;
        }
    },

    VLOG {
        public int getCode() {
            return 2;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return null;
        }

        public int getTitle() {
            return R.string.video_type_vlog;
        }
    },

    CROWD_ASK {
        public int getCode() {
            return 6;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "please";
        }

        public int getTitle() {
            return R.string.video_type_crowd;
        }
    },

    CROWD_GIVE {
        public int getCode() {
            return 6;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "give";
        }

        public int getTitle() {
            return -1;
        }

    },

    NEWS {
        public int getCode() {
            return 10;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return null;
        }

        public int getTitle() {
            return -1;
        }
    },

    GREETING_PEOPLE {
        public int getCode() {
            return 13;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "people";
        }

        public int getTitle() {
            return R.string.video_type_greetings;
        }
    },

    GREETING_RELIGION {
        public int getCode() {
            return 13;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "religion";
        }

        public int getTitle() {
            return -1;
        }
    },

    GREETING_EVENTS {
        public int getCode() {
            return 13;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "events";
        }

        public int getTitle() {
            return -1;
        }
    },

    ALIEN_LOOK_TASK {
        public int getCode() {
            return 16;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "look";
        }

        public int getTitle() {
            return R.string.video_type_look;
        }
    },

    ALIEN_LOOK_CHALLENGE {
        public int getCode() {
            return 16;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "challenge";
        }

        public int getTitle() {
            return -1;
        }
    };

    public static VideoType getById(int id) {
        for (VideoType type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

    public static VideoType getByCode(int code) {
        for (VideoType type : values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public int getId() {
        return ordinal() + 1;
    }

    public static boolean isGreetingType(VideoType type) {
        if (type != null) {
            return type.isGreetingType();
        }
        return false;
    }

    public static boolean isEmoticonType(VideoType type) {
        if (type != null) {
            return type.isEmoticonType();
        }
        return false;
    }

    public boolean isGreetingType() {
        return this.equals(GREETING_EVENTS) || this.equals(GREETING_PEOPLE) || this.equals(GREETING_RELIGION);
    }

    public boolean isEmoticonType() {
        return this.equals(EMOTICON_ALL) || this.equals(EMOTICON_BEST) || this.equals(EMOTICON_MY_CHOICE);
    }

    public boolean isFlowType() {
        return this.equals(FLOW_ALL) || this.equals(FLOW_BEST) || this.equals(FLOW_MY_CHOICE);
    }

    public int getCode() {
        return this.getCode();
    }

    @Override
    public String getRequestParam() {
        return this.getRequestParam();
    }

    public String getAdditionParam() {
        return this.getAdditionParam();
    }

    public int getTitle() {
        return this.getTitle();
    }

    public String getDefaultFlowParams(String... arg) {
        String params = "";
        switch (this) {
            case EMOTICON_ALL:
            case EMOTICON_BEST:
            case EMOTICON_MY_CHOICE:
                params = "{\"id_smile\": \"" + arg[0] + "\""
                        + ", \"video_type\": \"" + getRequestParam() + "\"}";
                break;
        }
        return params;
    }
}
