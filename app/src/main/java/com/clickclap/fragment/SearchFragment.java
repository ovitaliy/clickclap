package com.clickclap.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.BaseActivity;
import com.clickclap.activities.SearchResultActivity;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.enums.Education;
import com.clickclap.enums.Hobby;
import com.clickclap.enums.Interest;
import com.clickclap.enums.Job;
import com.clickclap.enums.Language;
import com.clickclap.enums.Pet;
import com.clickclap.enums.Religion;
import com.clickclap.enums.Sport;
import com.clickclap.enums.VideoType;
import com.clickclap.loaders.LocationDataLoader;
import com.clickclap.model.Category;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.CategoriesRequestListener;
import com.clickclap.rest.listener.UserSearchRequestListener;
import com.clickclap.rest.model.UserSearchResponse;
import com.clickclap.rest.request.GetCategoriesRequest;
import com.clickclap.rest.request.user.SearchUserRequest;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.Utils;
import com.clickclap.view.CirclePickerItem;
import com.clickclap.view.CirclePickerView;
import com.clickclap.view.CityAutoCompleteTextView;
import com.clickclap.view.CountrySpinnerView;
import com.clickclap.view.TabView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Deni on 23.06.2015.
 */
public class SearchFragment extends BaseFragment implements TabView.OnTabSelectListener,
        AdapterView.OnItemSelectedListener,
        View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor> {
    private EditText mFirstNameField;
    private EditText mLastNameField;

    private View mHobbyButton;
    private View mSportButton;
    private View mPetButton;
    private View mReligionButton;
    private View mInterestButton;

    private int LOADER_CAT_ID = 1;

    //temp vars to store current ids
    private VideoType mVideoType;

    private Spinner mProfileLangs;

    private LocationDataLoader mProfileLocationDataLoader;
    private LocationDataLoader mContentLocationDataLoader;
    private Spinner mProfileOccupation;
    private Spinner mProfileEducation;


    private ViewGroup mTab1;
    private ViewGroup mTab2;
    private ViewGroup mTab3;

    private Button mDateFrom;
    private Button mDateTo;
    private EditText mProfession;
    private Spinner mCategoryView;
    private Spinner mContentTypeView;

    ArrayAdapter<CharSequence> mJobAdapter;

    private List<Integer> mCategoriesIds = new ArrayList<>();
    private List<Integer> mContentTypeIds = new ArrayList<>();

    int mInterest = -1;
    int mHobby = -1;
    int mReligion = -1;
    int mSport = -1;
    int mPet = -1;

    private int ctId;

    TabView mTabView;

    ImageButton mSearchButton;

    private ProgressDialog mProgressDialog;

    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        mFirstNameField = (EditText) view.findViewById(R.id.first_name);
        mLastNameField = (EditText) view.findViewById(R.id.last_name);

        mProfileLangs = (Spinner) view.findViewById(R.id.sp_lang);
        ArrayAdapter<CharSequence> langProfileAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.langs, R.layout.spinner_item);

        Language lang;
        if (AppUser.get() != null) {
            lang = AppUser.get().getLanguage();
        } else {
            lang = Language.getSystem();
        }

        langProfileAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mProfileLangs.setAdapter(langProfileAdapter);

        mProfileLangs.setSelection(lang.getId());

        mProfession = (EditText) view.findViewById(R.id.profession);

        mProfileLocationDataLoader = new LocationDataLoader(
                mSpiceManager,
                (CityAutoCompleteTextView) view.findViewById(R.id.profile_city),
                (CountrySpinnerView) view.findViewById(R.id.sp_country),
                null
        );

        mContentLocationDataLoader = new LocationDataLoader(
                mSpiceManager,
                (CityAutoCompleteTextView) view.findViewById(R.id.content_city),
                (CountrySpinnerView) view.findViewById(R.id.sp_video_country),
                null
        );

        mTabView = (TabView) view.findViewById(R.id.tabhost);
        mTabView.setOnTabSelectListener(this);

        mTab1 = (ViewGroup) view.findViewById(R.id.tab_profile);
        mTab2 = (ViewGroup) view.findViewById(R.id.tab_questionarie);
        mTab3 = (ViewGroup) view.findViewById(R.id.tab_content);

        mCategoryView = (Spinner) view.findViewById(R.id.sp_category);
        mCategoryView.setOnItemSelectedListener(this);

        mContentTypeView = (Spinner) view.findViewById(R.id.sp_video_type);
        mContentTypeView.setOnItemSelectedListener(this);

        mSearchButton = (ImageButton) view.findViewById(R.id.search);
        mSearchButton.setOnClickListener(this);

        CharSequence[] edus = new CharSequence[Education.values().length + 1];
        edus[0] = getString(R.string.search_education_choose);
        int i = 1;
        for (Education education : Education.values()) {
            edus[i++] = education.getTitle();
        }
        ArrayAdapter mEducationAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, edus);
        mProfileEducation = (Spinner) view.findViewById(R.id.sp_edu);
        mEducationAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mProfileEducation.setAdapter(mEducationAdapter);

        CharSequence[] jobs = new CharSequence[Job.values().length + 1];
        jobs[0] = getString(R.string.search_occupation_choose);
        i = 1;
        for (Job job : Job.values()) {
            jobs[i++] = job.getTitle();
        }
        mJobAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, jobs);
        mProfileOccupation = (Spinner) view.findViewById(R.id.sp_job);
        mJobAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mProfileOccupation.setAdapter(mJobAdapter);

        mHobbyButton = view.findViewById(R.id.btn_hobbies);
        mHobbyButton.setOnClickListener(this);
        mSportButton = view.findViewById(R.id.btn_sport);
        mSportButton.setOnClickListener(this);
        mPetButton = view.findViewById(R.id.btn_pets);
        mPetButton.setOnClickListener(this);
        mInterestButton = view.findViewById(R.id.btn_ints);
        mInterestButton.setOnClickListener(this);
        mReligionButton = view.findViewById(R.id.btn_religion);
        mReligionButton.setOnClickListener(this);

        mDateFrom = (Button) view.findViewById(R.id.date_start);
        mDateFrom.setOnClickListener(this);

        mDateTo = (Button) view.findViewById(R.id.date_end);
        mDateTo.setOnClickListener(this);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        mDateFrom.setText(Const.DAY_MONTH_YEAR_FORMAT.format(cal.getTime()));

        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 31);

        mDateTo.setText(Const.DAY_MONTH_YEAR_FORMAT.format(cal.getTime()));

        toggleVisibility(mHobbyButton, R.id.hobby_picker);
        toggleVisibility(mSportButton, R.id.sport_picker);
        toggleVisibility(mPetButton, R.id.pet_picker);
        toggleVisibility(mInterestButton, R.id.interest_picker);
        toggleVisibility(mReligionButton, R.id.religion_picker);

        loadVideoTypes();

        onTabSelected(null, 0);
        return view;
    }

    private void loadVideoTypes() {
        List<CharSequence> typeTitles = new ArrayList<>();
        for (VideoType type : VideoType.values()) {
            if (type.getTitle() != -1) {
                mContentTypeIds.add(type.getCode());
                typeTitles.add(getResources().getString(type.getTitle()));
            }
        }

        typeTitles.add(0, getString(R.string.new_video_title));
        ArrayAdapter<CharSequence> c_adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, typeTitles);
        c_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mContentTypeView.setAdapter(c_adapter);
        mContentTypeView.setOnItemSelectedListener(SearchFragment.this);
    }

    @Override
    public void onStart() {
        super.onStart();
        initPickerFromResource(R.id.hobby_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Hobby.values())), mHobby);
        initPickerFromResource(R.id.sport_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Sport.values())), mSport);
        initPickerFromResource(R.id.pet_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Pet.values())), mPet);
        initPickerFromResource(R.id.religion_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Religion.values())), mReligion);
        initPickerFromResource(R.id.interest_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Interest.values())), mInterest);
    }

    public void initPickerFromResource(final int pickerId, ArrayList<CirclePickerItem> source, int initElemId) {
        CirclePickerView picker = (CirclePickerView) getView().findViewById(pickerId);
        int margin = Utils.convertDpToPixel(10, getActivity());
        int widht = DisplayInfo.getScreenWidth(getActivity()) - (margin * 2);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(widht, widht);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(margin, margin, margin, margin);
        picker.setLayoutParams(params);

        picker.fill(source,
                initElemId,
                new CirclePickerView.OnPickListener() {
                    @Override
                    public void OnPick(CirclePickerItem element) {
                        switch (pickerId) {
                            case R.id.hobby_picker:
                                mHobby = element.getId();
                                break;
                            case R.id.sport_picker:
                                mSport = element.getId();
                                break;
                            case R.id.pet_picker:
                                mPet = element.getId();
                                break;
                            case R.id.religion_picker:
                                mReligion = element.getId();
                                break;
                            case R.id.interest_picker:
                                mInterest = element.getId();
                                break;
                        }
                    }
                });
    }

    @Override
    public void onTabSelected(View view, int position) {
        switch (position) {
            case 0:
                mTab1.setVisibility(View.VISIBLE);
                mTab2.setVisibility(View.GONE);
                mTab3.setVisibility(View.GONE);
                break;
            case 1:
                mTab1.setVisibility(View.GONE);
                mTab2.setVisibility(View.VISIBLE);
                mTab3.setVisibility(View.GONE);
                break;
            case 2:
                mTab1.setVisibility(View.GONE);
                mTab2.setVisibility(View.GONE);
                mTab3.setVisibility(View.VISIBLE);
                break;
        }
    }


    private void loadCategories(final VideoType videoType) {
        if (videoType != null) {
            new AsyncTask<Void, Void, ArrayList<Category>>() {

                @Override
                protected ArrayList<Category> doInBackground(Void... voids) {
                    String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + videoType.getId();
                    Cursor cursor = getActivity().getContentResolver().query(ContentDescriptor.Categories.URI, null, selection, null, null);
                    ArrayList<Category> list = new ArrayList<>();
                    if (cursor.moveToFirst()) {
                        do {
                            Category category = Category.fromCursor(cursor);
                            list.add(category);
                        } while (cursor.moveToNext());
                    }
                    return list;
                }

                @Override
                protected void onPostExecute(ArrayList<Category> categories) {
                    super.onPostExecute(categories);
                    List<CharSequence> catTitles = new ArrayList<>();
                    for (int j = 0; j < categories.size(); j++) {
                        Category item = categories.get(j);
                        mCategoriesIds.add(item.getId());
                        catTitles.add(item.getTitle());
                    }
                    catTitles.add(0, getString(R.string.search_select_category));
                    ArrayAdapter<CharSequence> c_adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, catTitles);
                    c_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    mCategoryView.setAdapter(c_adapter);
                    mCategoryView.setOnItemSelectedListener(SearchFragment.this);
                    for (int i = 0; i < mCategoriesIds.size(); i++) {
                        int id = mCategoriesIds.get(i);
                        if (id == ctId) {
                            mCategoryView.setSelection(i + 1);
                            break;
                        }
                    }
                }
            }.execute();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
          /*  case R.id.sp_country:
                mProfileCities.clear();
                if (position > 0) {
                    mProfileCities.setCountryId(mProfileCountryIds.get(position - 1));
                }
                break;
            case R.id.sp_video_country:
                mContentCities.clear();
                if (position > 0) {
                    mContentCities.setCountryId(mProfileCountryIds.get(position - 1));
                }
                break;*/
            case R.id.sp_city:
                break;
            case R.id.sp_video_type:
                if (position == 0) {
                    mCategoryView.setSelection(0);
                    mCategoriesIds.clear();
                } else {
                    if (mContentTypeIds != null && mContentTypeIds.size() > 0) {
                        mVideoType = VideoType.getByCode(mContentTypeIds.get(position - 1));
                        ((BaseActivity) getActivity()).getSpiceManager().execute(new GetCategoriesRequest(mVideoType), new CategoriesRequestListener(mVideoType));
                        getActivity().getSupportLoaderManager().restartLoader(LOADER_CAT_ID, null, this);
                    }
                }
                break;
            case R.id.sp_category:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    protected void toggleVisibility(View view, int containerId) {
        view.setSelected(!view.isSelected());
        view.getRootView().findViewById(containerId).setVisibility(view.isSelected() ? View.VISIBLE : View.GONE);
    }

    private void pickDate(final Button dateView) {
        final Calendar c = Calendar.getInstance(Locale.getDefault());
        Date curDate;
        try {
            curDate = Const.DAY_MONTH_YEAR_FORMAT.parse(dateView.getText().toString());
            c.setTimeInMillis(curDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DatePickerDialog d = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        dateView.setText(Const.DAY_MONTH_YEAR_FORMAT.format(c.getTime()));
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        d.getDatePicker().setMaxDate(new Date().getTime());
        d.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_hobbies:
                toggleVisibility(v, R.id.hobby_picker);
                break;
            case R.id.btn_sport:
                toggleVisibility(v, R.id.sport_picker);
                break;
            case R.id.btn_pets:
                toggleVisibility(v, R.id.pet_picker);
                break;
            case R.id.btn_ints:
                toggleVisibility(v, R.id.interest_picker);
                break;
            case R.id.btn_religion:
                toggleVisibility(v, R.id.religion_picker);
                break;
            case R.id.date_start:
                pickDate(mDateFrom);
                break;
            case R.id.date_end:
                pickDate(mDateTo);
                break;
            case R.id.search:
                search();
                break;
        }
    }

    private void search() {

        mSearchButton.setEnabled(false);

        SearchUserRequest searchUserRequest = new SearchUserRequest();

        if (!TextUtils.isEmpty(mFirstNameField.getText().toString())) {
            searchUserRequest.setFirstName(mFirstNameField.getText().toString());
        }

        if (!TextUtils.isEmpty(mLastNameField.getText().toString())) {
            searchUserRequest.setLastName(mLastNameField.getText().toString());
        }

        if (mProfileLocationDataLoader.getSelectedCountryId() > 0) {
            searchUserRequest.setCountryId(String.valueOf(mProfileLocationDataLoader.getSelectedCountryId()));
        }

        if (mProfileLocationDataLoader.getSelectedCityId() > 0) {
            searchUserRequest.setCityId(String.valueOf(mProfileLocationDataLoader.getSelectedCityId()));
        }

        if (mProfileOccupation.getSelectedItemPosition() > 0) {
            Job job = Job.values()[mProfileOccupation.getSelectedItemPosition() - 1];
            searchUserRequest.setJob(job.getRequestParam());
        }

        if (mProfileEducation.getSelectedItemPosition() > 0) {
            Education education = Education.values()[mProfileEducation.getSelectedItemPosition() - 1];
            searchUserRequest.setEducation(education.getRequestParam());
        }

        searchUserRequest.setLang(String.valueOf(Language.getById((int) mProfileLangs.getSelectedItemId())));

        if (mInterest > -1) {
            searchUserRequest.setInterest(String.valueOf(mInterest));
        }
        if (mReligion > -1) {
            searchUserRequest.setReligion(String.valueOf(mReligion));
        }
        if (mPet > -1) {
            searchUserRequest.setPets(String.valueOf(mPet));
        }
        if (mSport > -1) {
            searchUserRequest.setSport(String.valueOf(mSport));
        }
        if (mHobby > -1) {
            searchUserRequest.setHobbie(String.valueOf(mHobby));
        }

        if (mCategoryView.getSelectedItemPosition() > 0) {
            int id = mCategoriesIds.get(mCategoryView.getSelectedItemPosition() - 1);
            searchUserRequest.setCategory(id);
        }

        try {
            if (mContentLocationDataLoader.getSelectedCountryId() > 0) {
                searchUserRequest.setVideoIdCountry(String.valueOf(mContentLocationDataLoader.getSelectedCountryId()));
            }

            if (mContentLocationDataLoader.getSelectedCityId() > 0) {
                searchUserRequest.setVideoIdCity(String.valueOf(mContentLocationDataLoader.getSelectedCityId()));
            }
        } catch (ArrayIndexOutOfBoundsException ignore) {
        }

        try {
            if (!TextUtils.isEmpty(mDateFrom.getText())) {
                searchUserRequest.setVideoCreatedAtFrom(String.valueOf(Const.DAY_MONTH_YEAR_FORMAT.parse(mDateFrom.getText().toString()).getTime()));
            }

            if (!TextUtils.isEmpty(mDateTo.getText())) {
                searchUserRequest.setVideoCreatedAtTo(String.valueOf(Const.DAY_MONTH_YEAR_FORMAT.parse(mDateTo.getText().toString()).getTime()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mSpiceManager.execute(searchUserRequest, new UserSearchRequestListener(searchUserRequest.getParams(), true) {
            @Override
            public void onRequestSuccess(UserSearchResponse response) {
                super.onRequestSuccess(response);
                if (isAdded()) {
                    startActivity(SearchResultActivity.newIntent(getActivity()));
                    mSearchButton.setEnabled(true);
                }
            }

            @Override
            public void onRequestFailure(SpiceException spiceException) {
                super.onRequestFailure(spiceException);
            }
        });

    }

    @Override
    public String getTitle() {
        return getString(R.string.action_search);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == LOADER_CAT_ID) {
            if (mVideoType != null) {
                String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + mVideoType.getId();
                return new CursorLoader(getActivity(), ContentDescriptor.Categories.URI, null, selection, null, null);
            }
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == LOADER_CAT_ID) {
            loadCategories(mVideoType);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
