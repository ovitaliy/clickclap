package com.clickclap.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.activities.NewVideoActivity;
import com.clickclap.adapter.GrimacesPagerAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.dialogs.ActivateWidgetDialogFragment;
import com.clickclap.enums.Emotion;
import com.clickclap.events.VideoUploadedEvent;
import com.clickclap.model.Grimace;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.GetGrimacesRequestListener;
import com.clickclap.rest.request.grimace.GetGrimacesRequest;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.NetworkUtil;
import com.clickclap.util.Utils;
import com.clickclap.view.TabView;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Denis on 31.03.2015.
 */
public class GrimaceFragment extends BaseFragment implements TabView.OnTabSelectListener, LoaderManager.LoaderCallbacks<Cursor> {
    private final static int LOADER_ID = 13231;
    private GrimacesPagerAdapter mPagerAdapter;

    private int mType;

    private ViewPager mViewPager;
    private LinearLayout mEmotionsContainer;

    private static final int COUNT_PER_PAGE = 3;

    private int mCellSize;

    private SpiceManager mSpiceManager;

    TabView mTabView;

    public static GrimaceFragment newInstance() {
        GrimaceFragment fragment = new GrimaceFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_grimaces, container, false);

        mTabView = (TabView) v.findViewById(R.id.tabhost);
        mTabView.setOnTabSelectListener(this);

        mViewPager = (ViewPager) v.findViewById(R.id.grimaces_pager);
        mEmotionsContainer = (LinearLayout) v.findViewById(R.id.emotions_container);

        mCellSize = (int) ((float) DisplayInfo.getScreenWidth(getActivity()) / (COUNT_PER_PAGE + 1));

        mPagerAdapter = new GrimacesPagerAdapter(getFragmentManager(), mCellSize);
        mViewPager.setAdapter(mPagerAdapter);

        mEmotionsContainer.getLayoutParams().width = mCellSize;
        checkWidget();
        return v;
    }

    private void checkWidget() {
        if (AppUser.get() != null && !AppUser.get().isWidget()) {
            if (!NetworkUtil.isOfflineMode()) {
                ActivateWidgetDialogFragment.newInstance().show(getActivity().getSupportFragmentManager(), "widget");
                AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.BUY_WIDGET);
            }
        }
    }

    private void loadGrimaces(int type) {
        mSpiceManager.execute(
                new GetGrimacesRequest(type),
                new GetGrimacesRequestListener(type));
    }


    @Override
    public void onResume() {
        super.onResume();
        mTabView.setActive(0);
    }

    @Override
    public void onTabSelected(View view, int position) {
        mViewPager.setCurrentItem(0);
        mPagerAdapter.clear();
        switch (position) {
            case 0:
                mType = Grimace.TYPE_COLLECTION;
                break;
            case 1:
                mType = Grimace.TYPE_USER;
                break;
            case 2:
                mType = Grimace.TYPE_MARKET;
                break;
        }
        Bundle args = new Bundle(1);
        args.putInt(Params.TYPE, mType);
        getActivity().getSupportLoaderManager().restartLoader(LOADER_ID, args, this);
        loadGrimaces(mType);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().getSupportLoaderManager().destroyLoader(LOADER_ID);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int type = args.getInt(Params.TYPE);
        return new CursorLoader(getActivity(),
                ContentDescriptor.Grimaces.URI,
                null,
                ContentDescriptor.Grimaces.Cols.TYPE + " = " + type,
                null,
                ContentDescriptor.Grimaces.Cols.ID_SMILE + " DESC");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VideoUploadedEvent event) {
        onTabSelected(null, mViewPager.getCurrentItem());
    }

    private void addEmotionToContainer(final int emotionId) {
        final Activity context = getActivity();
        if (context != null) {
            int margin = Utils.convertDpToPixel(5, context);
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mCellSize - (margin * 2), mCellSize - (margin * 2));
            params.setMargins(margin, margin, margin, margin);
            imageView.setLayoutParams(params);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setImageResource(Emotion.getById(emotionId).getResId(getActivity(), "drawable"));
            mEmotionsContainer.addView(imageView);

            if (mType == Grimace.TYPE_COLLECTION) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        NewVideoActivity.startNewGrimaceInstance(context, Emotion.getById(emotionId));
                    }
                });
            }
        }
    }

    private void loadCollection(Cursor data) {
        ArrayList<LinkedHashMap<Integer, Grimace[]>> pages = new ArrayList<>();
        mEmotionsContainer.removeAllViews();

        ArrayList<Integer> emotions = new ArrayList<>();
        ArrayList<Grimace> list = new ArrayList<>();

        if (data != null && data.moveToFirst()) {
            do {
                Grimace grimace = Grimace.fromCursor(data);
                list.add(grimace);
                int smileId = grimace.getSmileId();
                if (!emotions.contains(smileId)) {
                    emotions.add(smileId);
                }
            } while (data.moveToNext());
        }

        Collections.sort(emotions);

        for (int emotion : emotions) {
            addEmotionToContainer(emotion);
        }

        if (mType == Grimace.TYPE_COLLECTION) {//for grimace tab should be showed all grimaces
            for (Emotion emotion : Emotion.values()) {
                int smileId = emotion.getId();
                if (!emotions.contains(smileId)) {
                    emotions.add(smileId);
                    addEmotionToContainer(smileId);
                }
            }
        }

        mEmotionsContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (mEmotionsContainer.getHeight() > 0) {
                    mViewPager.getLayoutParams().height = mEmotionsContainer.getHeight();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        //noinspection deprecation
                        mEmotionsContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mEmotionsContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            }
        });
        mEmotionsContainer.invalidate();

        int positionOnCurPage = 0;
        int prevSmileId = 0;
        int pageNum = 0;
        for (Grimace grimace : list) {
            int curSmileId = grimace.getSmileId();
            if (curSmileId != prevSmileId) {
                pageNum = 0;
                positionOnCurPage = 0;
            } else if (positionOnCurPage >= COUNT_PER_PAGE) {
                pageNum++;
                positionOnCurPage = 0;
            }

            if (pageNum > pages.size() - 1) {
                LinkedHashMap<Integer, Grimace[]> page = new LinkedHashMap<>();
                for (int emotion : emotions) {
                    page.put(emotion, new Grimace[COUNT_PER_PAGE]);
                }
                positionOnCurPage = 0;
                pages.add(page);
            }

            LinkedHashMap<Integer, Grimace[]> page = pages.get(pageNum);

            Grimace[] grimaces = page.get(curSmileId);
            grimaces[positionOnCurPage] = grimace;

            prevSmileId = curSmileId;
            positionOnCurPage++;
        }

        if (mType == Grimace.TYPE_COLLECTION) {
            if (pages == null || pages.size() == 0) {
                LinkedHashMap<Integer, Grimace[]> page = new LinkedHashMap<>();
                for (int emotion : emotions) {
                    page.put(emotion, new Grimace[COUNT_PER_PAGE]);
                }
                pages.add(page);
            }

            //We should insert stub into end of any emotions row, wich will means than you can create new grimace
            ArrayList<Integer> emotionsWithStub = new ArrayList<>(emotions.size());
            for (LinkedHashMap<Integer, Grimace[]> page : pages) {
                for (Object o : page.entrySet()) {
                    Map.Entry row = (Map.Entry) o;
                    if (!emotionsWithStub.contains(row.getKey())) {
                        Grimace[] grimaces = (Grimace[]) row.getValue();
                        for (int i = 0; i < grimaces.length; i++) {
                            Grimace grimace = grimaces[i];
                            if (grimace == null) {
                                grimaces[i] = new Grimace();//stub
                                emotionsWithStub.add((Integer) row.getKey());
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (!mPagerAdapter.isNewDataIdentical(pages)) {
            mPagerAdapter.setData(pages, mType);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, final Cursor data) {
        loadCollection(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (mPagerAdapter != null) {
            mPagerAdapter.clear();
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_grimaces);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }
}