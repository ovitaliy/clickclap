package com.clickclap.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.RegistrationActivity;
import com.clickclap.adapter.MenuAdapter;
import com.clickclap.enums.Menu;
import com.clickclap.listener.OnItemClickListener;
import com.clickclap.listener.OnViewProfileListener;
import com.clickclap.model.UserInfo;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.UserInfoRequestListener;
import com.clickclap.rest.request.user.GetUserInfoRequest;
import com.clickclap.util.NetworkUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import java.util.Calendar;

/**
 * Created by Inteza23 on 06.11.2014.
 */
public class MenuFragment extends Fragment implements View.OnClickListener, OnItemClickListener<Menu> {
    private OnMenuListener mOnMenuListener;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private OnViewProfileListener mOnViewProfileListener;
    private SpiceManager mSpiceManager;
    TextView mCopyrightView;
    RecyclerView mRecyclerView;
    private ImageView mAvatarView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, true);
        mCopyrightView = (TextView) view.findViewById(R.id.copyright);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        view.findViewById(R.id.header).setOnClickListener(this);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MenuAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        setCopyright();
        return view;
    }

    private void setCopyright() {
        int curYear = Calendar.getInstance().get(Calendar.YEAR);
        String param = "";
        if (curYear > 2015) {
            param = " - " + curYear;
        }
        mCopyrightView.setText(String.format(getString(R.string.copyright), param));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppUser.get().getId() != 0) {
            fillUserInfo(AppUser.get());
        } else {
            initialLoad();
        }
    }

    private void initialLoad() {
        if (NetworkUtil.isOfflineMode()) {

        } else {
            mSpiceManager.execute(new GetUserInfoRequest(),
                    new UserInfoRequestListener(new UserInfoRequestListener.OnGetUserInfoListener() {
                        @Override
                        public void onGetUserInfo(UserInfo userInfo) {
                            if (userInfo == null || TextUtils.isEmpty(userInfo.getFName())) {
                                RegistrationActivity.startNewInstance(getActivity(), Const.Action.FILL_PROFILE);
                                return;
                            }
                            fillUserInfo(userInfo);
                        }
                    }));
        }
    }

    private void fillUserInfo(UserInfo userInfo) {
        View v = getView();
        if (v == null) {
            return;
        }

        mAvatarView = (ImageView) v.findViewById(R.id.avatar);
        TextView textView;

        textView = (TextView) v.findViewById(R.id.first_name);
        textView.setText(userInfo.getFName());

        textView = (TextView) v.findViewById(R.id.location);
        textView.setText(userInfo.getCountry() + ", " + userInfo.getCity());

        textView = (TextView) v.findViewById(R.id.language);
        textView.setText(userInfo.getLanguage().getTitle());

        if (!TextUtils.isEmpty(userInfo.getPhoto())) {
            ImageLoader.getInstance().displayImage(userInfo.getImageLoaderPhoto(), mAvatarView);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnMenuListener = (OnMenuListener) activity;
        mOnViewProfileListener = (OnViewProfileListener) activity;
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header:
                mOnViewProfileListener.onViewProfile(AppUser.getUid());
                break;
        }
    }

    @Override
    public void onItemClick(Menu item, View v) {
        switch (item) {
            case EMOTICONS:
                mOnMenuListener.onOpenEmoticons();
                break;
            case VISTORY:
                mOnMenuListener.onOpenVistory();
                break;
            case MARKETPLACE:
                mOnMenuListener.onOpenMarketplace();
                break;
            case INFO:
                mOnMenuListener.onOpenInfo();
                break;
            case GRIMACES:
                mOnMenuListener.onOpenGrimaces();
                break;
        }
    }

    public interface OnMenuListener {
        void onOpenEmoticons();

        void onOpenInfo();

        void onOpenGrimaces();

        void onOpenVistory();

        void onOpenMarketplace();
    }

}
