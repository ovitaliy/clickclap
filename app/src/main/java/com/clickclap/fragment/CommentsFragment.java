package com.clickclap.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.adapter.CommentListAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.dialogs.DialogBuilder;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.listener.OnViewProfileListener;
import com.clickclap.model.Comment;
import com.clickclap.model.Video;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.CommentsResponse;
import com.clickclap.rest.request.video.CommentVideoRequest;
import com.clickclap.rest.request.video.GetCommentsRequest;
import com.clickclap.view.items.VideoItemView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Владимир on 06.12.2014.
 */
public class CommentsFragment extends BaseFragment {

    private Video mVideo;
    private ArrayList<Comment> mComments;
    private ListView mListView;
    private CommentListAdapter mAdapter;
    private EditText mCommentText;
    private Button mSendButton;

    private VideoItemView.OnCommentListener mOnCommentListener;
    private VideoItemView.OnVideoChooseListener mOnVideoChooseListener;
    private VideoItemView.OnMoreListener mOnMoreListener;
    private OnViewProfileListener mOnViewProfileListener;

    private ProgressDialog mProgressDialog;

    private VideoItemView mVideoItemView;

    private SpiceManager mSpiceManager;

    public static CommentsFragment newInstance(Video video) {
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_comments);
        args.putSerializable("video", video);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args.containsKey("video")) {
            mVideo = (Video) args.getSerializable("video");
        }

        mComments = new ArrayList<>();
        getComments();
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mAdapter = new CommentListAdapter(getActivity());
        mAdapter.setOnViewProfileListener(mOnViewProfileListener);
        mListView = (ListView) view.findViewById(R.id.comments_list);

        mVideoItemView = new VideoItemView(getActivity(), AppUser.getUid(), null);
        mVideoItemView.setData(mVideo, null);
        mListView.addHeaderView(mVideoItemView, null, false);
        mVideoItemView.findViewById(R.id.buttons_panel).setVisibility(View.GONE);

        mVideoItemView.setOnMoreListener(mOnMoreListener);
        mVideoItemView.setOnVideoChooseListener(mOnVideoChooseListener);
        mVideoItemView.setOnViewProfileListener(mOnViewProfileListener);
        mVideoItemView.setOnCommentListener(mOnCommentListener);

        mListView.setAdapter(mAdapter);
        mAdapter.setData(mComments);

        mCommentText = (EditText) view.findViewById(R.id.comments_text);
        setHint(mVideo.getCommentsCount());

        mSendButton = (Button) view.findViewById(R.id.comments_send);
        mSendButton.setOnClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnMoreListener = (VideoItemView.OnMoreListener) activity;
        mOnVideoChooseListener = (VideoItemView.OnVideoChooseListener) activity;
        mOnViewProfileListener = (OnViewProfileListener) activity;
        mOnCommentListener = (VideoItemView.OnCommentListener) activity;

        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    /**
     * Set hint to mCommentText;
     *
     * @param count How many comments we have.
     */
    private void setHint(int count) {
        String hint;
        if (count == 0) {
            hint = App.getInstance().getResources().getString(R.string.no_comment);
        } else {
            hint = App.getInstance().getResources().getQuantityString(R.plurals.stream_displaying_comment_text, count, count);
        }
        mCommentText.setHint(hint);
    }

    private void showMessageAboutError(int message) {
        new DialogBuilder(getActivity())
                .setMessage(message)
                .create()
                .show();
    }

    /**
     * Send request to getting comments from server.
     */
    private void getComments() {

        mSpiceManager.execute(
                new GetCommentsRequest(mVideo.getVideoId()),
                new RequestListener<CommentsResponse>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {

                    }

                    @Override
                    public void onRequestSuccess(CommentsResponse response) {
                        if (isAdded()) {
                            if (response != null) {
                                List<Comment> comments = response.getComments();
                                if (comments == null || comments.size() == 0) {
                                    setHint(0);
                                    return;
                                } else {
                                    mComments.clear();
                                    List<Comment> list = response.getComments();
                                    for (int i = 0; i < list.size(); i++) {
                                        Comment comment = list.get(i);
                                        mComments.add(comment);
                                    }
                                    Collections.reverse(mComments);
                                    mAdapter.setData(mComments);
                                    setHint(mComments.size());
                                    mVideo.setCommentsCount(mComments.size());
                                    mVideoItemView.setData(mVideo, null);
                                }

                                //scroll to bottom
                                mListView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Select the last row so it will scroll into view...
                                        if (mComments != null) {
                                            mListView.setSelection(mComments.size());
                                        }
                                    }
                                });

                                saveCommentsCount(mComments.size());
                            }

                            if (mProgressDialog != null) {
                                mProgressDialog.dismiss();
                            }
                        }
                    }
                });

    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mCommentText.getWindowToken(), 0);
    }

    /**
     * Send comment to server.
     *
     * @param comment The comment that we send.
     */
    private void sendComment(String comment) {

        if (mProgressDialog == null || !mProgressDialog.isShowing()) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.show();
        }

        hideKeyboard();

        mSpiceManager.execute(
                new CommentVideoRequest(mVideo.getVideoId(), comment),
                new BaseRequestListener() {
                    @Override
                    public void onRequestSuccess(BaseResponse baseResponse) {
                        super.onRequestSuccess(baseResponse);
                        if (isAdded()) {
                            mCommentText.setText("");
                            getComments();
                        }
                    }
                }
        );
    }

    private void saveCommentsCount(int commentsCount) {
        ContentValues values = new ContentValues(1);
        values.put(ContentDescriptor.Videos.Cols.COMMENTS_COUNT, commentsCount);
        getActivity().getContentResolver().update(
                ContentDescriptor.Videos.URI,
                values,
                ContentDescriptor.Videos.Cols.ID + " = " + mVideo.getVideoId(),
                null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.comments_send:
                String comment = mCommentText.getText().toString();
                if (!comment.isEmpty()) {
                    sendComment(comment);
                }
                break;
        }
    }

    @Override
    public String getTitle() {
        return "Comments";
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
