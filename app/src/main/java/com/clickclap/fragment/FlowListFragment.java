package com.clickclap.fragment;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.PlayFlowActivity;
import com.clickclap.adapter.VideoListAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Emotion;
import com.clickclap.enums.VideoType;
import com.clickclap.events.LoadedFileEvent;
import com.clickclap.events.OnGetFlowEvent;
import com.clickclap.events.VideoUploadedEvent;
import com.clickclap.jobs.VideoPreloadRunnable;
import com.clickclap.listener.OnUpdateVideoListener;
import com.clickclap.listener.OnViewProfileListener;
import com.clickclap.model.Video;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.FlowRequestListener;
import com.clickclap.rest.model.FlowResponse;
import com.clickclap.rest.request.video.GetFlowRequest;
import com.clickclap.util.NetworkUtil;
import com.clickclap.view.items.VideoItemView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * Created by Владимир on 26.11.2014.
 */
public final class FlowListFragment extends BaseFragment implements
        AbsListView.OnScrollListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER = 1000;
    private static final int PRELOAD_COUNT = 3;
    private VideoListAdapter mVideoListAdapter;

    ArrayList<Video> mPreloadList;
    ArrayList<Video> mVideos;

    private Video mVideoParent;
    private String mTitle;
    private int mLoadedCount;

    private SpiceManager mSpiceManager;

    private int mReplyId;
    private VideoType mVideoType;
    private String mParams;
    private int mPosition;
    private Emotion mEmotion;

    public static class Builder {
        Bundle mArgs = new Bundle();

        public Builder(VideoType videoType) {
            setVideoType(videoType);
        }

        public Builder setReplyId(int replyId) {
            mArgs.putInt(Params.REPLY_ID, replyId);
            return this;
        }

        public Builder setParams(String params) {
            mArgs.putString(Params.PARAMS, params);
            return this;
        }

        public Builder setVideoType(VideoType videoType) {
            mArgs.putSerializable(Params.TYPE, videoType);
            return this;
        }

        public Builder setTitle(String title) {
            mArgs.putString(Params.TITLE, title);
            return this;
        }

        public Builder setCurrentPosition(int position) {
            mArgs.putInt(Params.POSITION, position);
            return this;
        }

        public Builder setEmotion(Emotion emotion) {
            mArgs.putSerializable(Params.EMOTION, emotion);
            return this;
        }

        public FlowListFragment build() {
            FlowListFragment fragment = new FlowListFragment();
            mArgs.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_video_list);
            fragment.setArguments(mArgs);
            return fragment;
        }
    }

    private ListView mVideoListView;

    private VideoItemView.OnCommentListener mOnCommentListener;
    private VideoItemView.OnVideoChooseListener mOnVideoChooseListener;
    private VideoItemView.OnMoreListener mOnMoreListener;
    private OnViewProfileListener mOnViewProfileListener;
    private OnUpdateVideoListener mOnUpdateVideoListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args.containsKey(Params.TYPE)) {
            mVideoType = (VideoType) args.getSerializable(Params.TYPE);
        }

        if (args.containsKey(Params.REPLY_ID)) {
            mReplyId = args.getInt(Params.REPLY_ID);
        }

        if (args.containsKey(Params.EMOTION)) {
            mEmotion = (Emotion) args.getSerializable(Params.EMOTION);
        }

        if (args.containsKey(Params.PARAMS)) {
            mParams = args.getString(Params.PARAMS);
        }

        if (args.containsKey("parent")) {
            mVideoParent = (Video) args.getSerializable("parent");
        }

        if (args.containsKey(Params.POSITION)) {
            mPosition = args.getInt(Params.POSITION);
        }

        mTitle = args.getString(Params.TITLE);
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mVideoListView = (ListView) view.findViewById(R.id.video_list_view);

        mVideoListAdapter = new VideoListAdapter(getActivity(), null, AppUser.getUid(), mVideoType, mVideoParent);
        mVideoListView.setAdapter(mVideoListAdapter);
        mVideoListAdapter.setOnCommentListener(mOnCommentListener);
        mVideoListAdapter.setOnMoreListener(mOnMoreListener);
        mVideoListAdapter.setOnVideoChooseListener(mOnVideoChooseListener);
        mVideoListAdapter.setOnViewProfileListener(mOnViewProfileListener);
        mVideoListAdapter.setOnPayVideoListener(mOnUpdateVideoListener);

        Bundle bundle = new Bundle(1);
        bundle.putInt("screen", getScreen());

        getActivity().getLoaderManager().restartLoader(LOADER, bundle, this);

        loadList(0);
    }

    private void loadList(long lastId) {
        if (!NetworkUtil.isOfflineMode()) {
            if (mLoader != null && !mLoader.isShowing()) {
                mLoader.show();
            }

            if (TextUtils.isEmpty(mParams) && mVideoType != null) {
                String arg = null;
                if (mEmotion != null) {
                    arg = String.valueOf(mEmotion.getId());
                }

                mParams = mVideoType.getDefaultFlowParams(arg);
            }

            GetFlowRequest request = new GetFlowRequest(mParams);

            if (lastId > 0) {
                request.setLast(lastId);
            }

            if (mReplyId != 0) {
                request.setReplyId(mReplyId);
            }

            mSpiceManager.execute(request, new FlowRequestListener(1, lastId > 0) {
                @Override
                public void onRequestFailure(SpiceException spiceException) {
                    super.onRequestFailure(spiceException);
                    if (isAdded()) {
                        if (mLoader != null && mLoader.isShowing()) {
                            mLoader.hide();
                        }
                    }
                }

                @Override
                public void onRequestSuccess(FlowResponse response) {
                    super.onRequestSuccess(response);
                    if (isAdded()) {
                        if (mLoader != null && mLoader.isShowing()) {
                            mLoader.hide();
                        }
                    }
                }
            });
        }
    }

    private int getScreen() {
        int screen;
        if (getArguments().getBoolean("isMyStream")) {
            screen = 2;
        } else {
            screen = 1;
        }
        return screen;
    }

    @Override
    public void onPause() {
        super.onPause();
        mVideoListView.setOnScrollListener(null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mSpiceManager = ((SpiceContext) activity).getSpiceManager();

        mOnCommentListener = (VideoItemView.OnCommentListener) activity;
        mOnMoreListener = (VideoItemView.OnMoreListener) activity;
        mOnVideoChooseListener = new VideoItemView.OnVideoChooseListener() {
            @Override
            public void onVideoChosen(Video video) {
                ArrayList<Video> videosToWatch = new ArrayList<>();
                boolean firstFinded = false;
                if (mVideos != null) {
                    for (Video video1 : mVideos) {
                        if (video.getMedia().equals(video1.getMedia())) {
                            firstFinded = true;
                        }
                        if (firstFinded) {
                            videosToWatch.add(video1);
                        }
                    }
                    PlayFlowActivity.startNewInstance(getActivity(), mParams, mReplyId, mTitle, videosToWatch, mVideoType);
                }
            }

            @Override
            public void onSmileGuessingStart(Video video) {
                ((VideoItemView.OnVideoChooseListener) getActivity()).onSmileGuessingStart(video);
            }
        };
        mOnViewProfileListener = (OnViewProfileListener) activity;
        mOnUpdateVideoListener = (OnUpdateVideoListener) activity;
    }

    private boolean mIsLastPage;

    private void runPreloader() {
        if (mLoadedCount < mPreloadList.size()) {
            new Thread(new VideoPreloadRunnable(mPreloadList.get(mLoadedCount).getMedia())).start();
//            VideoPreloadRunnable mVideoPreloadTask = new VideoPreloadRunnable() {
//                @Override
//                protected void onPostExecute(String result) {
//                    super.onPostExecute(result);
//                    Log.d(FlowListFragment.class.getSimpleName(), "loaded " + result);
//                    mLoadedCount++;
//                    if (!isCancelled()) {
//                        if (mLoadedCount < mPreloadList.size())
//                            runPreloader();
//                    }
//                }
//            };
//            mVideoPreloadTask.execute(mPreloadList.get(mLoadedCount).getMedia());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadedFileEvent(LoadedFileEvent event) {
        mLoadedCount++;
        if (isAdded()) {
            if (mLoadedCount < mPreloadList.size())
                runPreloader();
        }
    }

    private void preload() {
        int preloadCount = Math.min(PRELOAD_COUNT, mVideoListAdapter.getCount());
        mPreloadList = new ArrayList<>(preloadCount);
        for (int i = 0; i < preloadCount; i++) {
            Video video = Video.fromCursor((Cursor) mVideoListAdapter.getItem(i));
            mPreloadList.add(video);
        }
        runPreloader();
    }

    public void loadNextPage() {
        mVideoListView.setOnScrollListener(null);

        if (mVideoListView.getCount() > 0) {
            Video lastVideo = Video.fromCursor((Cursor) mVideoListAdapter.getItem(mVideoListView.getCount() - 1));
            loadList(lastVideo.getVideoId());
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount > 0) {
            Log.i("test", "testtest. first visible:" + firstVisibleItem);
            mPosition = firstVisibleItem;
        }
        if (!mIsLastPage && totalItemCount >= Const.LIMIT && firstVisibleItem + visibleItemCount > totalItemCount - 5) {
            loadNextPage();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int screen = args.getInt("screen");

        String where = ContentDescriptor.Videos.Cols.SCREEN + "=" + screen;
        return new CursorLoader(getActivity(), ContentDescriptor.Videos.URI, null, where, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mVideoListView.setOnScrollListener(null);
        mVideoListAdapter.changeCursor(data);
        if (mPosition > 0) {
            mVideoListView.setSelection(mPosition);
        }
        if (data.getCount() > 0) {
            mVideoListView.setOnScrollListener(this);
        }

        if (data.getCount() > 0) {
            preload();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            if (mVideoType != null) {
                switch (mVideoType) {
                    case FLOW_BEST:
                    case FLOW_MY_CHOICE:
                    case FLOW_ALL:
                        mTitle = getString(R.string.menu_flow);
                        break;
                    case CROWD_ASK:
                    case CROWD_GIVE:
                        mTitle = getString(R.string.menu_crowd);
                        break;
                    case NEWS:
                        mTitle = getString(R.string.imon_tab_news);
                        break;
                    case VLOG:
                        mTitle = getString(R.string.imon_tab_vlog);
                        break;
                    case EMOTICON_ALL:
                    case EMOTICON_BEST:
                    case EMOTICON_MY_CHOICE:
                        mTitle = getString(R.string.imon_tab_emoticons);
                        break;
                    case GREETING_EVENTS:
                        mTitle = getString(R.string.greeting_events);
                        break;
                    case GREETING_PEOPLE:
                        mTitle = getString(R.string.greeting_people);
                        break;
                    case GREETING_RELIGION:
                        mTitle = getString(R.string.greeting_religion);
                        break;
                }
            } else if (mReplyId > 0) {
                mTitle = getString(R.string.dialog_responses_watch);
            }
        }
        return mTitle;
    }

    @Subscribe
    public void onEvent(OnGetFlowEvent event) {
        ArrayList<Video> list = event.getVideos();
        int count = list.size();
        mIsLastPage = count < Const.LIMIT;
        mVideos = list;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Subscribe
    public void onEvent(VideoUploadedEvent event) {
        loadList(0);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Params.POSITION, mPosition);
    }
}
