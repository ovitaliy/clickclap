package com.clickclap.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.clickclap.R;
import com.clickclap.activities.FeedbackActivity;
import com.clickclap.adapter.FeedbackAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.Feedback;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.MessageListsRequestListener;
import com.clickclap.rest.request.message.GetMessageListsRequest;
import com.clickclap.view.TabView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Denis on 03.03.2015.
 */
public class FeedbackFragment extends BaseFragment implements
        TabView.OnTabSelectListener, AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    int mCurrentTab;
    private TabView mTabView;
    private Button mAddButton;
    private FeedbackAdapter mAdapter;
    private ListView mFeedbackList;

    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    public static FeedbackFragment newInstance() {
        FeedbackFragment fragment = new FeedbackFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_feedback);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mTabView = (TabView) view.findViewById(R.id.tabhost);
        mTabView.setOnTabSelectListener(this);
        mAddButton = (Button) view.findViewById(R.id.btn_add);
        mAddButton.setOnClickListener(this);

        mFeedbackList = (ListView) view.findViewById(R.id.feedbacks);

        mAdapter = new FeedbackAdapter(getActivity(), null, 0);
        mFeedbackList.setAdapter(mAdapter);
        mFeedbackList.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSpiceManager.execute(
                new GetMessageListsRequest(),
                new MessageListsRequestListener());
        onTabSelected(null, mCurrentTab);
    }

    @Override
    public void onTabSelected(View view, int position) {
        mCurrentTab = position;
        Bundle args = new Bundle(1);
        args.putInt("type", position);
        getActivity().getSupportLoaderManager().restartLoader(1139, args, this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Feedback feedback = Feedback.fromCursor((Cursor) mAdapter.getItem(position));
        openFeedback(feedback.getId());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ContentDescriptor.Feedback.Cols.TYPE + " = " + args.getInt("type");
        return new CursorLoader(getActivity(), ContentDescriptor.Feedback.URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    private void openFeedback(int id) {
        int feedbackType;
        switch (mCurrentTab) {
            case 0:
                feedbackType = Feedback.MOTION;
                break;
            case 1:
                feedbackType = Feedback.COMPLAIN;
                break;
            default:
                feedbackType = Feedback.ERROR;
                break;
        }
        getActivity().startActivity(FeedbackActivity.newIntent(getActivity(), id, feedbackType));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                openFeedback(-1);
                break;
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_feedback);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
