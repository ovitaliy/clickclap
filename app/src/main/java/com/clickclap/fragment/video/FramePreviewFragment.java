package com.clickclap.fragment.video;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.adapter.FrameListAdapter;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.util.FilePathHelper;
import com.clickclap.view.CircleLayout;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Владимир on 29.10.2014.
 */
public class FramePreviewFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {

    private ArrayList<String> mFrames;
    private FrameListAdapter mAdapter;

    private String mSelectedFrame;

    public static FramePreviewFragment newInstance() {
        FramePreviewFragment fragment = new FramePreviewFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_preview_base);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getTitle() {
        return getString(R.string.video_edit);
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mNextButton.setOnClickListener(this);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.preview);

        mFrames = new ArrayList<>();
        mListView.setOnItemSelectedListener(this);

        File[] frames = FilePathHelper.getVideoFrameFolder().listFiles();

        mAdapter = new FrameListAdapter(getActivity());
        makeFramesList(frames);
        int selected = 0;
        for (int i = 0; i < mFrames.size(); i++) {
            String videoPreviewPath = NewVideoInfo.get().getOriginalImagePath();
            String framePreviewPath = mFrames.get(i);
            if (videoPreviewPath.equals(framePreviewPath)) {
                selected = i;
                break;
            }
        }

        mListView.setAdapter(mAdapter);
        mListView.setSelectedItem(selected);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void setNextButtonEnabled() {
        mNextButton.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.button_done:
                if (mSelectedFrame != null) {
                    getActivity().onBackPressed();
                    NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());
                }
                break;
        }
    }

    private void makeFramesList(File[] paths) {
        mFrames.clear();
        for (File path : paths) {
            mFrames.add(path.getAbsolutePath());
        }
        mAdapter.setData(mFrames);
    }

    public void onItemSelected(Object data) {
        mSelectedFrame = (String) data;
        NewVideoInfo.get().setOriginalImagePath(mSelectedFrame);
        NewVideoInfo.get().setImageFilterApplied(null);
        VideoProcessTask.getInstance().applyPreviewFilter(true);
    }

    @Override
    public boolean isCanPlayVideo() {
        return false;
    }
}
