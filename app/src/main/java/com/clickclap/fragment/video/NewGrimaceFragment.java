package com.clickclap.fragment.video;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.adapter.VideoFilterListAdapter;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.util.BitmapDecoder;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.ImageCroper;
import com.clickclap.view.CircleLayout;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Denis on 30.03.2014.
 */
public class NewGrimaceFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {
    private VideoFilterListAdapter mAdapter;

    private static int EMPTY;

    private ArrayList<IFilter> mImageFilters;


    private IFilter mSelectedFilter;

    private String mSourceImagePath;


    public static NewGrimaceFragment newInstance(String image, int smileId) {
        NewGrimaceFragment fragment = new NewGrimaceFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_preview_base);
        args.putString("image", image);
        args.putInt("smile", smileId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey("image")) {
                mSourceImagePath = args.getString("image");
            }
        }
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.filter);

        view.findViewById(R.id.video_displaying_surface_view).setVisibility(View.GONE);

        mAdapter = new VideoFilterListAdapter(getActivity());

        mListView.setOnItemSelectedListener(this);
        mNextButton.setOnClickListener(this);

        initFilterList();
    }


    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void setNextButtonEnabled() {
        mNextButton.setEnabled(true);
    }


    private void initFilterList() {
        new ScaleImageTask().executeOnExecutor(ScaleImageTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.button_done:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onItemSelected(Object data) {
        mSelectedFilter = (IFilter) data;

        if (mSelectedFilter == null) {
            NewVideoInfo.get().setVideoPath(null);
            NewVideoInfo.get().setImagePath(null);
        }

        NewVideoInfo.get().setFilter(mSelectedFilter);
        VideoProcessTask.getInstance().applyPreviewFilter();


    }

    private class ScaleImageTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {

                BitmapFactory.Options options = new BitmapFactory.Options();
                //read only bitmap sizes
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mSourceImagePath, options);

                while (NewVideoInfo.isIsFilterBuilding())
                    Thread.sleep(50);
            } catch (Exception ignore) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mImageFilters = new ArrayList<>();
            Collections.addAll(mImageFilters, FilterController.getFilters());

            EMPTY = (int) Math.ceil(mImageFilters.size() / 2);

            mAdapter.setImagePath(FilePathHelper.getVideoPreviewImageSmallPath());
            mAdapter.setData(mImageFilters);
            mListView.setAdapter(mAdapter);

            int selectedIndex = EMPTY;
            if (NewVideoInfo.get().getFilter() != null) {
                for (int i = 0; i < mImageFilters.size(); i++) {
                    IFilter f = mImageFilters.get(i);
                    if (FilterController.areFiltersSame(NewVideoInfo.get().getImageFilterApplied(), f)) {
                        selectedIndex = i;
                        break;
                    }
                }
            }

            mListView.setSelectedItem(selectedIndex);

            onItemSelected(mImageFilters.get(selectedIndex));
        }
    }

    @Override
    public boolean isCanPlayVideo() {
        return false;
    }
}