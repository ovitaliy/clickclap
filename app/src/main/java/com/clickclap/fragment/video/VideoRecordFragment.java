package com.clickclap.fragment.video;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.clickclap.App;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.dialogs.DialogBuilder;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.enums.Emotion;
import com.clickclap.events.VideoProcessedEvent;
import com.clickclap.fragment.BaseFragment;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.UiUtil;
import com.clickclap.view.CircleView;
import com.clickclap.view.VideoSurfaceView;
import com.humanet.camera.CameraApi;
import com.humanet.camera.CameraApi16Impl;
import com.humanet.camera.CameraApi21Impl;
import com.humanet.filters.FilterController;
import com.humanet.video.ProcessCapturedVideoRunnable;

import org.apache.commons.io.FileUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class VideoRecordFragment extends BaseVideoFragment {

    private static final String LOG_TAG = "VideoRecordFragment";

    private static final int VIDEO_MAX_LENGTH = 7;
    private static final int PROGRESS_MAX = VIDEO_MAX_LENGTH * 1000;


    private ProgressDialog mProgressDialog;

    private long mTimeStartRecording;

    private boolean mIsRecording;
    // <--- recorder params

    private CircleView mCircleView;


    private VideoSurfaceView mSurfaceView;
    private ImageButton mStartRecordingButton, mCameraFlashButton, mChangeCameraButton;

    private boolean mIsFlashOn = false;
    private boolean mIsFacingCamera;
    private SurfaceHolder mPreviewHolder;
    private HolderCallback mHolderCallback;

    private TextView mTitleText;

    private UpdateTimeTask mTimeTask;

    private ProgressBar mProgressBar;

    private View mVideoContainer;

    private OnVideoRecordCompleteListener mListener = null;

    private CameraApi mCameraApi;

    public static VideoRecordFragment newInstance() {
        VideoRecordFragment fragment = new VideoRecordFragment();
        fragment.setArguments(new Bundle());
        fragment.setRetainInstance(true);
        return fragment;
    }

    public static VideoRecordFragment newInstance(int smileId) {
        Bundle args;
        VideoRecordFragment f = new VideoRecordFragment();
        args = new Bundle();
        args.putInt(Const.EXTRA_SMILE, smileId);
        f.setArguments(args);
        return f;
    }

    private void initSmileImage(View view, int smileId) {
        ImageView smileImage = (ImageView) view.findViewById(R.id.img_smile);
        if (smileId > 0) {
            smileImage.setVisibility(View.VISIBLE);
            Emotion emotion = Emotion.getById(smileId);
            smileImage.setImageDrawable(emotion.getDrawable());

            int smileTop = mCircleView.getTop() + mCircleView.getHeight() - smileImage.getHeight() / 2;
            smileImage.setY(smileTop);
        } else {
            smileImage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Build.VERSION.SDK_INT < 23)
            mCameraApi = new CameraApi16Impl();
        else
            mCameraApi = new CameraApi21Impl(getActivity());


        if (mCameraApi.getCamerasCount() > 1) {
            if (NewVideoInfo.get().getVideoType() == null
                    || NewVideoInfo.get().getReplyToVideoId() != 0
                    || (getArguments() != null
                    && getArguments().getInt(Const.EXTRA_SMILE) > 0)) {
                mIsFacingCamera = true;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_record, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCircleView = (CircleView) view.findViewById(R.id.surface_view);
        mVideoContainer = view.findViewById(R.id.video_container);

        initProgressBar(view);
        initSurfaceView(view);

        mVideoContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (mVideoContainer.getWidth() > 0) {
                    mCircleView.refresh();
                    mVideoContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });

        mCircleView.postDelayed(new Runnable() {
            @Override
            public void run() {
                final int smileId = getArguments().getInt(Const.EXTRA_SMILE);
                if (smileId > 0) {
                    initSmileImage(view, smileId);
                }
            }
        }, 1000);

        mTitleText = (TextView) view.findViewById(R.id.video_recording_title);

        mStartRecordingButton = (ImageButton) view.findViewById(R.id.btn_rec);
        mStartRecordingButton.setOnClickListener(mRecordOnClickListener);

        mHolderCallback = new HolderCallback();
        mPreviewHolder = mSurfaceView.getHolder();
        mPreviewHolder.addCallback(mHolderCallback);

        initChangeCameraButton(view);
    }

    @Override
    public void onResume() {
        super.onResume();

        mListener = (OnVideoRecordCompleteListener) getActivity();

        if (mHolderCallback.isAvailable()) {
            initCamera();
        }

        cleanUpDirectories();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mIsRecording) {
            stopRecording();
        }

        mCameraApi.releaseCamera();
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    private void initFlashButton(View view) {
        mCameraFlashButton = (ImageButton) view.findViewById(R.id.btn_flash);
        if (mCameraApi.hasFlash()) {
            mCameraFlashButton.setEnabled(true);
            mCameraFlashButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mIsFlashOn = !mIsFlashOn;
                    mCameraApi.turnFlash(!mIsFlashOn);
                }
            });

        } else {
            mCameraFlashButton.setEnabled(false);
        }
    }

    public void initProgressBar(View view) {
        mProgressBar = (ProgressBar) view.findViewById(R.id.video_recording_progress);
        mProgressBar.setMax(PROGRESS_MAX);
        mProgressBar.setProgress(0);
    }


    private void initSurfaceView(View view) {
        mSurfaceView = mCircleView.getSurfaceView();

        mSurfaceView.setOnTouchListener(mOnTouchListener);
    }

    private void initChangeCameraButton(View view) {
        mChangeCameraButton = (ImageButton) view.findViewById(R.id.btn_change_camera);
        if (mCameraApi.getCamerasCount() > 1) {
            mChangeCameraButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mIsFlashOn) {
                        mCameraApi.turnFlash(false);
                        mIsFlashOn = false;
                    }
                    mIsFacingCamera = !mIsFacingCamera;

                    initCamera();
                }
            });
        } else {
            mChangeCameraButton.setEnabled(false);
        }
    }


    private void initCamera() {
        try {
            if (mPreviewHolder != null && mHolderCallback.isAvailable()) {
                mCameraApi.setCameraFacing(mIsFacingCamera);
                mCameraApi.setSurfaceHolder(mPreviewHolder);

                initFlashButton(getView());
            }
        } catch (CameraApi.CameraException ex) {
            ex.printStackTrace();
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }
    }

    private void initSurfaceViewSize() {

        UiUtil.resizedToFitParent(
                mSurfaceView,
                (View) mSurfaceView.getParent(),
                mCameraApi.getCapturedImageHeight(),
                mCameraApi.getCapturedImageWidth()
        );

    }


    private class HolderCallback implements SurfaceHolder.Callback {

        private boolean mAvailable;

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            mAvailable = true;
            initCamera();
            initSurfaceViewSize();
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            initSurfaceViewSize();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
        }

        public boolean isAvailable() {
            return mAvailable;
        }
    }

    private View.OnClickListener mRecordOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!mIsRecording) {
                startRecording();
            } else {
                stopRecording();
            }

        }
    };

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void startRecording() {

        File f = FilePathHelper.getVideoTmpFile();
        if (f.exists() && f.delete()) {
            try {
                f.createNewFile();
            } catch (Exception ignore) {
            }
        }

        mTimeTask = new UpdateTimeTask();

        mProgressBar.post(new Runnable() {
            @Override
            public void run() {
                mTimeStartRecording = System.currentTimeMillis();
                new Timer().schedule(mTimeTask, 30, 30);
                mIsRecording = true;

                invalidateViewsVisibilitiesForRecordingStatus();
            }
        });

        mCameraApi.startRecord(f);
    }

    private void invalidateViewsVisibilitiesForRecordingStatus() {
        if (mIsRecording) {
            mChangeCameraButton.setVisibility(View.INVISIBLE);
            mCameraFlashButton.setVisibility(View.INVISIBLE);

            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setProgress(0);
        } else {

            mChangeCameraButton.setVisibility(View.VISIBLE);
            mCameraFlashButton.setVisibility(View.VISIBLE);

            mProgressBar.setVisibility(View.INVISIBLE);
            mProgressBar.setProgress(0);

            mTitleText.setText(R.string.video_record_title);
        }

    }


    public void stopRecording() {
        long recordLength = System.currentTimeMillis() - mTimeStartRecording;

        mTimeTask.cancel();
        mTimeTask = null;
        //  btnRecStart.setImageResource(R.drawable.ic_record_start);
        mStartRecordingButton.setEnabled(true);

        if (mIsRecording) {
            Log.v(LOG_TAG, "Finishing recording, calling stop and release on recorder");
            try {
                mCameraApi.stopRecord();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (recordLength > 1500) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle(R.string.processing);
            mProgressDialog.show();

            int size = Math.min(mCameraApi.getCapturedImageWidth(), mCameraApi.getCapturedImageHeight());
            NewVideoInfo.get().setSizes(size, size);

            new Thread(new ProcessCapturedVideoRunnable.Builder(App.getInstance())
                    .setCameraId(mIsFacingCamera ? 1 : 0)
                    .setCameraSideSize(size)
                    .setFramesFolder(FilePathHelper.getVideoFrameFolder().getAbsolutePath())
                    .setSourceVideoFilePath(FilePathHelper.getVideoTmpFile().getAbsolutePath())
                    .setTmpVideoFilePath(FilePathHelper.getVideoTmpFile2().getAbsolutePath())
                    .setAudioStreamPath(FilePathHelper.getAudioStreamFile().getAbsolutePath())
                    .setListener(new ProcessCapturedVideoRunnable.OnVideoProcessedListener() {
                        @Override
                        public void onVideoProcessed(boolean success) {
                            EventBus.getDefault().post(new VideoProcessedEvent(success));
                        }
                    })
                    .build())
                    .start();

        } else {
            showErrorAboutNoVideo();
        }

        mIsRecording = false;

        invalidateViewsVisibilitiesForRecordingStatus();
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true)
    public void onEventMainThread(VideoProcessedEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        if (mProgressDialog != null && !mProgressDialog.isDismissed()) {
            mProgressDialog.dismiss();
        }
        if (event.isSuccess()) {
            if (mListener != null) {
                FilterController.init(App.getInstance(), mIsFacingCamera ? 1 : 0);
                mListener.onVideoRecordComplete(FilePathHelper.getVideoTmpFile().getAbsolutePath(), mIsFacingCamera ? 1 : 0);
            }
        } else {
            new DialogBuilder(getActivity())
                    .setMessage("Error grab frames")
                    .setCancelable(true)
                    .create()
                    .show();
        }
    }


    private void showErrorAboutNoVideo() {
        new DialogBuilder(getActivity())
                .setMessage("Error")
                .create()
                .show();
    }


    private class UpdateTimeTask extends TimerTask {
        @Override
        public void run() {
            mTitleText.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        long d = System.currentTimeMillis() - mTimeStartRecording;
                        Date date = new Date(d);
                        mTitleText.setText(new SimpleDateFormat("mm:ss", Locale.getDefault()).format(date));
                        int videoRecordingLength = (int) date.getTime();
                        if (videoRecordingLength > VIDEO_MAX_LENGTH * 1000) {
                            stopRecording();
                        }
                        mProgressBar.setProgress(videoRecordingLength);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        stopRecording();
                    }
                }

            });
        }
    }

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (MotionEvent.ACTION_DOWN == event.getAction()) {
                mCameraApi.focus();
            }
            return false;
        }
    };

    private void cleanUpDirectories() {
        new Thread() {
            @Override
            public void run() {
                try {
                    FileUtils.cleanDirectory(FilePathHelper.getVideoFrameFolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public interface OnVideoRecordCompleteListener {
        void onVideoRecordComplete(String videoPath, int cameraId);
    }

}
