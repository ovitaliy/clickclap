package com.clickclap.fragment.video;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.clickclap.R;
import com.clickclap.activities.RegistrationActivity;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.VideoType;
import com.clickclap.events.SimpleOnItemSelectedListener;
import com.clickclap.listener.SimpleTextListener;
import com.clickclap.model.Category;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.view.NewVideoView;

import java.util.ArrayList;

/**
 * Created by Владимир on 11.11.2014.
 */

public class VideoPreviewFragment extends BaseVideoCreationFragment {

    public static final int ACTION_CREATE = 1;
    public static final int ACTION_DESCRIPTION = 2;
    public static final int ACTION_VIEW = 3;

    private Spinner mCategoriesView;

    private Button mGrimaceFilterView;

    private Button mSelectSoundView;
    private Button mSelectPreviewView;
    private Button mVideoFilterView;

    /**
     * MediaPlayer for playing our video.
     */
    private OnVideoCommandSelected mListener;

    private NewVideoView mVideoView;

    private int mCurrentAction;

    private boolean mSelectFrameButtonPressed = false;
    private boolean mSelectGrimaceFilterButtonPressed = false;

    public static VideoPreviewFragment newInstance(int action) {
        VideoPreviewFragment fragment = new VideoPreviewFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_video_preview);
        args.putInt("action", action);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        setNextButtonEnabled();
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    boolean isCrowdMode() {
        VideoType type = mVideoView.getVideoType();
        return VideoType.CROWD_ASK.equals(type);
    }

    private ArrayList<Category> getCategories() {
        final ArrayList<Category> categories = new ArrayList<>();

        if (mVideoView != null) {
            VideoType videoType = mVideoView.getVideoType();

            if (videoType == null) {
                videoType = VideoType.FLOW_ALL;
            }

            String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + videoType.getId()
                    + " AND " + ContentDescriptor.Categories.Cols.TECH + " = 0";

            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver().query(
                        ContentDescriptor.Categories.URI,
                        null,
                        selection,
                        null,
                        null);
                categories.clear();
                categories.add(new Category(-1, getString(R.string.hash_dialog_choose_category)));
                if (cursor.moveToFirst()) {
                    do {
                        Category category = Category.fromCursor(cursor);
                        if (category.getId() != 0) {
                            categories.add(category);
                        }
                    } while (cursor.moveToNext());
                }

            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
        return categories;
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mCurrentAction = getArguments().getInt("action");

        mImage = VideoProcessTask.getInstance().getPreview();
        if (mImage == null) {
            VideoProcessTask.getInstance().applyVideoFilter();
        } else {
            onVideoProcessStatus(Status.DONE);
        }

        mNextButton = (Button) view.findViewById(R.id.video_displaying_button_next);
        mNextButton.setOnClickListener(this);

        mSelectPreviewView = (Button) view.findViewById(R.id.select_preview);
        mSelectSoundView = (Button) view.findViewById(R.id.select_track);
        mVideoFilterView = (Button) view.findViewById(R.id.select_video_filter);
        mGrimaceFilterView = (Button) view.findViewById(R.id.select_grimace_filter);

        mSelectPreviewView.setOnClickListener(this);
        mSelectSoundView.setOnClickListener(this);
        mVideoFilterView.setOnClickListener(this);
        mGrimaceFilterView.setOnClickListener(this);

        if (mVideoView.isGrimaceMode()) {
            mSelectSoundView.setVisibility(View.GONE);
            mVideoFilterView.setVisibility(View.GONE);
            mGrimaceFilterView.setVisibility(View.VISIBLE);
        } else {
            mSelectSoundView.setVisibility(View.VISIBLE);
            mVideoFilterView.setVisibility(View.VISIBLE);
            mGrimaceFilterView.setVisibility(View.GONE);
        }

        view.findViewById(R.id.video_displaying_make_new).setOnClickListener(this);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mCircleView.getLayoutParams();

        View headerView = view.findViewById(R.id.header);
        if (getActivity() instanceof RegistrationActivity) {
            headerView.setVisibility(View.VISIBLE);
            params.addRule(RelativeLayout.BELOW, R.id.header);
        } else {
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            headerView.setVisibility(View.GONE);
        }

        EditText tagsView = (EditText) view.findViewById(R.id.tags);
        tagsView.addTextChangedListener(new SimpleTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                NewVideoInfo.get().setTags(s.toString());
            }
        });

        if (mCurrentAction == ACTION_DESCRIPTION) {
            view.findViewById(R.id.description_container).setVisibility(View.VISIBLE);
            view.findViewById(R.id.select_video_filter).setVisibility(View.GONE);
            view.findViewById(R.id.select_track).setVisibility(View.GONE);
            view.findViewById(R.id.select_preview).setVisibility(View.GONE);
            view.findViewById(R.id.video_displaying_make_new).setVisibility(View.GONE);

            if (mCategoriesView == null) {
                mCategoriesView = (Spinner) view.findViewById(R.id.sp_category);

                EditText amount = (EditText) view.findViewById(R.id.amount);
                amount.addTextChangedListener(new SimpleTextListener() {
                    @Override
                    public void afterTextChanged(Editable s) {
                        int cost = 0;
                        String costString = s.toString();
                        if (!TextUtils.isEmpty(costString)) {
                            cost = Integer.parseInt(costString);
                        }
                        NewVideoInfo.get().setCost(cost);
                        setNextButtonEnabled();
                    }
                });

                if (isCrowdMode()) {
                    amount.setVisibility(View.VISIBLE);
                } else {
                    amount.setVisibility(View.GONE);
                }

                mCategoriesView.setVisibility(View.VISIBLE);
                final ArrayList<Category> categories = getCategories();

                String[] categoriesTitles = getCategoriesTexts(categories);
                ArrayAdapter<CharSequence> categoriesAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item, categoriesTitles);
                categoriesAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                mCategoriesView.setAdapter(categoriesAdapter);

                if (NewVideoInfo.get().getCategoryId() != 0) {
                    int position = 0;
                    for (int i = 0; i < categories.size(); i++) {
                        Category category = categories.get(i);
                        if (category.getId() == NewVideoInfo.get().getCategoryId()) {
                            position = i;
                        }
                    }
                    mCategoriesView.setSelection(position);
                    mCategoriesView.setEnabled(false);
                }
                mCategoriesView.setOnItemSelectedListener(new SimpleOnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Category category = categories.get(position);
                        NewVideoInfo.get().setCategoryId(category.getId());
                        setNextButtonEnabled();
                    }
                });
            }

        }
    }

    @Override
    public void setNextButtonEnabled() {
        boolean validate = true;

        if (mCurrentAction == ACTION_DESCRIPTION) {
            if (mVideoView != null && !mVideoView.isGrimaceMode()) {
                if (mCategoriesView != null) {
                    if (mCategoriesView.getSelectedItemPosition() == 0) {
                        validate = false;
                    }
                }
            }

            if (isCrowdMode()) {
                if (NewVideoInfo.get().getCost() == 0) {
                    validate = false;
                }
            }
        }

        if (mVideoView != null && mVideoView.isGrimaceMode()) {
            mGrimaceFilterView.setEnabled(mSelectFrameButtonPressed);
            validate = mSelectGrimaceFilterButtonPressed;
        }

        mNextButton.setEnabled(validate);
    }

    /**
     * Take array mVoteList of Category and make from it string array.
     * Needed for HashAdapter, thar work with array.
     *
     * @param categories The categories we will use.
     * @return Array of categories.
     */
    private String[] getCategoriesTexts(ArrayList<Category> categories) {
        String[] strings = new String[categories.size()];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = categories.get(i).getTitle();
        }
        return strings;
    }

    @Override
    public void onVideoProcessStatus(Status status) {
        super.onVideoProcessStatus(status);

        if (mCurrentAction == ACTION_VIEW) {
            View v = getView();
            final boolean enabled = status.equals(Status.DONE);
            if (v != null) {
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mNextButton != null) {
                            mNextButton.setEnabled(enabled);
                            setNextButtonEnabled();
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (OnVideoCommandSelected) activity;
        mVideoView = (NewVideoView) activity;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.select_preview:
                mSelectFrameButtonPressed = true;
                mListener.onOpenFramePreview();
                setNextButtonEnabled();
                break;
            case R.id.select_track:
                mListener.onOpenTrack();
                break;
            case R.id.select_video_filter:
                mListener.onOpenFilter();
                break;
            case R.id.video_displaying_button_next:
                mNextButton.setEnabled(false);
                mListener.onComplete(mCurrentAction);
                break;
            case R.id.video_displaying_make_new:
                mListener.onOpenRecorder(mCurrentAction == ACTION_CREATE ? 1 : 2);
                break;
            case R.id.video_displaying_surface_view:
                break;
            case R.id.select_grimace_filter:
                mSelectGrimaceFilterButtonPressed = true;
                mListener.onOpenGrimace();
                setNextButtonEnabled();
                break;
        }
    }


    public interface OnVideoCommandSelected {
        void onOpenRecorder(int step);

        void onOpenFramePreview();

        void onOpenTrack();

        void onOpenFilter();

        void onOpenGrimace();

        void onComplete(int action);
    }
}
