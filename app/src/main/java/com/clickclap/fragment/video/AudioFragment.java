package com.clickclap.fragment.video;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.adapter.AudioListAdapter;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.util.FilePathHelper;
import com.clickclap.view.CircleLayout;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Владимир on 28.10.2014.
 */
public class AudioFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {

    private com.humanet.audio.AudioTrack mSelectedAudioTrack;

    private static int EMPTY;

    MediaPlayer player = new MediaPlayer();

    @Override
    public String getTitle() {
        return getString(R.string.video_edit);
    }

    public static AudioFragment newInstance() {
        Bundle args = new Bundle();
        AudioFragment fragment = new AudioFragment();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_preview_base);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);

        mNextButton.setOnClickListener(this);

        mListView.setOnItemSelectedListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadAudioList();
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.button_done:
                if (mSelectedAudioTrack == null) {
                    NewVideoInfo.get().setAudioPath(null);
                } else {
                    NewVideoInfo.get().setAudioPath(mSelectedAudioTrack.getPath());
                }
                VideoProcessTask.getInstance().applyAudio();
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void setNextButtonEnabled() {
        mNextButton.setEnabled(true);
    }


    /**
     * Look for audio on phone memory and load their.
     */
    private void loadAudioList() {
        new AsyncTask<Void, Void, ArrayList<com.humanet.audio.AudioTrack>>() {

            @Override
            protected ArrayList<com.humanet.audio.AudioTrack> doInBackground(Void... params) {
                ArrayList<com.humanet.audio.AudioTrack> audioTracks = com.humanet.audio.AudioLoader.getListFromMusicDirecotory(FilePathHelper.getAudioDirectory());
                EMPTY = audioTracks.size() / 2;
                audioTracks.add(EMPTY, getFakeTrack());
                return audioTracks;
            }

            @Override
            protected void onPostExecute(ArrayList<com.humanet.audio.AudioTrack> audioTracks) {
                super.onPostExecute(audioTracks);

                AudioListAdapter audioListAdapter = new AudioListAdapter(getActivity());
                audioListAdapter.setData(audioTracks);
                mListView.setAdapter(audioListAdapter);

                int selectedIndex = 0;
                if (NewVideoInfo.get().getAudioPath() == null) {
                    selectedIndex = EMPTY;
                } else {
                    for (int i = 0; i < audioTracks.size(); i++) {
                        if (tracksAreSame(NewVideoInfo.get().getAudioPath(), audioTracks.get(i))) {
                            selectedIndex = i;
                            break;
                        }
                    }
                }

                mListView.setSelectedItem(selectedIndex);
                mSelectedAudioTrack = audioTracks.get(selectedIndex);
            }
        }.execute();
    }

    private com.humanet.audio.AudioTrack getFakeTrack() {
        com.humanet.audio.AudioTrack fakeTrack = new com.humanet.audio.AudioTrack();
        fakeTrack.setTitle(App.getInstance().getString(R.string.video_record_no_audio));
        fakeTrack.setPath(FilePathHelper.getAudioStreamFile().getAbsolutePath());
        return fakeTrack;
    }

    private boolean isFakeTrack() {
        return mSelectedAudioTrack != null && tracksAreSame(mSelectedAudioTrack.getPath(), getFakeTrack());
    }

    public boolean tracksAreSame(String original, com.humanet.audio.AudioTrack track) {
        if (original == null) {
            return track.getPath() == null;
        }

        return track.getPath() != null && original.equals(track.getPath());
    }

    @Override
    public void onItemSelected(Object data) {
        mSelectedAudioTrack = (com.humanet.audio.AudioTrack) data;
        NewVideoInfo.get().setAudioPath(mSelectedAudioTrack.getPath());
        NewVideoInfo.get().setAudioApplied(null);
        if (mediaPlayer == null || !isVideoPlaying) {
            if (mSelectedAudioTrack != null) {
                try {
                    if (!TextUtils.isEmpty(mSelectedAudioTrack.getPath()) && !isFakeTrack()) {
                        player.reset();
                        player.setDataSource(mSelectedAudioTrack.getPath());
                        player.prepare();
                        player.start();
                    } else {
                        try {
                            player.stop();
                        } catch (IllegalStateException ignore) {
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                releaseMediaPlayer();
            }
        }

        if (isVideoPlaying)
            releaseMediaPlayer();
    }
}
