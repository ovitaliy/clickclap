package com.clickclap.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.NewVoteActivity;
import com.clickclap.activities.VoteActivity;
import com.clickclap.adapter.VoteAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.VoteListType;
import com.clickclap.model.Vote;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.GetVoteListRequestListener;
import com.clickclap.rest.request.vote.GetVotesListRequest;
import com.clickclap.view.TabView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Denis on 02.12.2015.
 */
public class PollListFragment extends BaseFragment implements
        View.OnClickListener,
        TabView.OnTabSelectListener,
        AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor>,
        Const {

    private static final int POLL_LOADER_ID = 1;

    public static PollListFragment newInstance() {
        Bundle args;
        PollListFragment f = new PollListFragment();
        args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_poll_list);
        f.setArguments(args);
        return f;
    }

    private TabView mTabView;
    private ListView mPollsListView;
    private int mCurrentTabPosition = 0;
    private Button mAddButton;

    private VoteAdapter mAdapter;

    private SpiceManager mSpiceManager;

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        mTabView = (TabView) view.findViewById(R.id.tabhost);
        mTabView.setOnTabSelectListener(this);

        mAddButton = (Button) view.findViewById(R.id.btn_add);
        mAddButton.setOnClickListener(this);

        mPollsListView = (ListView) view.findViewById(R.id.votes);
        mAdapter = new VoteAdapter(getActivity(), null, 0);
        mPollsListView.setAdapter(mAdapter);
        mPollsListView.setOnItemClickListener(this);
        mTabView.setActive(mCurrentTabPosition);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_poll);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    private void loadVoteList(VoteListType type) {
        Bundle args = new Bundle(1);
        if (type != null) {
            args.putInt(Params.TYPE, type.ordinal());
        } else {
            args.putInt(Params.TYPE, -1);
        }
        getActivity().getSupportLoaderManager().restartLoader(POLL_LOADER_ID, args, this);

        if (type != null) {
            mSpiceManager.execute(
                    new GetVotesListRequest(type),
                    new GetVoteListRequestListener(type));
        }
    }

    @Override
    public void onTabSelected(View view, int position) {
        mCurrentTabPosition = position;
        switch (position) {
            case 0:
                mAddButton.setVisibility(View.GONE);
                loadVoteList(VoteListType.NOW);
                break;
            case 1:
                mAddButton.setVisibility(View.GONE);
                loadVoteList(VoteListType.PAST);
                break;
            case 2:
                mAddButton.setVisibility(View.VISIBLE);
                loadVoteList(null);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                getActivity().startActivity(new Intent(getActivity(), NewVoteActivity.class));
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Vote vote = Vote.fromCursor((Cursor) mAdapter.getItem(position));
        getActivity().startActivity(VoteActivity.newInstance(getActivity(), vote.getId(), mCurrentTabPosition == 1));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                ContentDescriptor.Votes.URI,
                null,
                ContentDescriptor.Votes.Cols.TYPE + " = " + args.getInt(Params.TYPE, 0),
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        mAdapter.setIsMy(mCurrentTabPosition == 2);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
