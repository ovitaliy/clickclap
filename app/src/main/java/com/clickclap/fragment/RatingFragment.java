package com.clickclap.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.activities.MainActivity;
import com.clickclap.adapter.RatingAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.UserInfo;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.UserRatingResponse;
import com.clickclap.rest.request.user.RatingRequest;
import com.clickclap.view.TabView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

public class RatingFragment extends BaseFragment implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor>, TabView.OnTabSelectListener {
    private static final int LOADER_ID = 1;

    private SpiceManager mSpiceManager;
    private ListView mListView;
    private RatingAdapter mRatingAdapter;
    private TextView mRateView;
    private TextView mRatePositionView;

    private int mSelectedTab = 0;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    public static RatingFragment newInstance() {
        RatingFragment ratingFragment = new RatingFragment();
        return ratingFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRatingAdapter = new RatingAdapter(getActivity(), null, 0);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rating, container, false);

        ((TabView) view.findViewById(R.id.tabhost)).setOnTabSelectListener(this);

        mListView = (ListView) view.findViewById(R.id.tops_list);
        mListView.setOnItemClickListener(this);
        mListView.setAdapter(mRatingAdapter);

        mRateView = (TextView) view.findViewById(R.id.rate);
        mRatePositionView = (TextView) view.findViewById(R.id.rate_position);

        loadRating();

        return view;
    }

    private void setRating(int rating, int position) {
        mRateView.setText(getString(R.string.rating_rate, rating));
        if (position != -1) {
            mRatePositionView.setText(getString(R.string.rating_rate_position, position) + "%");
        }
    }

    private void loadRating() {
        int type = (mSelectedTab << 8) + ContentDescriptor.Users.RECORD_TYPE_RATING;

        Bundle bundle = new Bundle(1);
        bundle.putInt("type", type);

        getLoaderManager().restartLoader(LOADER_ID, bundle, this);

        String sType = mSelectedTab == 1 ? "global" : "local";
        mSpiceManager.execute(new RatingRequest(sType), new RatingResponse(type));

        mListView.scrollTo(0, 0);
    }

    @Override
    public String getTitle() {
        return getString(R.string.rating);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = (Cursor) mRatingAdapter.getItem(position);
        if (cursor != null) {
            UserInfo userInfo = UserInfo.fromCursor(cursor);
            if (userInfo != null) {
                ((MainActivity) getActivity()).onViewProfile(userInfo.getId());
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ContentDescriptor.Users.Cols.RECORD_TYPE +
                " = " + args.getInt("type");
        return new CursorLoader(getActivity(), ContentDescriptor.Users.URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mRatingAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mRatingAdapter.swapCursor(null);
    }

    @Override
    public void onTabSelected(View view, int position) {
        mSelectedTab = position;

        loadRating();
    }

    private class RatingResponse extends BaseRequestListener<UserRatingResponse> {

        private int mTypeId;

        public RatingResponse(int typeId) {
            mTypeId = typeId;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            super.onRequestFailure(spiceException);
        }

        @Override
        public void onRequestSuccess(final UserRatingResponse response) {
            super.onRequestSuccess(response);
            setRating(response.getRating(), response.getPosition());

            new Thread(new Runnable() {
                @Override
                public void run() {
                    UserInfo[] list = response.getTopUsers();
                    ContentValues[] contentValues = new ContentValues[list.length];
                    for (int i = 0; i < list.length; i++) {
                        UserInfo user = list[i];
                        ContentValues values = user.toContentValues();
                        values.put(ContentDescriptor.Users.Cols.RECORD_TYPE, mTypeId);
                        contentValues[i] = values;
                    }
                    if (isAdded()) {
                        String selection = ContentDescriptor.Users.Cols.RECORD_TYPE + " = " + mTypeId;
                        getActivity().getContentResolver().delete(ContentDescriptor.Users.URI, selection, null);
                        getActivity().getContentResolver().bulkInsert(ContentDescriptor.Users.URI, contentValues);
                    }
                }
            }).start();
        }
    }
}
