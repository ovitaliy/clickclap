package com.clickclap.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.clickclap.R;
import com.clickclap.activities.NewVideoActivity;
import com.clickclap.enums.Emotion;
import com.clickclap.model.Grimace;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BuyGrimaceRequestListener;
import com.clickclap.rest.listener.SetSellingGrimaceRequestListener;
import com.clickclap.rest.request.grimace.BuyGrimaceRequest;
import com.clickclap.rest.request.grimace.SetSellingGrimaceRequest;
import com.clickclap.util.Utils;
import com.clickclap.view.GrimaceCircleView;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Denis on 31.03.2015.
 */
public class GrimacePageFragment extends Fragment {
    private TableLayout mTableView;
    private HashMap<Integer, Grimace[]> mGrimaces;
    private ArrayList<Integer> mEmotions;
    private int mCellSize;
    private int mType;
    private SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    public static GrimacePageFragment newInstance(LinkedHashMap<Integer, Grimace[]> grimaces, int cellSize, int type) {
        GrimacePageFragment fragment = new GrimacePageFragment();
        Bundle args = new Bundle();
        args.putSerializable("grimaces", grimaces);

        //since linkedHashMap serialization problems
        ArrayList<Integer> emotions = new ArrayList<>();
        for (Map.Entry<Integer, Grimace[]> entry : grimaces.entrySet()) {
            emotions.add(entry.getKey());
        }

        args.putSerializable("emotions", emotions);
        args.putInt("cell_size", cellSize);
        args.putInt("type", type);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey("grimaces")) {
                mGrimaces = (HashMap<Integer, Grimace[]>) args.getSerializable("grimaces");
            } else {
                throw new IllegalArgumentException("Grimaces required!");
            }
            if (args.containsKey("emotions")) {
                mEmotions = (ArrayList<Integer>) args.getSerializable("emotions");
            } else {
                throw new IllegalArgumentException("Emotions required!");
            }
            mCellSize = args.getInt("cell_size");
            mType = args.getInt("type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grimace_page, container, false);
        mTableView = (TableLayout) view.findViewById(R.id.grimaces_table);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        fill();
    }

    private void fill() {
        mTableView.post(new Runnable() {
            @Override
            public void run() {
                Context context = mTableView.getContext();
                if (context == null) {
                    return;
                }
                if (!isVisible()) {
                    return;
                }
                int margin = Utils.convertDpToPixel(3, context);
                int cellSize = mCellSize - (margin * 2);
                mTableView.removeAllViews();
                for (final int i : mEmotions) {
                    Grimace[] grimaces = mGrimaces.get(i);
                    TableRow row = new TableRow(getActivity());
                    row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

                    for (final Grimace grimace : grimaces) {
                        final View cellView;

                        if (grimace != null) {
                            if (grimace.getId() == 0) {
                                cellView = new ImageView(context);
                                cellView.setBackgroundResource(R.drawable.btn_add);
                                cellView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        NewVideoActivity.startNewGrimaceInstance(getActivity(), Emotion.getById(i));
                                    }
                                });
                            } else {
                                cellView = new GrimaceCircleView(getActivity());
                                ((GrimaceCircleView) cellView).setGrimace(grimace);
                                ((GrimaceCircleView) cellView).setType(mType);

                                if (mType != Grimace.TYPE_COLLECTION) {
                                    cellView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            switch (mType) {
                                                case Grimace.TYPE_USER:
                                                    if (grimace != null) {
                                                        grimace.setSelling(!grimace.isSelling());
                                                        setSellingGrimace(grimace.getId(), grimace.isSelling());
                                                        ((GrimaceCircleView) cellView).setGrimace(grimace);
                                                    }
                                                    break;
                                                case Grimace.TYPE_MARKET:
                                                    if (grimace != null) {
                                                        if (!grimace.isBought()) {
                                                            grimace.setBought(true);
                                                            buyGrimace(grimace.getId());
                                                            ((GrimaceCircleView) cellView).setGrimace(grimace);
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    });
                                }
                            }
                        } else {
                            cellView = new View(context);
                        }
                        TableRow.LayoutParams params = new TableRow.LayoutParams(cellSize, cellSize);
                        params.setMargins(margin, margin, margin, margin);
                        cellView.setLayoutParams(params);

                        row.addView(cellView);
                    }
                    mTableView.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }
            }
        });
    }

    private void buyGrimace(int id) {
        mSpiceManager.execute(new BuyGrimaceRequest(id), new BuyGrimaceRequestListener(id));
    }

    private void setSellingGrimace(int id, boolean status) {
        mSpiceManager.execute(new SetSellingGrimaceRequest(id, status), new SetSellingGrimaceRequestListener(id, status));
    }
}
