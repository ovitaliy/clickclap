package com.clickclap.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ImageView;

import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.MainActivity;
import com.clickclap.activities.NewVideoActivity;
import com.clickclap.adapter.CategoryListAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.dialogs.TipsDialogFragment;
import com.clickclap.enums.VideoType;
import com.clickclap.model.Category;
import com.clickclap.model.SimpleListItem;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.CategoriesRequestListener;
import com.clickclap.rest.request.GetCategoriesRequest;
import com.clickclap.util.PrefHelper;
import com.clickclap.view.CategorizedFlowsView;
import com.clickclap.view.CircleLayout;
import com.clickclap.view.TabView;
import com.clickclap.view.VideoPreviewView;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;

public class EmoticonsFragment extends BaseFragment implements TabView.OnTabSelectListener,
        CircleLayout.OnItemSelectedListener,
        LoaderManager.LoaderCallbacks<Cursor>,
        View.OnClickListener, Const, CategorizedFlowsView {

    private VideoPreviewView mPreviewView;
    private CircleLayout mCircleLayout;

    private SimpleListItem mCurrentItem;

    private Tab mCurrentTab = Tab.BEST;

    private CategoryListAdapter mCategoriesAdapter;

    private SpiceManager mSpiceManager;

    private ImageView mImageView;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int type = args.getInt(Params.TYPE);
        String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + type
                + " AND " + ContentDescriptor.Categories.Cols.VIDEO_COUNT + " > 0";
        return new CursorLoader(getActivity(),
                ContentDescriptor.Categories.URI,
                null,
                selection,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            ArrayList<Category> categories = new ArrayList<>();
            if (data.moveToFirst()) {
                do {
                    categories.add(Category.fromCursor(data));
                } while (data.moveToNext());
            }

            categories = Category.reorder(categories);

            if (isAdded()) {
                if (!mCategoriesAdapter.isNewDataIdentical(categories)) {
                    mCategoriesAdapter.setData(categories);
                    mCircleLayout.setAdapter(mCategoriesAdapter);
                    mCircleLayout.setSelectedItem(mCategoriesAdapter.getInitialSelectionPosition());
                    mCircleLayout.invalidateAll();
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public enum Tab {
        ALL, BEST, MY_CHOICE;
    }

    public static EmoticonsFragment newInstance() {
        EmoticonsFragment f = new EmoticonsFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_emoticons);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentTab = (Tab) savedInstanceState.getSerializable("tab");
            mCurrentItem = (SimpleListItem) savedInstanceState.getSerializable("item");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    protected void initUI(final View view, Bundle savedInstanceState) {
        mImageView = (ImageView) view.findViewById(R.id.preview_image);
        mImageView.setOnClickListener(this);
        view.findViewById(R.id.new_video_shot).setOnClickListener(this);

        mPreviewView = (VideoPreviewView) view.findViewById(R.id.video_preview);
        mCircleLayout = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mCircleLayout.setOnItemSelectedListener(this);

        final TabView tabView = (TabView) view.findViewById(R.id.tabhost);
        tabView.setOnTabSelectListener(this);
        tabView.setActive(mCurrentTab.ordinal());

        mCategoriesAdapter = new CategoryListAdapter(getActivity());
        mCircleLayout.setAdapter(mCategoriesAdapter);
    }

    @Override
    public void openFlowList() {
        if (mCurrentItem != null) {
            FlowListFragment.Builder builder = new FlowListFragment.Builder(getVideoType());
            builder.setParams(((Category) mCurrentItem).getParams());
            ((MainActivity) getActivity()).startFragment(builder.build(), true);
        }
    }

    @Override
    public Category getCurrentCategory() {
        return null;
    }

    private synchronized void checkTips() {
        if (!PrefHelper.getBooleanPref("tips")) {
            PrefHelper.setBooleanPref("tips", true);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    new TipsDialogFragment().show(getFragmentManager(), "tips");
                }
            }, 1000);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkTips();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.preview_image:
                openFlowList();
                break;
            case R.id.new_video_shot:
                NewVideoActivity.startNewInstance(getActivity(), VideoType.EMOTICON_ALL);
                break;
        }
    }

    @Override
    public void onItemSelected(Object data) {
        mCurrentItem = (SimpleListItem) data;
        if (data != null) {
            mPreviewView.show(((SimpleListItem) data).getImage());
        } else {
            mPreviewView.hide();
        }
    }

    @Override
    public void unregisterForContextMenu(View view) {
        super.unregisterForContextMenu(view);
    }

    @Override
    public VideoType getVideoType() {
        switch (mCurrentTab) {
            case ALL:
                return VideoType.EMOTICON_ALL;
            case BEST:
                return VideoType.EMOTICON_BEST;
            case MY_CHOICE:
                return VideoType.EMOTICON_MY_CHOICE;
        }
        return null;
    }

    @Override
    public void loadCategories(VideoType type) {
        mSpiceManager.execute(
                new GetCategoriesRequest(type),
                new CategoriesRequestListener(type));
    }

    @Override
    public void onTabSelected(View view, int position) {
        mCurrentTab = Tab.values()[position];
        if (mCategoriesAdapter != null) {
            mCircleLayout.clear();
            mCategoriesAdapter.setData(null);
        }

        mPreviewView.hide();

        VideoType type = getVideoType();
        loadCategories(type);
        Bundle args = new Bundle(1);
        args.putInt(Params.TYPE, type.getId());
        getActivity().getSupportLoaderManager().restartLoader(1, args, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("item", mCurrentItem);
        outState.putSerializable("tab", mCurrentTab);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_emoticons);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }
}