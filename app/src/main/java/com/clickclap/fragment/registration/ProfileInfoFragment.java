package com.clickclap.fragment.registration;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.NewVideoActivity;
import com.clickclap.activities.RegistrationActivity;
import com.clickclap.enums.Education;
import com.clickclap.enums.Job;
import com.clickclap.enums.Language;
import com.clickclap.listener.SimpleTextListener;
import com.clickclap.loaders.LocationDataLoader;
import com.clickclap.model.UserInfo;
import com.clickclap.view.CircleImageView;
import com.clickclap.view.CityAutoCompleteTextView;
import com.clickclap.view.CountrySpinnerView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by michael on 30.10.14.
 */
public class ProfileInfoFragment extends BaseProfileFragment implements OnItemSelectedListener {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private EditText mFirstNameField;
    private EditText mLastNameField;
    private Spinner mLangs;
    private RadioGroup mGender;
    private Button mBirth;
    private Button mNext;

    private Activity mActivity;

    private Date mSelectedBirthDay;


    private LocationDataLoader mLocationDataLoader;

    private ImageView mAvatarView;

    ArrayAdapter<CharSequence> mEducationAdapter;
    ArrayAdapter<CharSequence> mJobAdapter;

    public static ProfileInfoFragment newInstance() {
        ProfileInfoFragment f = new ProfileInfoFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_profile_info);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        // init views
        mFirstNameField = (EditText) view.findViewById(R.id.field_first_name);
        mLastNameField = (EditText) view.findViewById(R.id.field_last_name);
        mLangs = (Spinner) view.findViewById(R.id.sp_lang);
        ArrayAdapter<CharSequence> langAdapter = ArrayAdapter.createFromResource(mActivity, R.array.langs, R.layout.spinner_item);
        langAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mLangs.setAdapter(langAdapter);
        mGender = (RadioGroup) view.findViewById(R.id.select_gender);
        mBirth = (Button) view.findViewById(R.id.txt_birth_field);
        mBirth.setOnClickListener(this);

        mNext = (Button) view.findViewById(R.id.btn_nav);
        if (getActivity() instanceof RegistrationActivity) {
            mNext.setVisibility(View.VISIBLE);
        } else {
            mNext.setVisibility(View.GONE);
        }


        CharSequence[] edus = new CharSequence[Education.values().length];
        int i = 0;
        for (Education education : Education.values()) {
            edus[i++] = education.getTitle();
        }

        mEducationAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, edus);
        Spinner sp = (Spinner) view.findViewById(R.id.sp_edu);
        mEducationAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        sp.setAdapter(mEducationAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppUser.get().setDegree(Education.values()[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Job

        CharSequence[] jobs = new CharSequence[Job.values().length];
        i = 0;
        for (Job job : Job.values()) {
            jobs[i++] = job.getTitle();
        }
        mJobAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, jobs);
        sp = (Spinner) view.findViewById(R.id.sp_job);
        mJobAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        sp.setAdapter(mJobAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppUser.get().setJobType(Job.values()[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp = (Spinner) view.findViewById(R.id.sp_job);
        if (AppUser.get().getJobType() != null) {
            sp.setSelection(Math.min(AppUser.get().getJobType().getId(), mJobAdapter.getCount() - 1));
        }

        sp = (Spinner) view.findViewById(R.id.sp_edu);
        if (AppUser.get().getDegree() != null) {
            sp.setSelection(Math.min(AppUser.get().getDegree().getId(), mEducationAdapter.getCount() - 1));
        }

        TextView textView;

        textView = (TextView) view.findViewById(R.id.profession);
        textView.setText(AppUser.get().getCraft());
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AppUser.get().setCraft(s.toString());
            }
        });

        mNext.setOnClickListener(this);
        //load avatar if available
        mAvatarView = (CircleImageView) view.findViewById(R.id.avatar);
        if (AppUser.get() != null) {
            mAvatarView.setOnClickListener(this);

            mGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    AppUser.get().setGender(mGender.getCheckedRadioButtonId() == R.id.male ? 0 : 1);
                }
            });

            mFirstNameField.setText(AppUser.get().getFName());
            mLastNameField.setText(AppUser.get().getLName());
            int gender = AppUser.get().getGender();
            switch (gender) {
                case 0:
                    mGender.check(R.id.male);
                    break;
                case 1:
                    mGender.check(R.id.female);
                    break;
            }

            long birthday = AppUser.get().getBirthDate();
            if (birthday != 0) {
                mSelectedBirthDay = new Date(birthday);
                mBirth.setText(sdf.format(mSelectedBirthDay));
            }
        }

        if (AppUser.get().baseDataAdded) {
            mLangs.setSelection(Language.getSystem().getId());
            Calendar c = Calendar.getInstance(Locale.getDefault());
            mBirth.setText(sdf.format(c.getTime()));
        } else {
            checkUserData();
        }
        mLangs.setOnItemSelectedListener(this);


        String city = AppUser.get() != null ? AppUser.get().getCity() : null;
        mLocationDataLoader = new LocationDataLoader(
                App.getSpiceManager(),
                (CityAutoCompleteTextView) view.findViewById(R.id.sp_city),
                (CountrySpinnerView) view.findViewById(R.id.sp_country),
                city
        );

        mLocationDataLoader.setLocationSelectListener(new LocationDataLoader.LocationSelectListener() {
            @Override
            public void onCountrySelected(int countryId) {
                AppUser.get().setCountryId(countryId);
            }

            @Override
            public void onCitySelected(int cityId) {
                AppUser.get().setCityId(cityId);
            }
        });


        if (!(getActivity() instanceof RegistrationActivity)) {
            view.findViewById(R.id.header_container).setVisibility(View.GONE);
        }

        mFirstNameField.addTextChangedListener(new SimpleTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                AppUser.get().setFName(s.toString());
            }
        });
        mLastNameField.addTextChangedListener(new SimpleTextListener() {

            @Override
            public void afterTextChanged(Editable s) {
                AppUser.get().setLName(s.toString());
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        ImageLoader.getInstance().displayImage(AppUser.get().getImageLoaderPhoto(), mAvatarView);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

//    String lang = getLang(mLangs.getSelectedItemPosition());
//    mUserInfo.setLanguage(lang);

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_birth_field:
                final Calendar c = Calendar.getInstance(Locale.getDefault());
                Date curDate;
                try {
                    curDate = sdf.parse(mBirth.getText().toString());
                    c.setTimeInMillis(curDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DatePickerDialog d = new DatePickerDialog(mActivity,
                        new OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                mSelectedBirthDay = c.getTime();
                                mBirth.setText(sdf.format(c.getTime()));
                                try {
                                    long birth_stamp;
                                    birth_stamp = sdf.parse(mBirth.getText().toString()).getTime();
                                    AppUser.get().setBirthDate(birth_stamp);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.YEAR, -1 * Const.USER_MIN_AGE);
                d.getDatePicker().setMaxDate(calendar.getTime().getTime());
                d.show();
                break;
            case R.id.btn_nav:
                checkMandatoryFields(AppUser.get());
                break;
            case R.id.avatar:
                updateAvatar();
                break;
        }
    }

    private void updateAvatar() {
        NewVideoActivity.startNewAvatarInstance(getActivity());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void checkUserData() {
        if (AppUser.get().baseDataAdded) {
            mLangs.setSelection(AppUser.get().getLanguage().getId());
            Date d = new Date(AppUser.get().getBirthDate());
            mBirth.setText(sdf.format(d));
        }
    }

    private void checkMandatoryFields(UserInfo info) {
        if (mLocationDataLoader.getSelectedCityId() > 0) {
            AppUser.get().setCity(mLocationDataLoader.getSelectedCityName());
            AppUser.get().setCityId(mLocationDataLoader.getSelectedCityId());
        }
        if (mLocationDataLoader.getSelectedCountryId() > 0) {
            AppUser.get().setCountry(mLocationDataLoader.getSelectedCountryName());
            AppUser.get().setCountryId(mLocationDataLoader.getSelectedCountryId());
        }

        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
        todayCalendar.set(Calendar.MINUTE, 0);
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setPositiveButton(R.string.ok, null);
        if (TextUtils.isEmpty(mFirstNameField.getText()) || TextUtils.isEmpty(mLastNameField.getText())) {
            builder.setMessage(R.string.alert_name)
                    .create()
                    .show();
        } else if (mLocationDataLoader.getSelectedCountryId() <= 0) {
            builder.setMessage(R.string.alert_cn)
                    .create()
                    .show();
        } else if (mLocationDataLoader.getSelectedCityId() <= 0) {
            builder.setMessage(R.string.alert_ct)
                    .create()
                    .show();
        } else if (mGender.getCheckedRadioButtonId() <= 0) {
            builder.setMessage(R.string.alert_gender)
                    .create()
                    .show();
        } else if (mSelectedBirthDay == null || mSelectedBirthDay != null && mSelectedBirthDay.after(todayCalendar.getTime())) {
            builder.setMessage(R.string.alert_birth)
                    .create()
                    .show();
        } else {
            info.baseDataAdded = true;
            mOnProfileFillListener.onFillProfile(info);
        }
    }

    @Override
    public void pushInfo(View view, UserInfo userInfo) {

    }
}