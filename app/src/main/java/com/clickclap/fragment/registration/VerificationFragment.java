package com.clickclap.fragment.registration;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.amberfog.countryflags.PhoneView;
import com.clickclap.R;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.fragment.BaseFragment;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.VerifyCodeResponse;
import com.clickclap.rest.request.user.GetSmsCodeRequest;
import com.clickclap.rest.request.user.VerifyCodeRequest;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.util.PrefHelper;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by michael on 28.10.14.
 */
public class VerificationFragment extends BaseFragment implements TextView.OnEditorActionListener, PhoneView.EnterPhoneListener {
    private static final int SMS_TIMEOUT = 20000;
    private static final int SMS_NOT_RECEIVED = 1;

    private Button mResendBtn, mNextBtn;
    private TextView mCodeHintView;
    private PhoneView mPhoneField;
    private EditText mCodeField;
    private int mError = -1;
    private boolean isSmsReceived = false,
    //                    isCodeResponseOk = false,
    isTimeoutExpired = false;

    private TextView mSubtitle;

    private Activity mActivity;

    private OnVerifyListener mOnVerifyListener;

    SpiceManager mSpiceManager;

    ProgressDialog mLoader;

    public static VerificationFragment newInstance() {
        VerificationFragment f = new VerificationFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_verification);
        f.setArguments(args);
        return f;
    }

    @Override
    protected void initUI(View v, Bundle savedInstanceState) {
        mActivity = getActivity();

        //title
        Spanned info = Html.fromHtml(getString(R.string.coin_info2));
        mSubtitle = (TextView) v.findViewById(R.id.txt_vf2);
        mSubtitle.setText(info);
        //phone number field
        mPhoneField = (PhoneView) v.findViewById(R.id.field_phone);
        mPhoneField.setOnEnterPhoneListener(this);
        //error block
        mResendBtn = (Button) v.findViewById(R.id.btn_resend);
        mResendBtn.setOnClickListener(this);
        mCodeHintView = (TextView) v.findViewById(R.id.txt_code);
        mCodeField = (EditText) v.findViewById(R.id.field_code);
        mCodeField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mError > 0) {
                    if (s.length() == 4) {
                        mNextBtn.setEnabled(true);
                    } else {
                        mNextBtn.setEnabled(false);
                    }
                }
            }
        });
        mCodeField.setOnEditorActionListener(this);

        if (mError < 0) {
            mResendBtn.setVisibility(INVISIBLE);
            mResendBtn.setEnabled(false);
            mCodeHintView.setVisibility(GONE);
            mCodeField.setVisibility(GONE);
            mCodeField.setEnabled(false);
        }
        //next button
        mNextBtn = (Button) v.findViewById(R.id.btn_nav);
        mNextBtn.setOnClickListener(this);
        mNextBtn.setEnabled(false);
        //phone field setup
//        mPhoneField.addTextChangedListener(mPhoneTextListener);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnVerifyListener = (OnVerifyListener) activity;
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();

        mLoader = new ProgressDialog(activity);
        mLoader.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_resend:
            case R.id.btn_nav:
                handleInput();
                break;
        }
    }

    private void handleInput() {

        View view = getActivity().getCurrentFocus();

        if (view instanceof EditText) {
            if (view.getId() == R.id.field_code) {
                if (((EditText) view).getText().length() < 4) {
                    return;
                }
            } else if (view.getId() == R.id.field_phone) {
                if (((EditText) view).getText().length() < 10) {
                    return;
                }
            }
        }

        if (TextUtils.isEmpty(mCodeField.getText())) {
            onSend(mPhoneField.getPhoneNumber());
        } else {
            isTimeoutExpired = false;
            sendVerificationCode(mCodeField.getText());
            mCodeField.setText("");
        }
    }

    private void showResend() {
        if (!mLoader.isDismissed()) {
            mLoader.dismiss();
        }

        if (isAdded()) {
            mSubtitle.setTextColor(Color.RED);
            mSubtitle.setText(R.string.verify_error);
            mResendBtn.setVisibility(VISIBLE);
            mResendBtn.setEnabled(true);
            mCodeHintView.setVisibility(VISIBLE);
            mCodeField.setVisibility(VISIBLE);
            mCodeField.setEnabled(true);

            mNextBtn.setEnabled(false);
        }
    }

    public void requestCode(String phone) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.CODE_REQUEST);
        mSpiceManager.execute(
                new GetSmsCodeRequest(phone),
                new BaseRequestListener() {
                    @Override
                    public void onRequestSuccess(BaseResponse baseResponse) {
                        super.onRequestSuccess(baseResponse);
                        if (baseResponse != null && baseResponse.isSuccess()) {
                            mError = -1;
                        } else {
                            mError = SMS_NOT_RECEIVED;
                            showResend();
                            mLoader.dismiss();
                        }
                    }

                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        mError = SMS_NOT_RECEIVED;
                        showResend();
                        mLoader.dismiss();
                    }
                });

//            isCodeResponseOk = false;
        isSmsReceived = false;
        isTimeoutExpired = false;
        mLoader.setTitle(getString(R.string.getting_code));
        mLoader.show();
        waitForSms(System.currentTimeMillis());
    }

    public void sendVerificationCode(CharSequence code) {
        Log.e("IMON", "Sms received: " + System.currentTimeMillis());
        if (isTimeoutExpired) {
            return;
        }
        isSmsReceived = true;

        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.NUMBER_VERIFICATION);
        mSpiceManager.execute(
                new VerifyCodeRequest(code.toString()),
                new BaseRequestListener<VerifyCodeResponse>() {
                    @Override
                    public void onRequestSuccess(VerifyCodeResponse response) {
                        super.onRequestSuccess(response);
                        if (response != null && response.isSuccess()) {
                            if (response.getUserId() != 0) {
                                mError = -1;
                                PrefHelper.setStringPref(PREF_PHONE, mPhoneField.getPhoneNumber());
                                mOnVerifyListener.onVerify(response.getStatus() == 0);
                            }
                        } else {
                            mError = SMS_NOT_RECEIVED;
                        }
                        mLoader.dismiss();
                    }

                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        super.onRequestFailure(spiceException);
                        mLoader.dismiss();
                    }
                });

        mLoader.setTitle(getString(R.string.sending_code));
        mLoader.show();
    }

    private void waitForSms(final long startTime) {
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                while (true) {
                    if (System.currentTimeMillis() - startTime > SMS_TIMEOUT) {
                        isTimeoutExpired = true;
                        Log.e("IMON", "Timeout expired: " + System.currentTimeMillis());
                        return SMS_NOT_RECEIVED;
                    } else {
                        if (isSmsReceived) {
                            return -1;
                        }
                    }
                }
            }

            @Override
            protected void onPostExecute(Integer res) {
                mError = res;
                if (res > 0) {
                    //timeout expired, show message
                    showResend();
                }
            }
        }.execute();
    }

    @Override
    public void onSend(String phoneNumber) {
        requestCode(phoneNumber);
        //mNextBtn.setEnabled(mPinEntryView.getText().length() == 4);
    }

    @Override
    public void onNumberIsValid(boolean isValid) {
        mNextBtn.setEnabled(isValid);
    }

    public interface OnVerifyListener {
        void onVerify(boolean isNewUser);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        switch (actionId) {
            case EditorInfo.IME_ACTION_DONE:
            case EditorInfo.IME_ACTION_SEND:
                handleInput();
                handled = true;
                break;
        }
        return handled;
    }
}
