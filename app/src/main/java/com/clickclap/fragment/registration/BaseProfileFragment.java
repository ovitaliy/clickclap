package com.clickclap.fragment.registration;

import android.app.Activity;
import android.view.View;

import com.clickclap.fragment.BaseFragment;
import com.clickclap.model.UserInfo;

/**
 * Created by ovitali on 16.01.2015.
 */
public abstract class BaseProfileFragment extends BaseFragment {
    protected OnProfileFillListener mOnProfileFillListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnProfileFillListener = (OnProfileFillListener) activity;
    }

    public abstract void pushInfo(View view, UserInfo userInfo);

    public interface OnProfileFillListener {
        void onFillProfile(UserInfo userInfo);
    }

    protected void toggleVisibility(View view, int containerId) {
        view.setSelected(!view.isSelected());
        view.getRootView().findViewById(containerId).setVisibility(view.isSelected() ? View.VISIBLE : View.GONE);
    }
}
