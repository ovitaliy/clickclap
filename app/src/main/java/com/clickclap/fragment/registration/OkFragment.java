package com.clickclap.fragment.registration;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.dialogs.ShareDialogFragment;
import com.clickclap.fragment.BaseFragment;
import com.clickclap.rest.model.AddVideoResponse;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by michael on 27.10.14.
 */
public class OkFragment extends BaseFragment {

    private int mAction;

    private String mShareUrl;
    private int mVideoId;

    public static OkFragment newInstance(int action, String shareUrl, int videoId) {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_ok);
        args.putInt(EXTRA_ACTION, action);
        args.putString("shareUrl", shareUrl);
        args.putInt("videoId", videoId);
        fragment.setArguments(args);
        return fragment;
    }

    public static OkFragment newInstance(int action) {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_ok);
        args.putInt(EXTRA_ACTION, action);
        fragment.setArguments(args);
        return fragment;
    }

    private OnCongratulationListener mOpenVerificationListener;

    public OkFragment() {
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        mAction = getArguments().getInt(EXTRA_ACTION);

        mShareUrl = getArguments().getString("shareUrl", null);
        mVideoId = getArguments().getInt("videoId", 0);

        Spanned info1;
        TextView infoView1 = (TextView) view.findViewById(R.id.info_text);
        TextView infoView2 = (TextView) view.findViewById(R.id.info_text_2);
        switch (mAction) {
            case EMOTICON_OK:
                infoView1.setText("");
                infoView2.setText(getString(R.string.greeting_1));
                view.findViewById(R.id.btn_nav).setEnabled(false);
                break;
            case VIDEO_OK:
                infoView1.setVisibility(View.GONE);
                infoView2.setText(R.string.greeting_2);
                break;
            case VERIFY_OK:
                infoView1.setVisibility(View.GONE);
                infoView2.setText(R.string.coin_info3);
                break;
            case PROFILE_OK:
                info1 = Html.fromHtml(getString(R.string.plus50));
                infoView1.setText(info1);
                infoView2.setText(R.string.coin_info4);
                break;
        }
        view.findViewById(R.id.btn_nav).setOnClickListener(this);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    /*@Override
    public void onResume() {
        super.onResume();
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_nav:
                nextScreen();
                break;
        }
    }

    private void nextScreen() {
        switch (mAction) {
            case EMOTICON_OK:

                break;
            case VIDEO_OK:
                mOpenVerificationListener.onOpenVerification();
                break;
            case VERIFY_OK:
                mOpenVerificationListener.onOpenFillProfile();
                break;
            case PROFILE_OK:
                mOpenVerificationListener.onRegistrationProcedureComplete();
                break;
        }
    }

    private void showInfo() {
        //go to InfoFragment according to mAction value
        switch (mAction) {
            case EMOTICON_OK:
                break;
            case VIDEO_OK:
                break;
            case VERIFY_OK:
                break;
            case PROFILE_OK:
                break;
        }

    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AddVideoResponse addVideoResponse) {
        if (getView() != null) {
            getView().findViewById(R.id.btn_nav).setEnabled(true);

            new ShareDialogFragment.Builder()
                    .setCanBeClosed(false)
                    .setLink(addVideoResponse.getShortUrl())
                    .setVideoId(addVideoResponse.getVideoId())
                    .setShareType(ShareDialogFragment.TYPE_EMOTICON)
                    .build()
                    .show(getFragmentManager(), ShareDialogFragment.TAG);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOpenVerificationListener = (OnCongratulationListener) activity;
    }

    public interface OnCongratulationListener {
        void onOpenVerification();

        void onOpenFillProfile();

        void onRegistrationProcedureComplete();
    }

}
