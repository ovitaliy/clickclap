package com.clickclap.fragment.registration;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.enums.Hobby;
import com.clickclap.enums.Interest;
import com.clickclap.enums.Pet;
import com.clickclap.enums.Religion;
import com.clickclap.enums.Sport;
import com.clickclap.view.CirclePickerItem;
import com.clickclap.view.EditableCirclePickerItem;
import com.clickclap.model.UserInfo;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.Utils;
import com.clickclap.view.CirclePickerView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by michael on 03.11.14.
 */
public class ProfileAboutFragment extends BaseProfileFragment {

    private View mHobbyButton;
    private View mSportButton;
    private View mPetButton;
    private View mReligionButton;
    private View mInterestButton;
    private ScrollView mScrollView;

    public static ProfileAboutFragment newInstance() {
        ProfileAboutFragment f = new ProfileAboutFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_profile_about_you);
        f.setArguments(args);
        return f;
    }

    @Override
    protected void initUI(final View view, Bundle savedInstanceState) {
        initPickerFromResource(view, R.id.hobby_picker, new ArrayList<EditableCirclePickerItem>(Arrays.asList(Hobby.values())));
        initPickerFromResource(view, R.id.sport_picker, new ArrayList<EditableCirclePickerItem>(Arrays.asList(Sport.values())));
        initPickerFromResource(view, R.id.pet_picker, new ArrayList<EditableCirclePickerItem>(Arrays.asList(Pet.values())));
        initPickerFromResource(view, R.id.religion_picker, new ArrayList<EditableCirclePickerItem>(Arrays.asList(Religion.values())));
        initPickerFromResource(view, R.id.interest_picker, new ArrayList<EditableCirclePickerItem>(Arrays.asList(Interest.values())));

        mHobbyButton = view.findViewById(R.id.btn_hobbies);
        mHobbyButton.setOnClickListener(this);
        mSportButton = view.findViewById(R.id.btn_sport);
        mSportButton.setOnClickListener(this);
        mPetButton = view.findViewById(R.id.btn_pets);
        mPetButton.setOnClickListener(this);
        mInterestButton = view.findViewById(R.id.btn_ints);
        mInterestButton.setOnClickListener(this);
        mReligionButton = view.findViewById(R.id.btn_religion);
        mReligionButton.setOnClickListener(this);

        mScrollView = (ScrollView) view.findViewById(R.id.content);
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(ScrollView.FOCUS_UP);
            }
        });

        toggleVisibility(mHobbyButton, R.id.hobby_picker);
        toggleVisibility(mSportButton, R.id.sport_picker);
        toggleVisibility(mPetButton, R.id.pet_picker);
        toggleVisibility(mInterestButton, R.id.interest_picker);
        toggleVisibility(mReligionButton, R.id.religion_picker);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_hobbies:
                toggleVisibility(v, R.id.hobby_picker);
                break;
            case R.id.btn_sport:
                toggleVisibility(v, R.id.sport_picker);
                break;
            case R.id.btn_pets:
                toggleVisibility(v, R.id.pet_picker);
                break;
            case R.id.btn_ints:
                toggleVisibility(v, R.id.interest_picker);
                break;
            case R.id.btn_religion:
                toggleVisibility(v, R.id.religion_picker);
                break;
        }
    }

    @Override
    protected void toggleVisibility(View view, int containerId) {
        super.toggleVisibility(view, containerId);
    }

    @Override
    public void pushInfo(View view, UserInfo userInfo) {
    }

    public void initPickerFromResource(View parent, final int pickerId, ArrayList<EditableCirclePickerItem> source) {
        CirclePickerView picker = (CirclePickerView) parent.findViewById(pickerId);
        int margin = Utils.convertDpToPixel(10, getActivity());
        int widht = DisplayInfo.getScreenWidth(getActivity()) - (margin * 2);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(widht, widht);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(margin, margin, margin, margin);
        picker.setLayoutParams(params);

        int initElemId = -1;

        switch (pickerId) {
            case R.id.hobby_picker:
                Hobby hobby = AppUser.get().getHobbie();
                if (hobby != null) {
                    initElemId = getElementId(source, hobby.getTitle());
                }
                break;
            case R.id.sport_picker:
                Sport sport = AppUser.get().getSport();
                if (sport != null) {
                    initElemId = getElementId(source, sport.getTitle());
                }
                break;
            case R.id.pet_picker:
                Pet pet = AppUser.get().getPet();
                if (pet != null) {
                    initElemId = getElementId(source, pet.getTitle());
                }
                break;
            case R.id.religion_picker:
                Religion religion = AppUser.get().getReligion();
                if (religion != null) {
                    initElemId = getElementId(source, religion.getTitle());
                }
                break;
            case R.id.interest_picker:
                Interest interest = AppUser.get().getInterest();
                if (interest != null) {
                    initElemId = getElementId(source, interest.getTitle());
                }
                break;
        }

        picker.fill(source,
                initElemId,
                new CirclePickerView.OnPickListener() {
                    @Override
                    public void OnPick(CirclePickerItem element) {
                        switch (pickerId) {
                            case R.id.hobby_picker:
                                AppUser.get().setHobbie((Hobby) element);
                                break;
                            case R.id.sport_picker:
                                AppUser.get().setSport((Sport) element);
                                break;
                            case R.id.pet_picker:
                                AppUser.get().setPet((Pet) element);
                                break;
                            case R.id.religion_picker:
                                AppUser.get().setReligion((Religion) element);
                                break;
                            case R.id.interest_picker:
                                AppUser.get().setInterest((Interest) element);
                                break;
                        }
                    }
                });
    }

    private int getElementId(ArrayList<EditableCirclePickerItem> list, String value) {

        EditableCirclePickerItem otherOptionElement = null;
        for (int i = 0; i < list.size(); i++) {
            EditableCirclePickerItem element = list.get(i);
            String title = element.getTitle();
            if (title != null) {
                if (title.equalsIgnoreCase(value)) {
                    return element.getId();
                }
            }

            if (element.isOtherOption()) {
                otherOptionElement = element;
            }
        }

        //element not found
        if (otherOptionElement != null) {
            otherOptionElement.setTitle(value);
            return otherOptionElement.getId();
        }

        return -1;
    }
}
