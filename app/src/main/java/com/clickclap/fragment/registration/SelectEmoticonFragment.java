package com.clickclap.fragment.registration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.activities.AgreementActivity;
import com.clickclap.activities.BaseActivity;
import com.clickclap.activities.RegistrationActivity;
import com.clickclap.enums.Emotion;
import com.clickclap.fragment.BaseFragment;
import com.clickclap.view.CirclePickerItem;
import com.clickclap.view.CirclePickerView;

import java.util.ArrayList;
import java.util.Arrays;

public class SelectEmoticonFragment extends BaseFragment {

    private TextView mHeader1;
    private TextView mHeader2;

    private CirclePickerView mSmileContainer;
    private int mCurrentSmileId = -1;
    private OnSelectSmileListener mOnSelectSmileListener;

    public static SelectEmoticonFragment newInstance() {
        SelectEmoticonFragment fragment = new SelectEmoticonFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_start);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setHeaderText() {
        if (getActivity() instanceof RegistrationActivity) {
            mHeader1.setText(R.string.welcome1);
            mHeader2.setText(R.string.welcome2);
            mHeader2.setVisibility(View.VISIBLE);
        } else {
            mHeader1.setText(R.string.select_emotion);
            mHeader2.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initUI(View v, Bundle savedInstanceState) {
        mSmileContainer = (CirclePickerView) v.findViewById(R.id.smiles_picker);
        v.findViewById(R.id.btn_nav).setOnClickListener(this);
        v.findViewById(R.id.agreement).setOnClickListener(this);

        mHeader1 = (TextView) v.findViewById(R.id.header_1);
        mHeader2 = (TextView) v.findViewById(R.id.header_2);

        setHeaderText();

        if (!(getActivity() instanceof RegistrationActivity)) {
            v.findViewById(R.id.agreement).setVisibility(View.GONE);
        }

        loadSmilesFromStorage(v);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    private void openAgreement() {
        if (getActivity() instanceof BaseActivity) {
            Intent intent = new Intent(getActivity(), AgreementActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_nav:
                if (mCurrentSmileId > 0) {
                    mOnSelectSmileListener.onSmileSelected(mCurrentSmileId);
                }
                break;
            case R.id.agreement:
                openAgreement();
                break;
            default:
                getView().findViewById(R.id.btn_nav).setEnabled(true);
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        loadSmilesFromStorage(getView());
    }

    private void loadSmilesFromStorage(View v) {
        if (mSmileContainer == null || mSmileContainer.getChildCount() > 2) {
        } else {
            mLoader.setTitle(getString(R.string.smiles_load));
            mLoader.show();
            if (isAdded()) {
                mSmileContainer.fill(new ArrayList<CirclePickerItem>(Arrays.asList(Emotion.values())),
                        mCurrentSmileId,
                        new CirclePickerView.OnPickListener() {
                            @Override
                            public void OnPick(CirclePickerItem element) {
                                mCurrentSmileId = element.getId();
                                getView().findViewById(R.id.btn_nav).setEnabled(true);
                            }
                        });

                mLoader.dismiss();

                if (mCurrentSmileId > 0) {
                    if (v != null) {
                        v.findViewById(R.id.btn_nav).setEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnSelectSmileListener = (OnSelectSmileListener) activity;
    }

    public interface OnSelectSmileListener {
        void onSmileSelected(int smileId);
    }
}
