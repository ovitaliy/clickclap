package com.clickclap.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.clickclap.R;
import com.clickclap.activities.BaseActivity;
import com.clickclap.adapter.InfoAdapter;
import com.clickclap.view.TabView;

/**
 * Created by Denis on 03.03.2015.
 */
public class InfoFragment extends BaseFragment implements
        TabView.OnTabSelectListener {

    int mCurrentTab;
    private TabView mTabView;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private InfoAdapter mAdapter;

    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_info);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mTabView = (TabView) view.findViewById(R.id.tabhost);
        mTabView.setOnTabSelectListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.info_list);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new InfoAdapter(new String[]{});
        mRecyclerView.setAdapter(mAdapter);

//        if (AppUser.get() == null) {//TODO
//            mBackButton.setVisibility(View.GONE);
//        } else {
//            mBackButton.setOnClickListener(this);
//        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        ((BaseActivity) getActivity()).startFragment(new NavigationInfoFragment(), true, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        onTabSelected(null, mCurrentTab);
    }

    @Override
    public void onTabSelected(View view, int position) {
        mCurrentTab = position;

        String text;
        switch (position) {
            case 0:
                text = getString(R.string.terms_info);
                break;
            case 1:
                text = getString(R.string.privacy_info);
                break;
            default:
                text = getString(R.string.about_info);
                break;
        }

        new TextProcessorTask().execute(text);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_info);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    class TextProcessorTask extends AsyncTask<String, Void, String[]> {
        @Override
        protected String[] doInBackground(String... strings) {
            String text = strings[0];
            return text.split("<br><br>");
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            mAdapter.setData(result);
            mAdapter.notifyDataSetChanged();
        }
    }
}
