package com.clickclap.fragment;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.adapter.FeedbackMessagesAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.dialogs.DialogBuilder;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.model.Feedback;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.listener.MessagesRequestListener;
import com.clickclap.rest.model.AddMessageResponse;
import com.clickclap.rest.request.message.AddMessageRequest;
import com.clickclap.rest.request.message.GetMessagesRequest;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.util.NetworkUtil;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Владимир on 06.12.2014.
 */
public class FeedbackChatFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView mListView;
    private FeedbackMessagesAdapter mAdapter;
    private EditText mCommentText;
    private Button mSendButton;
    private ProgressDialog mProgressDialog;

    private int mFeedbackId;
    private int mFeedbackType;
    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    public static FeedbackChatFragment newInstance(int feedbackId, @Feedback.FeedbackType int type) {
        FeedbackChatFragment fragment = new FeedbackChatFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_feedback_messages);
        args.putInt("feedback_id", feedbackId);
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args.containsKey("feedback_id")) {
            mFeedbackId = args.getInt("feedback_id");
        }

        mFeedbackType = args.getInt("type");

        if (mFeedbackId > 0) {
            loadMessages();
        }
    }

    private void loadMessages() {
        getActivity().getSupportLoaderManager().restartLoader(3838, null, this);
        mSpiceManager.execute(
                new GetMessagesRequest(mFeedbackId),
                new MessagesRequestListener(mFeedbackId));
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mAdapter = new FeedbackMessagesAdapter(getActivity(), null);
        mListView = (ListView) view.findViewById(R.id.comments_list);

        mListView.setAdapter(mAdapter);

        mCommentText = (EditText) view.findViewById(R.id.comments_text);

        mSendButton = (Button) view.findViewById(R.id.comments_send);
        mSendButton.setOnClickListener(this);
    }


    /**
     * Set hint to mCommentText;
     *
     * @param count How many comments we have.
     */
    private void setHint(int count) {
        String hint;
        if (count == 0) {
            hint = App.getInstance().getResources().getString(R.string.no_comment);
        } else {
            hint = App.getInstance().getResources().getQuantityString(R.plurals.stream_displaying_comment_text, count, count);
        }
        mCommentText.setHint(hint);
    }

    private void showMessageAboutError(int message) {
        new DialogBuilder(getActivity())
                .setMessage(message)
                .create()
                .show();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mCommentText.getWindowToken(), 0);
    }

    private void sendComment(String message) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.FEEDBACK_SEND);

        if (!NetworkUtil.isOfflineMode()) {
            if (mProgressDialog == null || !mProgressDialog.isShowing()) {
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.show();
            }

            hideKeyboard();
            mCommentText.setText("");

            mSpiceManager.execute(
                    new AddMessageRequest(mFeedbackId, message, mFeedbackType),
                    new BaseRequestListener<AddMessageResponse>() {
                        @Override
                        public void onRequestSuccess(AddMessageResponse response) {
                            super.onRequestSuccess(response);
                            if (isAdded()) {
                                if (response != null && response.isSuccess()) {
                                    mFeedbackId = response.getListId();
                                    loadMessages();
                                }
                                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                    mProgressDialog.dismiss();
                                }
                            }
                        }
                    });
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.comments_send:
                String comment = mCommentText.getText().toString();
                if (!comment.isEmpty()) {
                    sendComment(comment);
                }
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK + " = " + mFeedbackId;
        return new CursorLoader(getActivity(), ContentDescriptor.FeedbackMessages.URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_feedback);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
