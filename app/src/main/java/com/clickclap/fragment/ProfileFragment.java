package com.clickclap.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.util.LongSparseArray;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.activities.BaseActivity;
import com.clickclap.activities.EditProfileActivity;
import com.clickclap.activities.TokenActivity;
import com.clickclap.adapter.GrimacesPagerAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Emotion;
import com.clickclap.model.Grimace;
import com.clickclap.model.UserInfo;
import com.clickclap.model.VideoStatistic;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.listener.GetGrimacesRequestListener;
import com.clickclap.rest.listener.UserInfoRequestListener;
import com.clickclap.rest.request.grimace.GetGrimacesRequest;
import com.clickclap.rest.request.user.FollowUnfollowUserRequest;
import com.clickclap.rest.request.user.GetUserInfoRequest;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.Utils;
import com.clickclap.view.CirclePickerItem;
import com.clickclap.view.CirclePickerView;
import com.clickclap.view.TabView;
import com.clickclap.view.items.StatisticItem;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ProfileFragment extends BaseFragment implements TabView.OnTabSelectListener, LoaderManager.LoaderCallbacks<Cursor> {
    private final static int LOADER_ID = 13234;

    private GrimacesPagerAdapter mPagerAdapter;

    private int mDefTab = 0;
    private ViewPager mViewPager;
    private LinearLayout mEmotionsContainer;
    private static final int COUNT_PER_PAGE = 3;
    private int mCellSize;

    private int mCurTab;

    private TextView mHeaderButton;

    SpiceManager mSpiceManager;

    public static ProfileFragment newInstance(int userId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(Params.USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileFragment newInstance(int userId, int defTab) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(Params.USER_ID, userId);
        args.putInt("def_tab", defTab);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getInt(Params.USER_ID);
            if (getArguments().containsKey("def_tab")) {
                mDefTab = getArguments().getInt("def_tab");
            }
        }
    }

    private int mUserId;

    private FrameLayout mContainer;
    private UserInfo mUserInfo;

    private TextView mLanguageView;

    private CheckedTextView mActionButton;

    TabView mTabView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_profile, container, false);

        mTabView = (TabView) v.findViewById(R.id.tabhost);
        mTabView.setOnTabSelectListener(this);

        mContainer = (FrameLayout) v.findViewById(R.id.container);

        if (mUserId == AppUser.getUid()) {
            mUserInfo = AppUser.get();
        }

        mActionButton = (CheckedTextView) v.findViewById(R.id.action);
        mActionButton.setOnClickListener(this);

        if (mUserInfo != null && mUserInfo.isMe()) {
            mActionButton.setBackgroundResource(R.drawable.btn_edit);
        } else {
            mActionButton.setBackgroundResource(R.drawable.friend_button);
        }

        mLanguageView = (TextView) v.findViewById(R.id.language);
        mCurTab = mDefTab;

        mHeaderButton = (TextView) ((BaseActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.header_button);
        mHeaderButton.setOnClickListener(this);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mUserId == AppUser.getUid()) {
            fillProfile(AppUser.get());
        }

        mHeaderButton.setVisibility(View.VISIBLE);

        loadUserInfo();

        mTabView.setActive(mCurTab);
    }

    @Override
    public void onPause() {
        super.onPause();
        mHeaderButton.setVisibility(View.GONE);
    }

    private void fillProfile(UserInfo userInfo) {
        fillProfile(getView(), userInfo);
    }

    private void fillProfile(View v, UserInfo userInfo) {
        if (isAdded()) {
            TextView textView;
            mUserInfo = userInfo;

            textView = (TextView) v.findViewById(R.id.first_name);
            if (textView != null) {
                textView.setText(userInfo.getFName());
            }

            textView = (TextView) v.findViewById(R.id.location);
            if (textView != null) {
                textView.setText(userInfo.getCountry() + ", " + userInfo.getCity());
            }

            if (userInfo.getPhoto() != null) {
                ImageView avatar = (ImageView) v.findViewById(R.id.avatar);
                if (avatar != null) {
                    ImageLoader.getInstance().displayImage(userInfo.getImageLoaderPhoto(), avatar);
                }
            }

            if (mActionButton != null) {
                if (mUserInfo.getRelationship() == 1) {
                    mActionButton.setChecked(true);
                }
            }

            if (mLanguageView != null) {
                mLanguageView.setText(userInfo.getLanguage().getTitle());
            }

            if (mHeaderButton != null) {
                if (!mUserInfo.isMe() && !TextUtils.isEmpty(mUserInfo.getPhone())) {
                    mHeaderButton.setText(null);
                    mHeaderButton.setBackground(App.getInstance().getResources().getDrawable(R.drawable.phone));
                } else if (mUserInfo.isMe()) {
                    mHeaderButton.setBackground(App.getInstance().getResources().getDrawable(R.drawable.ic_token_count));
                    mHeaderButton.setText(String.valueOf(mUserInfo.getToken()));
                }
            }
            if (mCurTab == 3) {
                showTokenTab();
            }
        }
    }

    private void loadGrimaces() {
        mSpiceManager.execute(
                new GetGrimacesRequest(Grimace.TYPE_USER, mUserId),
                new GetGrimacesRequestListener(Grimace.TYPE_USER));
    }

    private void showGrimaces() {
        if (isAdded()) {
            mContainer.removeAllViews();
            View grimacesGrid = getActivity().getLayoutInflater().inflate(R.layout.grimaces_grid, mContainer, false);
            mContainer.addView(grimacesGrid, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            mViewPager = (ViewPager) grimacesGrid.findViewById(R.id.grimaces_pager);
            mEmotionsContainer = (LinearLayout) grimacesGrid.findViewById(R.id.emotions_container);

            mCellSize = (int) ((float) DisplayInfo.getScreenWidth(getActivity()) / (COUNT_PER_PAGE + 1));
            mPagerAdapter = new GrimacesPagerAdapter(getFragmentManager(), mCellSize);
            mViewPager.setAdapter(mPagerAdapter);

            mEmotionsContainer.getLayoutParams().width = mCellSize;

            getActivity().getSupportLoaderManager().restartLoader(LOADER_ID, null, this);

            loadGrimaces();
        }
    }

    private void showTokenTab() {
        mContainer.removeAllViews();
        View tokenView = getActivity().getLayoutInflater().inflate(R.layout.token, mContainer, false);
        mContainer.addView(tokenView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    public void onTabSelected(View view, int position) {
        mContainer.removeAllViews();

        switch (position) {
            case 0:
                showGrimaces();
                break;
            case 1:
                showQuestionnaire();
                break;
            case 2:
                showStatistic();
                break;
            case 3:
                showTokenTab();
                break;
        }

        mCurTab = position;
    }

    public void showQuestionnaire() {
        ArrayList<CirclePickerItem> list = new ArrayList<>(5);

        int i = 1;
        if (mUserInfo != null) {
            if (mUserInfo.getSport() != null) {
                list.add(new StatisticItem(i++, mUserInfo.getSport()));
            }
            if (mUserInfo.getPet() != null) {
                list.add(new StatisticItem(i++, mUserInfo.getPet()));
            }
            if (mUserInfo.getHobbie() != null) {
                list.add(new StatisticItem(i++, mUserInfo.getHobbie()));
            }
            if (mUserInfo.getReligion() != null) {
                list.add(new StatisticItem(i++, mUserInfo.getReligion()));
            }
            if (mUserInfo.getInterest() != null) {
                list.add(new StatisticItem(i++, mUserInfo.getInterest()));
            }
            if (mUserInfo.getDegree() != null) {
                list.add(new StatisticItem(i++, mUserInfo.getDegree()));
            }
            if (mUserInfo.getJobType() != null) {
                list.add(new StatisticItem(i++, mUserInfo.getJobType()));
            }
        }

        CirclePickerView circlePickerView = new CirclePickerView(getActivity());
        circlePickerView.setBackgroundResource(R.drawable.circle_picker_bg);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        mContainer.addView(circlePickerView, params);
        circlePickerView.fill(list, -1, null);
    }

    public void showStatistic() {
        ArrayList<CirclePickerItem> list = new ArrayList<>(11);

        if (mUserInfo != null) {
            List<VideoStatistic> categories = mUserInfo.getCategoryStatisticList();
            if (categories != null) {
                int count = categories.size();
                LongSparseArray<VideoStatistic> map = new LongSparseArray();
                for (int i = 0; i < count; i++) {
                    VideoStatistic categoryStatistic = mUserInfo.getCategoryStatisticList().get(i);
                    if (categoryStatistic.getDuration() > 0 && categoryStatistic.getTitleResId() != 0 && categoryStatistic.getImageResId() != 0) {
                        VideoStatistic item = map.get(categoryStatistic.getTitleResId());
                        if (item == null) {
                            item = categoryStatistic;
                        } else {
                            item.setDuration(item.getDuration() + categoryStatistic.getDuration());
                        }

                        map.put(categoryStatistic.getTitleResId(), item);
                    }
                }

                for (int i = 0; i < map.size(); i++) {
                    long key = map.keyAt(i);
                    VideoStatistic val = map.get(key);
                    list.add(new StatisticItem(i,
                            getString(val.getTitleResId()) + "\n" + Utils.formatMillisToTime((long) (val.getDuration() * 1000)),
                            val.getImageResId()));
                }
            }
        }

        CirclePickerView circlePickerView = new CirclePickerView(getActivity());
        circlePickerView.setBackgroundResource(R.drawable.circle_picker_bg);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        mContainer.addView(circlePickerView, params);
        circlePickerView.fill(list, -1, null);
    }

    private void loadUserInfo() {
        mSpiceManager.execute(new GetUserInfoRequest(mUserId), new UserInfoRequestListener(mUserId, new UserInfoRequestListener.OnGetUserInfoListener() {
            @Override
            public void onGetUserInfo(UserInfo userInfo) {
                fillProfile(userInfo);
            }
        }));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                ContentDescriptor.Grimaces.URI,
                null,
                ContentDescriptor.Grimaces.Cols.TYPE + " = " + Grimace.TYPE_USER + " AND " + ContentDescriptor.Grimaces.Cols.ID_AUTHOR + " = " + mUserId,
                null,
                ContentDescriptor.Grimaces.Cols.ID_SMILE + " DESC");
    }

    private void addEmotionToContainer(int emotionId) {
        Context context = getActivity();
        if (context != null) {
            int margin = Utils.convertDpToPixel(5, context);
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mCellSize - (margin * 2), mCellSize - (margin * 2));
            params.setMargins(margin, margin, margin, margin);
            imageView.setLayoutParams(params);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setImageResource(Emotion.getById(emotionId).getResId(getActivity(), "drawable"));
            mEmotionsContainer.addView(imageView);
        }
    }

    private void loadCollection(Cursor data) {
        final ArrayList<LinkedHashMap<Integer, Grimace[]>> pages = new ArrayList<>();
        mEmotionsContainer.removeAllViews();
        final ArrayList<Integer> emotions = new ArrayList<>();
        final ArrayList<Grimace> list = new ArrayList<>();
        if (data != null && data.moveToFirst()) {
            do {
                Grimace grimace = Grimace.fromCursor(data);
                list.add(grimace);
                int smileId = grimace.getSmileId();
                if (!emotions.contains(smileId)) {
                    emotions.add(smileId);
                    addEmotionToContainer(smileId);
                }
            } while (data.moveToNext());
        }

        mEmotionsContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (mEmotionsContainer.getHeight() > 0) {
                    mViewPager.getLayoutParams().height = mEmotionsContainer.getHeight();

                    int positionOnCurPage = 0;
                    int prevSmileId = 0;
                    int pageNum = 0;
                    for (Grimace grimace : list) {
                        int curSmileId = grimace.getSmileId();
                        if (curSmileId != prevSmileId) {
                            pageNum = 0;
                            positionOnCurPage = 0;
                        } else if (positionOnCurPage >= COUNT_PER_PAGE) {
                            pageNum++;
                            positionOnCurPage = 0;
                        }

                        if (pageNum > pages.size() - 1) {
                            LinkedHashMap<Integer, Grimace[]> page = new LinkedHashMap<>();
                            for (int emotion : emotions) {
                                page.put(emotion, new Grimace[COUNT_PER_PAGE]);
                            }
                            positionOnCurPage = 0;
                            pages.add(page);
                        }

                        LinkedHashMap<Integer, Grimace[]> page = pages.get(pageNum);

                        Grimace[] grimaces = page.get(curSmileId);
                        grimaces[positionOnCurPage] = grimace;

                        prevSmileId = curSmileId;
                        positionOnCurPage++;
                    }

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        mEmotionsContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mEmotionsContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }

                    if (!mPagerAdapter.isNewDataIdentical(pages)) {
                        int type;
                        if (AppUser.getUid() == mUserId) {
                            type = Grimace.TYPE_COLLECTION;
                        } else {
                            type = Grimace.TYPE_MARKET;
                        }
                        mPagerAdapter.setData(pages, type);
                    }
                }
            }
        });
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        loadCollection(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void onActionButtonClick() {
        if (mUserInfo != null) {
            if (mUserInfo.isMe()) {
                EditProfileActivity.startNewInstance(getActivity());
            } else {
                mUserInfo.setRelationship(mUserInfo.getRelationship() == 1 ? 0 : 1);
                mActionButton.setChecked(mUserInfo.getRelationship() == 1);
                mSpiceManager.execute(new FollowUnfollowUserRequest(mUserInfo.getId(), mUserInfo.getRelationship() == 1),
                        new BaseRequestListener());
            }
        }
    }

    private String normalizePhone(String phone) {
        if (!phone.contains("+")) {
            phone = "+" + phone;
        }
        return phone;
    }

    private void makeCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + normalizePhone(mUserInfo.getPhone())));
        startActivity(intent);
    }

    private void openTokenScreen() {
        startActivity(TokenActivity.newIntent(getActivity()));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.action:
                onActionButtonClick();
                break;
            case R.id.header_button:
                if (mUserInfo.isMe()) {
                    openTokenScreen();
                } else {
                    makeCall();
                }
                break;
        }
    }

    @Override
    public String getTitle() {
        if (mUserId == AppUser.getUid()) {
//            return getString(R.string.search_tab_profile);
        }
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}