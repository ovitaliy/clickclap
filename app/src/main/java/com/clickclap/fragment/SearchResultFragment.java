package com.clickclap.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.clickclap.R;
import com.clickclap.activities.BaseActivity;
import com.clickclap.adapter.SearchResultAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.UserFinded;

/**
 * Created by Deni on 23.06.2015.
 */
public class SearchResultFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener {
    ListView mListView;
    SearchResultAdapter mAdapter;

    public static SearchResultFragment newInstance() {
        SearchResultFragment fragment = new SearchResultFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);
        mListView = (ListView) view.findViewById(R.id.result);
        mListView.setOnItemClickListener(this);
        mAdapter = new SearchResultAdapter(getActivity(), null, 0);
        mListView.setAdapter(mAdapter);
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ContentDescriptor.Users.Cols.RECORD_TYPE +
                " = " + ContentDescriptor.Users.RECORD_TYPE_SEARCH;
        return new CursorLoader(getActivity(), ContentDescriptor.Users.URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserFinded user = UserFinded.fromCursor((Cursor) mAdapter.getItem(position));
        ((BaseActivity) getActivity()).startFragment(ProfileFragment.newInstance(user.getId()), true);
    }

    @Override
    public String getTitle() {
        return getString(R.string.action_search);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
