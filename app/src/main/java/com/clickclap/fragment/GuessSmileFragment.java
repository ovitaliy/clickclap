package com.clickclap.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.adapter.SmileListAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.dialogs.DialogBuilder;
import com.clickclap.enums.Emotion;
import com.clickclap.events.LoadedFileEvent;
import com.clickclap.jobs.VideoPreloadRunnable;
import com.clickclap.model.Video;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.request.video.GuessVideoSmileRequest;
import com.clickclap.util.BackgroundCutter;
import com.clickclap.util.DisplayInfo;
import com.clickclap.view.CircleLayout;
import com.clickclap.view.square.SquareRelativeLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Владимир on 25.11.2014.
 */
public class GuessSmileFragment extends BaseFragment implements CircleLayout.OnItemSelectedListener {

    private Button mNextButton;

    private SmileListAdapter mAdapter;

    private ImageButton mStartVideoButton;

    ImageView mPreviewImage;
    CircleLayout mCircleLayout;

    private Video mVideo;
    private Emotion mSelectedSmile;

    protected MediaPlayer mediaPlayer;
    private SurfaceView mSurfaceView;

    private VideoPreloadRunnable mVideoPreloadTask;

    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    public static GuessSmileFragment newInstance(Video video) {
        GuessSmileFragment fragment = new GuessSmileFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_guess_smile);
        args.putSerializable("video", video);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);

        mVideo = (Video) getArguments().getSerializable("video");

        mPreviewImage = (ImageView) view.findViewById(R.id.preview_image);
        mCircleLayout = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mCircleLayout.setOnItemSelectedListener(this);

        mNextButton = (Button) view.findViewById(R.id.guess_smiles_button_next);
        mNextButton.setOnClickListener(this);

        mStartVideoButton = (ImageButton) view.findViewById(R.id.video_displaying_start_button);
        mStartVideoButton.setOnClickListener(this);

        mSurfaceView = (SurfaceView) view.findViewById(R.id.video_displaying_surface_view);

        //init smiles mVoteList
        mAdapter = new SmileListAdapter(getActivity(), createSmileArray());
        mCircleLayout.setAdapter(mAdapter);
        mCircleLayout.setSelectedItem(2);

        ImageLoader.getInstance().displayImage(mVideo.getThumbUrl(), mPreviewImage);

        initBackgroundImage(view);
    }

    public void initBackgroundImage(final View view) {
        DisplayInfo displayInfo = new DisplayInfo(getActivity());
        final int mScreenWidth = displayInfo.getScreenWidth();
        final int mScreenHeight = displayInfo.getContentHeight();

        mStartVideoButton = (ImageButton) view.findViewById(R.id.video_displaying_start_button);
        mStartVideoButton.setOnClickListener(this);

        mSurfaceView = (SurfaceView) view.findViewById(R.id.video_displaying_surface_view);

        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                SquareRelativeLayout squareRelativeLayout = (SquareRelativeLayout) view.findViewById(R.id.squareMarker);
                squareRelativeLayout.getLayoutParams().width = App.WIDTH_WITHOUT_MARGINS;
                squareRelativeLayout.getLayoutParams().height = App.WIDTH_WITHOUT_MARGINS;
                final BackgroundCutter cutter = new BackgroundCutter();
                Bitmap background = cutter.cutBitmapColor(R.color.screen_background, mScreenWidth, mScreenWidth, squareRelativeLayout);
                if (background == null) {
                    view.postDelayed(this, 100);
                } else {
                    squareRelativeLayout.setBackgroundDrawable(new BitmapDrawable(App.getInstance().getResources(), background));
                }

            }
        }, 100);
        //setting position to progress bar
    }

    @Override
    public void onItemSelected(Object data) {
        mSelectedSmile = (Emotion) data;
    }

    /**
     * Create smiles and init them.
     */
    private ArrayList<Emotion> createSmileArray() {
        Emotion videoEmotion = null;
        Emotion[] emotions = Emotion.values();

        ArrayList<Emotion> emotionList = new ArrayList<>(emotions.length);
        Collections.addAll(emotionList, emotions);

        for (int i = emotionList.size() - 1; i >= 0; i--) {
            Emotion emotion = emotionList.get(i);
            if (emotion.getId() == mVideo.getSmileId()) {
                videoEmotion = emotion;
                emotionList.remove(i);
            }
        }

        Collections.shuffle(emotionList);
        while (emotionList.size() > 4) {
            emotionList.remove(3);
        }
        emotionList.add(videoEmotion);
        Collections.shuffle(emotionList);

        return emotionList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.guess_smiles_button_next:
                checkChosenSmile();
                break;
            case R.id.video_displaying_start_button:
                loadVideo();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void loadVideo() {
        mStartVideoButton.setVisibility(View.GONE);
        new Thread(new VideoPreloadRunnable(mVideo.getMedia())).start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadedFileEvent(LoadedFileEvent event) {
        if (isAdded()) {
            play(event.getPath());
        }
    }

    private void checkChosenSmile() {
        Emotion smile = mSelectedSmile;
        if (smile.getId() == mVideo.getSmileId()) {
            showMessageAboutWin();
        } else {
            showMessageAboutFail();
        }

        mSpiceManager.execute(new GuessVideoSmileRequest(mVideo.getVideoId(), mSelectedSmile.getId()), new BaseRequestListener() {
            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                super.onRequestSuccess(baseResponse);
            }
        });
    }

    private void showMessageAboutWin() {
        ContentValues values = new ContentValues(1);
        values.put(ContentDescriptor.Videos.Cols.GUESSED, 1);
        getActivity().getContentResolver().update(ContentDescriptor.Videos.URI,
                values,
                ContentDescriptor.Videos.Cols.ID + " = " + mVideo.getVideoId(),
                null);

        new DialogBuilder(getActivity())
                .setMessage(R.string.guess_smile_win)
                .setPositiveButton(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().onBackPressed();

                    }
                })
                .create()
                .show();
    }

    private void showMessageAboutFail() {
        new DialogBuilder(getActivity())
                .setMessage(R.string.guess_smile_fail)
                .setNegativeButton(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().onBackPressed();
                    }
                })
                .create()
                .show();
    }

    public void play(String file) {
        if (getView() == null)
            return;

        releaseMediaPlayer();

        mediaPlayer = new MediaPlayer();
        mStartVideoButton.setVisibility(View.INVISIBLE);
        mPreviewImage.setVisibility(View.INVISIBLE);

        try {
            mediaPlayer.setDataSource(file);
            mediaPlayer.prepare();
            mediaPlayer.setDisplay(mSurfaceView.getHolder());
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    releaseMediaPlayer();
                    mStartVideoButton.setVisibility(View.VISIBLE);
                    mPreviewImage.setVisibility(View.VISIBLE);
                }
            });
            mediaPlayer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public String getTitle() {
        return getActivity().getString(R.string.guess_emotion);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
