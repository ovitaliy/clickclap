package com.clickclap.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.activities.BaseActivity;
import com.clickclap.view.CirclePickerItem;
import com.clickclap.view.CirclePickerView;
import com.clickclap.view.NavigateView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Deni on 18.08.2015.
 */
public class NavigationMarketplaceFragment extends BaseFragment implements NavigateView<NavigationMarketplaceFragment.SubItem> {
    CirclePickerView mPicker;
    SubItem mSubItem;
    Button mNext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);
        mPicker = (CirclePickerView) view.findViewById(R.id.picker);
        mNext = (Button) view.findViewById(R.id.btn_nav);
        mNext.setOnClickListener(this);

        mPicker.fill(getSubMenuItems(),
                -1,
                new CirclePickerView.OnPickListener() {
                    @Override
                    public void OnPick(CirclePickerItem element) {
                        setSelectedSubMenuItem((SubItem) element);
                    }
                });

        updateNextButtonEnabledStatus();
        return view;
    }

    private void openCrowd() {
        ((BaseActivity) getActivity()).startFragment(CrowdFragment.newInstance(), true);
    }

    private void openMonCoin() {
        if (AppUser.get() != null) {
            ((BaseActivity) getActivity()).startFragment(MonCoinFragment.newInstance(), true);
        }
    }

    private void openRating() {
        if (AppUser.get() != null) {
            ((BaseActivity) getActivity()).startFragment(RatingFragment.newInstance(), true);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_nav:
                navigateToSubMenuItem(getSelectedSubMenuItem());
                break;
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_marketplace);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }

    @Override
    public void navigateToSubMenuItem(SubItem item) {
        if (item != null) {
            switch (item) {
                case CROWD:
                    openCrowd();
                    break;
                case MON_COIN:
                    openMonCoin();
                    break;
                case RATING:
                    openRating();
                    break;
            }
        }
    }

    @Override
    public void navigateBack() {
        //unused
    }

    @Override
    public void setSelectedSubMenuItem(SubItem item) {
        mSubItem = item;
        updateNextButtonEnabledStatus();
    }

    @Override
    public void updateNextButtonEnabledStatus() {
        mNext.post(new Runnable() {
            @Override
            public void run() {
                if (getSelectedSubMenuItem() == null) {
                    mNext.setEnabled(false);
                } else {
                    mNext.setEnabled(true);
                }
            }
        });
    }

    @Override
    public SubItem getSelectedSubMenuItem() {
        return mSubItem;
    }

    @Override
    public ArrayList<CirclePickerItem> getSubMenuItems() {
        return new ArrayList<CirclePickerItem>(Arrays.asList(SubItem.values()));
    }

    public enum SubItem implements CirclePickerItem {
        CROWD {
            @Override
            public Drawable getDrawable() {
                return ContextCompat.getDrawable(App.getInstance(), R.drawable.crowd);
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.menu_crowd);
            }
        },
        MON_COIN {
            @Override
            public Drawable getDrawable() {
                return ContextCompat.getDrawable(App.getInstance(), R.drawable.moncoin);
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.menu_mon_coin);
            }
        }, RATING {
            @Override
            public Drawable getDrawable() {
                return ContextCompat.getDrawable(App.getInstance(), R.drawable.rating);
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.rating);
            }
        };

        @Override
        public int getId() {
            return ordinal() + 1;
        }
    }
}
