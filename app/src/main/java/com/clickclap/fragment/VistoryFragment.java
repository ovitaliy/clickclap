package com.clickclap.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;

import com.clickclap.R;
import com.clickclap.activities.MainActivity;
import com.clickclap.activities.NewVideoActivity;
import com.clickclap.adapter.CategoryListAdapter;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.VideoType;
import com.clickclap.model.Category;
import com.clickclap.model.SimpleListItem;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.CategoriesRequestListener;
import com.clickclap.rest.request.GetCategoriesRequest;
import com.clickclap.util.FilePathHelper;
import com.clickclap.view.CategorizedFlowsView;
import com.clickclap.view.CircleLayout;
import com.clickclap.view.TabView;
import com.clickclap.view.VideoPreviewView;
import com.octo.android.robospice.SpiceManager;

import java.io.File;
import java.util.ArrayList;

public class VistoryFragment extends BaseFragment implements TabView.OnTabSelectListener,
        CircleLayout.OnItemSelectedListener,
        LoaderManager.LoaderCallbacks<Cursor>,
        View.OnClickListener, CategorizedFlowsView {

    private static final int CATEGORIES_LOADER_ID = 1;

    private VideoPreviewView mPreviewView;
    private CircleLayout mCircleLayout;

    private SimpleListItem mCurrentItem;

    private Tab mCurrentTab = Tab.FLOWS;

    private CategoryListAdapter mCategoriesAdapter;

    TabView mTabView;

    SpiceManager mSpiceManager;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int type = args.getInt(Params.TYPE);
        String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + type
                + " AND " + ContentDescriptor.Categories.Cols.VIDEO_COUNT + " > 0";
        return new CursorLoader(getActivity(),
                ContentDescriptor.Categories.URI,
                null,
                selection,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            ArrayList<Category> list = new ArrayList<>();
            if (data.moveToFirst()) {
                do {
                    list.add(Category.fromCursor(data));
                } while (data.moveToNext());
            }

            list = Category.reorder(list);

            if (isAdded()) {
                if (!mCategoriesAdapter.isNewDataIdentical(list)) {
                    mCategoriesAdapter.setData(list);
                    mPreviewView.setVisibility(View.VISIBLE);
                    mCircleLayout.setAdapter(mCategoriesAdapter);
                    mCircleLayout.setSelectedItem(mCategoriesAdapter.getInitialSelectionPosition());
                    mCircleLayout.invalidateAll();
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public enum Tab {
        FLOWS, VLOG, MY_CHOICE;
    }

    public static VistoryFragment newInstance() {
        VistoryFragment f = new VistoryFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_vistory);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentTab = (Tab) savedInstanceState.getSerializable("tab");
            mCurrentItem = (SimpleListItem) savedInstanceState.getSerializable("item");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    protected void initUI(final View view, Bundle savedInstanceState) {
        view.findViewById(R.id.preview_image).setOnClickListener(this);
        view.findViewById(R.id.new_video_shot).setOnClickListener(this);

        mPreviewView = (VideoPreviewView) view.findViewById(R.id.video_preview);
        mCircleLayout = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mCircleLayout.setOnItemSelectedListener(this);

        mTabView = (TabView) view.findViewById(R.id.tabhost);
        mTabView.setOnTabSelectListener(this);

        mCategoriesAdapter = new CategoryListAdapter(getActivity());
    }

    @Override
    public void openFlowList() {
        if (mCurrentItem != null) {
            FlowListFragment.Builder builder = new FlowListFragment.Builder(getVideoType());
            builder.setParams(((Category) mCurrentItem).getParams());

            String title = null;
            switch (mCurrentTab) {
                case VLOG:
                    title = getString(R.string.vistory_tab_vlog);
                    break;
                case MY_CHOICE:
                    title = getString(R.string.vistory_tab_my_choice);
                    break;
                case FLOWS:
                    title = getString(R.string.vistory_tab_news);
                    break;
            }
            builder.setTitle(title);

            ((MainActivity) getActivity()).startFragment(builder.build(), true);
        }
    }

    @Override
    public Category getCurrentCategory() {
        return (Category) mCurrentItem;
    }

    private void clearVideoCache() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File cacheDirectory = FilePathHelper.getVideoCacheDirectory();
                if (cacheDirectory.exists()) {
                    File[] files = cacheDirectory.listFiles();
                    if (files != null) { //some JVMs return null for empty dirs
                        for (File f : files) {
                            if (!f.isDirectory()) {
                                boolean deleted = f.delete();
                                Log.i("Cache clean", f.getAbsolutePath() + (deleted ? "success" : "failed"));
                            }
                        }
                    }
                }
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        clearVideoCache();
        mTabView.setActive(mCurrentTab.ordinal());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.preview_image:
                openFlowList();
                break;
            case R.id.new_video_shot:
                switch (mCurrentTab) {
                    case FLOWS:
                    case MY_CHOICE:
                        NewVideoActivity.startNewInstance(getActivity(), VideoType.FLOW_ALL);
                        break;
                    case VLOG:
                        NewVideoActivity.startNewInstance(getActivity(), VideoType.VLOG);
                        break;
                }
                break;
        }
    }

    @Override
    public void onItemSelected(Object data) {
        mCurrentItem = (SimpleListItem) data;
        if (data != null) {
            mPreviewView.show(((SimpleListItem) data).getImage());
        } else {
            mPreviewView.hide();
        }
    }

    @Override
    public VideoType getVideoType() {
        VideoType type;
        switch (mCurrentTab) {
            case VLOG:
                type = VideoType.VLOG;
                break;
            case MY_CHOICE:
                type = VideoType.FLOW_MY_CHOICE;
                break;
            default:
                type = VideoType.FLOW_ALL;
                break;
        }
        return type;
    }

    @Override
    public void loadCategories(VideoType type) {
        mSpiceManager.execute(
                new GetCategoriesRequest(type),
                new CategoriesRequestListener(type));
    }

    @Override
    public void onTabSelected(View view, int position) {
        mCurrentTab = Tab.values()[position];
        if (mCategoriesAdapter != null) {
            mCircleLayout.clear();
            mPreviewView.setVisibility(View.GONE);
        }

        if (mCategoriesAdapter != null) {
            mCategoriesAdapter.clear();
        }

        mPreviewView.hide();

        VideoType type = getVideoType();
        Bundle args = new Bundle(1);
        args.putInt(Params.TYPE, type.getId());
        loadCategories(type);
        getActivity().getSupportLoaderManager().restartLoader(CATEGORIES_LOADER_ID, args, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("item", mCurrentItem);
        outState.putSerializable("tab", mCurrentTab);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_vistory);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }
}