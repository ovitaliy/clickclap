package com.clickclap.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.activities.ContactsActivity;
import com.clickclap.activities.SearchResultActivity;
import com.clickclap.adapter.GrimaceFilterListAdapter;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.enums.Emotion;
import com.clickclap.listener.OnContactSelectListener;
import com.clickclap.media.filter.grimace.AnimeFilter;
import com.clickclap.media.filter.grimace.CheFilter;
import com.clickclap.media.filter.grimace.ComicFilter;
import com.clickclap.media.filter.grimace.OilFilter;
import com.clickclap.media.filter.grimace.PixelFilter;
import com.clickclap.media.filter.grimace.PopArtFilter;
import com.clickclap.media.filter.grimace.base.BaseGrimaceFilter;
import com.clickclap.model.Filter;
import com.clickclap.model.FriendItem;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.UserSearchRequestListener;
import com.clickclap.rest.model.UserSearchResponse;
import com.clickclap.rest.request.user.SearchUserRequest;
import com.clickclap.util.BitmapDecoder;
import com.clickclap.util.BitmapLruCache;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.ImageCroper;
import com.clickclap.util.Utils;
import com.clickclap.view.CircleImageView;
import com.clickclap.view.CircleLayout;
import com.clickclap.view.CirclePickerItem;
import com.clickclap.view.CirclePickerView;
import com.octo.android.robospice.SpiceManager;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Deni on 23.06.2015.
 */
public class SearchGrimaceFragment extends BaseFragment implements OnContactSelectListener, View.OnClickListener, CircleLayout.OnItemSelectedListener {
    private SparseArray<TextView> mRelativesViews;
    private CirclePickerView mSmileContainer;

    private ArrayList<BaseGrimaceFilter> mImageFilters = new ArrayList<>();
    private ArrayList<Filter> mList;

    private Filter mSelectedFilter;

    private BitmapLruCache mBitmapsCache;

    protected CircleImageView mImageView;

    private int mBorderSize;

    String mSourceImagePath;

    protected CircleLayout mListView;

    private static int EMPTY;

    protected Bitmap mImage;

    private GrimaceFilterListAdapter mAdapter;

    int mCurrentSmileId = -1;
    int mCurrentFilterId = -1;

    SpiceManager mSpiceManager;

    private ProgressDialog mProgressDialog;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_grimace, container, false);
        mBitmapsCache = BitmapLruCache.newInstance(BitmapLruCache.Size.SMALL);

        TextView relative1 = (TextView) view.findViewById(R.id.relative_1);
        TextView relative2 = (TextView) view.findViewById(R.id.relative_2);
        TextView relative3 = (TextView) view.findViewById(R.id.relative_3);
        TextView relative4 = (TextView) view.findViewById(R.id.relative_4);
        TextView relative5 = (TextView) view.findViewById(R.id.relative_5);

        mRelativesViews = new SparseArray<>(5);
        mRelativesViews.put(relative1.getId(), relative1);
        mRelativesViews.put(relative2.getId(), relative2);
        mRelativesViews.put(relative3.getId(), relative3);
        mRelativesViews.put(relative4.getId(), relative4);
        mRelativesViews.put(relative5.getId(), relative5);

        relative1.setOnClickListener(this);
        relative2.setOnClickListener(this);
        relative3.setOnClickListener(this);
        relative4.setOnClickListener(this);
        relative5.setOnClickListener(this);

        relative1.setOnTouchListener(mOnTouchRelativeClear);
        relative2.setOnTouchListener(mOnTouchRelativeClear);
        relative3.setOnTouchListener(mOnTouchRelativeClear);
        relative4.setOnTouchListener(mOnTouchRelativeClear);
        relative5.setOnTouchListener(mOnTouchRelativeClear);

        mSmileContainer = (CirclePickerView) view.findViewById(R.id.smiles_picker);

        mImageView = (CircleImageView) view.findViewById(R.id.video_displaying_image_view);


        int size = DisplayInfo.getScreenWidth(getActivity()) - Utils.convertDpToPixel(20, getActivity());
        mSmileContainer.getLayoutParams().width = size;
        mSmileContainer.getLayoutParams().height = size;

        View squareMakerView = view.findViewById(R.id.squareMarker);
        squareMakerView.getLayoutParams().width = size;
        squareMakerView.getLayoutParams().height = size;

        mListView = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mListView.setOnItemSelectedListener(this);

        mAdapter = new GrimaceFilterListAdapter(getActivity());

        mBorderSize = getResources().getDimensionPixelSize(R.dimen.stream_item_small_border);

        mImage = ((BitmapDrawable) ContextCompat.getDrawable(App.getInstance(), R.drawable.beach_big)).getBitmap();

        mSourceImagePath = getActivity().getExternalCacheDir().getPath() + "/beach.png";
        FileOutputStream outStream;
        if (!new File(mSourceImagePath).exists()) {
            try {
                outStream = new FileOutputStream(new File(mSourceImagePath));
                mImage.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        view.findViewById(R.id.search).setOnClickListener(this);
        loadSmilesFromStorage();
        initFilterList();
        return view;
    }

    private void loadSmilesFromStorage() {
        if (mSmileContainer == null || mSmileContainer.getChildCount() > 2) {
        } else {
            mSmileContainer.fill(new ArrayList<CirclePickerItem>(Arrays.asList(Emotion.values())),
                    mCurrentSmileId,
                    new CirclePickerView.OnPickListener() {
                        @Override
                        public void OnPick(CirclePickerItem element) {
                            mCurrentSmileId = element.getId();
                        }
                    });
        }
    }

    private void initFilterList() {
        final PixelFilter pixel = new PixelFilter();
        final CheFilter che = new CheFilter();
        final OilFilter oil = new OilFilter();
        final ComicFilter comic = new ComicFilter();
        final AnimeFilter anime = new AnimeFilter();
        final PopArtFilter popArt = new PopArtFilter();

        mImageFilters.add(pixel);
        mImageFilters.add(che);
        mImageFilters.add(oil);
        mImageFilters.add(comic);
        mImageFilters.add(anime);
        mImageFilters.add(popArt);
        EMPTY = mImageFilters.size() / 2;
        mImageFilters.add(EMPTY, null);

        EMPTY = mImageFilters.size() / 2;

        new ScaleImageTask() {
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                /*mList = new ArrayList<>();
                for (BaseGrimaceFilter imageFilter : mImageFilters) {
                    Filter filter = new Filter();
                    filter.setImagePath(mSourceImagePath);
                    filter.setFilter(imageFilter);
                    mList.add(filter);

                    if (imageFilter == null) {
                        addBitmapToCache(filter, BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.beach_big));
                    } else {
                        if (imageFilter.equals(pixel)) {
                            addBitmapToCache(filter, BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.filter_pixel));
                        } else if (imageFilter.equals(che)) {
                            addBitmapToCache(filter, BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.filter_che));
                        } else if (imageFilter.equals(oil)) {
                            addBitmapToCache(filter, BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.filter_oil));
                        } else if (imageFilter.equals(comic)) {
                            addBitmapToCache(filter, BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.filer_comic));
                        } else if (imageFilter.equals(anime)) {
                            addBitmapToCache(filter, BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.filter_anime));
                        } else if (imageFilter.equals(popArt)) {
                            addBitmapToCache(filter, BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.filter_popart));
                        }
                    }
                }
                mAdapter.setData(mList);
                mListView.setAdapter(mAdapter);
                mListView.setSelectedItem(mAdapter.getInitialSelectionPosition());
                onItemSelected(mList.get(mAdapter.getInitialSelectionPosition()));
                */
            }
        }.executeOnExecutor(ScaleImageTask.THREAD_POOL_EXECUTOR);

    }

    private Bitmap getBitmapFromCache(Filter filter) {
        String key = String.valueOf(filter.hashCode());
        Log.d("GrimaceCache", key + " requested (" + filter.toString() + ")");
        return mBitmapsCache.get(key);
    }

    private void addBitmapToCache(Filter filter, Bitmap bitmap) {
        String key = String.valueOf(filter.hashCode());
        mBitmapsCache.put(key, bitmap);
        Log.d("GrimaceCache", key + " added (" + filter.toString() + ")");
    }

    private Bitmap getFilteredBitmap(Filter filter) {
        Bitmap outputBitmap = getBitmapFromCache(mSelectedFilter);

        if (outputBitmap == null) {
            final BaseGrimaceFilter grimaceFilter = (BaseGrimaceFilter) filter.getFilter();

            Bitmap filteredBitmap;
            if (grimaceFilter != null) {
                filteredBitmap = grimaceFilter.apply(mImage);
            } else {
                filteredBitmap = mImage;
            }

            outputBitmap = ImageCroper.getCircularBitmap(filteredBitmap, mBorderSize, App.getInstance().getResources().getColor(R.color.yellow));
            addBitmapToCache(filter, outputBitmap);

        }
        return outputBitmap;
    }

    @Override
    public void onItemSelected(Object data) {

        mSelectedFilter = (Filter) data;

        if (mSelectedFilter.getFilter() != null) {
            mCurrentFilterId = ((BaseGrimaceFilter) mSelectedFilter.getFilter()).getId();
        }

        final Bitmap bitmap = getFilteredBitmap(mSelectedFilter);
        if (bitmap != null) {
            mImageView.setImageBitmapWithoutRounding(bitmap);
        }
    }

    private void updateRelatives() {
        ArrayList<FriendItem> friends = new ArrayList<>();

        for (int i = 0; i < mRelativesViews.size(); i++) {
            TextView view = mRelativesViews.valueAt(i);
            FriendItem item = (FriendItem) view.getTag();
            if (item != null) {
                friends.add(item);
            }
        }
    }

    @Override
    public void onContactsSelected(int relationship, ArrayList<Parcelable> contacts) {
        int index = -1;
        for (int i = 0; i < mRelativesViews.size(); i++) {
            int key = mRelativesViews.keyAt(i);
            if (key == relationship) {
                index = i;
                break;
            }
        }

        int count = Math.min(mRelativesViews.size() - index, contacts.size());
        int maxIndex = index + count;
        int j = 0;

        for (int i = index; i < maxIndex; i++) {
            TextView view = mRelativesViews.valueAt(i);
            FriendItem item = (FriendItem) contacts.get(j++);
            view.setText(item.getName());
            view.setTag(item);
        }

        updateRelatives();
    }

    //http://stackoverflow.com/a/19194441/1735100
    private View.OnTouchListener mOnTouchRelativeClear = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (view.getRight() - ((EditText) view).getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    ((EditText) view).setText("");
                    view.setTag(null);
                    updateRelatives();
                    return true;
                }
            }
            return false;
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && data.hasExtra(Params.ACTION)) {
            int action = data.getIntExtra(Params.ACTION, 0);
            if (resultCode == Activity.RESULT_OK && action == Action.GET_CONTACTS) {
                onContactsSelected(data.getIntExtra(Params.RELATIONSHIP, 0), data.getParcelableArrayListExtra(Params.CONTACTS));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_1:
            case R.id.relative_2:
            case R.id.relative_3:
            case R.id.relative_4:
            case R.id.relative_5:
                startActivityForResult(ContactsActivity.newPickInstance(getActivity(), v.getId()), 1000);
                break;
            case R.id.search:
                search();
                break;
        }
    }

    private void search() {
        SearchUserRequest searchUserRequest = new SearchUserRequest();

        ArrayList<String> phones = new ArrayList<>();
        for (int i = 0; i < mRelativesViews.size(); i++) {
            TextView view = mRelativesViews.valueAt(i);
            FriendItem item = (FriendItem) view.getTag();
            if (item != null) {
                if (item.getPhoneNumbers() != null && item.getPhoneNumbers().size() > 0) {
                    for (String phone : item.getPhoneNumbers()) {
                        phones.add(phone);
                    }
                }
            }
        }

        if (phones.size() > 0) {
            searchUserRequest.setPhones(phones.toArray(new String[phones.size()]));
        }

        if (mCurrentSmileId > 0) {
            searchUserRequest.setSmileId(mCurrentSmileId);
        }

        if (mCurrentFilterId > 0) {
            searchUserRequest.setFilterId(mCurrentFilterId);
        }


        mSpiceManager.execute(searchUserRequest, new UserSearchRequestListener(searchUserRequest.getParams(), false) {
            @Override
            public void onRequestSuccess(UserSearchResponse response) {
                super.onRequestSuccess(response);
                if (isAdded()) {
                    startActivity(SearchResultActivity.newIntent(getActivity()));
                }
            }
        });
    }

    private class ScaleImageTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Bitmap bitmap = BitmapDecoder.createSquareBitmap(mSourceImagePath, App.IMAGE_SMALL_WIDTH, 0, false);
                FileOutputStream fileOutputStream = new FileOutputStream(FilePathHelper.getVideoPreviewImageSmallPath());
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.action_search);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
