package com.clickclap.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.clickclap.model.Filter;
import com.clickclap.util.OnCacheUpdateListener;
import com.clickclap.view.items.VideoFilterItemView;
import com.humanet.filters.videofilter.IFilter;

import java.util.ArrayList;

/**
 * Created by Владимир on 31.10.2014.
 */
public class VideoFilterListAdapter extends BaseAdapter {

    private String mImagePath;

    private Context mContext;
    private ArrayList<IFilter> mData;

    public VideoFilterListAdapter(Context context) {
        mContext = context;
        mData = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public IFilter getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VideoFilterItemView view = (VideoFilterItemView) convertView;
        if (view == null) {
            view = new VideoFilterItemView(mContext);
        }
        IFilter filter = getItem(position);
        view.setData(mImagePath, filter);
        return view;
    }

    public void setData(ArrayList<IFilter> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }


}
