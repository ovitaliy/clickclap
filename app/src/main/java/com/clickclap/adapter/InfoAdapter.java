package com.clickclap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clickclap.R;

/**
 * Created by denisvasilenko on 08.12.15.
 */
public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.ViewHolder> {
    String[] mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v;
        }
    }

    public InfoAdapter(String[] data) {
        mDataset = data;
    }

    public void setData(String[] data) {
        mDataset = data;
    }

    @Override
    public InfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        TextView v = new TextView(context);
        v.setTextColor(context.getResources().getColor(R.color.yellow));
        v.setTextSize(18);
        v.setMovementMethod(LinkMovementMethod.getInstance());
        v.setPadding(0, 0, 0, 15);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final InfoAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(Html.fromHtml(mDataset[position]));
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
