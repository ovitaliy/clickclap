package com.clickclap.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.clickclap.view.items.FrameItemView;

import java.util.ArrayList;

/**
 * Created by Владимир on 29.10.2014.
 */
public class FrameListAdapter extends BaseAdapter {


    private Context mContext;
    private ArrayList<String> mData;
    private int mSelected = 0;

    public FrameListAdapter(Context context) {
        mContext = context;
        mData = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FrameItemView view = (FrameItemView) convertView;
        if(view == null) {
            view = new FrameItemView(mContext);
        }
        view.setData(getItem(position));
        return view;
    }


    public void setData(ArrayList<String> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }



}
