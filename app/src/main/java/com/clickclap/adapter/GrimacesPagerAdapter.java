package com.clickclap.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.clickclap.fragment.GrimacePageFragment;
import com.clickclap.model.Grimace;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by denisvasilenko on 24.09.15.
 */
public class GrimacesPagerAdapter extends FragmentStatePagerAdapter {
    int mCellSize;
    int mType;
    ArrayList<LinkedHashMap<Integer, Grimace[]>> mData;

    public void clear() {
        if (mData != null) {
            mData.clear();
            try {
                notifyDataSetChanged();
            } catch (IllegalStateException e) {
            }
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public boolean isNewDataIdentical(ArrayList<LinkedHashMap<Integer, Grimace[]>> newData) {
        if (mData == null) {
            return false;
        }

        if (newData.equals(mData)) {
            return true;
        }

        if (mData.size() != newData.size()) {
            return false;
        }

        boolean identical = true;
        if (newData.size() == mData.size()) {
            for (int i = 0; i < mData.size(); i++) {
                if (newData.get(i).size() != mData.get(i).size()) {
                    identical = false;
                } else {
                    for (int j = 0; j < newData.size(); j++) {
                        LinkedHashMap<Integer, Grimace[]> newList = newData.get(j);
                        LinkedHashMap<Integer, Grimace[]> dataList = mData.get(j);

                        for (Map.Entry<Integer, Grimace[]> entry : newList.entrySet()) {
                            int key = entry.getKey();

                            if (newList.get(key).length != dataList.get(key).length) {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return identical;
    }

    public void setData(final ArrayList<LinkedHashMap<Integer, Grimace[]>> data, int type) {
        mData = data;
        mType = type;
        notifyDataSetChanged();
    }

    public GrimacesPagerAdapter(FragmentManager fm, int cellSize) {
        super(fm);
        mCellSize = cellSize;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = GrimacePageFragment.newInstance(mData.get(position), mCellSize, mType);
        return fragment;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mData != null) {
            count = mData.size();
        }
        return count;
    }

}