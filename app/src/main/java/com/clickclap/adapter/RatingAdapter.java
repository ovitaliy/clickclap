package com.clickclap.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.model.UserInfo;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by denisvasilenko on 01.03.16.
 */
public class RatingAdapter extends CursorAdapter {
    private LayoutInflater mLayoutInflater;

    public RatingAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.item_rating_user, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        UserInfo user = UserInfo.fromCursor(cursor);
        ImageView avatarView = (ImageView) view.findViewById(R.id.avatar);
        avatarView.setImageDrawable(null);
        if (!TextUtils.isEmpty(user.getPhoto())) {
            ImageLoader.getInstance().displayImage(user.getImageLoaderPhoto(), avatarView);
        }

        if (user.getId() == AppUser.getUid()) {
            view.setBackgroundColor(view.getResources().getColor(R.color.yellow_gray));
        } else {
            view.setBackground(null);
        }

        TextView rateView = (TextView) view.findViewById(R.id.rating);
        rateView.setText(String.valueOf(user.getRating()));
        String location = user.getCity();
        /*if (user.getLanguage() != null) {
            if (!TextUtils.isEmpty(location)) {
                location += ", ";
            }
            location += user.getLanguage().getTitle();
        }*/

        TextView locationView = (TextView) view.findViewById(R.id.location);
        locationView.setText(location);

        TextView nameView = (TextView) view.findViewById(R.id.name);
        nameView.setText(user.getFullName());
    }
}
