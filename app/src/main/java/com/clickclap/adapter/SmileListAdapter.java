package com.clickclap.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.clickclap.enums.Emotion;
import com.clickclap.view.items.SmileItemView;

import java.util.ArrayList;

/**
 * Created by Владимир on 25.11.2014.
 */
public class SmileListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Emotion> mData;

    public SmileListAdapter(Context context, ArrayList<Emotion> list) {
        mContext = context;
        mData = list;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Emotion getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SmileItemView view = (SmileItemView) convertView;
        if (view == null) {
            view = new SmileItemView(mContext);
        }

        Emotion smile = getItem(position);

        view.setData(smile);

        return view;
    }

}
