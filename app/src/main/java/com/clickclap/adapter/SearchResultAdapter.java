package com.clickclap.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.enums.Hobby;
import com.clickclap.enums.Interest;
import com.clickclap.enums.Language;
import com.clickclap.enums.Pet;
import com.clickclap.enums.Religion;
import com.clickclap.enums.Sport;
import com.clickclap.model.UserFinded;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Deni on 22.06.2015.
 */
public class SearchResultAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;
    Context mContext;

    public SearchResultAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.item_search, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView firstNameView = (TextView) view.findViewById(R.id.first_name);
        TextView lastNameView = (TextView) view.findViewById(R.id.last_name);
        TextView cityView = (TextView) view.findViewById(R.id.city);
        TextView countryView = (TextView) view.findViewById(R.id.country);
        TextView detailsView = (TextView) view.findViewById(R.id.details);
        TextView ratingView = (TextView) view.findViewById(R.id.rating);
        ImageView avatarView = (ImageView) view.findViewById(R.id.avatar);

        UserFinded user = UserFinded.fromCursor(cursor);

        if (user.getFirstName() != null && !TextUtils.isEmpty(user.getFirstName().value)) {
            String name = "";
            if (user.getFirstName().required) {
                name = String.format("<b>%s</b>", user.getFirstName().value);
            } else {
                name = user.getFirstName().value;
            }
            firstNameView.setText(Html.fromHtml(name));
        }

        if (user.getLastName() != null && !TextUtils.isEmpty(user.getLastName().value)) {
            String name = "";
            if (user.getLastName().required) {
                name = String.format("<b>%s</b>", user.getLastName().value);
            } else {
                name = user.getLastName().value;
            }
            lastNameView.setText(Html.fromHtml(name));
        }

        if (user.getIdCountry() != null && !TextUtils.isEmpty(user.getIdCountry().value)) {
            String name;
            if (user.getIdCountry().required) {
                name = String.format("<b>%s</b>", user.getCountry().getAsString());
            } else {
                name = user.getCountry().getAsString();
            }
            countryView.setText(Html.fromHtml(name));
        }

        if (user.getIdCity() != null && !TextUtils.isEmpty(user.getIdCity().value)) {
            String name;
            if (user.getIdCity().required) {
                name = String.format("<b>%s</b>", user.getCity().getAsString());
            } else {
                name = user.getCity().getAsString();
            }
            cityView.setText(Html.fromHtml(name));
        }

        StringBuilder details = new StringBuilder();
        if (user.getLang() != null && !TextUtils.isEmpty(user.getLang().value)) {
            Language lang = Language.getFromString(user.getLang().value);
            if (user.getLang().required) {
                details.append(String.format("<b>%s</b>", lang.getTitle()));
            } else {
                details.append(lang.getTitle());
            }
            details.append(", ");
        }

        if (user.getCraft() != null) {
            String craft = user.getCraft().value;
            if (!TextUtils.isEmpty(craft)) {
                if (user.getCraft().required) {
                    details.append(String.format("<b>%s</b>", craft));
                } else {
                    details.append(craft);
                }
                details.append(", ");
            }
        }

        if (user.getHobby() != null && !TextUtils.isEmpty(user.getHobby().value)) {
            String hobby = Hobby.getById(user.getHobby().value).getTitle();
            if (user.getHobby().required) {
                details.append(String.format("<b>%s</b>", hobby));
            } else {
                details.append(hobby);
            }
            details.append(", ");
        }

        if (user.getInterest() != null && !TextUtils.isEmpty(user.getInterest().value)) {
            String interest = Interest.getById(user.getInterest().value).getTitle();
            if (user.getInterest().required) {
                details.append(String.format("<b>%s</b>", interest));
            } else {
                details.append(interest);
            }
            details.append(", ");
        }

        if (user.getSport() != null && !TextUtils.isEmpty(user.getSport().value)) {
            String sport = Sport.getById(user.getSport().value).getTitle();
            if (user.getSport().required) {
                details.append(String.format("<b>%s</b>", sport));
            } else {
                details.append(sport);
            }
            details.append(", ");
        }

        if (user.getPets() != null && !TextUtils.isEmpty(user.getPets().value)) {
            String pets = Pet.getById(user.getPets().value).getTitle();
            if (user.getPets().required) {
                details.append(String.format("<b>%s</b>", pets));
            } else {
                details.append(pets);
            }
            details.append(", ");
        }

        if (user.getReligion() != null && !TextUtils.isEmpty(user.getReligion().value)) {
            String religion = Religion.getById(user.getReligion().value).getTitle();
            if (user.getReligion().required) {
                details.append(String.format("<b>%s</b>", religion));
            } else {
                details.append(religion);
            }
            details.append(", ");
        }

        if (details.length() > 1) {
            detailsView.setText(Html.fromHtml(details.substring(0, details.length() - 2)));
        }

        ratingView.setText(String.valueOf(user.getRating()));

        ImageLoader.getInstance().displayImage(user.getPhoto(), avatarView);
    }
}
