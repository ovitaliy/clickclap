package com.clickclap.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.model.Vote;

/**
 * Created by Denis on 28.04.2015.
 */
public class VoteAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;
    private boolean mIsMy;

    public VoteAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setIsMy(boolean my) {
        mIsMy = my;
    }

    @Override
    public int getItemViewType(int position) {
        return mIsMy ? 1 : 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        if (mIsMy) {
            return mLayoutInflater.inflate(R.layout.item_vote_question_my, viewGroup, false);
        } else {
            return mLayoutInflater.inflate(R.layout.item_vote_question, viewGroup, false);
        }
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        Vote vote = Vote.fromCursor(cursor);

        TextView textView = (TextView) view.findViewById(R.id.question);
        textView.setText(vote.getTitle());

        if (mIsMy) {
            TextView statusView = (TextView) view.findViewById(R.id.status);
            String title;
            switch (vote.getStatus()) {
                case Vote.CREATED:
                    title = App.getInstance().getString(R.string.vote_status_wait);
                    break;
                case Vote.DECLINED:
                    title = App.getInstance().getString(R.string.vote_status_declined);
                    break;
                default:
                    title = App.getInstance().getString(R.string.vote_status_approoved);
                    break;
            }
            statusView.setText(title);
        } else {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.voted);
            checkBox.setChecked(vote.isVoted());
        }
    }
}
