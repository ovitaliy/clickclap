package com.clickclap.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.view.BaseModel;

import java.util.Collection;
import java.util.List;

/**
 * Created by ovitali on 28.08.2015.
 */
public final class SimpleSpinnerAdapter<T extends BaseModel> extends BaseAdapter {

    private List<T> mList;

    public SimpleSpinnerAdapter(List<T> list) {
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public T getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;

        if (convertView != null) {
            textView = (TextView) convertView;
        } else {
            textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        textView.setText(getItem(position).getTitle());



        return textView;
    }




    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView textView;

        if (convertView != null) {
            textView = (TextView) convertView;
        } else {
            textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_dropdown_item, parent, false);
        }

        textView.setText(getItem(position).getTitle());

        return textView;
    }

    @SuppressWarnings("Unchecked")
    public void set(Collection<T> list) {
        if (mList.size() > 0) {
            mList.clear();
        }
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void add(Collection<T> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        if (mList != null && mList.size() > 0) {
            mList.clear();
            notifyDataSetChanged();
        }
    }
}
