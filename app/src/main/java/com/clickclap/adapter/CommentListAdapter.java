package com.clickclap.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.clickclap.listener.OnViewProfileListener;
import com.clickclap.model.Comment;
import com.clickclap.view.CommentView;

import java.util.ArrayList;

/**
 * Created by Владимир on 26.11.2014.
 */
public class CommentListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Comment> mData;
    private OnViewProfileListener mOnViewProfileListener;


    public CommentListAdapter(Context context) {
        mContext = context;
        mData = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommentView view = (CommentView) convertView;
        if(view == null) {
            view = new CommentView(mContext);
            view.setOnViewProfileListener(mOnViewProfileListener);
        }
        Comment data = getComment(position);
        view.setData(data);
        return view;
    }

    public Comment getComment(int position) {
        return (Comment) getItem(position);
    }

    public void setData(ArrayList<Comment> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setOnViewProfileListener(OnViewProfileListener listener){
        mOnViewProfileListener = listener;
    }

}
