package com.clickclap.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.model.VoteOption;

/**
 * Created by Denis on 28.04.2015.
 */
public class VoteOptionsAdapter extends CursorAdapter {
    private LayoutInflater mLayoutInflater;
    private int mVotedCount;
    private int mVoted;
    private boolean mFinished;

    public VoteOptionsAdapter(Context context, Cursor c) {
        super(context, c, 0);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setFinished(boolean finished) {
        mFinished = finished;
    }

    public void setVoted(int voted) {
        mVoted = voted;
    }

    public void setVotedCount(int votedCount) {
        mVotedCount = votedCount;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (mVoted != 0 || mFinished) ? 1 : 0;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        if (mVoted != 0 || mFinished) {
            return mLayoutInflater.inflate(R.layout.item_voted_option, viewGroup, false);
        } else {
            return mLayoutInflater.inflate(R.layout.item_vote_option, viewGroup, false);
        }
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        VoteOption option = VoteOption.fromCursor(cursor);

        if (mVoted != 0 || mFinished) {
            TextView titleView = (TextView) view.findViewById(R.id.title);
            String title = option.getTitle() + (mVoted == option.getId() ? " (" + context.getString(R.string.your_choice) + ")" : "");
            titleView.setText(title);

            View progressView = view.findViewById(R.id.statistic);
            TextView countView = (TextView) view.findViewById(R.id.count);

            int progress = Math.round(((float) option.getVotes() / (float) mVotedCount) * 100f);

            int maxWidth = (App.WIDTH / 3) * 2;

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) progressView.getLayoutParams();
            params.width = maxWidth / 100 * progress;

            countView.setText(String.valueOf(option.getVotes()) + "(" + String.valueOf(progress) + "%)");
        } else {
            TextView textView = (TextView) view;
            textView.setText(option.getTitle());
        }

    }
}
