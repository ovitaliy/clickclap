package com.clickclap.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import com.clickclap.R;
import com.clickclap.media.filter.grimace.base.BaseGrimaceFilter;
import com.clickclap.model.Filter;
import com.clickclap.util.BitmapDecoder;
import com.clickclap.util.BitmapLruCache;
import com.clickclap.util.ImageCroper;
import com.clickclap.view.CircleLayout;
import com.clickclap.view.items.GrimaceFilterItemView;
import com.clickclap.view.items.VideoFilterItemView;
import com.humanet.filters.videofilter.IFilter;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis on 30.03.2014.
 */
public class GrimaceFilterListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<IFilter> mData;

    private String mImagePath;

    public GrimaceFilterListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public IFilter getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VideoFilterItemView view = (VideoFilterItemView) convertView;
        if (view == null) {
            view = new VideoFilterItemView(mContext);
        }
        IFilter filter = getItem(position);
        view.setData(mImagePath, filter);
        return view;
    }


    public void setData(ArrayList<IFilter> data) {
        mData = data;
        notifyDataSetChanged();
    }


}