package com.clickclap.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.clickclap.model.Balance;
import com.clickclap.view.BalanceItemView;

/**
 * Created by Владимир on 13.11.2014.
 */
public class BalanceListAdapter extends CursorAdapter {

    private Context mContext;

    public BalanceListAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new BalanceItemView(mContext);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((BalanceItemView) view).setData(Balance.fromCursor(cursor));
    }
}
