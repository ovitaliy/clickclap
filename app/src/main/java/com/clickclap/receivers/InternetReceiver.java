package com.clickclap.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.clickclap.jobs.UploadGrimaceJob;
import com.clickclap.jobs.UploadVideoJob;
import com.clickclap.util.NetworkUtil;

public class InternetReceiver extends BroadcastReceiver {
    public InternetReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean offlineMode = NetworkUtil.isOfflineMode();

        if (!offlineMode) {
            UploadVideoJob.startUploader();
            UploadGrimaceJob.startUploader();
        }
    }
}
