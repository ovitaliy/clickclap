package com.clickclap.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import java.lang.ref.WeakReference;

/**
 * Created by denisvasilenko on 16.02.16.
 */
public class SmsReceiver extends WakefulBroadcastReceiver {
    public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    WeakReference<SmsReceiverListener> mListener;

    public SmsReceiver(SmsReceiverListener listener) {
        mListener = new WeakReference<>(listener);
    }

    public boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private String getCodeFromSms(String body) {
        String[] words = body.split("\\s+");
        if (words.length > 0) {
            for (String word : words) {
                if (isNumeric(word)) {
                    return word;
                }
            }
        }
        return null;
    }

    @SuppressWarnings("ImplicitArrayToString")
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SMS_RECEIVED)) {
            SmsMessage[] msgs;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            } else {
                Bundle data = intent.getExtras();
                if (data != null) {
                    Object[] pdus = (Object[]) data.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    }
                } else {
                    return;
                }
            }

            String code = "";
            String body = msgs[0].getMessageBody().toLowerCase();
            boolean contains = body.contains("clickclap code") || body.contains("clikclap code") || body.contains("thehumanet code");
            if (contains) {
                code = getCodeFromSms(body);
                if (!TextUtils.isEmpty(code)) {
                    if (mListener != null && mListener.get() != null) {
                        mListener.get().onGetSmsCode(code);
                    }
                }
            }
        }
    }

    public interface SmsReceiverListener {
        void onGetSmsCode(CharSequence code);
    }
}