package com.clickclap.rest;

import com.clickclap.Const;
import com.clickclap.util.GsonHelper;
import com.clickclap.util.PrefHelper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by ovitali on 23.12.2014.
 */
public class ArgsMap extends HashMap<String, String> {

    public ArgsMap() {
        this(true);
    }

    public ArgsMap(boolean withAuthToken) {
        if (withAuthToken) {
            put("auth_token", PrefHelper.getStringPref(Const.PREF_TOKEN));
        }
    }

    @SuppressWarnings("ImplicitArrayToString")
    public String put(String key, String[] value) {
        return super.put(key, String.valueOf(value));
    }

    public String put(String key, int value) {
        return super.put(key, String.valueOf(value));
    }

    public String put(String key, long value) {
        return super.put(key, String.valueOf(value));
    }

    public void putJsonString(String string) {
        JsonElement jelem = GsonHelper.GSON.fromJson(string, JsonElement.class);
        JsonObject jsonObject = jelem.getAsJsonObject();
        Set<Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            put(entry.getKey(), entry.getValue().getAsString());
        }
    }
}
