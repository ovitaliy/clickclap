package com.clickclap.rest.request;

import android.util.Log;

import com.clickclap.enums.Language;
import com.clickclap.model.City;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.DatabaseRestApi;
import com.clickclap.rest.model.CitiesListResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class GetCitiesRequest extends RetrofitSpiceRequest<CitiesListResponse, DatabaseRestApi> {
    private int mCountryId;
    private String mTypedName;

    public GetCitiesRequest(int countryId, String typedName) {
        super(CitiesListResponse.class, DatabaseRestApi.class);
        mCountryId = countryId;
        mTypedName = typedName;
    }

    @Override
    public CitiesListResponse loadDataFromNetwork() throws Exception {
        Thread.sleep(50);
        if (isCancelled())
            return null;

        City[] cities;
        ArgsMap argsMap = buildRequestParams(mCountryId, mTypedName);

        Log.i("CitiesRequest", argsMap.toString());
        CitiesListResponse response = getService().getCities(argsMap);
        if (response.getCities() == null){
            response.setCities(new City[0]);
        }
        return response;
    }

    public static ArgsMap buildRequestParams(int countryId, String typedName) {
        ArgsMap argsMap = new ArgsMap(true);
        argsMap.put("id_country", countryId);
        if (typedName != null) {
            argsMap.put("q", typedName);
            argsMap.put("lang", Language.getSystem().getRequestParam());
        }

        return argsMap;
    }
}
