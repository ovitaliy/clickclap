package com.clickclap.rest.request;

import android.text.TextUtils;

import com.clickclap.enums.VideoType;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.FlowRestApi;
import com.clickclap.rest.model.CategoriesListResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class GetCategoriesRequest extends RetrofitSpiceRequest<CategoriesListResponse, FlowRestApi> {
    CategoriesRequestType mRequestType;
    VideoType mVideoType;

    public GetCategoriesRequest(VideoType videoType, CategoriesRequestType requestType) {
        super(CategoriesListResponse.class, FlowRestApi.class);
        mRequestType = requestType;
        mVideoType = videoType;
    }

    public GetCategoriesRequest(VideoType videoType) {
        super(CategoriesListResponse.class, FlowRestApi.class);
        mRequestType = CategoriesRequestType.FULL;
        mVideoType = videoType;
    }

    public static ArgsMap getArgsMap(CategoriesRequestType requestType, VideoType videoType) {
        ArgsMap args = new ArgsMap(true);
        args.put("type", requestType.getRequestParam());
        args.put("video_type", videoType.getRequestParam());
        if (!TextUtils.isEmpty(videoType.getAdditionParam())) {
            args.put("params", videoType.getAdditionParam());
        }
        return args;
    }

    @Override
    public CategoriesListResponse loadDataFromNetwork() throws Exception {
        ArgsMap args = getArgsMap(mRequestType, mVideoType);
        return getService().getCategories(args);
    }
}
