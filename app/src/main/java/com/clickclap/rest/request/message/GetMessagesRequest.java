package com.clickclap.rest.request.message;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.MessageRestApi;
import com.clickclap.rest.model.MessagesResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetMessagesRequest extends RetrofitSpiceRequest<MessagesResponse, MessageRestApi> {
    int mListId;

    public GetMessagesRequest(int listId) {
        super(MessagesResponse.class, MessageRestApi.class);
        mListId = listId;
    }

    public static ArgsMap getArgsMap(int listId) {
        ArgsMap map = new ArgsMap(true);
        map.put("id_list", listId);
        return map;
    }

    @Override
    public MessagesResponse loadDataFromNetwork() throws Exception {
        return getService().getMessages(getArgsMap(mListId));
    }
}
