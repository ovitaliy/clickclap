package com.clickclap.rest.request.user;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetSmsCodeRequest extends RetrofitSpiceRequest<BaseResponse, UserRestApi> {
    String mPhone;

    public GetSmsCodeRequest(String phone) {
        super(BaseResponse.class, UserRestApi.class);
        mPhone = phone;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("phone", mPhone);
        return getService().getSmsCode(map);
    }
}
