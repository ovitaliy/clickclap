package com.clickclap.rest.request.vote;

import com.clickclap.AppUser;
import com.clickclap.model.TypedJsonString;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VoteRestApi;
import com.clickclap.rest.model.AddVoteResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddVoteRequest extends RetrofitSpiceRequest<AddVoteResponse, VoteRestApi> {
    String[] mOptions;
    String mDescription;

    public AddVoteRequest(String description, String[] options) {
        super(AddVoteResponse.class, VoteRestApi.class);
        mDescription = description;
        mOptions = options;
    }

    public static TypedJsonString getArgsMap(String title, String[] options) {
        ArgsMap map = new ArgsMap(true);
        map.put("title", title);
        map.put("lang", AppUser.get().getLanguage().getRequestParam());

        JSONObject object = new JSONObject(map);
        try {
            object.put("options", new JSONArray(Arrays.asList(options)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new TypedJsonString(object.toString());
    }

    @Override
    public AddVoteResponse loadDataFromNetwork() throws Exception {
        return getService().addVote(getArgsMap(mDescription, mOptions));
    }
}
