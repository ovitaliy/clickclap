package com.clickclap.rest.request.video;

import android.text.TextUtils;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.FlowRestApi;
import com.clickclap.rest.model.FlowResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class GetFlowRequest extends RetrofitSpiceRequest<FlowResponse, FlowRestApi> {
    String mParams;
    int mReplyId;
    long mLastId;

    public GetFlowRequest(String params) {
        super(FlowResponse.class, FlowRestApi.class);
        mParams = params;
    }

    public void setReplyId(int replyId) {
        mReplyId = replyId;
    }

    public void setLast(long lastId) {
        mLastId = lastId;
    }

    public static ArgsMap getArgsMap(String params, int replyId, long lastId) {
        ArgsMap argsMap = new ArgsMap(true);

        if (replyId != 0) {
            argsMap.put("id_reply", replyId);
        } else {
            if (!TextUtils.isEmpty(params)) {
                argsMap.putJsonString(params);
            }
        }

        if (lastId > 0) {
            argsMap.put("last", lastId);
        }
        return argsMap;
    }

    @Override
    public FlowResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = getArgsMap(mParams, mReplyId, mLastId);
        return getService().getFlow(map);
    }
}
