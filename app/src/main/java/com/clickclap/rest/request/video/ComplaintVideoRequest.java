package com.clickclap.rest.request.video;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class ComplaintVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    private int mVideoId;

    public ComplaintVideoRequest(int videoId) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap params = new ArgsMap(true);
        params.put("id_video", mVideoId);
        return getService().complaintVideo(params);
    }
}
