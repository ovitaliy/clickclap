package com.clickclap.rest.request.grimace;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class BuyGrimaceRequest extends RetrofitSpiceRequest<BaseResponse, GrimaceRestApi> {
    int mGrimaceId;

    public BuyGrimaceRequest(int grimaceId) {
        super(BaseResponse.class, GrimaceRestApi.class);
        mGrimaceId = grimaceId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_grimace", mGrimaceId);
        return getService().buyGrimace(map);
    }
}
