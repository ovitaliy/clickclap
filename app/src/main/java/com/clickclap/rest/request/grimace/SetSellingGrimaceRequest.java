package com.clickclap.rest.request.grimace;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class SetSellingGrimaceRequest extends RetrofitSpiceRequest<BaseResponse, GrimaceRestApi> {
    int mGrimaceId;
    boolean mStatus;

    public SetSellingGrimaceRequest(int grimaceId, boolean status) {
        super(BaseResponse.class, GrimaceRestApi.class);
        mGrimaceId = grimaceId;
        mStatus = status;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_grimace", mGrimaceId);
        map.put("status", mStatus ? 1 : 0);
        return getService().setSellingGrimace(map);
    }
}
