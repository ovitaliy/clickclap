package com.clickclap.rest.request.video;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class MarkVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    int mVideoId;
    int mMark;

    public MarkVideoRequest(int videoId, int mark) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
        mMark = mark;
    }

    public static ArgsMap getArgsMap(int videoId, int mark) {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", videoId);
        map.put("mark", mark);
        return map;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = getArgsMap(mVideoId, mMark);
        return getService().markVideo(map);
    }
}
