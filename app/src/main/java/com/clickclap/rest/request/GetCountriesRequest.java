package com.clickclap.rest.request;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.SparseArray;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.Country;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.DatabaseRestApi;
import com.clickclap.rest.model.CountriesListResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class GetCountriesRequest extends RetrofitSpiceRequest<CountriesListResponse, DatabaseRestApi> {

    public GetCountriesRequest() {
        super(CountriesListResponse.class, DatabaseRestApi.class);
    }


    @Override
    public CountriesListResponse loadDataFromNetwork() throws Exception {
        Country[] countries = loadFromDataBase();
        CountriesListResponse response;

        if (countries.length > 50) {
            response = new CountriesListResponse();
            response.setCountries(countries);

            return response;
        }

        response = getService().getCountries(new ArgsMap(true));

        countries = response.getCountries();
        int count = countries.length;
        SparseArray<Country> countriesList = new SparseArray<>(count);
        ContentValues[] contentValues = new ContentValues[count];
        for (int i = 0; i < count; i++) {
            Country country = countries[i];
            countriesList.put(country.getId(), country);
            contentValues[i] = country.toContentValues();
        }

        App.getInstance().getContentResolver().delete(ContentDescriptor.Countries.URI,
                null,
                null);

        App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Countries.URI,
                contentValues);

        return response;
    }

    private Country[] loadFromDataBase() {
        Cursor cursor = App.getInstance().getContentResolver().query(
                ContentDescriptor.Countries.URI,
                null,
                null, null, null
        );
        List<Country> countries = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                countries.add(Country.fromCursor(cursor));
            } while (cursor.moveToNext());
            cursor.close();
        }

        return countries.toArray(new Country[countries.size()]);
    }
}
