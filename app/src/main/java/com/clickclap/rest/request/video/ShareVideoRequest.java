package com.clickclap.rest.request.video;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class ShareVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    int mVideoId;
    String mPhones;

    public ShareVideoRequest(int videoId, String phones) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
        mPhones = phones;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        map.put("numbers", mPhones);
        return getService().shareVideo(map);
    }
}
