package com.clickclap.rest.request.vote;

import com.clickclap.enums.VoteListType;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VoteRestApi;
import com.clickclap.rest.model.VoteListsResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVotesListRequest extends RetrofitSpiceRequest<VoteListsResponse, VoteRestApi> {
    VoteListType mType;

    public GetVotesListRequest(VoteListType type) {
        super(VoteListsResponse.class, VoteRestApi.class);
        mType = type;
    }

    public static ArgsMap getArgsMap(VoteListType type) {
        ArgsMap map = new ArgsMap(true);
        if (type != null) {
            map.put("type", type.getRequestParam());
        }
        return map;
    }

    @Override
    public VoteListsResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        if (mType != null) {
            map.put("type", mType.getRequestParam());
            return getService().getVoteList(map);
        } else {
            return getService().getMyList(map);
        }
    }
}
