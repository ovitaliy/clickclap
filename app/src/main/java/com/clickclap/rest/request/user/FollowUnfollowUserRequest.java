package com.clickclap.rest.request.user;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class FollowUnfollowUserRequest extends RetrofitSpiceRequest<BaseResponse, UserRestApi> {
    boolean mFollow;
    int mUserId;

    public FollowUnfollowUserRequest(int userId, boolean follow) {
        super(BaseResponse.class, UserRestApi.class);
        mUserId = userId;
        mFollow = follow;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_user", mUserId);
        if (mFollow) {
            return getService().followUser(map);
        } else {
            return getService().unfollowUser(map);
        }
    }
}
