package com.clickclap.rest.request;

import com.clickclap.rest.ServerRequestParams;

/**
 * Created by denisvasilenko on 01.10.15.
 */
public enum CategoriesRequestType implements ServerRequestParams {
    SHORT {
        @Override
        public String getRequestParam() {
            return "short";
        }
    }, FULL {
        @Override
        public String getRequestParam() {
            return "full";
        }
    };
}
