package com.clickclap.rest.request.message;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.MessageRestApi;
import com.clickclap.rest.model.AddMessageResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddMessageRequest extends RetrofitSpiceRequest<AddMessageResponse, MessageRestApi> {
    int mListId;
    String mMessage;
    int mType;

    public AddMessageRequest(int listId, String message, int type) {
        super(AddMessageResponse.class, MessageRestApi.class);
        mListId = listId;
        mMessage = message;
        mType = type;
    }

    public static ArgsMap getArgsMap(int listId, String message, int type) {
        ArgsMap map = new ArgsMap(true);
        map.put("type", type);
        map.put("message", message);
        if (listId > 0) {
            map.put("id_list", listId);
        }
        return map;
    }

    @Override
    public AddMessageResponse loadDataFromNetwork() throws Exception {
        return getService().addMessage(getArgsMap(mType, mMessage, mListId));
    }
}
