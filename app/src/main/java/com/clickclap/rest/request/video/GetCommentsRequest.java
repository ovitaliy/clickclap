package com.clickclap.rest.request.video;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.CommentsResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetCommentsRequest extends RetrofitSpiceRequest<CommentsResponse, VideoRestApi> {
    int mVideoId;

    public GetCommentsRequest(int videoId) {
        super(CommentsResponse.class, VideoRestApi.class);
        mVideoId = videoId;
    }

    @Override
    public CommentsResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);

        return getService().getComments(map);
    }
}
