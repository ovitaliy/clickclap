package com.clickclap.rest.request.vote;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VoteRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class SendVoteRequest extends RetrofitSpiceRequest<BaseResponse, VoteRestApi> {
    int mVoteId;
    int mOptionId;

    public SendVoteRequest(int voteId, int optionId) {
        super(BaseResponse.class, VoteRestApi.class);
        mVoteId = voteId;
        mOptionId = optionId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_vote", mVoteId);
        map.put("id_option", mOptionId);
        return getService().sendVote(map);
    }
}
