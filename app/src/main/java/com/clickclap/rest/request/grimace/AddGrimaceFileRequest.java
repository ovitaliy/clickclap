package com.clickclap.rest.request.grimace;

import com.clickclap.rest.api.FileRestApi;
import com.clickclap.rest.model.AddGrimaceFileResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.io.File;

import retrofit.mime.TypedFile;

/**
 * Created by denisvasilenko on 18.09.15.
 */
@Deprecated
public class AddGrimaceFileRequest extends RetrofitSpiceRequest<AddGrimaceFileResponse, FileRestApi> {
    private String mImagePath;

    public AddGrimaceFileRequest(String imagePath) {
        super(AddGrimaceFileResponse.class, FileRestApi.class);
        mImagePath = imagePath;
    }

    @Override
    public AddGrimaceFileResponse loadDataFromNetwork() throws Exception {
        TypedFile typedFile = new TypedFile("image/png", new File(mImagePath));
        return getService().uploadGrimaceFile(typedFile);
    }
}