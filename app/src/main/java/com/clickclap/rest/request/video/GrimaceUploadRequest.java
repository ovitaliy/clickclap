package com.clickclap.rest.request.video;

import android.app.NotificationManager;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.Grimace;
import com.clickclap.model.UploadingMedia;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.CountingFileRequestBody;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.model.AddGrimaceResponse;
import com.google.gson.Gson;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.commons.io.IOUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.Date;

/**
 * Created by ovitali on 19.10.2015.
 */
public final class GrimaceUploadRequest extends RetrofitSpiceRequest<AddGrimaceResponse, GrimaceRestApi> {

    private static final String TAG = "VideoUploadRequest";

    private static final int NOTIFICATION_ID = 3;

    private Grimace mGrimace;
    private int mGrimaceId;


    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;

    public GrimaceUploadRequest(int grimaceId) {
        super(AddGrimaceResponse.class, GrimaceRestApi.class);

        mGrimaceId = grimaceId;
    }

    @Override
    public AddGrimaceResponse loadDataFromNetwork() throws Exception {
        mGrimace = getGrimace();

        if (mGrimace != null) {
            try {
                Context context = App.getInstance();

                mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                mBuilder = new NotificationCompat.Builder(context);
                mBuilder.setContentTitle("Video Upload").setSmallIcon(android.R.drawable.stat_sys_upload);
                Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
                mBuilder.setLargeIcon(bm);


                UploadingMedia uploadingMedia = uploadFile();

                AddGrimaceResponse addGrimaceResponse = addGrimace(uploadingMedia.getMedia());

                EventBus.getDefault().post(addGrimaceResponse);

                App.getInstance().getContentResolver().delete(ContentDescriptor.Grimaces.URI,  "_id " +
                        "=" + mGrimaceId, null);

                App.syncGrimaces();

                return addGrimaceResponse;
            } finally {
                if (mNotifyManager != null) {
                    mNotifyManager.cancel(NOTIFICATION_ID);
                }
            }
        } else {
            return null;
        }
    }


    private AddGrimaceResponse addGrimace(String uploadedImage) throws Exception {
        ArgsMap params = new ArgsMap(true);
        params.put("media", uploadedImage);
        params.put("id_effect", mGrimace.getEffect());
        params.put("id_smile", mGrimace.getSmileId());
        return getService().addGrimace(params);
    }

    private UploadingMedia uploadFile() throws Exception {

        File imgFile = new File(mGrimace.getUrl());

        Log.i(TAG, "start uploading:" + new Date());

        final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

        final OkHttpClient client = new OkHttpClient();

        MultipartBuilder multipartBuilder = new MultipartBuilder();
        multipartBuilder.type(MultipartBuilder.FORM);

        multipartBuilder.addPart(
                Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"file\""),
                new CountingFileRequestBody(RequestBody.create(MEDIA_TYPE_PNG, imgFile), new CountingFileRequestBody.Listener() {
                    int mLastProgress = -1;

                    @Override
                    public void onRequestProgress(long bytesWritten, long contentLength) {
                        int progress = (int) ((bytesWritten / (float) contentLength) * 100);
                        if (progress != mLastProgress && progress > mLastProgress) {
                            publishProgress(progress);
                            Log.d(TAG, "uploaded:" + progress + "%");
                            mLastProgress = progress;
                        }
                    }
                })
        );

        RequestBody requestBody = multipartBuilder.build();

        Request request = new Request.Builder()
                .url(Const.MEDIA_URL + "grimace/")
                .post(requestBody)
                .build();

        Response requestResponse = client.newCall(request).execute();

        String responseString = IOUtils.toString(requestResponse.body().byteStream());

        UploadingMedia uploadingMedia = new Gson().fromJson(responseString, UploadingMedia.class);

        Log.d(TAG, "response:" + responseString);

        return uploadingMedia;
    }


    @Override
    protected void publishProgress(float progress) {
        super.publishProgress(progress);

        if (progress < 100) {
            mBuilder.setProgress(100, (int) progress, false);
            mBuilder.setContentText("Progress: " + progress + "%");
            mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());
        } else {
            mNotifyManager.cancel(NOTIFICATION_ID);
        }
    }


    private Grimace getGrimace() {
        Cursor cursor = App.getInstance().getContentResolver().query(ContentDescriptor.Grimaces.URI,
                null,
                ContentDescriptor.Grimaces.Cols.TO_UPLOAD + " = " + 1 + " AND _id=" + mGrimaceId,
                null,
                null);
        Grimace grimace = null;
        if (cursor != null && cursor.moveToFirst()) {
            grimace = Grimace.fromCursor(cursor);
            cursor.close();
        }


        return grimace;
    }
}
