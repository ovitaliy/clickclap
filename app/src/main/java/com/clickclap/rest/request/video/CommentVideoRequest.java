package com.clickclap.rest.request.video;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class CommentVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    int mVideoId;
    String mComment;

    public CommentVideoRequest(int videoId, String comment) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
        mComment = comment;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap params = new ArgsMap(true);
        params.put("id_video", mVideoId);
        params.put("comment", mComment);
        return getService().commentVideo(params);
    }
}
