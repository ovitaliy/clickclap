package com.clickclap.rest.request.grimace;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.model.AddGrimaceResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddGrimaceRequest extends RetrofitSpiceRequest<AddGrimaceResponse, GrimaceRestApi> {
    private String mMedia;
    private int mSmileId;
    private int mEffectId;

    public AddGrimaceRequest(String url, int smileId, int effectId) {
        super(AddGrimaceResponse.class, GrimaceRestApi.class);
        mMedia = url;
        mSmileId = smileId;
        mEffectId = effectId;
    }

    @Override
    public AddGrimaceResponse loadDataFromNetwork() throws Exception {

        ArgsMap params = new ArgsMap(true);
        params.put("media", mMedia);
        params.put("id_effect", mEffectId);
        params.put("id_smile", mSmileId);
        return getService().addGrimace(params);
    }
}
