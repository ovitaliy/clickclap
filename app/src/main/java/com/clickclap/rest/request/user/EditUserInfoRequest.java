package com.clickclap.rest.request.user;

import android.text.TextUtils;

import com.clickclap.model.UserInfo;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 17.09.15.
 */
public class EditUserInfoRequest extends RetrofitSpiceRequest<BaseResponse, UserRestApi> {
    UserInfo mUserInfo;

    public EditUserInfoRequest(UserInfo user) {
        super(BaseResponse.class, UserRestApi.class);
        this.mUserInfo = user;
    }

    public static ArgsMap getArgsMap(UserInfo userInfo) {
        ArgsMap map = new ArgsMap(true);

        String photo = userInfo.getPhoto();
        if (!TextUtils.isEmpty(photo) && photo.contains("http")) {
            map.put("photo", userInfo.getPhoto());
        }

        map.put("first_name", userInfo.getFName());

        if (!TextUtils.isEmpty(userInfo.getPatronymic())) {
            map.put("patronymic", userInfo.getPatronymic());
        }

        if (!TextUtils.isEmpty(userInfo.getLName())) {
            map.put("last_name", userInfo.getLName());
        }

        map.put("lang", userInfo.getLanguage().getRequestParam());
        map.put("id_country", userInfo.getCountryId());
        map.put("id_city", userInfo.getCityId());
        map.put("gender", userInfo.getGender());
        map.put("birth_date", userInfo.getBirthDate());


        if (userInfo.getDegree() != null) {
            map.put("degree", userInfo.getDegree().getRequestParam());
        }

        if (userInfo.getJobType() != null) {
            map.put("job", userInfo.getJobType().getRequestParam());
        }

        if (userInfo.getHobbie() != null) {
            map.put("hobbie", userInfo.getHobbie().getRequestParam());
        }

        if (userInfo.getSport() != null) {
            map.put("sport", userInfo.getSport().getRequestParam());
        }

        if (userInfo.getPet() != null) {
            map.put("pets", userInfo.getPet().getRequestParam());
        }

        if (userInfo.getInterest() != null) {
            map.put("interest", userInfo.getInterest().getRequestParam());
        }

        if (userInfo.getReligion() != null) {
            map.put("religion", userInfo.getReligion().getRequestParam());
        }

        if (userInfo.getCraft() != null) {
            map.put("craft", userInfo.getCraft());
        }

        map.put("widget", userInfo.isWidget() ? 1 : 0);

        return map;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = getArgsMap(mUserInfo);
        return getService().editUserInfo(map);
    }
}
