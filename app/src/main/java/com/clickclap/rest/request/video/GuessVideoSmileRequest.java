package com.clickclap.rest.request.video;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GuessVideoSmileRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    int mVideoId;
    int mSmileId;

    public GuessVideoSmileRequest(int videoId, int smileId) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
        mSmileId = smileId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        map.put("id_smile", mSmileId);
        return getService().guessVideoSmile(map);
    }
}
