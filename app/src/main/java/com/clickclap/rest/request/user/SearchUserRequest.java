package com.clickclap.rest.request.user;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.UserSearchResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class SearchUserRequest extends RetrofitSpiceRequest<UserSearchResponse, UserRestApi> {
    private ArgsMap mParams;

    public ArgsMap getParams() {
        return mParams;
    }

    public SearchUserRequest() {
        super(UserSearchResponse.class, UserRestApi.class);
        mParams = new ArgsMap(true);
    }

    public void setSmileId(int smileId) {
        mParams.put("id_smile", smileId);
    }

    public void setFilterId(int filterId) {
        mParams.put("id_filter", filterId);
    }

    public void setFirstName(String firstName) {
        mParams.put("first_name", firstName);
    }

    public void setLastName(String lastName) {
        mParams.put("last_name", lastName);
    }

    public void setCountryId(String id) {
        mParams.put("id_country", id);
    }

    public void setJob(String id) {
        mParams.put("job", id);
    }

    public void setEducation(String id) {
        mParams.put("degree", id);
    }

    public void setCityId(String id) {
        mParams.put("id_city", id);
    }

    public void setLang(String lang) {
        mParams.put("lang", lang);
    }

    public void setPhones(String phones[]) {
        StringBuilder toPhones = new StringBuilder();
        for (String item : phones) {
            toPhones.append(item);
            toPhones.append(",");
        }
        String result = "";
        if (toPhones.length() > 1) {
            result = toPhones.substring(0, toPhones.length() - 1);
        }
        mParams.put("phones", result);
    }

    public void setHobbie(String hobbie) {
        mParams.put("hobbie", hobbie);
    }

    public void setInterest(String interest) {
        mParams.put("interest", interest);
    }

    public void setSport(String sport) {
        mParams.put("sport", sport);
    }

    public void setPets(String pets) {
        mParams.put("pets", pets);
    }

    public void setReligion(String religion) {
        mParams.put("religion", religion);
    }

    public void setVideoIdCountry(String videoIdCountry) {
        mParams.put("video_id_country", videoIdCountry);
    }

    public void setCategory(int category) {
        mParams.put("id_category", String.valueOf(category));
    }

    public void setVideoIdCity(String videoIdCity) {
        mParams.put("video_id_city", videoIdCity);
    }

    public void setVideoCreatedAtFrom(String createdAtFrom) {
        mParams.put("video_created_at_from", createdAtFrom);
    }

    public void setVideoCreatedAtTo(String createdAtTo) {
        mParams.put("video_created_at_to", createdAtTo);
    }

    @Override
    public UserSearchResponse loadDataFromNetwork() throws Exception {
        return getService().searchUser(mParams);
    }
}
