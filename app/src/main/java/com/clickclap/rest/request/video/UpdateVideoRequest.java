package com.clickclap.rest.request.video;

import com.clickclap.enums.VideoUpdateAction;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class UpdateVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    int mVideoId;
    VideoUpdateAction mAction;

    public UpdateVideoRequest(int videoId, VideoUpdateAction action) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
        mAction = action;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        map.put(mAction.getRequestParam(), 1);

        return getService().updateVideo(map);
    }
}
