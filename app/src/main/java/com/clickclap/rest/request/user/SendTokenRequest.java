package com.clickclap.rest.request.user;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class SendTokenRequest extends RetrofitSpiceRequest<BaseResponse, UserRestApi> {
    String mName;
    String mNumber;

    public SendTokenRequest(String name, String number) {
        super(BaseResponse.class, UserRestApi.class);
        mName = name;
        mNumber = number;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("name", mName);
        map.put("number", mNumber);
        return getService().sendToken(map);
    }
}
