package com.clickclap.rest.request.grimace;

import com.clickclap.AppUser;
import com.clickclap.model.Grimace;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.model.GrimacesListResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GetGrimacesRequest extends RetrofitSpiceRequest<GrimacesListResponse, GrimaceRestApi> {
    private int mType;
    private int mUserId;

    public GetGrimacesRequest(int type, int userId) {
        super(GrimacesListResponse.class, GrimaceRestApi.class);
        mType = type;
        mUserId = userId;
    }

    public GetGrimacesRequest(int type) {
        super(GrimacesListResponse.class, GrimaceRestApi.class);
        mType = type;
        mUserId = 0;
    }

    @Override
    public GrimacesListResponse loadDataFromNetwork() throws Exception {
        ArgsMap args = new ArgsMap(true);
        switch (mType) {
            case Grimace.TYPE_COLLECTION:
                args.put("type", "collection");
                break;
            case Grimace.TYPE_USER:
                args.put("type", "user");
                break;
            case Grimace.TYPE_MARKET:
                args.put("type", "market");
                break;
        }

        if (mUserId != 0 && mUserId != AppUser.getUid()) {
            args.put("id_user", mUserId);
        }

        return getService().getGrimaces(args);
    }
}
