package com.clickclap.rest.request.message;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.MessageRestApi;
import com.clickclap.rest.model.MessageListsResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetMessageListsRequest extends RetrofitSpiceRequest<MessageListsResponse, MessageRestApi> {

    public GetMessageListsRequest() {
        super(MessageListsResponse.class, MessageRestApi.class);
    }

    @Override
    public MessageListsResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        return getService().getMessageLists(map);
    }
}
