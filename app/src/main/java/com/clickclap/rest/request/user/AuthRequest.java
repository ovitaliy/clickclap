package com.clickclap.rest.request.user;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.AuthResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class AuthRequest extends RetrofitSpiceRequest<AuthResponse, UserRestApi> {
    String mGcmToken;

    public AuthRequest(String gcmToken) {
        super(AuthResponse.class, UserRestApi.class);
        mGcmToken = gcmToken;
    }

    public AuthRequest() {
        super(AuthResponse.class, UserRestApi.class);
        mGcmToken = "";
    }

    @Override
    public AuthResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(false);
        map.put("token", mGcmToken);
        map.put("os", "android");
        return getService().auth(map);
    }
}
