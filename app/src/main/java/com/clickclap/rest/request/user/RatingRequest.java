package com.clickclap.rest.request.user;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.model.UserRatingResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovitali on 13.10.2015.
 */
public class RatingRequest extends RetrofitSpiceRequest<UserRatingResponse, UserRestApi> {
    private ArgsMap argsMap;


    public RatingRequest(String type) {
        super(UserRatingResponse.class, UserRestApi.class);
        argsMap = new ArgsMap(true);
        argsMap.put("type", type);
    }

    @Override
    public UserRatingResponse loadDataFromNetwork() throws Exception {
        return getService().getRating(argsMap);
    }
}
