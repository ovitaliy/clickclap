package com.clickclap.rest.request.video;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 05.01.15.
 */
public class ShareGrimaceRequest extends RetrofitSpiceRequest<BaseResponse, GrimaceRestApi> {
    int mVideoId;
    String mPhones;

    public ShareGrimaceRequest(int videoId, String phones) {
        super(BaseResponse.class, GrimaceRestApi.class);
        mVideoId = videoId;
        mPhones = phones;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_grimace", mVideoId);
        map.put("numbers", mPhones);
        return getService().shareGrimace(map);
    }
}
