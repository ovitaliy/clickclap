package com.clickclap.rest.request.vote;

import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.api.VoteRestApi;
import com.clickclap.rest.model.VoteResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVoteRequest extends RetrofitSpiceRequest<VoteResponse, VoteRestApi> {
    int mVoteId;

    public GetVoteRequest(int voteId) {
        super(VoteResponse.class, VoteRestApi.class);
        mVoteId = voteId;
    }

    public static ArgsMap getArgsMap(int voteId) {
        ArgsMap map = new ArgsMap(true);
        map.put("id_vote", voteId);
        return map;
    }

    @Override
    public VoteResponse loadDataFromNetwork() throws Exception {
        return getService().getVote(getArgsMap(mVoteId));
    }
}
