package com.clickclap.rest.service;

import android.app.Application;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.Const;
import com.clickclap.rest.api.DatabaseRestApi;
import com.clickclap.rest.api.FileRestApi;
import com.clickclap.rest.api.FlowRestApi;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.api.MessageRestApi;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.api.VoteRestApi;
import com.clickclap.util.GsonHelper;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.retrofit.GsonRetrofitObjectPersisterFactory;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.lang.reflect.Type;

import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedInput;

/**
 * A pre-set, easy to use, retrofit service. It will use retrofit for network
 * requests and both networking and caching will use Gson. To use it, just add
 * to your manifest :
 * <p/>
 * <pre>
 * &lt;service
 *   android:name="com.octo.android.robospice.retrofit.AppRetrofitSpiceService"
 *   android:exported="false" /&gt;
 * </pre>
 *
 * @author SNI
 */
public class AppRetrofitSpiceService extends RetrofitSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(DatabaseRestApi.class);
        addRetrofitInterface(UserRestApi.class);
        addRetrofitInterface(FlowRestApi.class);
        addRetrofitInterface(VideoRestApi.class);
        addRetrofitInterface(GrimaceRestApi.class);
        addRetrofitInterface(MessageRestApi.class);
        addRetrofitInterface(VoteRestApi.class);
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        cacheManager.addPersister(new GsonRetrofitObjectPersisterFactory(application, getConverter(), getCacheFolder()));
        return cacheManager;
    }

    @Override
    protected Converter createConverter() {
        return new GsonConverter(GsonHelper.GSON);
    }

    public File getCacheFolder() {
        return null;
    }

    public static class TestConverter extends GsonConverter {
        private final Gson gson;

        public TestConverter() {
            super(GsonHelper.GSON);
            this.gson = GsonHelper.GSON;
        }

        @Override
        public Object fromBody(TypedInput body, Type type) throws ConversionException {
            String data;
            try {
                data = IOUtils.toString(body.in());
                Log.i("API: Response", data);
                JsonElement jsonElement = new JsonParser().parse(data);
                if (jsonElement instanceof JsonObject) {
                    JsonObject jsonObject = (JsonObject) jsonElement;
                    if (jsonObject.has("response")) {
                        jsonElement = jsonObject.get("response");
                    }
                }
                return gson.fromJson(jsonElement, type);
            } catch (Exception ex) {
                throw new ConversionException(ex);
            }
        }
    }

    @Override
    protected String getServerUrl() {
        return Const.BASE_API_URL;
    }
}
