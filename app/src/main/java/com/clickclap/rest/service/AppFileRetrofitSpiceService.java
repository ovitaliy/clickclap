package com.clickclap.rest.service;

import android.app.Application;

import com.clickclap.Const;
import com.clickclap.rest.api.DatabaseRestApi;
import com.clickclap.rest.api.FileRestApi;
import com.clickclap.rest.api.FlowRestApi;
import com.clickclap.rest.api.GrimaceRestApi;
import com.clickclap.rest.api.MessageRestApi;
import com.clickclap.rest.api.UserRestApi;
import com.clickclap.rest.api.VideoRestApi;
import com.clickclap.rest.api.VoteRestApi;
import com.clickclap.util.GsonHelper;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.retrofit.GsonRetrofitObjectPersisterFactory;

import java.io.File;

import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;

/**
 * A pre-set, easy to use, retrofit service. It will use retrofit for network
 * requests and both networking and caching will use Gson. To use it, just add
 * to your manifest :
 * <p/>
 * <pre>
 * &lt;service
 *   android:name="com.octo.android.robospice.retrofit.AppRetrofitSpiceService"
 *   android:exported="false" /&gt;
 * </pre>
 *
 * @author SNI
 */
public class AppFileRetrofitSpiceService extends RetrofitSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(FileRestApi.class);
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        cacheManager.addPersister(new GsonRetrofitObjectPersisterFactory(application, getConverter(), getCacheFolder()));
        return cacheManager;
    }

    @Override
    protected Converter createConverter() {
        return new GsonConverter(GsonHelper.GSON);
    }

    public File getCacheFolder() {
        return null;
    }

    @Override
    protected String getServerUrl() {
        return Const.MEDIA_URL;
    }
}
