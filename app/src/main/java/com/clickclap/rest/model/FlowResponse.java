package com.clickclap.rest.model;

import com.clickclap.model.Video;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class FlowResponse extends BaseResponse {
    @SerializedName("flow")
    ArrayList<Video> mFlow;

    public ArrayList<Video> getFlow() {
        return mFlow;
    }
}