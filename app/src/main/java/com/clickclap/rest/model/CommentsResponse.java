package com.clickclap.rest.model;

import com.clickclap.model.Comment;
import com.clickclap.model.Video;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class CommentsResponse extends BaseResponse {
    @SerializedName("comment")
    List<Comment> mComments;

    public List<Comment> getComments() {
        return mComments;
    }
}