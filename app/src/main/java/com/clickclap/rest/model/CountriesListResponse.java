package com.clickclap.rest.model;

import com.clickclap.model.Country;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class CountriesListResponse extends BaseResponse {
    @SerializedName("countries")
    Country[] mCountries;

    @SerializedName("count")
    int mCount;

    public Country[] getCountries() {
        return mCountries;
    }

    public void setCountries(Country[] countries) {
        mCountries = countries;
    }

    public int getCount() {
        return mCount;
    }
}