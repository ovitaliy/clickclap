package com.clickclap.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddVideoResponse extends BaseResponse {
    @SerializedName("id_video")
    int mVideoId;

    @SerializedName("short_url")
    String mShortUrl;

    @SerializedName("coins_change")
    int mCoinsChange;

    @SerializedName("coins")
    int mCoins;

    public int getCoins() {
        return mCoins;
    }

    public int getCoinsChange() {
        return mCoinsChange;
    }

    public String getShortUrl() {
        return mShortUrl;
    }

    public int getVideoId() {
        return mVideoId;
    }
}