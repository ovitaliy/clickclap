package com.clickclap.rest.model;

import com.clickclap.model.City;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class CitiesListResponse extends BaseResponse {
    @SerializedName("cities")
    City[] mCities;

    @SerializedName("count")
    int mCount;

    public City[] getCities() {
        return mCities;
    }

    public void setCities(City[] cities) {
        mCities = cities;
    }

    public int getCount() {
        return mCount;
    }
}