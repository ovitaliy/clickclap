package com.clickclap.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddGrimaceResponse extends BaseResponse {
    @SerializedName("id")
    int mId;

    public int getId() {
        return mId;
    }
}