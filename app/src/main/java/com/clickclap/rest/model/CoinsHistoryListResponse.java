package com.clickclap.rest.model;

import com.clickclap.model.Balance;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class CoinsHistoryListResponse extends BaseResponse {
    @SerializedName("balance")
    int mBalance;

    @SerializedName("history")
    List<Balance> mHistory;

    public int getBalance() {
        return mBalance;
    }

    public List<Balance> getHistory() {
        return mHistory;
    }
}
