package com.clickclap.rest.model;

import com.clickclap.model.Category;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class CategoriesListResponse extends BaseResponse {
    @SerializedName("categories")
    List<Category> mCategories;

    @SerializedName("count")
    int mCount;

    public List<Category> getCategories() {
        return mCategories;
    }

    public int getCount() {
        return mCount;
    }
}
