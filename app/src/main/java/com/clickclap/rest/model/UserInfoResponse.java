package com.clickclap.rest.model;

import com.clickclap.model.UserInfo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class UserInfoResponse extends BaseResponse {

    @SerializedName("user")
    UserInfo mUser;

    public UserInfo getUser() {
        return mUser;
    }
}