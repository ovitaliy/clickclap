package com.clickclap.rest.model;

import com.clickclap.model.FeedbackMessage;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class MessagesResponse extends BaseResponse {
    @SerializedName("messages")
    List<FeedbackMessage> mMessages;

    public List<FeedbackMessage> getMessages() {
        return mMessages;
    }
}