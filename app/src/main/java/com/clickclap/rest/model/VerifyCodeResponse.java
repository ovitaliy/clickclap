package com.clickclap.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class VerifyCodeResponse extends BaseResponse {
    @SerializedName("status")
    int mStatus;

    @SerializedName("id_user")
    int mUserId;

    public int getStatus() {
        return mStatus;
    }

    public int getUserId() {
        return mUserId;
    }
}