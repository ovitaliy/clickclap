package com.clickclap.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class BaseCoinsResponse extends BaseResponse {
    @SerializedName("coins_change")
    int mCoinsChange;

    public int getCoinsChange() {
        return mCoinsChange;
    }
}