package com.clickclap.rest.model;

import com.clickclap.model.Grimace;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GrimacesListResponse extends BaseResponse {
    @SerializedName("grimace")
    List<Grimace> mGrimaces;

    public List<Grimace> getGrimaces() {
        return mGrimaces;
    }
}