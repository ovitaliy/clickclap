package com.clickclap.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddGrimaceFileResponse extends BaseResponse {
    @SerializedName("media")
    String mMedia;

    public String getMedia() {
        return mMedia;
    }
}