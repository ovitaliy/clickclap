package com.clickclap.rest.api;

import com.clickclap.rest.model.CitiesListResponse;
import com.clickclap.rest.model.CountriesListResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface DatabaseRestApi {
    @POST("/database.getCountries")
    CountriesListResponse getCountries(@Body Map<String, String> options);

    @POST("/database.getCities")
    CitiesListResponse getCities(@Body Map<String, String> options);
}
