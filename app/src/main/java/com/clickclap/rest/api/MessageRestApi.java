package com.clickclap.rest.api;

import com.clickclap.rest.model.AddMessageResponse;
import com.clickclap.rest.model.MessageListsResponse;
import com.clickclap.rest.model.MessagesResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public interface MessageRestApi {

    @POST("/message.get")
    MessagesResponse getMessages(@Body Map<String, String> options);

    @POST("/message.getList")
    MessageListsResponse getMessageLists(@Body Map<String, String> options);

    @POST("/message.add")
    AddMessageResponse addMessage(@Body Map<String, String> options);
}