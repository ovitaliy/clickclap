package com.clickclap.rest.api;

import com.clickclap.rest.model.CategoriesListResponse;
import com.clickclap.rest.model.FlowResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface FlowRestApi {

    @POST("/flow.get")
    FlowResponse getFlow(@Body Map<String, String> options);

    @POST("/flow.getCategories")
    CategoriesListResponse getCategories(@Body Map<String, String> options);
}