package com.clickclap.rest.api;

import com.clickclap.rest.model.AddVideoResponse;
import com.clickclap.rest.model.BaseCoinsResponse;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.CommentsResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface VideoRestApi {

    @POST("/cc/video.add")
    AddVideoResponse addVideo(@Body Map<String, String> options);

    @POST("/video.share")
    BaseResponse shareVideo(@Body Map<String, String> options);

    @POST("/video.complaint")
    BaseResponse complaintVideo(@Body Map<String, String> options);

    @POST("/video.guess")
    BaseResponse guessVideoSmile(@Body Map<String, String> options);

    @POST("/video.pay")
    BaseCoinsResponse payForVideo(@Body Map<String, String> options);

    @POST("/video.mark")
    BaseResponse markVideo(@Body Map<String, String> options);

    @POST("/video.update")
    BaseResponse updateVideo(@Body Map<String, String> options);

    @POST("/video.getComments")
    CommentsResponse getComments(@Body Map<String, String> options);

    @POST("/video.comment")
    BaseResponse commentVideo(@Body Map<String, String> options);
}