package com.clickclap.rest.api;

import com.clickclap.rest.model.AuthResponse;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.CoinsHistoryListResponse;
import com.clickclap.rest.model.UserInfoResponse;
import com.clickclap.rest.model.UserRatingResponse;
import com.clickclap.rest.model.UserSearchResponse;
import com.clickclap.rest.model.VerifyCodeResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface UserRestApi {

    @POST("/user.add")
    AuthResponse auth(@Body Map<String, String> options);

    @POST("/user.get")
    UserInfoResponse getUserInfo(@Body Map<String, String> options);

    @POST("/user.edit")
    BaseResponse editUserInfo(@Body Map<String, String> options);

    @POST("/user.token")
    BaseResponse sendToken(@Body Map<String, String> options);

    @POST("/user.follow")
    BaseResponse followUser(@Body Map<String, String> options);

    @POST("/user.unfollow")
    BaseResponse unfollowUser(@Body Map<String, String> options);

    @POST("/user.coinsHistory")
    CoinsHistoryListResponse getCoinsHistory(@Body Map<String, String> options);

    @POST("/user.search")
    UserSearchResponse searchUser(@Body Map<String, String> options);

    @POST("/user.getCode")
    BaseResponse getSmsCode(@Body Map<String, String> options);

    @POST("/user.verify")
    VerifyCodeResponse verifySmsCode(@Body Map<String, String> options);

    @POST("/user.rating")
    UserRatingResponse getRating(@Body Map options);
}
