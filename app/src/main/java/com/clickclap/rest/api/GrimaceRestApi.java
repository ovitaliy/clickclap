package com.clickclap.rest.api;

import com.clickclap.rest.model.AddGrimaceResponse;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.GrimacesListResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public interface GrimaceRestApi {

    /**
     * this method calls from {@link com.clickclap.rest.service.AppFileRetrofitSpiceService}, that's why added api-app-name ("cc") to request
     */
    @POST("/cc/grimace.add")
    AddGrimaceResponse addGrimace(@Body Map<String, String> options);

    @POST("/grimace.get")
    GrimacesListResponse getGrimaces(@Body Map<String, String> options);

    @POST("/grimace.buy")
    BaseResponse buyGrimace(@Body Map<String, String> options);

    @POST("/grimace.selling")
    BaseResponse setSellingGrimace(@Body Map<String, String> options);

    @POST("/grimace.share")
    BaseResponse shareGrimace(@Body Map<String, String> options);
}