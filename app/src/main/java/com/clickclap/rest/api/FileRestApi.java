package com.clickclap.rest.api;

import com.clickclap.rest.model.AddGrimaceFileResponse;

import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public interface FileRestApi {

    @Multipart
    @POST("/grimace/")
    AddGrimaceFileResponse uploadGrimaceFile(@Part("file") TypedFile file);

    @Multipart
    @POST("/upload/")
    AddGrimaceFileResponse uploadMediaFile(@Part("file") TypedFile file);
}