package com.clickclap.rest.api;

import com.clickclap.model.TypedJsonString;
import com.clickclap.rest.model.AddVoteResponse;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.model.VoteListsResponse;
import com.clickclap.rest.model.VoteResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public interface VoteRestApi {

    @POST("/vote.add")
    AddVoteResponse addVote(@Body TypedJsonString options);

    @POST("/vote.vote")
    BaseResponse sendVote(@Body Map<String, String> options);

    @POST("/vote.getList")
    VoteListsResponse getVoteList(@Body Map<String, String> options);

    @POST("/vote.my")
    VoteListsResponse getMyList(@Body Map<String, String> options);

    @POST("/vote.get")
    VoteResponse getVote(@Body Map<String, String> options);
}