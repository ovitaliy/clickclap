package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.events.VideoUploadedEvent;
import com.clickclap.model.Grimace;
import com.clickclap.rest.model.GrimacesListResponse;
import com.clickclap.util.FilePathHelper;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GetGrimacesCacheRequestListener extends BaseRequestListener<GrimacesListResponse> {
    public static final String TAG = "GrimacesCacheListener";

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final GrimacesListResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    App.getInstance().getContentResolver().delete(
                            ContentDescriptor.Grimaces.URI,
                            ContentDescriptor.Grimaces.Cols.TYPE + " = " + Grimace.TYPE_COLLECTION,
                            null);

                    List<Grimace> list = response.getGrimaces();

                    if (list != null) {
                        int count = list.size();
                        ContentValues[] contentValueses = new ContentValues[count];

                        if (!FilePathHelper.getGrimacesPath().exists()) {
                            FilePathHelper.getGrimacesPath().mkdirs();
                        }

                        for (int i = 0; i < count; i++) {
                            Grimace grimace = list.get(i);
                            ContentValues values = grimace.toContentValues();
                            values.put(ContentDescriptor.Grimaces.Cols.TYPE, Grimace.TYPE_COLLECTION);
                            contentValueses[i] = values;
                        }
                        App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Grimaces.URI, contentValueses);

                        try {
                            for (int i = 0; i < count; i++) {
                                Grimace grimace = list.get(i);
                                FileOutputStream fos = null;
                                try {
                                    //save bitmap
                                    File file = FilePathHelper.getGrimaceFile(grimace.getUrl());
                                    if (!file.exists()) {
                                        URL url = null;
                                        try {
                                            url = new URL(grimace.getUrl());
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                            continue;
                                        }
                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                        fos = new FileOutputStream(file);
                                        if (bmp != null) {
                                            if (bmp.compress(Bitmap.CompressFormat.PNG, 100, fos)) {
                                                Log.i("GRIMACE SYNC", "Grimace " + grimace.getUrl() + " saved " + file.getPath());
                                            } else {
                                                Log.i("GRIMACE SYNC", "Cant save grimace " + grimace.getUrl());
                                            }
                                        }
                                    }
                                } finally {
                                    if (fos != null) {
                                        fos.close();
                                    }
                                }
                            }

                            // if grimace fragment is open, it catch this event to refresh
                            EventBus.getDefault().post(new VideoUploadedEvent());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();

        }
    }
}
