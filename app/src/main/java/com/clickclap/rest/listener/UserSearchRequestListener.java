package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.text.TextUtils;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Hobby;
import com.clickclap.enums.Interest;
import com.clickclap.enums.Language;
import com.clickclap.enums.Pet;
import com.clickclap.enums.Religion;
import com.clickclap.enums.Sport;
import com.clickclap.model.City;
import com.clickclap.model.Country;
import com.clickclap.model.UserFinded;
import com.clickclap.rest.ArgsMap;
import com.clickclap.rest.model.UserSearchResponse;
import com.clickclap.util.LocationHelper;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class UserSearchRequestListener extends BaseRequestListener<UserSearchResponse> {
    private ArgsMap mParams;
    private boolean mSelectMatches;

    public UserSearchRequestListener(ArgsMap params, boolean selectMatches) {
        mParams = params;
        mSelectMatches = selectMatches;
    }

    @Override
    public void onRequestSuccess(UserSearchResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {
            App.getInstance().getContentResolver().delete(ContentDescriptor.Users.URI, null, null);

            List<UserFinded> list = response.getUsers();

            if (list == null) {
                return;
            }

            ContentValues[] contentValues = new ContentValues[list.size()];
            for (int i = 0; i < list.size(); i++) {
                UserFinded user = list.get(i);

                if (mSelectMatches) {
                    String requiredFirstName = mParams.get("first_name");
                    if (!TextUtils.isEmpty(requiredFirstName)) {
                        requiredFirstName = requiredFirstName.toLowerCase();
                        if (user.getFirstName() != null && !TextUtils.isEmpty(user.getFirstName().value)) {
                            if (user.getFirstName().value.toLowerCase().contains(requiredFirstName)) {
                                user.getFirstName().required = true;
                            }
                        }
                    }

                    String requiredLastName = mParams.get("last_name");
                    if (!TextUtils.isEmpty(requiredLastName)) {
                        requiredLastName = requiredLastName.toLowerCase();
                        if (user.getLastName() != null && !TextUtils.isEmpty(user.getLastName().value)) {
                            if (user.getLastName().value.toLowerCase().contains(requiredLastName)) {
                                user.getLastName().required = true;
                            }
                        }
                    }

                    String requiredCountryId = mParams.get("id_country");
                    if (requiredCountryId != null) {
                        if (user.getIdCountry() != null && !TextUtils.isEmpty(user.getIdCountry().value)) {
                            Country country = LocationHelper.getCountry(Integer.parseInt(user.getIdCountry().value));
                            if (country != null) {
                                if (country.getId() == Integer.parseInt(requiredCountryId)) {
                                    user.getIdCountry().required = true;
                                }
                            }
                        }
                    }

                    String requiredCityId = mParams.get("id_city");
                    if (requiredCityId != null) {
                        if (requiredCityId != null) {
                            if (user.getIdCity() != null && !TextUtils.isEmpty(user.getIdCity().value)) {
                                City city = LocationHelper.getCity(App.getInstance(), Integer.parseInt(user.getIdCity().value));
                                if (city != null) {
                                    if (city.getId() == Integer.parseInt(requiredCityId)) {
                                        user.getIdCity().required = true;
                                    }
                                }
                            }
                        }
                    }

                    String requiredLang = mParams.get("lang");
                    if (user.getLang() != null && !TextUtils.isEmpty(user.getLang().value)) {
                        Language lang = Language.getFromString(user.getLang().value);
                        if (!TextUtils.isEmpty(requiredLang)) {
                            Language reqLang = Language.getFromString(requiredLang);
                            if (reqLang != null) {
                                if (reqLang.equals(lang)) {
                                    user.getLang().required = true;
                                }
                            }
                        }
                    }

                    String requiredCraft = mParams.get("craft");
                    if (!TextUtils.isEmpty(requiredCraft)) {
                        requiredCraft = requiredCraft.toLowerCase();
                        if (user.getCraft() != null && !TextUtils.isEmpty(user.getCraft().value)) {
                            if (user.getCraft().value.toLowerCase().contains(requiredCraft)) {
                                user.getCraft().required = true;
                            }
                        }
                    }

                    String requiredHobby = mParams.get("hobbie");
                    if (requiredHobby != null) {
                        if (user.getHobby() != null && !TextUtils.isEmpty(user.getHobby().value)) {
                            Hobby hobby = Hobby.getById(user.getHobby().value);
                            if (hobby != null) {
                                if (hobby.getId() == Integer.parseInt(requiredHobby)) {
                                    user.getHobby().required = true;
                                }
                            }
                        }
                    }

                    String requiredInterest = mParams.get("interest");
                    if (requiredInterest != null) {
                        if (user.getInterest() != null && !TextUtils.isEmpty(user.getInterest().value)) {
                            Interest interest = Interest.getById(user.getInterest().value);
                            if (interest != null) {
                                if (interest.getId() == Integer.parseInt(requiredInterest)) {
                                    user.getInterest().required = true;
                                }
                            }
                        }
                    }

                    String requiredSport = mParams.get("sport");
                    if (requiredSport != null) {
                        if (user.getSport() != null && !TextUtils.isEmpty(user.getSport().value)) {
                            Sport sport = Sport.getById(user.getSport().value);
                            if (sport != null) {
                                if (sport.getId() == Integer.parseInt(requiredSport)) {
                                    user.getSport().required = true;
                                }
                            }
                        }
                    }

                    String requiredPet = mParams.get("pets");
                    if (requiredPet != null) {
                        if (user.getPets() != null && !TextUtils.isEmpty(user.getPets().value)) {
                            Pet pet = Pet.getById(user.getPets().value);
                            if (pet != null) {
                                if (pet.getId() == Integer.parseInt(requiredPet)) {
                                    user.getPets().required = true;
                                }
                            }
                        }
                    }

                    String requiredReligion = mParams.get("religion");
                    if (requiredReligion != null) {
                        if (user.getReligion() != null && !TextUtils.isEmpty(user.getReligion().value)) {
                            Religion religion = Religion.getById(user.getReligion().value);
                            if (religion != null) {
                                if (religion.getId() == Integer.parseInt(requiredReligion)) {
                                    user.getReligion().required = true;
                                }
                            }
                        }
                    }
                }
                ContentValues values = user.toContentValues();
                values.put(ContentDescriptor.Users.Cols.RECORD_TYPE, ContentDescriptor.Users.RECORD_TYPE_SEARCH);
                contentValues[i] = values;
            }
            App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Users.URI, contentValues);
        }

    }
}