package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.City;
import com.clickclap.rest.model.CitiesListResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.List;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class CitiesRequestListener extends BaseRequestListener<CitiesListResponse> {
    public static final String TAG = "CitiesListener";
    public int responseCountryId;

    public CitiesRequestListener(int countryId) {
        responseCountryId = countryId;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final CitiesListResponse response) {
        super.onRequestSuccess(response);
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                if (response != null) {
                    List<City> cities = response.getCities();
                    if (cities != null) {
                        int count = cities.size();
                        ContentValues[] contentValues = new ContentValues[count];
                        for (int i = 0; i < count; i++) {
                            City city = cities.get(i);
                            contentValues[i] = city.toContentValues();
                        }

                        App.getInstance().getContentResolver().delete(ContentDescriptor.Cities.URI,
                                null,
                                null);

                        App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Cities.URI,
                                contentValues);
                    }
                }
            }
        }).start();*/
    }
}
