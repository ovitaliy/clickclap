package com.clickclap.rest.listener;

import android.util.Log;

import com.clickclap.Const;
import com.clickclap.rest.model.AuthResponse;
import com.clickclap.util.PrefHelper;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class AuthRequestListener extends BaseRequestListener<AuthResponse> {
    public static final String TAG = "AuthListener";

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(AuthResponse response) {
        super.onRequestSuccess(response);
        if (response != null) {
            String token = response.getToken();
            Log.i(TAG, "token: " + token);
            PrefHelper.setStringPref(Const.PREF_TOKEN, token);
        }
    }
}