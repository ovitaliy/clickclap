package com.clickclap.rest.listener;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.events.OnGetFlowEvent;
import com.clickclap.model.Video;
import com.clickclap.rest.model.FlowResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class FlowRequestListener extends BaseRequestListener<FlowResponse> {
    public static final String TAG = "FlowListener";
    private int mScreen;
    private boolean mPagination;

    public FlowRequestListener(int screen, boolean isPagination) {
        mScreen = screen;
        mPagination = isPagination;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final FlowResponse response) {
        super.onRequestSuccess(response);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (response != null) {
                    Context context = App.getInstance();
                    ArrayList<Video> videos = new ArrayList<>();
                    if (!mPagination) {
                        context.getContentResolver().delete(
                                ContentDescriptor.Videos.URI,
                                ContentDescriptor.Videos.Cols.SCREEN + " = " + mScreen,
                                null);
                    }

                    ArrayList<Video> list = response.getFlow();

                    if (list != null) {
                        int count = list.size();

                        final ContentResolver contentResolver = context.getContentResolver();

                        ContentValues[] contentValueses = new ContentValues[count];
                        for (int i = 0; i < count; i++) {
                            Video video = list.get(i);
                            videos.add(video);
                            ContentValues values = video.toContentValues();
                            values.put(ContentDescriptor.Videos.Cols.SCREEN, mScreen);
                            contentValueses[i] = values;
                        }
                        contentResolver.bulkInsert(ContentDescriptor.Videos.URI, contentValueses);

                        EventBus.getDefault().post(new OnGetFlowEvent(list));
                    }
                }
            }
        }).start();
    }
}
