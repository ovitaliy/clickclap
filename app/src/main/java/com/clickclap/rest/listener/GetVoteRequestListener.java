package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.VoteOption;
import com.clickclap.rest.model.VoteResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVoteRequestListener extends BaseRequestListener<VoteResponse> {
    public static final String TAG = "GetVoteListener";
    int mVoteId;

    public GetVoteRequestListener(int id) {
        mVoteId = id;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(VoteResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {
            VoteResponse.Vote vote = response.getVote();
            List<VoteOption> optionsList = vote.getOptions();
            int totalVotes = 0;
            if (optionsList != null) {
                int count = optionsList.size();

                ContentValues[] contentValues = new ContentValues[count];
                for (int i = 0; i < count; i++) {
                    VoteOption option = optionsList.get(i);
                    totalVotes += option.getVotes();
                    ContentValues values = option.toContentValues();
                    values.put(ContentDescriptor.VoteOptions.Cols.VOTE_ID, mVoteId);
                    contentValues[i] = values;
                }

                App.getInstance().getContentResolver().delete(ContentDescriptor.VoteOptions.URI,
                        ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVoteId, null);
                App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.VoteOptions.URI,
                        contentValues);
            }

            ContentValues voteValues = new ContentValues(5);
            voteValues.put(ContentDescriptor.Votes.Cols.DESCR_ENG, vote.getDescriptionEng());
            voteValues.put(ContentDescriptor.Votes.Cols.DESCR_RUS, vote.getDescriptionRus());
            voteValues.put(ContentDescriptor.Votes.Cols.DESCR_ESP, vote.getDescriptionEsp());
            voteValues.put(ContentDescriptor.Votes.Cols.MY_VOTE, vote.getMyVote());
            voteValues.put(ContentDescriptor.Votes.Cols.VOTES_TOTAL, totalVotes);

            App.getInstance().getContentResolver().update(ContentDescriptor.Votes.URI,
                    voteValues,
                    ContentDescriptor.Votes.Cols.ID + " = " + mVoteId,
                    null);
        }
    }
}
