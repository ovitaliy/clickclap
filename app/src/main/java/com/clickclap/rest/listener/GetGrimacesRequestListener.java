package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.Grimace;
import com.clickclap.rest.model.GrimacesListResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GetGrimacesRequestListener extends BaseRequestListener<GrimacesListResponse> {
    public static final String TAG = "GetGrimacesListener";
    private int mType;

    public GetGrimacesRequestListener(int type) {
        mType = type;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final GrimacesListResponse response) {
        super.onRequestSuccess(response);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (response != null && response.isSuccess()) {
                    App.getInstance().getContentResolver().delete(ContentDescriptor.Grimaces.URI,
                            ContentDescriptor.Grimaces.Cols.TYPE + " = " + mType, null);

                    List<Grimace> list = response.getGrimaces();

                    if (list != null) {
                        int count = list.size();
                        ContentValues[] contentValueses = new ContentValues[count];

                        for (int i = 0; i < count; i++) {
                            ContentValues values = list.get(i).toContentValues();
                            values.put(ContentDescriptor.Grimaces.Cols.TYPE, mType);
                            contentValueses[i] = values;
                        }

                        App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Grimaces.URI, contentValueses);
                    }
                }
            }
        }).start();
    }
}
