package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class SetSellingGrimaceRequestListener extends BaseRequestListener<BaseResponse> {
    public static final String TAG = "SellingGrimaceListener";
    int mGrimaceId;
    boolean mStatus;

    public SetSellingGrimaceRequestListener(int grimaceId, boolean status) {
        mGrimaceId = grimaceId;
        mStatus = status;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(BaseResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {
            ContentValues values = new ContentValues(1);
            values.put(ContentDescriptor.Grimaces.Cols.SELLING, mStatus ? 1 : 0);
            App.getInstance().getContentResolver().update(ContentDescriptor.Grimaces.URI,
                    values,
                    ContentDescriptor.Grimaces.Cols.ID + " = " + mGrimaceId,
                    null);
        }
    }

}
