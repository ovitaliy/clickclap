package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.clickclap.model.Vote;
import com.clickclap.rest.model.AddVoteResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddVoteRequestListener extends BaseRequestListener<AddVoteResponse> {
    public static final String TAG = "AddVoteListener";
    String mDescription;
    List<String> mOptions;

    public AddVoteRequestListener(String description, List<String> options) {
        mDescription = description;
        mOptions = options;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(AddVoteResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {
            int voteId = response.getId();

            if (voteId == 0) {
                Cursor cursor = App.getInstance().getContentResolver().query(ContentDescriptor.Votes.URI, new String[]{"_id"}, null, null, "_id DESC");
                try {
                    if (cursor.moveToFirst()) {
                        voteId = cursor.getInt(0) + 100;
                    }
                } finally {
                    cursor.close();
                }
            }

            Language lang = AppUser.get().getLanguage();

            ContentValues values = new ContentValues();
            values.put(ContentDescriptor.Votes.Cols.ID, voteId);
            values.put(ContentDescriptor.Votes.Cols.TYPE, -1);
            values.put(ContentDescriptor.Votes.Cols.STATUS, Vote.CREATED);
            switch (lang) {
                case RUS:
                    values.put(ContentDescriptor.Votes.Cols.DESCR_RUS, mDescription);
                    values.put(ContentDescriptor.Votes.Cols.TITLE_RUS, mDescription);
                    break;
                case ENG:
                    values.put(ContentDescriptor.Votes.Cols.DESCR_ENG, mDescription);
                    values.put(ContentDescriptor.Votes.Cols.TITLE_ENG, mDescription);
                    break;
                case ESP:
                    values.put(ContentDescriptor.Votes.Cols.DESCR_ESP, mDescription);
                    values.put(ContentDescriptor.Votes.Cols.TITLE_ESP, mDescription);
                    break;
            }

            App.getInstance().getContentResolver().insert(ContentDescriptor.Votes.URI, values);

            if (mOptions != null) {
                values.clear();
                for (int i = 0; i < mOptions.size(); i++) {
                    String option = mOptions.get(i);
                    values.put(ContentDescriptor.VoteOptions.Cols.VOTE_ID, voteId);
                    values.put(ContentDescriptor.VoteOptions.Cols.OPTION_ID, i);
                    values.put(ContentDescriptor.VoteOptions.Cols.TITLE_ENG, option);
                    values.put(ContentDescriptor.VoteOptions.Cols.TITLE_RUS, option);
                    values.put(ContentDescriptor.VoteOptions.Cols.TITLE_ESP, option);
                    App.getInstance().getContentResolver().insert(ContentDescriptor.VoteOptions.URI, values);
                }
            }
        }
    }
}
