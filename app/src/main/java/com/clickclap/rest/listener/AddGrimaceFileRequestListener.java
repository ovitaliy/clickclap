package com.clickclap.rest.listener;

import com.clickclap.rest.model.AddGrimaceFileResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddGrimaceFileRequestListener extends BaseRequestListener<AddGrimaceFileResponse> {
    public static final String TAG = "AddGrimaceFileListener";

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
    }

    @Override
    public void onRequestSuccess(AddGrimaceFileResponse response) {
        super.onRequestSuccess(response);
    }
}
