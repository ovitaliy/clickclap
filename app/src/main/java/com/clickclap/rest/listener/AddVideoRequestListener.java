package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.VideoInfo;
import com.clickclap.rest.model.AddVideoResponse;
import com.clickclap.service.UploadMediaService;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddVideoRequestListener extends BaseRequestListener<AddVideoResponse> {
    public static final String TAG = "AddVideoListener";
    OnAddedVideoListener mListener;
    VideoInfo mVideoInfo;
    Boolean mClearCache;

    public AddVideoRequestListener(VideoInfo videoInfo, OnAddedVideoListener listener) {
        mListener = listener;
        mVideoInfo = videoInfo;
    }

    public AddVideoRequestListener(VideoInfo videoInfo, OnAddedVideoListener listener, boolean clearCache) {
        mListener = listener;
        mVideoInfo = videoInfo;
        mClearCache = clearCache;
    }

    public AddVideoRequestListener(VideoInfo videoInfo) {
        mVideoInfo = videoInfo;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(AddVideoResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response != null) {
            if (response.isSuccess()) {

                Context context = App.getInstance();

                if (mVideoInfo.getLocalId() > 0) {
                    context.getContentResolver().delete(ContentDescriptor.Videos.URI, "_id = " + mVideoInfo.getLocalId(), null);
                }

                int videoId = response.getVideoId();

                boolean clearCacheAfterUpload = mClearCache != null ? mClearCache : TextUtils.isEmpty(AppUser.get().getPhoto());//if this first video, we should preserve cache to use it for registration

                context.startService(UploadMediaService.newIntent(context, videoId, mVideoInfo.getVideoPath(), mVideoInfo.getImagePath(), clearCacheAfterUpload));
                if (mVideoInfo.getReplyToVideoId() > 0) {
                    Cursor cursor = context.getContentResolver().query(ContentDescriptor.Videos.URI,
                            new String[]{ContentDescriptor.Videos.Cols.REPLIES},
                            ContentDescriptor.Videos.Cols.ID + " = " + mVideoInfo.getReplyToVideoId(),
                            null,
                            null);

                    int repliesCount = 1;//new reply we just added
                    try {
                        if (cursor.moveToFirst()) {
                            repliesCount += cursor.getInt(0);
                        }
                    } finally {
                        cursor.close();
                    }

                    ContentValues values = new ContentValues(1);
                    values.put(ContentDescriptor.Videos.Cols.REPLIES, repliesCount);
                    context.getContentResolver().update(ContentDescriptor.Videos.URI, values, ContentDescriptor.Videos.Cols.ID + " = " + mVideoInfo.getReplyToVideoId(), null);
                }

                if (mListener != null) {
                    mListener.onAddedVideo(videoId, response.getShortUrl());
                }
            }
        }
    }

    public interface OnAddedVideoListener {
        void onAddedVideo(int videoId, String shortUrl);
    }
}
