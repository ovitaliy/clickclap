package com.clickclap.rest.listener;

import android.content.ContentValues;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class BuyGrimaceRequestListener extends BaseRequestListener<BaseResponse> {
    public static final String TAG = "BuyGrimaceListener";
    int mGrimaceId;

    public BuyGrimaceRequestListener(int grimaceId) {
        mGrimaceId = grimaceId;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
    }

    @Override
    public void onRequestSuccess(BaseResponse response) {
        super.onRequestSuccess(response);
        ContentValues values = new ContentValues(1);
        if (response != null && response.isSuccess()) {
            values.put(ContentDescriptor.Grimaces.Cols.BOUGHT, 1);
        } else {
            values.put(ContentDescriptor.Grimaces.Cols.BOUGHT, 0);
        }
        App.getInstance().getContentResolver().update(ContentDescriptor.Grimaces.URI,
                values,
                ContentDescriptor.Grimaces.Cols.ID + " = " + mGrimaceId,
                null);
    }

}
