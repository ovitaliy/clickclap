package com.clickclap.rest.listener;

import android.support.annotation.CallSuper;
import android.util.Log;

import com.clickclap.enums.ServerError;
import com.clickclap.events.ServerErrorEvent;
import com.clickclap.rest.model.BaseResponse;
import com.octo.android.robospice.exception.NetworkException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;

import retrofit.RetrofitError;

/**
 * Created by denisvasilenko on 17.09.15.
 */
public class BaseRequestListener<T extends BaseResponse> implements RequestListener<T> {
    @Override
    @CallSuper
    public void onRequestFailure(SpiceException spiceException) {
        String message;
        if (spiceException != null) {
            message = spiceException.getMessage();
        } else {
            message = "";
        }
        if (spiceException instanceof NetworkException) {
            NetworkException exception = (NetworkException) spiceException;
            if (exception.getCause() instanceof RetrofitError) {
                RetrofitError error = (RetrofitError) exception.getCause();
                int httpErrorCode = error.getResponse().getStatus();
                if (httpErrorCode == 401) {
                    EventBus.getDefault().post(new ServerErrorEvent(new int[]{ServerError.WRONG_ACCESS_TOKEN.getCode()}, null));
                }
                return;
            }
        }
        Log.e("SERVER", "Request failure: " + message);
    }

    @Override
    @CallSuper
    public void onRequestSuccess(T response) {
        Log.v("SERVER", "Success");
        if (response.isError()) {
            EventBus.getDefault().post(new ServerErrorEvent(response.getErrorCodes(), response.getErrorMessages()));
        }
    }
}
