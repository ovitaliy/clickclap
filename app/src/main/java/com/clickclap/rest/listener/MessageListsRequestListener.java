package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.Feedback;
import com.clickclap.rest.model.MessageListsResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class MessageListsRequestListener extends BaseRequestListener<MessageListsResponse> {
    public static final String TAG = "MessageListsListener";

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(MessageListsResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {
            List<Feedback> list = response.getLists();
            if (list != null) {
                int count = list.size();

                ArrayList<ContentValues> contentValues = new ArrayList<>(count);
                for (int i = 0; i < count; i++) {
                    Feedback feedback = list.get(i);
                    if (feedback.getId() == 0)
                        continue;
                    ContentValues values = feedback.toContentValues();
                    contentValues.add(values);
                }

                App.getInstance().getContentResolver().delete(ContentDescriptor.Feedback.URI,
                        null, null);
                App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Feedback.URI,
                        contentValues.toArray(new ContentValues[contentValues.size()]));
            }
        }
    }
}
