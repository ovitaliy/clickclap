package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.text.TextUtils;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.model.FeedbackMessage;
import com.clickclap.rest.model.MessagesResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class MessagesRequestListener extends BaseRequestListener<MessagesResponse> {
    public static final String TAG = "MessagesListener";
    private int mListId;

    public MessagesRequestListener(int listId) {
        mListId = listId;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(MessagesResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {
            List<FeedbackMessage> list = response.getMessages();
            if (list != null) {
                int count = list.size();

                ContentValues[] contentValues = new ContentValues[count];
                for (int i = 0; i < count; i++) {
                    FeedbackMessage message = list.get(i);
                    if (!TextUtils.isEmpty(message.getMessage())) {
                        ContentValues values = message.toContentValues();
                        values.put(ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK, mListId);
                        contentValues[i] = values;
                    }
                }

                App.getInstance().getContentResolver().delete(ContentDescriptor.FeedbackMessages.URI,
                        ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK + " = " + mListId, null);
                App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.FeedbackMessages.URI,
                        contentValues);
            }
        }
    }
}
