package com.clickclap.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.VoteListType;
import com.clickclap.model.Vote;
import com.clickclap.rest.model.VoteListsResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVoteListRequestListener extends BaseRequestListener<VoteListsResponse> {
    public static final String TAG = "GetVoteListListener";
    VoteListType mType;

    public GetVoteListRequestListener(VoteListType type) {
        mType = type;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(VoteListsResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {
            List<Vote> voteList = response.getVotes();
            if (voteList != null) {
                int count = voteList.size();

                int typeId;
                if (mType != null) {
                    typeId = mType.ordinal();
                } else {
                    typeId = -1;
                }

                ContentValues[] contentValues = new ContentValues[count];
                for (int i = 0; i < count; i++) {
                    ContentValues values = voteList.get(i).toContentValues();
                    values.put(ContentDescriptor.Votes.Cols.TYPE, typeId);
                    contentValues[i] = values;
                }

                App.getInstance().getContentResolver().delete(ContentDescriptor.Votes.URI,
                        ContentDescriptor.Votes.Cols.TYPE + " = " + typeId, null);
                App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Votes.URI,
                        contentValues);
            }
        }
    }
}
