package com.clickclap.util;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;

import com.clickclap.App;
import com.clickclap.Const;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by ovitali on 23.12.2014.
 */
public class FilePathHelper implements Const {
    public static File getCacheDir() {
        Context context = App.getInstance();
        File directory = context.getExternalFilesDir(null);
        if (directory == null) {
            directory = context.getFilesDir();
        }
        if (!directory.exists())
            try {
                FileUtils.forceMkdir(directory);
            } catch (IOException e) {
                e.printStackTrace();
            }

        return directory;
    }

    public static File getGrimacesPath() {
        return new File(Environment.getExternalStorageDirectory().getPath() + "/grimaces/");
    }

    public static File getGrimaceFile(String url) {
        String name = String.valueOf(url.hashCode());
        return new File(getGrimacesPath(), name.replace("-", "") + ".png");
    }

    public static File getAudioTmpFile() {
        File file = new File(getCacheDir(), "audio_tmp.mp3");
        return file;
    }

    public static File getVideoTmpFile() {
        File file = new File(getCacheDir(), "video_tmp.mp4");
        return file;
    }

    public static File getImageTmpFile() {
        File file = new File(getCacheDir(), "image_tmp.png");
        return file;
    }

    public static File getVideoTmpFile2() {
        File file = new File(getCacheDir(), "video_tmp_2.mp4");
        return file;
    }

    public static File getVideoTmpFile3() {
        File file = new File(getCacheDir(), "video_tmp_3.mp4");
        return file;
    }

    public static String getVideoFrameImagePath(int id) {
        return getVideoFrameImage(id).getAbsolutePath();
    }

    public static File getVideoFrameImage(int id) {
        File folder = getVideoFrameFolder();
        File file = new File(folder, String.format("frame%d.png", id));
        return file;
    }

    public static File getVideoPreviewImage() {
        File folder = getCacheDir();
        File file = new File(folder, "video_preview.png");
        return file;
    }

    public static File getVideoPreviewImageSmall() {
        File folder = getCacheDir();
        File file = new File(folder, "video_preview_small.png");
        return file;
    }

    public static String getVideoPreviewImagePath() {
        return getVideoPreviewImage().getAbsolutePath();
    }

    public static String getGameVideoImagePath(String videoUrl) {
        return new File(getVideoCacheDirectory(), getFileName(videoUrl) + ".png").getAbsolutePath();
    }

    public static String getVideoPreviewImageSmallPath() {
        return getVideoPreviewImageSmall().getAbsolutePath();
    }

    public static File getVideoFilteredImagePreviewFolder() {
        File folder = new File(getCacheDir(), "filters");
        if (!folder.exists())
            folder.mkdirs();
        return folder;
    }

    public static File getFilteredImagePreview(String filterName) {
        File file = new File(getVideoFilteredImagePreviewFolder(), filterName + ".png");
        return file;
    }


    public static File getAudioStreamFile() {
        File file = new File(getCacheDir(), "audio_tmp_2.aac");
        return file;
    }

    public static File getVideoFrameFolder() {
        File folder = new File(getCacheDir(), "frames");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getFilterFolder() {
        return new File(getVideoFrameFolder(), "filters");
    }

    public static File getGrimaceTempFolder() {
        File folder = new File(getCacheDir(), "grimaces");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getVideoCacheDirectory() {
        File folder = new File(getCacheDir(), "videoCache");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getUploadDirectory() {
        File folder = new File(getCacheDir(), "upload");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getAudioDirectory() {
        File folder = getCacheDir();
        File file = new File(folder, DIR_AUDIO);
        return file;
    }

    public static String getFileName(@NonNull String fileName) {
        int endIndex = fileName.lastIndexOf(".");
        int startIndex = fileName.lastIndexOf("/") + 1;

        if (endIndex > startIndex)
            return fileName.substring(startIndex, endIndex).replaceAll("[^A-Za-z0-9]", "");
        else
            return fileName.substring(startIndex).replaceAll("[^A-Za-z0-9]", "");
    }

    public static String getVideoFileName(@NonNull String fileName) {
        String ext = fileName.substring(fileName.lastIndexOf("."));

        return getFileName(fileName) + ext;
    }
}
