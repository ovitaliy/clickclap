package com.clickclap.util;

import com.clickclap.enums.BalanceChangeType;
import com.clickclap.enums.Education;
import com.clickclap.enums.Hobby;
import com.clickclap.enums.Interest;
import com.clickclap.enums.Job;
import com.clickclap.enums.Language;
import com.clickclap.enums.Pet;
import com.clickclap.enums.Religion;
import com.clickclap.enums.Side;
import com.clickclap.enums.Sport;
import com.clickclap.model.Category;
import com.clickclap.model.UserFinded;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created by Denis on 03.03.2015.
 */
public class GsonHelper {

    public static final Gson GSON;

    static {
        GSON = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateAsSecondsDeserializer())
                .registerTypeAdapter(Date.class, new DateAsSecondsSerializer())
                .registerTypeAdapter(BalanceChangeType.class, new BalanceChangeType.Deserializer())
                .registerTypeAdapter(Boolean.class, new BooleanDeserializer())
                .registerTypeAdapter(Boolean.class, new BooleanSerializer())
                .registerTypeAdapter(Language.class, new LanguageDeserializer())
                .registerTypeAdapter(Language.class, new LanguageSerializer())
                .registerTypeAdapter(Side.class, new SideDeserializer())
                .registerTypeAdapter(Hobby.class, new HobbyDeserializer())
                .registerTypeAdapter(Hobby.class, new HobbySerializer())
                .registerTypeAdapter(Interest.class, new InterestDeserializer())
                .registerTypeAdapter(Interest.class, new InterestSerializer())
                .registerTypeAdapter(Sport.class, new SportDeserializer())
                .registerTypeAdapter(Sport.class, new SportSerializer())
                .registerTypeAdapter(Religion.class, new ReligionDeserializer())
                .registerTypeAdapter(Religion.class, new ReligionSerializer())
                .registerTypeAdapter(Pet.class, new PetDeserializer())
                .registerTypeAdapter(Pet.class, new PetSerializer())
                .registerTypeAdapter(Category.class, new Category.Deserializer())
                .registerTypeAdapter(Education.class, new EducationDeserializer())
                .registerTypeAdapter(Education.class, new EducationSerializer())
                .registerTypeAdapter(Job.class, new JobDeserializer())
                .registerTypeAdapter(Job.class, new JobSerializer())
                .registerTypeAdapter(UserFinded.ResultField.class, new UserFinded.ResultField.Deserializer())
                .create();

    }

    public static class DateAsSecondsDeserializer implements JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            long seconds = element.getAsLong();
            return new Date(seconds * 1000);
        }
    }

    public static class DateAsSecondsSerializer implements JsonSerializer<Date> {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getTime() / 1000);
        }
    }

    public static class BooleanDeserializer implements JsonDeserializer<Boolean> {
        @Override
        public Boolean deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            if (element.isJsonNull()) {
                return false;
            } else {
                int value = element.getAsInt();
                return value == 1;
            }
        }
    }

    public static class BooleanSerializer implements JsonSerializer<Boolean> {

        @Override
        public JsonElement serialize(Boolean src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src ? 1 : 0);
        }
    }

    public static class LanguageDeserializer implements JsonDeserializer<Language> {
        @Override
        public Language deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String lang = element.getAsString();
            return Language.getFromString(lang);
        }
    }

    public static class LanguageSerializer implements JsonSerializer<Language> {

        @Override
        public JsonElement serialize(Language src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getSuffix());
        }
    }

    public static class SideDeserializer implements JsonDeserializer<Side> {
        @Override
        public Side deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String side = element.getAsString();
            if ("left".equals(side)) {
                return Side.LEFT;
            } else {
                return Side.RIGHT;
            }
        }
    }

    public static class HobbyDeserializer implements JsonDeserializer<Hobby> {
        @Override
        public Hobby deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String id = element.getAsString();
            return Hobby.getById(id);
        }
    }

    public static class HobbySerializer implements JsonSerializer<Hobby> {

        @Override
        public JsonElement serialize(Hobby src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getId());
        }
    }

    public static class ReligionDeserializer implements JsonDeserializer<Religion> {
        @Override
        public Religion deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String id = element.getAsString();
            return Religion.getById(id);
        }
    }

    public static class ReligionSerializer implements JsonSerializer<Religion> {

        @Override
        public JsonElement serialize(Religion src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getId());
        }
    }

    public static class PetDeserializer implements JsonDeserializer<Pet> {
        @Override
        public Pet deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String id = element.getAsString();
            return Pet.getById(id);
        }
    }

    public static class PetSerializer implements JsonSerializer<Pet> {

        @Override
        public JsonElement serialize(Pet src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getId());
        }
    }

    public static class SportDeserializer implements JsonDeserializer<Sport> {
        @Override
        public Sport deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String id = element.getAsString();
            return Sport.getById(id);
        }
    }

    public static class SportSerializer implements JsonSerializer<Sport> {

        @Override
        public JsonElement serialize(Sport src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getId());
        }
    }

    public static class InterestDeserializer implements JsonDeserializer<Interest> {
        @Override
        public Interest deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String id = element.getAsString();
            return Interest.getById(id);
        }
    }

    public static class InterestSerializer implements JsonSerializer<Interest> {

        @Override
        public JsonElement serialize(Interest src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getId());
        }
    }

    public static class JobDeserializer implements JsonDeserializer<Job> {
        @Override
        public Job deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String id = element.getAsString();
            return Job.getById(id);
        }
    }

    public static class JobSerializer implements JsonSerializer<Job> {

        @Override
        public JsonElement serialize(Job src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getId());
        }
    }

    public static class EducationDeserializer implements JsonDeserializer<Education> {
        @Override
        public Education deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String id = element.getAsString();
            return Education.getById(id);
        }
    }

    public static class EducationSerializer implements JsonSerializer<Education> {

        @Override
        public JsonElement serialize(Education src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getId());
        }
    }

}