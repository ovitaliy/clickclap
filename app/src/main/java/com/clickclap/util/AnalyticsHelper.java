package com.clickclap.util;

import android.content.Context;
import android.support.annotation.StringDef;

import com.appsflyer.AppsFlyerLib;
import com.clickclap.App;
import com.clickclap.AppUser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Deni on 15.10.2015.
 */
public final class AnalyticsHelper {
    private static final String TAG = "AnalyticsHelper";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            ENTER_IN_APP,
            EMOTICON_CREATE,
            CODE_REQUEST,
            NUMBER_VERIFICATION,
            FILL_PROFILE_BASE,
            FILL_PROFILE_EXTENDED,
            VIDEO_RECORDING,
            ANSWER_RECORDING,
            GRIMACE_CREATE,
            BUY_WIDGET,
            TOKEN_SEND,
            SHARE,
            TASK_CREATE,
            TASK_COMPLETE,
            POLL_CREATE,
            POLL_PARTICIPATE,
            CROWD_CREATE,
            CROWD_DONATE,
            FEEDBACK_SEND
    })
    public @interface TrackEventTitle {
    }

    public static void init(Context context) {
        Mixpanel.init(context);
        AppsFlyer.init(context);
    }

    public static void identifyUser() {
        Mixpanel.identifyUser();
        AppsFlyer.identifyUser();
    }

    public static void trackEvent(Context context, @TrackEventTitle String event) {
        Mixpanel.trackEvent(event);
        AppsFlyer.trackEvent(context, event);
    }

    public static final String ENTER_IN_APP = "Вход в приложение";
    public static final String EMOTICON_CREATE = "Creation an emoticon";
    public static final String CODE_REQUEST = "Запрос кода";
    public static final String NUMBER_VERIFICATION = "Подтверждение номера";
    public static final String FILL_PROFILE_BASE = "Заполнение базовой анкеты";
    public static final String FILL_PROFILE_EXTENDED = "Заполнение расширенной анкеты";
    public static final String VIDEO_RECORDING = "Съемка видео";
    public static final String ANSWER_RECORDING = "Съемка ответа";
    public static final String GRIMACE_CREATE = "Съемка гримасы";
    public static final String BUY_WIDGET = "Покупка виджета";
    public static final String TOKEN_SEND = "Отправка токена";
    public static final String SHARE = "Шер видео/гримасы";
    public static final String TASK_CREATE = "Создание задания";
    public static final String TASK_COMPLETE = "Выполнение задания";
    public static final String POLL_CREATE = "Создание голосования";
    public static final String POLL_PARTICIPATE = "Участие в голосовании";
    public static final String CROWD_CREATE = "Создание крауд видео";
    public static final String CROWD_DONATE = "Пожертвование на крауд видео";
    public static final String FEEDBACK_SEND = "Отправка сообщения в обратной связи";

    public static class AppsFlyer {
        static final String TOKEN = "uy8XpfD8rwR9CNWeuzZAKc";

        public static void init(Context context) {
            AppsFlyerLib.setAppsFlyerKey(TOKEN);
            AppsFlyerLib.sendTracking(context.getApplicationContext());
        }

        public static void identifyUser() {
            if (AppUser.get() != null) {
                AppsFlyerLib.setCustomerUserId(String.valueOf(AppUser.getUid()));
            }
        }

        public static void trackEvent(Context context, @TrackEventTitle String event) {
            if (context != null) {
                AppsFlyerLib.trackEvent(context.getApplicationContext(), event, null);
            }
        }
    }

    public static class Mixpanel {
        static MixpanelAPI mMixpanel;
        static final String TOKEN = "f6ad06c615aa6a04a8e902c9d1a71e35";

        public static void init(Context context) {
            mMixpanel = MixpanelAPI.getInstance(context, TOKEN);
        }

        public static void identifyUser() {
            if (AppUser.get() != null) {
                MixpanelAPI mixpanelAPI = getInstance();
                mixpanelAPI.identify(String.valueOf(AppUser.getUid()));
                mixpanelAPI.getPeople().identify(String.valueOf(AppUser.getUid()));
                mixpanelAPI.getPeople().set("$first_name", AppUser.get().getFName());
                mixpanelAPI.getPeople().set("$last_name", AppUser.get().getLName());
                mixpanelAPI.getPeople().set("App ID", AppUser.get().getId());
            }
        }

        private static synchronized MixpanelAPI getInstance() {
            if (mMixpanel == null) {
                init(App.getInstance());
            }
            return mMixpanel;
        }

        public static void trackEvent(@TrackEventTitle String event) {
            getInstance().track(event);
        }
    }
}
