package com.clickclap.util;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

/**
 * Created by michael on 05.12.14.
 */
public class PhoneTextListener implements TextWatcher {

    private View mButton;
    private EditText mPhoneField;

    private int mBeforeTextLength, mOffset;


    public PhoneTextListener() {
        mBeforeTextLength = 0;
        mOffset = 0;
    }

    public void setViews(View btn, EditText field) {
        mButton = btn;
        mPhoneField = field;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        mBeforeTextLength = s.length();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mBeforeTextLength<s.length()) { //if new char added
            onCharAdded(s);
        } else { //if last char deleted
            onCharDeleted(s);
        }
    }


    private void onCharAdded(Editable s) {
        if (s.length()==1) { //on the first input
            //choose ukr or rus template
            if (s.charAt(0)=='3') {
                mOffset = 1;
            } else {
                mOffset = 0;
            }
            mPhoneField.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16+mOffset)});
            mPhoneField.removeTextChangedListener(this);
            s.insert(0,"+");
            mPhoneField.addTextChangedListener(this);
            mButton.setEnabled(true);
        } else {
            switch (s.length()-mOffset) {
                case 3:
                    mPhoneField.removeTextChangedListener(this);
                    s.insert(s.length()-1,"(");
                    mPhoneField.addTextChangedListener(this);
                    break;
                case 7:
                    mPhoneField.removeTextChangedListener(this);
                    s.insert(s.length()-1,") ");
                    mPhoneField.addTextChangedListener(this);
                    break;
                case 12:
                    mPhoneField.removeTextChangedListener(this);
                    s.insert(s.length()-1,"-");
                    mPhoneField.addTextChangedListener(this);
                    break;
            }
        }
    }

    private void onCharDeleted(Editable s) {
        if (s.length()==1) {
            s.clear();
            mOffset = 0;
            mButton.setEnabled(false);
        } else {
            switch (s.length()-mOffset) {
                case 3:
                    mPhoneField.removeTextChangedListener(this);
                    s.delete(s.length()-1,s.length());
                    mPhoneField.addTextChangedListener(this);
                    break;
                case 8:
                    mPhoneField.removeTextChangedListener(this);
                    s.delete(s.length()-2,s.length());
                    mPhoneField.addTextChangedListener(this);
                    break;
                case 12:
                    mPhoneField.removeTextChangedListener(this);
                    s.delete(s.length()-1,s.length());
                    mPhoneField.addTextChangedListener(this);
                    break;
            }
        }
    }
}
