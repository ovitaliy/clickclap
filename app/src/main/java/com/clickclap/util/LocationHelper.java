package com.clickclap.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.util.SparseArray;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.clickclap.model.City;
import com.clickclap.model.Country;
import com.clickclap.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis on 07.05.2015.
 */
public class LocationHelper {
    private static SparseArray<Country> sCountries = new SparseArray<>();
    private static SparseArray<City> sCities = new SparseArray<>();

    public static void update(SparseArray<Country> countries) {
        sCountries = countries;
    }

    private static void loadCountries() {
        ContentResolver contentResolver = App.getInstance().getContentResolver();
        String[] projection = new String[4];
        projection[0] = ContentDescriptor.Countries.Cols.ID;
        if (AppUser.get() != null) {
            Language lang = AppUser.get().getLanguage();
            switch (lang) {
                case RUS:
                    projection[1] = ContentDescriptor.Countries.Cols.NAME_RUS;
                    break;
                case ENG:
                    projection[1] = ContentDescriptor.Countries.Cols.NAME_ENG;
                    break;
                case ESP:
                    projection[1] = ContentDescriptor.Countries.Cols.NAME_ESP;
                    break;
            }
            projection[2] = ContentDescriptor.Countries.Cols.PHONE_CODE;
            projection[3] = ContentDescriptor.Countries.Cols.PHONE_LENGHT;

            Cursor cursor = contentResolver.query(ContentDescriptor.Countries.URI,
                    projection,
                    null,
                    null,
                    null);

            try {
                if (cursor.moveToFirst()) {
                    do {
                        Country country = Country.fromCursor(cursor);
                        sCountries.put(country.getId(), country);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    public static Country getCountry(int countryId) {
        if (sCountries == null || sCountries.size() == 0) {
            loadCountries();
        }

        return sCountries.get(countryId);
    }

    public static List<Country> getCountriesList() {
        if (sCountries == null || sCountries.size() == 0) {
            loadCountries();
        }
        List<Country> arrayList = new ArrayList<>(sCountries.size());
        for (int i = 0; i < sCountries.size(); i++)
            arrayList.add(sCountries.valueAt(i));
        return arrayList;
    }

    public static City getCity(Context context, int cityId) {
        ContentResolver contentResolver = context.getContentResolver();
        if (sCities.get(cityId) == null) {
            String[] projection = new String[2];
            projection[0] = ContentDescriptor.Cities.Cols.ID;
            Language lang = AppUser.get() != null ? AppUser.get().getLanguage() : Language.getSystem();
            switch (lang) {
                case RUS:
                    projection[1] = ContentDescriptor.Cities.Cols.NAME_RUS;
                    break;
                case ENG:
                    projection[1] = ContentDescriptor.Cities.Cols.NAME_ENG;
                    break;
                case ESP:
                    projection[1] = ContentDescriptor.Cities.Cols.NAME_ESP;
                    break;
            }

            Cursor cursor = contentResolver.query(ContentDescriptor.Cities.URI,
                    projection,
                    ContentDescriptor.Cities.Cols.ID + " = " + cityId,
                    null,
                    null);

            try {
                if (cursor.moveToFirst()) {
                    City city = City.fromCursor(cursor);
                    sCities.put(city.getId(), city);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        return sCities.get(cityId);
    }

    public static List<City> getCities(int countryId) {
        List<City> cities = new ArrayList<>();
        ContentResolver contentResolver = App.getInstance().getContentResolver();
        String[] projection = new String[2];
        projection[0] = ContentDescriptor.Cities.Cols.ID;
        Language lang;
        UserInfo user = AppUser.get();
        if (user != null) {
            lang = user.getLanguage();
        } else {
            lang = Language.getSystem();
        }
        switch (lang) {
            case RUS:
                projection[1] = ContentDescriptor.Cities.Cols.NAME_RUS;
                break;
            case ENG:
                projection[1] = ContentDescriptor.Cities.Cols.NAME_ENG;
                break;
            case ESP:
                projection[1] = ContentDescriptor.Cities.Cols.NAME_ESP;
                break;
        }

        Cursor cursor = contentResolver.query(ContentDescriptor.Cities.URI,
                projection,
                ContentDescriptor.Cities.Cols.COUNTRY_ID + " = " + countryId,
                null,
                null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    City city = City.fromCursor(cursor);
                    sCities.put(city.getId(), city);
                    cities.add(city);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return cities;
    }

    public static synchronized List<City> getCitiesFromServer(int countryId, String name) {
       /* ArgsMap argsMap = new ArgsMap(true);
        argsMap.put("id_country", countryId);

        if (!TextUtils.isEmpty(name)) {
            argsMap.put("lang", Language.getSystem().getRequestParam());
            argsMap.put("q", name);
        }

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new JSONObject(argsMap).toString());
        Request request = new Request.Builder()
                .url(Const.BASE_API_URL + "/database.getCities")
                .post(body)
                .build();

        OkHttpClient client = new OkHttpClient();
        List<City> cities = new ArrayList<>();
        try {
            Response response = client.newCall(request).execute();
            CitiesListResponse listResponse = GsonHelper.GSON.fromJson(response.body().string(), CitiesListResponse.class);
            if (listResponse != null) {
                List<City> list = listResponse.getCities();
                for (City city : list) {
                    city.setCountryId(countryId);
                }
                cities.addAll(listResponse.getCities());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cities;*/
        return null;
    }
}
