package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Deni on 22.06.2015.
 */
public class UserFinded implements Comparable<UserFinded> {
    private static final String REQUIRED_MARK = "*";

    @SerializedName("id")
    private int mId;

    @SerializedName("first_name")
    private ResultField mFirstName;

    @SerializedName("last_name")
    private ResultField mLastName;

    @SerializedName("photo")
    private String mPhoto;

    @SerializedName("photo_200")
    private String mPhoto200;

    @SerializedName("photo_100")
    private String mPhoto100;

    @SerializedName("photo_50")
    private String mPhoto50;

    @SerializedName("id_city")
    private ResultField mIdCity;

    @SerializedName("id_country")
    private ResultField mIdCountry;

    @SerializedName("country_eng")
    private ResultField mCountryEng;

    @SerializedName("country_esp")
    private ResultField mCountryEsp;

    @SerializedName("country_rus")
    private ResultField mCountryRus;

    @SerializedName("city_eng")
    private ResultField mCityEng;

    @SerializedName("city_esp")
    private ResultField mCityEsp;

    @SerializedName("city_rus")
    private ResultField mCityRus;

    @SerializedName("lang")
    private ResultField mLang;

    @SerializedName("hobbie")
    private ResultField mHobby;

    @SerializedName("pets")
    private ResultField mPets;

    @SerializedName("interest")
    private ResultField mInterest;

    @SerializedName("religion")
    private ResultField mReligion;

    @SerializedName("craft")
    private ResultField mCraft;

    @SerializedName("sport")
    private ResultField mSport;

    @SerializedName("tags")
    private ArrayList<ResultField> mTags;

    @SerializedName("rating")
    private int mRating;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public ResultField getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = new ResultField(firstName);
    }

    public ResultField getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = new ResultField(lastName);
    }


    public ResultField getIdCity() {
        return mIdCity;
    }

    public void setIdCity(String idCity) {
        mIdCity = new ResultField(idCity);
    }

    public ResultField getIdCountry() {
        return mIdCountry;
    }

    public void setIdCountry(String idCountry) {
        mIdCountry = new ResultField(idCountry);
    }

    public ResultField getLang() {
        return mLang;
    }

    public void setLang(String lang) {
        mLang = new ResultField(lang);
    }

    public ResultField getHobby() {
        return mHobby;
    }

    public void setHobby(String hobby) {
        mHobby = new ResultField(hobby);
    }

    public ResultField getPets() {
        return mPets;
    }

    public void setPets(String pets) {
        mPets = new ResultField(pets);
    }

    public ResultField getInterest() {
        return mInterest;
    }

    public void setInterest(String interest) {
        mInterest = new ResultField(interest);
    }

    public ResultField getReligion() {
        return mReligion;
    }

    public void setReligion(String religion) {
        mReligion = new ResultField(religion);
    }

    public ResultField getCraft() {
        return mCraft;
    }

    public void setCraft(String craft) {
        mCraft = new ResultField(craft);
    }

    public ArrayList<ResultField> getTags() {
        return mTags;
    }

    public void setTags(ArrayList<String> tags) {
        mTags = new ArrayList<>();
        for (String string : tags) {
            mTags.add(new ResultField(string));
        }
    }

    public void setTags(String tags) {
        if (tags != null) {
            String[] result = tags.split(",");
            setTags(new ArrayList<>(Arrays.asList(result)));
        }
    }

    public ResultField getSport() {
        return mSport;
    }

    public void setSport(String sport) {
        mSport = new ResultField(sport);
    }

    public int getRating() {
        return mRating;
    }

    public void setRating(int rating) {
        mRating = rating;
    }

    public ResultField getCity() {
        switch (Language.getSystem()) {
            case RUS:
                return mCityRus;
            case ESP:
                return mCityEsp;
            default:
                return mCityEng;
        }
    }

    public ResultField getCountry() {
        switch (Language.getSystem()) {
            case RUS:
                return mCountryRus;
            case ESP:
                return mCountryEsp;
            default:
                return mCountryEng;
        }
    }

    @Override
    public int compareTo(UserFinded another) {
        if (getRating() > another.getRating()) {
            return -1;
        } else if (getRating() < another.getRating()) {
            return 1;
        } else {
            return 0;
        }
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public String getPhoto200() {
        return mPhoto200;
    }

    public void setPhoto200(String photo200) {
        mPhoto200 = photo200;
    }

    public String getPhoto100() {
        return mPhoto100;
    }

    public void setPhoto100(String photo100) {
        mPhoto100 = photo100;
    }

    public String getPhoto50() {
        return mPhoto50;
    }

    public void setPhoto50(String photo50) {
        mPhoto50 = photo50;
    }

    public ResultField getCityEng() {
        return mCityEng;
    }

    public void setCityEng(String cityEng) {
        mCityEng = new ResultField(cityEng);
    }

    public ResultField getCityEsp() {
        return mCityEsp;
    }

    public void setCityEsp(String cityEsp) {
        mCityEsp = new ResultField(cityEsp);
    }

    public ResultField getCityRus() {
        return mCityRus;
    }

    public void setCityRus(String cityRus) {
        mCityRus = new ResultField(cityRus);
    }

    public ResultField getCountryRus() {
        return mCountryRus;
    }

    public void setCountryRus(String countryRus) {
        mCountryRus = new ResultField(countryRus);
    }

    public ResultField getCountryEsp() {
        return mCountryEsp;
    }

    public void setCountryEsp(String countryEsp) {
        mCountryEsp = new ResultField(countryEsp);
    }

    public ResultField getCountryEng() {
        return mCountryEng;
    }

    public void setCountryEng(String countryEng) {
        mCountryEng = new ResultField(countryEng);
    }

    public static class ResultField {
        public String value;
        public boolean required;

        ResultField(String field) {
            if (!TextUtils.isEmpty(field)) {
                required = field.contains(REQUIRED_MARK);
                value = field.replace(REQUIRED_MARK, "");
            }
        }

        public String getAsString() {
            return value + (required ? "*" : "");
        }

        public static class Deserializer implements JsonDeserializer<ResultField> {
            @Override
            public ResultField deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                String jsonString = json.getAsString();
                ResultField resultField = new ResultField(jsonString);
                return resultField;
            }
        }
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues(15);
        contentValues.put(ContentDescriptor.Users.Cols.ID, getId());
        contentValues.put(ContentDescriptor.Users.Cols.PHOTO, getPhoto());
        if (getFirstName() != null && !TextUtils.isEmpty(getFirstName().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.FIRST_NAME, getFirstName().getAsString());
        }
        if (getLastName() != null && !TextUtils.isEmpty(getLastName().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.LAST_NAME, getLastName().getAsString());
        }
        if (getCraft() != null && !TextUtils.isEmpty(getCraft().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.CRAFT, getCraft().getAsString());
        }
        if (getHobby() != null && !TextUtils.isEmpty(getHobby().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.HOBBY, getHobby().getAsString());
        }
        if (getInterest() != null && !TextUtils.isEmpty(getInterest().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.INTEREST, getInterest().getAsString());
        }
        if (getIdCity() != null && !TextUtils.isEmpty(getIdCity().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.ID_CITY, getIdCity().getAsString());
        }
        if (getIdCountry() != null && !TextUtils.isEmpty(getIdCountry().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.ID_COUNTRY, getIdCountry().getAsString());
        }
        if (getLang() != null && !TextUtils.isEmpty(getLang().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.LANG, getLang().getAsString());
        }
        if (getPets() != null && !TextUtils.isEmpty(getPets().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.PETS, getPets().getAsString());
        }
        if (getSport() != null && !TextUtils.isEmpty(getSport().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.SPORT, getSport().getAsString());
        }
        if (getReligion() != null && !TextUtils.isEmpty(getReligion().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.RELIGION, getReligion().getAsString());
        }

        if (getCityEng() != null && !TextUtils.isEmpty(getCityEng().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.CITY_ENG, getCityEng().getAsString());
        }

        if (getCityEsp() != null && !TextUtils.isEmpty(getCityEsp().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.CITY_ESP, getCityEsp().getAsString());
        }

        if (getCityRus() != null && !TextUtils.isEmpty(getCityRus().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.CITY_RUS, getCityRus().getAsString());
        }

        if (getCountryEng() != null && !TextUtils.isEmpty(getCountryEng().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.COUNTRY_ENG, getCountryEng().getAsString());
        }

        if (getCountryEsp() != null && !TextUtils.isEmpty(getCountryEsp().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.COUNTRY_ESP, getCountryEsp().getAsString());
        }

        if (getCountryRus() != null && !TextUtils.isEmpty(getCountryRus().value)) {
            contentValues.put(ContentDescriptor.Users.Cols.COUNTRY_RUS, getCountryRus().getAsString());
        }

        contentValues.put(ContentDescriptor.Users.Cols.RATING, getRating());

        if (getTags() != null) {
            ArrayList<ResultField> tags = getTags();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < tags.size(); i++) {
                ResultField tag = tags.get(i);
                stringBuilder.append(tag.getAsString());
                if (i < tags.size() - 1) {
                    stringBuilder.append(",");
                }
            }
            contentValues.put(ContentDescriptor.Users.Cols.TAGS, stringBuilder.toString());
        }
        return contentValues;
    }

    public static UserFinded fromCursor(Cursor cursor) {
        UserFinded user = new UserFinded();
        user.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Users.Cols.ID)));
        user.setPhoto(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.PHOTO)));
        user.setCraft(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CRAFT)));
        user.setFirstName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.FIRST_NAME)));
        user.setLastName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.LAST_NAME)));
        user.setHobby(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.HOBBY)));
        user.setInterest(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.INTEREST)));
        user.setIdCity(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.ID_CITY)));
        user.setIdCountry(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.ID_COUNTRY)));
        user.setTags(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.TAGS)));
        user.setPets(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.PETS)));
        user.setSport(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.SPORT)));
        user.setLang(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.LANG)));
        user.setReligion(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.RELIGION)));
        user.setRating(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Users.Cols.RATING)));
        user.setCountryEng(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.COUNTRY_ENG)));
        user.setCountryEsp(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.COUNTRY_ESP)));
        user.setCountryRus(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.COUNTRY_RUS)));
        user.setCityEng(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CITY_ENG)));
        user.setCityEsp(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CITY_ESP)));
        user.setCityRus(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CITY_RUS)));
        return user;
    }

}
