package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 03.02.2015.
 */
public class VoteOption {
    @SerializedName("id")
    private int mId;

    @SerializedName("option_rus")
    private String mTitleRus;

    @SerializedName("option_eng")
    private String mTitleEng;

    @SerializedName("option_esp")
    private String mTitleEsp;

    @SerializedName("votes")
    private int mVotes;

    private String mTitle;

    private int mVoteId;

    public int getVoteId() {
        return mVoteId;
    }

    public void setVoteId(int voteId) {
        mVoteId = voteId;
    }

    public VoteOption() {
    }

    public VoteOption(int voteId) {
        mVoteId = voteId;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            switch (Language.getSystem()) {
                case ENG:
                    setTitle(mTitleEng);
                    break;
                case RUS:
                    setTitle(mTitleRus);
                    break;
                default:
                    setTitle(mTitleEsp);
                    break;
            }
        }
        return mTitle;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(6);
        values.put(ContentDescriptor.VoteOptions.Cols.OPTION_ID, mId);
        values.put(ContentDescriptor.VoteOptions.Cols.VOTE_ID, mVoteId);
        values.put(ContentDescriptor.VoteOptions.Cols.TITLE_RUS, mTitleRus);
        values.put(ContentDescriptor.VoteOptions.Cols.TITLE_ENG, mTitleEng);
        values.put(ContentDescriptor.VoteOptions.Cols.TITLE_ESP, mTitleEsp);
        values.put(ContentDescriptor.VoteOptions.Cols.VOTES, mVotes);
        return values;
    }

    public static VoteOption fromCursor(Cursor cursor) {
        VoteOption option = new VoteOption();
        option.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VoteOptions.Cols.OPTION_ID)));
        option.setVoteId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VoteOptions.Cols.VOTE_ID)));
        String title;
        Language lang = AppUser.get().getLanguage();
        switch (lang) {
            case ENG:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VoteOptions.Cols.TITLE_ENG));
                break;
            case RUS:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VoteOptions.Cols.TITLE_RUS));
                break;
            default:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VoteOptions.Cols.TITLE_ESP));
                break;
        }
        option.setTitle(title);
        option.setVotes(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VoteOptions.Cols.VOTES)));
        return option;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setVotes(int votes) {
        mVotes = votes;
    }

    public int getVotes() {
        return mVotes;
    }
}
