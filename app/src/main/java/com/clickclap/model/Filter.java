package com.clickclap.model;

import java.io.Serializable;

/**
 * Created by Владимир on 31.10.2014.
 */
public class Filter implements Serializable {
    private String mImagePath;
    private com.clickclap.media.filter.Filter mFilter;

    public Filter() {
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    public com.clickclap.media.filter.Filter getFilter() {
        return mFilter;
    }

    public void setFilter(com.clickclap.media.filter.Filter filter) {
        mFilter = filter;
    }

}
