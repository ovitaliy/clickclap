package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.IntDef;
import android.text.TextUtils;

import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Denis on 28.04.2015.
 */
public class Vote {
    public static final int UNDEFINED = -1;
    public static final int FINISHED = 0;
    public static final int ACTIVE = 1;
    public static final int CREATED = 3;
    public static final int DECLINED = 4;

    @IntDef({UNDEFINED, FINISHED, ACTIVE, CREATED, DECLINED})
    @Retention(RetentionPolicy.SOURCE)
    @interface ListStatus {
    }

    @SerializedName("id")
    private int mId;

    @SerializedName("title_rus")
    private String mTitleRus;

    @SerializedName("title_esp")
    private String mTitleEsp;

    @SerializedName("title_eng")
    private String mTitleEng;

    private String mDescrRus;
    private String mDescrEsp;
    private String mDescrEng;

    private int mTotalVotes;

    @SerializedName("status")
    private int mStatus;

    @SerializedName("my_vote")
    private Integer mMyVote;

    private String mTitle;

    @SerializedName("description")
    private String mDescr;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(12);
        if (TextUtils.isEmpty(mDescrEng) && TextUtils.isEmpty(mDescrEsp) && TextUtils.isEmpty(mDescrEsp)) {
            mDescrEng = mDescr;
            mDescrEsp = mDescr;
            mDescrRus = mDescr;
        }
        values.put(ContentDescriptor.Votes.Cols.ID, mId);
        values.put(ContentDescriptor.Votes.Cols.TITLE_RUS, mTitleRus);
        values.put(ContentDescriptor.Votes.Cols.TITLE_ENG, mTitleEng);
        values.put(ContentDescriptor.Votes.Cols.TITLE_ESP, mTitleEsp);
        values.put(ContentDescriptor.Votes.Cols.DESCR_ENG, mDescrEng);
        values.put(ContentDescriptor.Votes.Cols.DESCR_ESP, mDescrEsp);
        values.put(ContentDescriptor.Votes.Cols.DESCR_RUS, mDescrRus);
        values.put(ContentDescriptor.Votes.Cols.STATUS, mStatus);
        values.put(ContentDescriptor.Votes.Cols.MY_VOTE, mMyVote);
        values.put(ContentDescriptor.Votes.Cols.VOTES_TOTAL, mTotalVotes);
        return values;
    }

    public static Vote fromCursor(Cursor cursor) {
        Vote vote = new Vote();
        vote.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.ID)));
        @ListStatus int status = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.STATUS));
        vote.setStatus(status);
        String title;
        String descr;
        Language lang = Language.getSystem();
        switch (lang) {
            case ENG:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_ENG));
                descr = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_ENG));
                break;
            case RUS:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_RUS));
                descr = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_RUS));
                break;
            default:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_ESP));
                descr = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_ESP));
                break;
        }
        vote.setTitle(title);
        vote.setDescr(descr);
        vote.setMyVote(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.MY_VOTE)));
        vote.setTotalVotes(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.VOTES_TOTAL)));
        return vote;
    }

    public void setStatus(@ListStatus int status) {
        mStatus = status;
    }

    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            switch (Language.getSystem()) {
                case ENG:
                    setTitle(mTitleEng);
                    break;
                case RUS:
                    setTitle(mTitleRus);
                    break;
                default:
                    setTitle(mTitleEsp);
                    break;
            }
        }
        return mTitle;
    }

    public String getDescr() {
        if (TextUtils.isEmpty(mDescr)) {
            switch (Language.getSystem()) {
                case ENG:
                    setTitle(mDescrEng);
                    break;
                case RUS:
                    setTitle(mDescrRus);
                    break;
                default:
                    setTitle(mDescrEsp);
                    break;
            }
        }
        return mDescr;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Boolean isVoted() {
        return mMyVote != null && mMyVote != 0;
    }

    public
    @ListStatus
    int getStatus() {
        return mStatus;
    }

    public void setDescr(String descr) {
        mDescr = descr;
    }

    public int getMyVote() {
        return mMyVote;
    }

    public void setMyVote(int myVote) {
        mMyVote = myVote;
    }

    public void setTotalVotes(int totalVotes) {
        mTotalVotes = totalVotes;
    }

    public int getTotalVotes() {
        return mTotalVotes;
    }
}
