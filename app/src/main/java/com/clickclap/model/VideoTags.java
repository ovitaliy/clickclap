package com.clickclap.model;

/**
 * Created by Владимир on 17.11.2014.
 */
public class VideoTags {
    private Category mCategory;
    private String [] mTags;

    public VideoTags() {
    }

    public Category getCategory() {
        return mCategory;
    }

    public void setCategory(Category category) {
        mCategory = category;
    }

    public String[] getTags() {
        return mTags;
    }

    public void setTags(String[] tags) {
        mTags = tags;
    }
}
