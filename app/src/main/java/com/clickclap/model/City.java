package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.clickclap.view.BaseModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Denis on 07.05.2015.
 */
public class City implements BaseModel {
    @SerializedName("id")
    private int mId;

    @SerializedName("city_rus")
    private String mTitleRus;

    @SerializedName("city_eng")
    private String mTitleEng;

    @SerializedName("city_esp")
    private String mTitleEsp;

    @SerializedName("id_country")
    private int mCountryId;

    private String mTitle;

    public String getTitleRus() {
        return mTitleRus;
    }

    public void setTitleRus(String titleRus) {
        mTitleRus = titleRus;
    }

    public String getTitleEng() {
        return mTitleEng;
    }

    public void setTitleEng(String titleEng) {
        mTitleEng = titleEng;
    }

    public String getTitleEsp() {
        return mTitleEsp;
    }

    public void setTitleEsp(String titleEsp) {
        mTitleEsp = titleEsp;
    }

    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            switch (Language.getSystem()) {
                case RUS:
                    setTitle(mTitleRus);
                    break;
                case ESP:
                    setTitle(mTitleEsp);
                    break;
                case ENG:
                    setTitle(mTitleEng);
                    break;
            }
        }
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContentDescriptor.Cities.Cols.ID, mId);
        contentValues.put(ContentDescriptor.Cities.Cols.NAME_ENG, mTitleEng);
        contentValues.put(ContentDescriptor.Cities.Cols.NAME_RUS, mTitleRus);
        contentValues.put(ContentDescriptor.Cities.Cols.NAME_ESP, mTitleEsp);
        contentValues.put(ContentDescriptor.Cities.Cols.COUNTRY_ID, mCountryId);
        return contentValues;
    }

    public static City fromCursor(Cursor cursor) {
        City country = new City();
        country.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.ID)));

        String title = "";


        switch (Language.getSystem()) {
            case RUS:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.NAME_RUS));
                break;
            case ESP:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.NAME_ESP));
                break;
            case ENG:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.NAME_ENG));
                break;
        }

        country.setTitle(title);
        country.setCountryId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.ID)));

        return country;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }
}
