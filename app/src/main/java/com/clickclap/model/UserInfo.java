package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Education;
import com.clickclap.enums.Hobby;
import com.clickclap.enums.Interest;
import com.clickclap.enums.Job;
import com.clickclap.enums.Language;
import com.clickclap.enums.Pet;
import com.clickclap.enums.Religion;
import com.clickclap.enums.Sport;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michael on 05.11.14.
 */
public class UserInfo implements Serializable, Cloneable {
    public Boolean baseDataAdded = false;

    @SerializedName("id")
    private int id;

    @SerializedName("gender")
    private int mGender = -1;

    @SerializedName("id_country")
    private int mCountryId = -1;

    @SerializedName("id_city")
    private int mCityId = -1;

    @SerializedName("birth_date")
    private long mBirthDate = -1;

    @SerializedName("lang")
    private Language mLanguage;

    @SerializedName("photo")
    private String mPhoto;

    @SerializedName("photo_200")
    private String mPhoto200;

    @SerializedName("photo_100")
    private String mPhoto100;

    @SerializedName("photo_50")
    private String mPhoto50;

    @SerializedName("first_name")
    private String mFName;

    @SerializedName("last_name")
    private String mLName;

    @SerializedName("patronymic")
    private String mPatronymic;

    @SerializedName("degree")
    private Education mDegree;

    @SerializedName("school")
    private String mSchool;

    @SerializedName("university")
    private String mUniversity;

    @SerializedName("job")
    private Job mJobType;

    private long mJobStart, mJobEnd;

    private String mJobTitle;

    @SerializedName("craft")
    private String mCraft;

    @SerializedName("hobbie")
    private Hobby mHobbie;

    @SerializedName("sport")
    private Sport mSport;

    @SerializedName("interest")
    private Interest mInterest;

    @SerializedName("pets")
    private Pet mPet;

    @SerializedName("religion")
    private Religion mReligion;

    @SerializedName("country_rus")
    private String mCountryRus;

    @SerializedName("country_eng")
    private String mCountryEng;

    @SerializedName("country_esp")
    private String mCountryEsp;

    @SerializedName("city_rus")
    private String mCityRus;

    @SerializedName("city_eng")
    private String mCityEng;

    @SerializedName("city_esp")
    private String mCityEsp;

    @SerializedName("coins")
    private int balance;

    @SerializedName("phone")
    private String phone;

    @SerializedName("relationship")
    private int mRelationship;

    @SerializedName("widget")
    private int mWidget;

    @SerializedName("tokens")
    private int mToken;

    @SerializedName("category_stat")
    private Float[] mVideoStatistic;

    @SerializedName("rating")
    private int mRating;

    @SerializedName("my_percent")
    private int mPercent;

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues(20);
        contentValues.put(ContentDescriptor.Users.Cols.ID, getId());
        contentValues.put(ContentDescriptor.Users.Cols.PHOTO, getPhoto());
        contentValues.put(ContentDescriptor.Users.Cols.PHOTO_100, mPhoto100);
        contentValues.put(ContentDescriptor.Users.Cols.FIRST_NAME, getFName());
        contentValues.put(ContentDescriptor.Users.Cols.LAST_NAME, getLName());
        contentValues.put(ContentDescriptor.Users.Cols.CRAFT, getCraft());
        contentValues.put(ContentDescriptor.Users.Cols.HOBBY, getHobbie().getId());
        contentValues.put(ContentDescriptor.Users.Cols.INTEREST, getInterest().getId());
        contentValues.put(ContentDescriptor.Users.Cols.ID_CITY, getCityId());
        contentValues.put(ContentDescriptor.Users.Cols.ID_COUNTRY, getCountryId());
        contentValues.put(ContentDescriptor.Users.Cols.LANG, getLanguage().getId());
        contentValues.put(ContentDescriptor.Users.Cols.PETS, getPet().getId());
        contentValues.put(ContentDescriptor.Users.Cols.SPORT, getSport().getId());
        contentValues.put(ContentDescriptor.Users.Cols.RELIGION, getReligion().getId());
        contentValues.put(ContentDescriptor.Users.Cols.CITY_ENG, getCityEng());
        contentValues.put(ContentDescriptor.Users.Cols.CITY_ESP, getCityEsp());
        contentValues.put(ContentDescriptor.Users.Cols.CITY_RUS, getCityRus());
        contentValues.put(ContentDescriptor.Users.Cols.COUNTRY_ENG, getCountryEng());
        contentValues.put(ContentDescriptor.Users.Cols.COUNTRY_ESP, getCountryEsp());
        contentValues.put(ContentDescriptor.Users.Cols.COUNTRY_RUS, getCountryRus());
        contentValues.put(ContentDescriptor.Users.Cols.RATING, getRating());
        return contentValues;
    }

    public static UserInfo fromCursor(Cursor cursor) {
        UserInfo user = new UserInfo();
        user.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Users.Cols.ID)));
        user.setPhoto(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.PHOTO)));
        user.setPhoto100(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.PHOTO_100)));
        user.setCraft(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CRAFT)));
        user.setFName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.FIRST_NAME)));
        user.setLName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.LAST_NAME)));
        String hobby = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.HOBBY));
        user.setHobbie(Hobby.getById(hobby));
        String interest = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.INTEREST));
        user.setInterest(Interest.getById(interest));
        user.setCityId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Users.Cols.ID_CITY)));
        user.setCountryId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Users.Cols.ID_COUNTRY)));
        String pet = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.PETS));
        user.setPet(Pet.getById(pet));
        String sport = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.SPORT));
        user.setSport(Sport.getById(sport));
        int language = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Users.Cols.LANG));
        user.setLanguage(Language.getById(language));
        String religion = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.RELIGION));
        user.setReligion(Religion.getById(religion));
        user.setRating(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Users.Cols.RATING)));
        user.setCountryEng(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.COUNTRY_ENG)));
        user.setCountryEsp(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.COUNTRY_ESP)));
        user.setCountryRus(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.COUNTRY_RUS)));
        user.setCityEng(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CITY_ENG)));
        user.setCityEsp(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CITY_ESP)));
        user.setCityRus(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Users.Cols.CITY_RUS)));
        return user;
    }

    transient private List<VideoStatistic> mVideoStatisticsList;

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public List<VideoStatistic> getCategoryStatisticList() {
        if (mVideoStatisticsList == null && mVideoStatistic != null) {
            int length = mVideoStatistic.length;
            ArrayList<VideoStatistic> categoryStatistics = new ArrayList<>(length);
            for (int i = 0; i < mVideoStatistic.length; i++) {
                Float seconds = mVideoStatistic[i];
                if (seconds == null) {
                    seconds = 0f;
                }
                VideoStatistic categoryStatistic = new VideoStatistic(i, seconds);
                categoryStatistics.add(categoryStatistic);
            }
            mVideoStatisticsList = categoryStatistics;
        }
        return mVideoStatisticsList;
    }

    public String getFName() {
        return (mFName != null) ? mFName : "";
    }

    public void setFName(String FName) {
        this.mFName = FName;
    }

    public String getLName() {
        return (mLName != null) ? mLName : "";
    }

    public void setLName(String LName) {
        this.mLName = LName;
    }

    public String getPatronymic() {
        return (mPatronymic != null) ? mPatronymic : "";
    }

    public void setPatronymic(String patronymic) {
        mPatronymic = patronymic;
    }

    public String getFullName() {
        String name = getFName();
        if (!TextUtils.isEmpty(mPatronymic)) {
            name += (" " + mPatronymic);
        }
        if (!TextUtils.isEmpty(mLName)) {
            name += (" " + mLName);
        }
        return name;
    }

    public Language getLanguage() {
        if (mLanguage == null) {
            mLanguage = Language.getSystem();
        }
        return mLanguage;
    }

    public void setLanguage(Language language) {
        mLanguage = language;
    }

    public int getGender() {
        return mGender;
    }

    public void setGender(int gender) {
        mGender = gender;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }

    public int getCityId() {
        return mCityId;
    }

    public void setCityId(int cityId) {
        mCityId = cityId;
    }

    public long getBirthDate() {
        return mBirthDate;
    }

    public void setBirthDate(long birthDate) {
        mBirthDate = birthDate;
    }

    public Education getDegree() {
        return mDegree;
    }

    public void setDegree(Education degree) {
        mDegree = degree;
    }

    public String getSchool() {
        return mSchool;
    }

    public void setSchool(String school) {
        mSchool = school;
    }

    public String getUniversity() {
        return mUniversity;
    }

    public void setUniversity(String university) {
        mUniversity = university;
    }

    public Job getJobType() {
        return mJobType;
    }

    public void setJobType(Job jobType) {
        mJobType = jobType;
    }

    public long getJobStart() {
        return mJobStart;
    }

    public void setJobStart(long jobStart) {
        mJobStart = jobStart;
    }

    public long getJobEnd() {
        return mJobEnd;
    }

    public void setJobEnd(long jobEnd) {
        mJobEnd = jobEnd;
    }

    public String getJobTitle() {
        return mJobTitle;
    }

    public void setJobTitle(String jobTitle) {
        mJobTitle = jobTitle;
    }

    public Hobby getHobbie() {
        return mHobbie;
    }

    public void setHobbie(Hobby hobbie) {
        mHobbie = hobbie;
    }

    public Sport getSport() {
        return mSport;
    }

    public void setSport(Sport sport) {
        mSport = sport;
    }

    public Interest getInterest() {
        return mInterest;
    }

    public void setInterest(Interest interest) {
        mInterest = interest;
    }

    public Pet getPet() {
        return mPet;
    }

    public void setPet(Pet pet) {
        mPet = pet;
    }

    public Religion getReligion() {
        return mReligion;
    }

    public void setReligion(Religion religion) {
        mReligion = religion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
     switch (Language.getSystem()) {
            case ENG:
                return mCountryEng;
            case RUS:
                return mCountryRus;
            default:
                return mCountryEsp;
        }
    }

    public void setCountry(String country) {
        switch (Language.getSystem()) {
            case ENG:
                mCountryEng = country;
            case RUS:
                mCountryRus = country;
            default:
                mCountryEsp = country;
        }
    }

    public String getCity() {
        switch (Language.getSystem()) {
            case ENG:
                return mCityEng;
            case RUS:
                return mCityRus;
            default:
                return mCityEsp;
        }
    }

    public void setCity(String city) {
        switch (Language.getSystem()) {
            case ENG:
                mCityEng = city;
            case RUS:
                mCityRus = city;
            default:
                mCityEsp = city;
        }
    }

    public String getCraft() {
        return mCraft;
    }

    public void setCraft(String craft) {
        mCraft = craft;
    }

    public void setCountryRus(String countryRus) {
        mCountryRus = countryRus;
    }

    public void setCountryEng(String countryEng) {
        mCountryEng = countryEng;
    }

    public void setCountryEsp(String countryEsp) {
        mCountryEsp = countryEsp;
    }

    public void setCityRus(String cityRus) {
        mCityRus = cityRus;
    }

    public void setCityEng(String cityEng) {
        mCityEng = cityEng;
    }

    public void setCityEsp(String cityEsp) {
        mCityEsp = cityEsp;
    }

    public String getCityEng() {
        return mCityEng;
    }

    public String getCityEsp() {
        return mCityEsp;
    }

    public String getCityRus() {
        return mCityRus;
    }

    public String getCountryEng() {
        return mCountryEng;
    }

    public String getCountryEsp() {
        return mCountryEsp;
    }

    public String getCountryRus() {
        return mCountryRus;
    }

    public int getRelationship() {
        return mRelationship;
    }

    public void setRelationship(int relationship) {
        mRelationship = relationship;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isWidget() {
        return mWidget == 1;
    }

    public void setWidget(boolean widget) {
        mWidget = widget ? 1 : 0;
    }

    public int getToken() {
        return mToken;
    }

    public void setToken(int token) {
        mToken = token;
    }

    public Float[] getVideoStatistic() {
        return mVideoStatistic;
    }

    public void setCategoriesStat(Float videoStatistic[]) {
        mVideoStatistic = videoStatistic;
    }

    @Override
    public UserInfo clone() {
        UserInfo user = new UserInfo();
        user.setPhoto(getPhoto());
        user.setPhoto50(getPhoto50());
        user.setPhoto100(getPhoto100());
        user.setPhoto200(getPhoto200());
        user.baseDataAdded = baseDataAdded;
        user.setBalance(getBalance());
        user.setBirthDate(getBirthDate());
        user.setCategoriesStat(getVideoStatistic());
        user.setCityId(getCityId());
        user.setCountryId(getCountryId());
        user.setDegree(getDegree());
        user.setFName(getFName());
        user.setLName(getLName());
        user.setLanguage(getLanguage());
        user.setGender(getGender());
        user.setHobbie(getHobbie());
        user.setId(getId());
        user.setInterest(getInterest());
        user.setJobType(getJobType());
        user.setPet(getPet());
        user.setReligion(getReligion());
        user.setSchool(getSchool());
        user.setUniversity(getUniversity());
        user.setSport(getSport());
        user.setCountry(getCountry());
        user.setCity(getCity());
        user.setCraft(getCraft());
        return user;
    }

    public boolean isMe() {
        return getId() == AppUser.getUid();
    }

    public String getPhoto() {
        return mPhoto;
    }

    public String getImageLoaderPhoto() {
        return getImageLoaderPhoto(getAvatarSmall());
    }

    public static String getImageLoaderPhoto(String photo) {
        if (photo != null && !photo.contains("http")) {
            return Uri.fromFile(new File(photo)).toString();//if it local file it should starts with 'file' to manage ImageLoader show it
        }
        return photo;
    }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public String getPhoto200() {
        return mPhoto200;
    }

    public void setPhoto200(String photo200) {
        mPhoto200 = photo200;
    }

    public String getPhoto100() {
        return mPhoto100;
    }

    public void setPhoto100(String photo100) {
        mPhoto100 = photo100;
    }

    public String getPhoto50() {
        return mPhoto50;
    }

    public void setPhoto50(String photo50) {
        mPhoto50 = photo50;
    }

    public String getAvatarSmall() {
        if (!TextUtils.isEmpty(mPhoto100)) {
            if (mPhoto100.endsWith("-100.png")) {
                String fileName = mPhoto100.substring(mPhoto100.lastIndexOf("/" + 1));
                String fixedFileName = "/100" + fileName.replace("-100.png", ".png");
                mPhoto100 = mPhoto100.replace(fileName, fixedFileName);
            }
            return mPhoto100;
        }
        return mPhoto;
    }

    public int getRating() {
        return mRating;
    }

    public void setRating(int rating) {
        mRating = rating;
    }

    public int getPercent() {
        return mPercent;
    }

    public void setPercent(int percent) {
        mPercent = percent;
    }
}