package com.clickclap.model;

import android.text.TextUtils;

import com.clickclap.enums.VideoType;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;

import java.io.Serializable;

/**
 * Created by Владимир on 29.10.2014.
 */
public class VideoInfo implements Serializable, Cloneable {

    private int mLocalId;
    private String mVideoPath;
    private String mImagePath;
    private String mAudioPath;
    private int mSmileId = -1;
    private IFilter mFilter;
    private int mCameraId;
    private int mReplyToVideoId;
    private int mCategoryId;
    private String mTags;

    private boolean mIsAvatar;
    private boolean mIsGrimace;

    private int mCost;

    private VideoType mVideoType;

    private String mOriginalVideoPath;
    private String mOriginalImagePath;

    private IFilter mVideoFilterApplied;
    private IFilter mImageFilterApplied;
    private String mAudioAppliedPath;

    private String mUploadedVideoPath;
    private String mUploadedImagePath;

    private int mWidth;
    private int mHeight;



    public String getOriginalVideoPath() {
        return mOriginalVideoPath;
    }

    public void setOriginalVideoPath(String originalVideoPath) {
        mOriginalVideoPath = originalVideoPath;
        mVideoPath = mOriginalVideoPath;
    }

    public String getOriginalImagePath() {
        return mOriginalImagePath;
    }

    public void setOriginalImagePath(String originalImagePath) {
        mOriginalImagePath = originalImagePath;
        mImagePath = mOriginalImagePath;
    }

    public IFilter getVideoFilterApplied() {
        return mVideoFilterApplied;
    }

    public void setVideoFilterApplied(IFilter videoFilterApplied) {
        mVideoFilterApplied = videoFilterApplied;
    }

    public IFilter getImageFilterApplied() {
        return mImageFilterApplied;
    }

    public void setImageFilterApplied(IFilter videoFilterApplied) {
        mImageFilterApplied = videoFilterApplied;
    }

    public void setAudioApplied(String audioApplied) {
        mAudioAppliedPath = audioApplied;
    }


    public boolean isAudioApplied() {
        return mAudioPath.equals(mAudioAppliedPath);
    }

    public boolean isVideoFilterApplied() {
        return FilterController.areFiltersSame(mFilter, mVideoFilterApplied);
    }

    public boolean isImageFilterApplied() {
        return FilterController.areFiltersSame(mFilter, mImageFilterApplied);
    }

    public int getCameraId() {
        return mCameraId;
    }

    public void setCameraId(int mCameraId) {
        this.mCameraId = mCameraId;
    }

    public IFilter getFilter() {
        return mFilter;
    }

    public void setFilter(IFilter mFilter) {
        this.mFilter = mFilter;
    }

    public VideoInfo(VideoType videoType) {
        mVideoType = videoType;
    }

    public String getAudioPath() {
        return mAudioPath;
    }

    public void setAudioPath(String mAudioPath) {
        this.mAudioPath = mAudioPath;
    }

    public String getVideoPath() {
        if (TextUtils.isEmpty(mVideoPath)) {
            mVideoPath = mOriginalVideoPath;
        }
        return mVideoPath;
    }

    public void setVideoPath(String videoPath) {
        mVideoPath = videoPath;
        if (mVideoPath != null) {
            if (!mVideoPath.equals(videoPath)) {
                mVideoFilterApplied = null;
            }
        } else {
            mVideoFilterApplied = null;
        }
    }

    public String getImagePath() {
        if (TextUtils.isEmpty(mImagePath)) {
            mImagePath = mOriginalImagePath;
        }
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
        if (mImagePath != null) {
            if (!mImagePath.equals(imagePath)) {
                mImageFilterApplied = null;
            }
        } else {
            mImageFilterApplied = null;
        }
    }

    public int getSmileId() {
        return mSmileId;
    }

    public void setSmileId(int smileId) {
        mSmileId = smileId;
    }

    public int getReplyToVideoId() {
        return mReplyToVideoId;
    }

    public void setReplyToVideoId(int replyToVideoId) {
        mReplyToVideoId = replyToVideoId;
    }

    public String getTags() {
        return mTags;
    }

    public void setTags(String tags) {
        mTags = tags;
    }

    @SuppressWarnings("deprecation")
    public VideoType getVideoType() {
        return mVideoType;
    }

    public int getCost() {
        return mCost;
    }

    public void setCost(int cost) {
        mCost = cost;
    }

    public void setVideoType(VideoType videoType) {
        mVideoType = videoType;
    }

    public int getLocalId() {
        return mLocalId;
    }

    public void setLocalId(int localId) {
        mLocalId = localId;
    }


    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int categoryId) {
        mCategoryId = categoryId;
    }


    public boolean isReply() {
        return getReplyToVideoId() > 0;
    }

    public void setSizes(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public String getUploadedVideoPath() {
        return mUploadedVideoPath;
    }

    public void setUploadedVideoPath(String uploadedVideoPath) {
        mUploadedVideoPath = uploadedVideoPath;
    }

    public String getUploadedImagePath() {
        return mUploadedImagePath;
    }

    public void setUploadedImagePath(String uploadedImagePath) {
        mUploadedImagePath = uploadedImagePath;
    }

    public boolean isAvatar() {
        return mIsAvatar;
    }

    public void setAvatar(boolean avatar) {
        mIsAvatar = avatar;
    }

    public boolean isGrimace() {
        return mIsGrimace;
    }

    public void setGrimace(boolean grimace) {
        mIsGrimace = grimace;
    }

    @Override
    public VideoInfo clone() {
        try {
            Object clone = super.clone();
            return (VideoInfo) clone;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
