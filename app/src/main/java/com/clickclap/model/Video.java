package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.IntDef;
import android.text.TextUtils;

import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Date;
import java.util.List;

/**
 * Created by Владимир on 24.11.2014.
 */
public class Video implements Serializable {


    public static final int STATUS_UPLOAD_REQUIRED = 1;
    public static final int STATUS_UPLOADING = -1;
    public static final int STATUS_DONE = 0;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATUS_UPLOAD_REQUIRED, STATUS_UPLOADING, STATUS_DONE})
    public @interface UploadingStatus {
    }


    @SerializedName("id")
    private int mVideoId;

    @SerializedName("author_id")
    private int mAuthorId;

    @SerializedName("id_smile")
    private int mSmileId;

    @SerializedName("guess")
    private Boolean mSmileGuessed;

    @SerializedName("views")
    private int mViews;

    @SerializedName("comments")
    private int mCommentsCount;

    @SerializedName("likes")
    private int mLikes;

    @SerializedName("replays")
    private int mReplays;

    private String mUserEditedComment;

    @SerializedName("id_category")
    private int mCategoryId;

    @SerializedName("id_reply")
    private int mReplyId;

    @SerializedName("length")
    private float mLength;

    @SerializedName("created_at")
    private long mCreatedAt;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("author_nickname")
    private String mAuthorName;

    @SerializedName("author_photo")
    private String mAuthorPhotoUrl;

    @SerializedName("media")
    private String mMedia;

    @SerializedName("short_url")
    private String mShortUrl;

    private String mPath;

    @SerializedName("thumb")
    private String mThumbUrl;

    private String mThumbPath;

    @SerializedName("my_mark")
    private int mMyMark;

    @SerializedName("id_city")
    private int mCityId;

    @SerializedName("id_country")
    private int mCountryId;

    @SerializedName("cost")
    private int mCost;

    @SerializedName("available")
    private Boolean mAvailable;

    @SerializedName("earn")
    private int mEarn;

    @SerializedName("id_game")
    private int mGameId;

    @SerializedName("end")
    private int mEnd;

    @SerializedName("winner")
    private int mWinner;

    @SerializedName("type")
    private int mType;

    @SerializedName("product_type")
    private int mProductType;

    @SerializedName("comment")
    private List<Comment> mComments;

    @SerializedName("city_eng")
    private String mCityEng;

    @SerializedName("city_rus")
    private String mCityRus;

    @SerializedName("city_esp")
    private String mCityEsp;

    @SerializedName("country_eng")
    private String mCountryEng;

    @SerializedName("country_esp")
    private String mCountryEsp;

    @SerializedName("country_rus")
    private String mCountryRus;

    private transient String mCountry;
    private transient String mCity;

    @UploadingStatus
    private int mUploadingStatus;

    private String mUploadedVideoPath;
    private String mUploadedImagePath;

    public Video() {
    }

    public String getCity() {
        if (TextUtils.isEmpty(mCity)) {
            switch (Language.getSystem()) {
                case RUS:
                    mCity = mCityRus;
                    break;
                case ESP:
                    mCity = mCityEsp;
                    break;
                case ENG:
                    mCity = mCityEng;
                    break;
            }
        }
        return mCity;
    }

    public String getCountry() {
        if (TextUtils.isEmpty(mCountry)) {
            switch (Language.getSystem()) {
                case RUS:
                    mCountry = mCountryRus;
                    break;
                case ESP:
                    mCountry = mCountryEsp;
                    break;
                case ENG:
                    mCountry = mCountryEng;
                    break;
            }
        }
        return mCountry;
    }

    public float getLength() {
        return mLength;
    }

    public void setLength(float length) {
        mLength = length;
    }


    public String getUserEditedComment() {
        return mUserEditedComment;
    }

    public void setUserEditedComment(String userEditedComment) {
        mUserEditedComment = userEditedComment;
    }

    public boolean isSmileGuessed() {
        return mSmileGuessed == null ? false : mSmileGuessed;
    }

    public void setSmileGuessed(boolean smileGuessed) {
        mSmileGuessed = smileGuessed;
    }

    public int getVideoId() {
        return mVideoId;
    }

    public void setVideoId(int videoId) {
        mVideoId = videoId;
    }

    public int getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(int authorId) {
        mAuthorId = authorId;
    }

    public int getSmileId() {
        return mSmileId;
    }

    public void setSmileId(int smileId) {
        mSmileId = smileId;
    }

    public int getViews() {
        return mViews;
    }

    public void setViews(int views) {
        mViews = views;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        mCommentsCount = commentsCount;
    }

    public int getLikes() {
        return mLikes;
    }

    public void setLikes(int likes) {
        mLikes = likes;
    }

    public int getReplays() {
        return mReplays;
    }

    public void setReplays(int replays) {
        mReplays = replays;
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int categoryId) {
        mCategoryId = categoryId;
    }

    public int getReplyId() {
        return mReplyId;
    }

    public void setReplyId(int replyId) {
        mReplyId = replyId;
    }

    public long getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(long createdAt) {
        mCreatedAt = createdAt;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getAuthorName() {
        return mAuthorName;
    }

    public void setAuthorName(String authorName) {
        mAuthorName = authorName;
    }

    public String getAuthorPhotoUrl() {
        return mAuthorPhotoUrl;
    }

    public void setAuthorPhotoUrl(String authorPhotoUrl) {
        mAuthorPhotoUrl = authorPhotoUrl;
    }

    public String getMedia() {
        return mMedia;
    }

    public void setMedia(String media) {
        mMedia = media;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getThumbUrl() {
        return mThumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        mThumbUrl = thumbUrl;
    }

    @Deprecated
    public String getThumbPath() {
        return mThumbPath;
    }

    @Deprecated
    public void setThumbPath(String thumbPath) {
        mThumbPath = thumbPath;
    }

    public int getMyMark() {
        return mMyMark;
    }

    public void setMyMark(int myMark) {
        mMyMark = myMark;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ContentDescriptor.Videos.Cols.ID, getVideoId());
        values.put(ContentDescriptor.Videos.Cols.VIDEO_URL, getMedia());
        values.put(ContentDescriptor.Videos.Cols.VIDEO_SHORT_URL, getShortUrl());
        values.put(ContentDescriptor.Videos.Cols.THUMB_URL, getThumbUrl());
        values.put(ContentDescriptor.Videos.Cols.CREATED_AT, getCreatedAt());
        values.put(ContentDescriptor.Videos.Cols.AUTHOR_ID, getAuthorId());
        values.put(ContentDescriptor.Videos.Cols.AUTHOR_NAME, getAuthorName());
        values.put(ContentDescriptor.Videos.Cols.VIEWS, getViews());
        values.put(ContentDescriptor.Videos.Cols.LIKES, getLikes());
        values.put(ContentDescriptor.Videos.Cols.REPLIES, getReplays());
        values.put(ContentDescriptor.Videos.Cols.COMMENTS_COUNT, getCommentsCount());
        values.put(ContentDescriptor.Videos.Cols.SMILE_ID, getSmileId());
        values.put(ContentDescriptor.Videos.Cols.LENGTH, getLength());
        values.put(ContentDescriptor.Videos.Cols.MY_MARK, getMyMark());
        values.put(ContentDescriptor.Videos.Cols.GUESSED, isSmileGuessed());
        values.put(ContentDescriptor.Videos.Cols.CITY_ID, getCityId());
        values.put(ContentDescriptor.Videos.Cols.CITY_ENG, getCityEng());
        values.put(ContentDescriptor.Videos.Cols.CITY_ESP, getCityEsp());
        values.put(ContentDescriptor.Videos.Cols.CITY_RUS, getCityRus());
        values.put(ContentDescriptor.Videos.Cols.COUNTRY_ID, getCountryId());
        values.put(ContentDescriptor.Videos.Cols.COUNTRY_ENG, getCountryEng());
        values.put(ContentDescriptor.Videos.Cols.COUNTRY_ESP, getCountryEsp());
        values.put(ContentDescriptor.Videos.Cols.COUNTRY_RUS, getCountryRus());
        values.put(ContentDescriptor.Videos.Cols.COST, getCost());
        values.put(ContentDescriptor.Videos.Cols.TAGS, getTitle());
        values.put(ContentDescriptor.Videos.Cols.EARN, getEarn());
        values.put(ContentDescriptor.Videos.Cols.GAME_ID, getGameId());
        values.put(ContentDescriptor.Videos.Cols.REPLAY_ID, getReplyId());
        values.put(ContentDescriptor.Videos.Cols.PRODUCT_TYPE, getProductType());
        values.put(ContentDescriptor.Videos.Cols.TYPE, getType());
        values.put(ContentDescriptor.Videos.Cols.CATEGORY_ID, getCategoryId());
        values.put(ContentDescriptor.Videos.Cols.AVAILABLE, isAvailable() ? 1 : 0);
        values.put(ContentDescriptor.Videos.Cols.UPLOADING_STATUS, mUploadingStatus);
        return values;
    }

    public static Video fromCursor(Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.ID));
        String videoUrl = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.VIDEO_URL));
        String videoShortUrl = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.VIDEO_SHORT_URL));
        String imageUrl = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.THUMB_URL));
        long createdAt = cursor.getLong(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CREATED_AT));
        int authorId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.AUTHOR_ID));
        String authorName = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.AUTHOR_NAME));
        int views = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.VIEWS));
        int likes = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.LIKES));
        int replies = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.REPLIES));
        int smileId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.SMILE_ID));
        int commentsCount = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COMMENTS_COUNT));
        int myMark = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.MY_MARK));
        int cityId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CITY_ID));
        String cityRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CITY_RUS));
        String cityEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CITY_ENG));
        String cityEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CITY_ESP));

        int countryId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COUNTRY_ID));
        String countryRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COUNTRY_RUS));
        String countryEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COUNTRY_ENG));
        String countryEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COUNTRY_ESP));

        int cost = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COST));
        int earn = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.EARN));
        int paid = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.AVAILABLE));

        int smileGuessed = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.GUESSED));
        boolean isSmileGuessed = smileGuessed > 0;

        int length = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.LENGTH));
        int gameId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.GAME_ID));
        int replayId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.REPLAY_ID));

        int productType = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.PRODUCT_TYPE));
        int type = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.TYPE));
        int categoryId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CATEGORY_ID));
        String tags = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.TAGS));

        @UploadingStatus int uploadingStatus = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.UPLOADING_STATUS));
        String uploadedVideo = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.UPLOADED_VIDEO));
        String uploadedImage = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.UPLOADED_IMAGE));

        Video video = new Video();
        video.setVideoId(id);
        video.setMedia(videoUrl);
        video.setThumbUrl(imageUrl);
        video.setCreatedAt(createdAt);
        video.setAuthorId(authorId);
        video.setAuthorName(authorName);
        video.setViews(views);
        video.setLikes(likes);
        video.setReplays(replies);
        video.setCommentsCount(commentsCount);
        video.setSmileId(smileId);
        video.setLength(length);
        video.setSmileGuessed(isSmileGuessed);
        video.setMyMark(myMark);
        video.setShortUrl(videoShortUrl);
        video.setCityId(cityId);
        video.setCityEng(cityEng);
        video.setCityEsp(cityEsp);
        video.setCityRus(cityRus);
        video.setCountryId(countryId);
        video.setCountryEsp(countryEsp);
        video.setCountryEng(countryEng);
        video.setCountryRus(countryRus);
        video.setCost(cost);
        video.setAvailable(paid == 1);
        video.setEarn(earn);
        video.setReplyId(replayId);
        video.setTitle(tags);
        video.setGameId(gameId);
        video.setProductType(productType);
        video.setType(type);
        video.setCategoryId(categoryId);

        video.setUploadingStatus(uploadingStatus);
        video.setUploadedVideoPath(uploadedVideo);
        video.setUploadedImagePath(uploadedImage);

        return video;
    }

    public String getShortUrl() {
        return mShortUrl;
    }

    public void setShortUrl(String shortUrl) {
        mShortUrl = shortUrl;
    }

    public int getCityId() {
        return mCityId;
    }

    public void setCityId(int cityId) {
        mCityId = cityId;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }

    public int getCost() {
        return mCost;
    }

    public void setCost(int cost) {
        mCost = cost;
    }

    public Boolean isAvailable() {
        return mAvailable == null ? false : mAvailable;
    }

    public void setAvailable(Boolean available) {
        mAvailable = available;
    }

    public int getEarn() {
        return mEarn;
    }

    public void setEarn(int earn) {
        mEarn = earn;
    }

    public Boolean getSmileGuessed() {
        return mSmileGuessed;
    }

    public void setSmileGuessed(Boolean smileGuessed) {
        mSmileGuessed = smileGuessed;
    }

    public Boolean getAvailable() {
        return mAvailable;
    }

    public int getGameId() {
        return mGameId;
    }

    public void setGameId(int gameId) {
        mGameId = gameId;
    }

    public int getEnd() {
        return mEnd;
    }

    public void setEnd(int end) {
        mEnd = end;
    }

    public int getWinner() {
        return mWinner;
    }

    public void setWinner(int winner) {
        mWinner = winner;
    }

    public int getProductType() {
        return mProductType;
    }

    public void setProductType(int productType) {
        mProductType = productType;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }

    public List<Comment> getComments() {
        return mComments;
    }

    public void setComments(List<Comment> comments) {
        mComments = comments;
    }

    public String getCityEng() {
        return mCityEng;
    }

    public void setCityEng(String cityEng) {
        mCityEng = cityEng;
    }

    public String getCityRus() {
        return mCityRus;
    }

    public void setCityRus(String cityRus) {
        mCityRus = cityRus;
    }

    public String getCityEsp() {
        return mCityEsp;
    }

    public void setCityEsp(String cityEsp) {
        mCityEsp = cityEsp;
    }

    public String getCountryEng() {
        return mCountryEng;
    }

    public void setCountryEng(String countryEng) {
        mCountryEng = countryEng;
    }

    public String getCountryEsp() {
        return mCountryEsp;
    }

    public void setCountryEsp(String countryEsp) {
        mCountryEsp = countryEsp;
    }

    public String getCountryRus() {
        return mCountryRus;
    }

    public void setCountryRus(String countryRus) {
        mCountryRus = countryRus;
    }

    @UploadingStatus
    public int getUploadingStatus() {
        return mUploadingStatus;
    }

    public void setUploadingStatus(@UploadingStatus int uploadingStatus) {
        mUploadingStatus = uploadingStatus;
    }

    public String getUploadedVideoPath() {
        return mUploadedVideoPath;
    }

    public void setUploadedVideoPath(String uploadedVideoPath) {
        mUploadedVideoPath = uploadedVideoPath;
    }

    public String getUploadedImagePath() {
        return mUploadedImagePath;
    }

    public void setUploadedImagePath(String uploadedImagePath) {
        mUploadedImagePath = uploadedImagePath;
    }

}