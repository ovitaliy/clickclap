package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.clickclap.AppUser;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.clickclap.view.BaseModel;
import com.clickclap.view.CirclePickerItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Denis on 07.05.2015.
 */
public class Country implements BaseModel {
    @SerializedName("id")
    private int mId;

    @SerializedName("country_rus")
    private String mTitleRus;

    @SerializedName("country_eng")
    private String mTitleEng;

    @SerializedName("country_esp")
    private String mTitleEsp;

    @SerializedName("phone_code")
    private String mPhoneCode;

    @SerializedName("phone_lenght")
    private String mPhoneLenght;

    private String mTitle;

    public String getTitleRus() {
        return mTitleRus;
    }

    public void setTitleRus(String titleRus) {
        mTitleRus = titleRus;
    }

    public String getTitleEng() {
        return mTitleEng;
    }

    public void setTitleEng(String titleEng) {
        mTitleEng = titleEng;
    }

    public String getTitleEsp() {
        return mTitleEsp;
    }

    public void setTitleEsp(String titleEsp) {
        mTitleEsp = titleEsp;
    }

    public String getPhoneCode() {
        return mPhoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        mPhoneCode = phoneCode;
    }

    public String getPhoneLenght() {
        return mPhoneLenght;
    }

    public void setPhoneLenght(String phoneLenght) {
        mPhoneLenght = phoneLenght;
    }

    public Country(String title) {
        mTitle = title;
    }

    public Country() {
    }

    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            switch (Language.getSystem()) {
                case RUS:
                    setTitle(mTitleRus);
                    break;
                case ESP:
                    setTitle(mTitleEsp);
                    break;
                case ENG:
                    setTitle(mTitleEng);
                    break;
            }
        }
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContentDescriptor.Countries.Cols.ID, mId);
        contentValues.put(ContentDescriptor.Countries.Cols.NAME_ENG, mTitleEng);
        contentValues.put(ContentDescriptor.Countries.Cols.NAME_RUS, mTitleRus);
        contentValues.put(ContentDescriptor.Countries.Cols.NAME_ESP, mTitleEsp);
        contentValues.put(ContentDescriptor.Countries.Cols.PHONE_CODE, mPhoneCode);
        contentValues.put(ContentDescriptor.Countries.Cols.PHONE_LENGHT, mPhoneLenght);
        return contentValues;
    }

    public static Country fromCursor(Cursor cursor) {
        Country country = new Country();
        country.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.ID)));
        country.setPhoneCode(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.PHONE_CODE)));
        country.setPhoneLenght(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.PHONE_LENGHT)));

        String title = "";
        switch (Language.getSystem()) {
            case RUS:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.NAME_RUS));
                break;
            case ESP:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.NAME_ESP));
                break;
            case ENG:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.NAME_ENG));
                break;
        }

        country.setTitle(title);

        return country;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }
}
