package com.clickclap.model;

import com.clickclap.R;
import com.clickclap.enums.VideoType;

public class VideoStatistic {
    private int mImageResId;
    private int mTitleResId;
    private int mId;
    private float mDuration;

    public VideoStatistic(int videoTypeCode, float duration) {
        mDuration = duration;
        mId = videoTypeCode;
        VideoType type = VideoType.getByCode(videoTypeCode);
        if (type != null) {
            switch (type) {
                case FLOW_ALL:
                case FLOW_BEST:
                case FLOW_MY_CHOICE:
                    mImageResId = R.drawable.flow;
                    mTitleResId = R.string.menu_flow;
                    break;
                case EMOTICON_ALL:
                case EMOTICON_BEST:
                case EMOTICON_MY_CHOICE:
                    mImageResId = R.drawable.category_emoticons;
                    mTitleResId = R.string.emoticon;
                    break;
                case VLOG:
                    mImageResId = R.drawable.category_vlog;
                    mTitleResId = R.string.imon_tab_vlog;
                    break;
                case CROWD_ASK:
                case CROWD_GIVE:
                    mImageResId = R.drawable.crowd;
                    mTitleResId = R.string.menu_crowd;
                    break;
                case GREETING_EVENTS:
                case GREETING_PEOPLE:
                case GREETING_RELIGION:
                    mImageResId = R.drawable.greetings;
                    mTitleResId = R.string.menu_greetings;
                    break;
                case NEWS:
                    mImageResId = R.drawable.interest_news;
                    mTitleResId = R.string.interest_news;
                    break;
                case ALIEN_LOOK_TASK:
                case ALIEN_LOOK_CHALLENGE:
                    mImageResId = R.drawable.alien_look;
                    mTitleResId = R.string.menu_alien_look;
                    break;
            }
        }
    }

    public int getImageResId() {
        return mImageResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getId() {
        return mId;
    }

    public float getDuration() {
        return mDuration;
    }

    public void setDuration(float duration) {
        mDuration = duration;
    }
}
