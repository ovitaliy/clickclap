package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.IntDef;

import com.clickclap.db.ContentDescriptor;
import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Denis on 28.04.2015.
 */
public class Feedback {
    @SerializedName("id")
    private int mId;

    @SerializedName("type")
    private int mType;

    @SerializedName("last_msg")
    private String mLastMessage;

    @SerializedName("n")
    private int mNew;

    public static final int MOTION = 0;
    public static final int ERROR = 1;
    public static final int COMPLAIN = 2;

    @IntDef({MOTION, ERROR, COMPLAIN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FeedbackType {
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(4);
        values.put(ContentDescriptor.Feedback.Cols.ID, mId);
        values.put(ContentDescriptor.Feedback.Cols.TYPE, mType);
        values.put(ContentDescriptor.Feedback.Cols.LAST_MESSAGE, mLastMessage);
        values.put(ContentDescriptor.Feedback.Cols.NEW, mNew);
        return values;
    }

    public static Feedback fromCursor(Cursor cursor) {
        Feedback feeback = new Feedback();
        feeback.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.ID)));
        @FeedbackType int status = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.TYPE));
        feeback.setType(status);
        feeback.setNew(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.NEW)));
        feeback.setLastMessage(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.LAST_MESSAGE)));
        return feeback;
    }

    public
    @FeedbackType
    int getType() {
        return mType;
    }

    public void setType(@FeedbackType int type) {
        mType = type;
    }

    public String getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(String lastMessage) {
        mLastMessage = lastMessage;
    }

    public int getNew() {
        return mNew;
    }

    public void setNew(int aNew) {
        mNew = aNew;
    }
}
