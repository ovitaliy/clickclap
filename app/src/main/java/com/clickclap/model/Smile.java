package com.clickclap.model;

import java.io.Serializable;

/**
 * Created by Владимир on 25.11.2014.
 */
public class Smile implements Serializable{
    private int mSmileId;
    private int mSmileRes;

    public Smile() {
    }

    public int getSmileId() {
        return mSmileId;
    }

    public void setSmileId(int smileId) {
        mSmileId = smileId;
    }

    public int getSmileRes() {
        return mSmileRes;
    }

    public void setSmileRes(int smileRes) {
        mSmileRes = smileRes;
    }
}
