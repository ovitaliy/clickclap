package com.clickclap.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.clickclap.App;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.clickclap.enums.VideoType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Владимир on 17.11.2014.
 */
public class Category implements SimpleListItem, Serializable {
    public static final int FAVORITES_ID = -1;
    public static final int ALL_ID = -2;

    @SerializedName("id")
    private int mId;

    @SerializedName("count")
    private int mVideosCount;

    @SerializedName("main")
    private Boolean mMain;

    @SerializedName("tech")
    private Boolean mTech;

    @SerializedName("params")
    private String mParams;

    @SerializedName("category_rus")
    private String mTitleRus;

    @SerializedName("category_eng")
    private String mTitleEng;

    @SerializedName("category_esp")
    private String mTitleEsp;

    @SerializedName("thumb")
    private String mThumb;

    private String mTitle;

    protected void setTitle(String title) {
        mTitle = title;
    }

    public Category() {
    }

    public Category(int id) {
        mId = id;
    }

    public Category(int id, String title) {
        mId = id;
        mTitle = title;
    }

    public int getVideosCount() {
        return mVideosCount;
    }

    public void setVideosCount(int videosCount) {
        this.mVideosCount = videosCount;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public Boolean isMain() {
        return mMain;
    }

    public void setMain(boolean main) {
        mMain = main;
    }

    @Override
    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            switch (Language.getSystem()) {
                case RUS:
                    setTitle(mTitleRus);
                    break;
                case ESP:
                    setTitle(mTitleEsp);
                    break;
                default:
                    setTitle(mTitleEng);
                    break;
            }
        }
        if (mTitle == null) {
            mTitle = "";
        }
        return mTitle;
    }

    public boolean isAllOrFavorite() {
        return (getId() == FAVORITES_ID || getId() == ALL_ID);
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues(10);
        contentValues.put(ContentDescriptor.Categories.Cols.ID, getId());
        contentValues.put(ContentDescriptor.Categories.Cols.NAME_ENG, mTitleEng);
        contentValues.put(ContentDescriptor.Categories.Cols.NAME_ESP, mTitleEsp);
        contentValues.put(ContentDescriptor.Categories.Cols.NAME_RUS, mTitleRus);
        contentValues.put(ContentDescriptor.Categories.Cols.THUMBNAIL, getImage());
        contentValues.put(ContentDescriptor.Categories.Cols.VIDEO_COUNT, getVideosCount());
        if (isMain() != null) {
            contentValues.put(ContentDescriptor.Categories.Cols.MAIN, isMain() ? 1 : 0);
        }
        if (isTech() != null) {
            contentValues.put(ContentDescriptor.Categories.Cols.TECH, isTech() ? 1 : 0);
        }
        if (getParams() != null) {
            contentValues.put(ContentDescriptor.Categories.Cols.PARAMS, getParams().toString());
        }
        return contentValues;
    }

    public static Category fromCursor(Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.ID));
        String titleEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.NAME_ENG));
        String titleEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.NAME_ESP));
        String titleRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.NAME_RUS));
        String thumb = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.THUMBNAIL));
        String params = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.PARAMS));
        int count = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.VIDEO_COUNT));
        boolean main = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.MAIN)) == 1;
        boolean tech = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Categories.Cols.TECH)) == 1;
        Category category = new Category(id);
        category.setTitleEng(titleEng);
        category.setTitleEsp(titleEsp);
        category.setTitleRus(titleRus);
        category.setThumb(thumb);
        category.setVideosCount(count);
        category.setMain(main);
        category.setTech(tech);
        category.setParams(params);

        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof Category)) return false;

        return (getId() == ((Category) o).getId()
                && getTitle().equals(((Category) o).getTitle())
                && getImage().equals(((Category) o).getImage()));
    }

    public static ArrayList<Category> reorder(ArrayList<Category> categories) {
//        if (categories != null) {
//
//            for (int i = categories.size() - 1; i >= 0; i--) {
//                if (categories.get(i).getVideosCount() == 0) {
//                    categories.remove(i);
//                }
//            }
//
//            Category emoticons = null;
//            Category all = null;
//            Category favorits = null;
//            Category main = null;
//            for (int i = categories.size() - 1; i >= 0; i--) {
//                Category category = categories.get(i);
//                int id = category.getId();
//                if (id == Category.ALL_ID) {
//                    all = categories.remove(i);
//                } else if (id == Category.FAVORITES_ID) {
//                    favorits = categories.remove(i);
//                } else if (id == 0) {
//                    emoticons = categories.remove(0);
//                } else if (category.isMain()) {
//                    main = categories.remove(i);
//                }
//            }
//
//            int center = categories.size() / 2;
//
//            if (favorits != null) {
//                categories.add(center, favorits);
//            }
//
//            int index;
//            if (all != null) {
//                index = center + 1;
//                if (index < categories.size()) {
//                    categories.add(index, all);
//                } else {
//                    categories.add(all);
//                }
//            }
//
//            if (emoticons != null) {
//                index = center + 2;
//                if (index < categories.size()) {
//                    categories.add(index, emoticons);
//                } else {
//                    categories.add(emoticons);
//                }
//            }
//
//            if (main != null) {
//                index = center;
//                if (index < categories.size()) {
//                    categories.add(index, main);
//                } else {
//                    categories.add(main);
//                }
//            }
//
//        } else {
//            categories = new ArrayList<>(0);
//        }

        return categories;
    }

    public Boolean isTech() {
        return mTech;
    }

    public void setTech(boolean tech) {
        mTech = tech;
    }

    public String getParams() {
        return mParams;
    }

    public void setParams(String params) {
        mParams = params;
    }

    public void setTitleRus(String titleRus) {
        mTitleRus = titleRus;
    }

    public void setTitleEng(String titleEng) {
        mTitleEng = titleEng;
    }

    public void setTitleEsp(String titleEsp) {
        mTitleEsp = titleEsp;
    }

    @Override
    public String getImage() {
        if (mThumb == null) {
            mThumb = "";
        }
        return mThumb;
    }

    public void setThumb(String thumb) {
        mThumb = thumb;
    }

    public static class Deserializer implements JsonDeserializer<Category> {

        private static final String ID = "id";
        private static final String CATEGORY_ESP = "category_esp";
        private static final String CATEGORY_RUS = "category_rus";
        private static final String CATEGORY_ENG = "category_eng";
        private static final String VIDEO_COUNT = "count";
        private static final String THUMB = "thumb";
        private static final String MAIN = "main";
        private static final String TECH = "tech";
        private static final String PARAMS = "params";

        @Override
        public Category deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Category category = new Category();

            JsonObject jsonObject = json.getAsJsonObject();

            if (jsonObject.has(ID)) {
                JsonElement elem = jsonObject.get(ID);
                if (elem != null && !elem.isJsonNull()) {
                    category.setId(elem.getAsInt());
                }
            }

            if (jsonObject.has(CATEGORY_ESP)) {
                JsonElement elem = jsonObject.get(CATEGORY_ESP);
                if (elem != null && !elem.isJsonNull()) {
                    category.setTitleEsp(elem.getAsString());
                }
            }

            if (jsonObject.has(CATEGORY_RUS)) {
                JsonElement elem = jsonObject.get(CATEGORY_RUS);
                if (elem != null && !elem.isJsonNull()) {
                    category.setTitleRus(elem.getAsString());
                }
            }

            if (jsonObject.has(CATEGORY_ENG)) {
                JsonElement elem = jsonObject.get(CATEGORY_ENG);
                if (elem != null && !elem.isJsonNull()) {
                    category.setTitleEng(elem.getAsString());
                }
            }

            if (jsonObject.has(VIDEO_COUNT)) {
                JsonElement elem = jsonObject.get(VIDEO_COUNT);
                if (elem != null && !elem.isJsonNull()) {
                    category.setVideosCount(jsonObject.get(VIDEO_COUNT).getAsInt());
                }
            }

            if (jsonObject.has(THUMB)) {
                JsonElement elem = jsonObject.get(THUMB);
                if (elem != null && !elem.isJsonNull()) {
                    category.setThumb(elem.getAsString());
                }
            }

            if (jsonObject.has(MAIN)) {
                JsonElement elem = jsonObject.get(MAIN);
                if (elem != null && !elem.isJsonNull()) {
                    category.setMain(elem.getAsInt() == 1);
                }
            }

            if (jsonObject.has(TECH)) {
                JsonElement elem = jsonObject.get(TECH);
                if (elem != null && !elem.isJsonNull()) {
                    category.setTech(elem.getAsInt() == 1);
                }
            }

            if (jsonObject.has(PARAMS)) {
                JsonElement elem = jsonObject.get(PARAMS);
                if (elem != null && !elem.isJsonNull()) {
                    category.setParams(elem.toString());
                }
            }

            return category;
        }
    }

    public static Category getFromDatabase(int id, VideoType videoType) {
        if (videoType.equals(VideoType.FLOW_MY_CHOICE))
            videoType = VideoType.NEWS;

        Cursor cursor = App.getInstance().getContentResolver().query(ContentDescriptor.Categories.URI, null,
                "id=" + id + " AND " + ContentDescriptor.Categories.Cols.VIDEO_TYPE + "=" + videoType.getId(),
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            Category category = Category.fromCursor(cursor);
            cursor.close();
            return category;
        }

        return null;
    }
}
