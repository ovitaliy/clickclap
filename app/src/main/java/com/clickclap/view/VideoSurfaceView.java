package com.clickclap.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by ovitali on 17.09.2015.
 */
public class VideoSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    public VideoSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        getHolder().addCallback(this);
    }

    public VideoSurfaceView(Context context) {
        this(context, null);
    }

    private int calculatedX;
    private int calculatedY;
    private int calculatedWidth;
    private int calculatedHeight;

    private boolean created = false;
    private boolean destroyed = false;


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (calculatedWidth != 0 && calculatedHeight != 0) {
            getLayoutParams().width = calculatedWidth;
            getLayoutParams().height = calculatedHeight;

            getHolder().setFixedSize(calculatedWidth, calculatedHeight);

            setX(calculatedX);
            setY(calculatedY);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (calculatedWidth == 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            super.onMeasure(
                    MeasureSpec.makeMeasureSpec(calculatedWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(calculatedHeight, MeasureSpec.EXACTLY)
            );
        }
    }


    public void setCalculatedX(int calculatedX) {
        setX(calculatedX);
        this.calculatedX = calculatedX;
    }

    public void setCalculatedY(int calculatedY) {
        setY(calculatedY);
        this.calculatedY = calculatedY;
    }

    public boolean setCalculatedSizes(int calculatedWidth, int calculatedHeight) {
        if (this.calculatedWidth == calculatedWidth && this.calculatedHeight == calculatedHeight) {
            return false;
        }
        setCalculatedWidth(calculatedWidth);
        setCalculatedHeight(calculatedHeight);

        return true;
    }

    public void setCalculatedWidth(int calculatedWidth) {
        getLayoutParams().width = calculatedWidth;
        this.calculatedWidth = calculatedWidth;
    }

    public void setCalculatedHeight(int calculatedHeight) {
        getLayoutParams().height = calculatedHeight;
        this.calculatedHeight = calculatedHeight;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        created = true;
        //getHolder().setFormat(PixelFormat.RGB_888);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        created = false;
        destroyed = true;
    }

    public boolean isCreated() {
        return created;
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
