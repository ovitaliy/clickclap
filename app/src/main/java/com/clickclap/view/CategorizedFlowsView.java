package com.clickclap.view;

import com.clickclap.enums.VideoType;
import com.clickclap.model.Category;

/**
 * Created by denisvasilenko on 05.10.15.
 */
public interface CategorizedFlowsView {
    VideoType getVideoType();

    void loadCategories(VideoType videoType);

    void openFlowList();

    Category getCurrentCategory();
}
