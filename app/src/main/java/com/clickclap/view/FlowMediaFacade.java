package com.clickclap.view;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public final class FlowMediaFacade extends MediaFacade implements IMediaEventListener {

    private final ImageView mImageView;
    private final YellowProgressBar mProgressBar;
    private View mPlayButton;


    public FlowMediaFacade(VideoSurfaceView surfaceView, ImageView imageView, YellowProgressBar progressBar, View playButton) {
        this(surfaceView, imageView, progressBar);
        mPlayButton = playButton;

        if (playButton != null) {
            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (videoUrl != null) {
                        play(videoUrl);
                    }
                }
            });
        }
    }

    public FlowMediaFacade(VideoSurfaceView surfaceView, ImageView imageView, YellowProgressBar progressBar) {
        super(surfaceView);
        mImageView = imageView;
        mProgressBar = progressBar;

        mImageView.setAdjustViewBounds(true);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    @Override
    public void onVideoPlayingStarted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        hidePreview();
        hideProgressBar();
        hidePlayButton();
    }

    @Override
    public void onVideoPlayingCompleted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        showPreview();
        showProgressBar();
    }

    private void hidePreview() {
        if (mImageView != null) {
            mImageView.setVisibility(View.INVISIBLE);
        }
    }

    private void showPreview() {
        if (mImageView != null) {
            mImageView.setVisibility(View.VISIBLE);
        }
    }

    public void loadVideoPreview(final String url) {
        closeMediaPlayer();
        showPreview();
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().delayBeforeLoading(50).cacheOnDisk(true).build();
        ImageLoader.getInstance().displayImage(url, mImageView, displayImageOptions);
    }

    public void setImage(String url) {
        mImageView.setImageDrawable(null);
        if (url != null) {
            ImageLoader.getInstance().displayImage(url, mImageView, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    hideProgressBar();
                }
            });
        }
    }

    public void showPlayButton() {
        if (mPlayButton != null) {
            mPlayButton.setVisibility(View.VISIBLE);
        }
    }

    public void hidePlayButton() {
        if (mPlayButton != null) {
            mPlayButton.setVisibility(View.INVISIBLE);
        }
    }


    public void showProgressBar() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressBar() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }


    public static String getVideoUrl(Uri url) {
        if (url == null)
            return null;

        return url.toString();
    }


    public YellowProgressBar getProgressBar() {
        return mProgressBar;
    }

}
