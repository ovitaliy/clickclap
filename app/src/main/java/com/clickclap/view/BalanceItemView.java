package com.clickclap.view;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.enums.BalanceChangeType;
import com.clickclap.model.Balance;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Владимир on 13.11.2014.
 */
public class BalanceItemView extends RelativeLayout {

    private Context mContext;
    private CircleImageView mImageView;
    private TextView mTitleTextView;
    private TextView mTextView;
    private TextView mCoinsView;
    private TextView mDateView;

    public BalanceItemView(Context context) {
        super(context);
        mContext = context;
        inflate(mContext, R.layout.item_balance, this);
        mImageView = (CircleImageView) findViewById(R.id.item_balance_image);
        mTitleTextView = (TextView) findViewById(R.id.item_balance_title);
        mTextView = (TextView) findViewById(R.id.item_balance_text);
        mCoinsView = (TextView) findViewById(R.id.item_balance_mon_coin);
        mDateView = (TextView) findViewById(R.id.item_balance_date);
    }

    public void setData(Balance data) {
        mImageView.setImageDrawable(null);
        if (data.getObjectId() > 0) {
            ImageLoader.getInstance().displayImage(data.getObjectImg(), mImageView);
        }

        BalanceChangeType type = data.getBalanceChangeType();
        String title;
        if (type != null) {
            title = type.getTitle();
        } else {
            title = "???";
        }
        mTitleTextView.setText(title);
        mCoinsView.setText(Math.abs(data.getChange()) + " mc");
        if (data.getChange() > 0) {
            mCoinsView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.positive_coin, 0, 0, 0);
            mCoinsView.setTextColor(mContext.getResources().getColor(R.color.positive_mon_coin_text));
        } else {
            mCoinsView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.negative_coin, 0, 0, 0);
            mCoinsView.setTextColor(mContext.getResources().getColor(R.color.negative_mon_coin_text));
        }

        mDateView.setText(Const.DAY_MONTH_YEAR_FORMAT.format(data.getCreatedAt()));
    }
}
