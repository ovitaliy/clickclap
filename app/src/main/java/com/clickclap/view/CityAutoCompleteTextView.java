package com.clickclap.view;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.listener.SimpleTextListener;
import com.clickclap.loaders.LocationDataLoader;
import com.clickclap.model.City;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ovi on 12/23/15.
 */
public class CityAutoCompleteTextView extends AutoCompleteTextView implements CityListView {

    private CityAutoCompleteAdapter mAdapter;

    private City mSelectedCity;

    private LocationDataLoader mLocationDataLoader;

    private AdapterView.OnItemSelectedListener mListener;

    public CityAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public CityAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSaveEnabled(false);
        setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        setTextSize(18);
        setThreshold(0);

        setHintTextColor(getResources().getColor(R.color.light_gray));

        setHint(R.string.search_choose_city);

        mAdapter = new CityAutoCompleteAdapter();
        setAdapter(mAdapter);

        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setSelectedPosition(position);
                if (mListener != null)
                    mListener.onItemSelected(parent, view, position, id);
            }
        });

        setOnItemSelectedListener(null);

        addTextChangedListener(new SimpleTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString().toLowerCase();
                if (mSelectedCity != null && mSelectedCity.getTitle().toLowerCase().equals(text)) {
                    return;
                }
                mSelectedCity = null;
                for (int i = 0; i < mAdapter.getCount(); i++) {
                    City c = mAdapter.getItem(i);
                    if (c.getTitle().toLowerCase().equals(text)) {
                        mSelectedCity = c;
                    }
                }

                if (mSelectedCity == null && text.length() > 1)
                    mLocationDataLoader.loadCities(text);
            }
        });

        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showDropDown();
            }
        });

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (hasFocus())
                    if (!isPopupShowing())
                        showDropDown();
                    else
                        requestFocus();

                return false;
            }
        });

    }

    public void setLocationDataLoader(LocationDataLoader locationDataLoader) {
        mLocationDataLoader = locationDataLoader;
    }

    public int getSelectedItemId() {
        return mSelectedCity != null ? mSelectedCity.getId() : -1;
    }


    public void setSelectedPosition(int position) {
        mSelectedCity = mAdapter.getItem(position);
        String cityName = mSelectedCity.getTitle();
        setText(cityName);
        setSelection(cityName.length());
    }

    public City getSelectedItem() {
        return mSelectedCity;
    }

    public void clear() {
        mSelectedCity = null;
        setText("");
        mAdapter.clear();
    }

    @Override
    public void setCityList(List<City> cities) {
        mAdapter.set(cities);
        setVisibility(VISIBLE);
    }

    @Override
    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener l) {
        mListener = l;
        super.setOnItemSelectedListener(new WrappedOnItemSelectedListener());
    }

    private class WrappedOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            setSelectedPosition(position);

            if (mListener != null)
                mListener.onItemSelected(parent, view, position, id);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            if (mListener != null)
                mListener.onNothingSelected(parent);
        }
    }


    private static class CityAutoCompleteAdapter extends BaseAdapter implements Filterable {

        private List<City> mAllItems = new ArrayList<>();
        private List<City> mFilteredItems = new ArrayList<>();

        @Override
        public int getCount() {
            return mFilteredItems.size();
        }

        @Override
        public City getItem(int position) {
            return mFilteredItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mFilteredItems.get(position).getId();
        }

        public void clear() {
            if (mAllItems != null && mAllItems.size() > 0) {
                mAllItems.clear();
                notifyDataSetChanged();
            }
        }

        public void set(Collection<City> list) {
            if (mAllItems.size() > 0) {
                mAllItems.clear();
                mFilteredItems.clear();
            }
            mAllItems.addAll(list);
            mFilteredItems.addAll(list);

            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView;

            if (convertView != null) {
                textView = (TextView) convertView;
            } else {
                textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_autocomplete_city, parent, false);
            }

            textView.setText(getItem(position).getTitle());


            return textView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();

                    if (constraint == null) {
                        return filterResults;
                    }

                    String start = constraint.toString().toLowerCase();

                    List<City> result = new ArrayList<>();

                    for (int i = 0; i < mAllItems.size(); i++) {
                        City city = mAllItems.get(i);
                        if (city.getTitle().toLowerCase().startsWith(start)) {
                            result.add(city);
                        }
                    }

                    filterResults.values = result;
                    filterResults.count = result.size();

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results.count > 0) {
                        mFilteredItems = (List<City>) results.values;
                    } else {
                        mFilteredItems.clear();
                        mFilteredItems.addAll(mAllItems);
                    }
                    notifyDataSetChanged();

                }
            };
        }
    }


}
