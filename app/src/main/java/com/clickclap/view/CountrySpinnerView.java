package com.clickclap.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.clickclap.adapter.SimpleSpinnerAdapter;
import com.clickclap.model.Country;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovi on 12/29/15.
 */
public class CountrySpinnerView extends Spinner implements CountryListView {

    private SimpleSpinnerAdapter<Country> mAdapter;

    public CountrySpinnerView(Context context) {
        this(context, null);
    }

    public CountrySpinnerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOnItemSelectedListener(null);
        mAdapter = new SimpleSpinnerAdapter<>(new ArrayList<Country>(0));
        setAdapter(mAdapter);
    }

    @Override
    public void setCountryList(List<Country> countries) {
        mAdapter.set(countries);
    }



    @Override
    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        super.setOnItemSelectedListener(new WrappedOnItemSelectedListener(listener));
    }

    private class WrappedOnItemSelectedListener implements OnItemSelectedListener {

        private OnItemSelectedListener mListener;

        public WrappedOnItemSelectedListener(OnItemSelectedListener listener) {
            mListener = listener;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (mListener != null)
                mListener.onItemSelected(parent, view, position, id);

        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            if (mListener != null)
                mListener.onNothingSelected(parent);
        }
    }
}
