package com.clickclap.view;

import android.media.MediaPlayer;

/**
 * Created by ovitali on 25.11.2015.
 */
public interface IMediaEventListener {

    void onVideoPlayingStarted(MediaFacade mediaFacade, MediaPlayer mediaPlayer);

    void onVideoPlayingCompleted(MediaFacade mediaFacade, MediaPlayer mediaPlayer);
}
