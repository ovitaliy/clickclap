package com.clickclap.view;

import com.clickclap.enums.VideoType;

/**
 * Created by denisvasilenko on 08.09.15.
 */
public interface NewVideoView {
    VideoType getVideoType();

    boolean isGrimaceMode();
}
