package com.clickclap.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.clickclap.R;
import com.clickclap.events.ProgressedImageLoadingListener;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Denis on 08.04.2015.
 */
public class VideoPreviewView extends FrameLayout {
    private CircleImageView mPreviewImage;
    private ImageButton mPlayButton;
    private YellowProgressBar mProgressBar;

    public VideoPreviewView(Context context) {
        super(context);
        init();
    }

    public VideoPreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VideoPreviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setClickable(false);
        mPreviewImage = new CircleImageView(getContext());
        LayoutParams previewParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        previewParams.gravity = Gravity.CENTER;
        mPreviewImage.setBorderSize(3);
        mPreviewImage.setBorderColor(Color.YELLOW);
        mPreviewImage.setId(R.id.preview_image);
        addView(mPreviewImage, previewParams);

        mProgressBar = new YellowProgressBar(getContext());
        LayoutParams progressParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        progressParams.gravity = Gravity.CENTER;
        addView(mProgressBar, progressParams);

        LayoutParams playParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        playParams.gravity = Gravity.CENTER;
        mPlayButton = new ImageButton(getContext());
        mPlayButton.setClickable(false);
        mPlayButton.setBackgroundResource(R.drawable.start_button);
        mPlayButton.setId(R.id.play_button);
        addView(mPlayButton, playParams);
    }

    public void hide(){
        mPreviewImage.setImageDrawable(null);
        mProgressBar.setVisibility(GONE);
        mPreviewImage.setVisibility(GONE);
        mPlayButton.setVisibility(GONE);
    }

    public void show(String imageUrl){
        mPreviewImage.setImageDrawable(null);
        mPreviewImage.setVisibility(VISIBLE);
        mPlayButton.setVisibility(VISIBLE);
        ImageLoader.getInstance().displayImage(imageUrl, mPreviewImage, new ProgressedImageLoadingListener(mProgressBar));
    }
}
