package com.clickclap.view;

import java.util.ArrayList;

/**
 * Created by denisvasilenko on 06.10.15.
 */
public interface NavigateView<T extends CirclePickerItem> {
    void navigateToSubMenuItem(T item);

    void navigateBack();

    void setSelectedSubMenuItem(T item);

    void updateNextButtonEnabledStatus();

    T getSelectedSubMenuItem();

    ArrayList<CirclePickerItem> getSubMenuItems();
}
