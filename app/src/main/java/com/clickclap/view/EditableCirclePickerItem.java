package com.clickclap.view;

/**
 * Created by Deni on 18.08.2015.
 */
public interface EditableCirclePickerItem extends CirclePickerItem {
    boolean isOtherOption();

    void setTitle(String title);
}
