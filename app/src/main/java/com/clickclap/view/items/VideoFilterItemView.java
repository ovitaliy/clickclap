package com.clickclap.view.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.media.filter.video.BaseVideoFilter;
import com.clickclap.model.Filter;
import com.clickclap.util.BitmapDecoder;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.ImageCroper;
import com.clickclap.util.OnCacheUpdateListener;
import com.clickclap.view.CircleImageView;
import com.clickclap.view.CircleLayout;
import com.humanet.filters.videofilter.IFilter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.ref.WeakReference;

/**
 * Created by Владимир on 03.12.2014.
 */
public class VideoFilterItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTitleView;
    private IFilter mData;

    public VideoFilterItemView(Context context) {
        super(context);
        View.inflate(context, R.layout.item_grimace, this);

        setGravity(Gravity.CENTER_HORIZONTAL);
        setOrientation(LinearLayout.VERTICAL);

        mImageView = (CircleImageView) findViewById(R.id.item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mImageView.setBorderSize(getResources().getDimensionPixelOffset(R.dimen.stream_item_small_border));


        mTitleView = (TextView) findViewById(R.id.item_title);
    }


    public void setData(String image, IFilter data) {
        super.setData(data);
        mData = data;
        String title = data.getTitle();
        if (title.contains("Base")) {
            title = getContext().getString(R.string.video_record_no_filter);
        }
        mTitleView.setText(title);

        String imagePath = "file://" + FilePathHelper.getFilteredImagePreview(data.getTitle()).getAbsolutePath();


        ImageLoader.getInstance().displayImage(imagePath,
                mImageView,
                DisplayImageOptions.createSimple());
    }
}
