package com.clickclap.view.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.model.SimpleListItem;
import com.clickclap.view.CircleImageView;
import com.clickclap.view.CircleLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.ref.WeakReference;

/**
 * Created by ovitali on 02.02.2015.
 */
public class BaseItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTextView;

    public static int IMAGE_WIDTH = -1;

    public BaseItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_audio, this);
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);

        mImageView = (CircleImageView) findViewById(R.id.audio_item_image);
        mTextView = (TextView) findViewById(R.id.audio_item_title);

        mImageView.getLayoutParams().width = App.IMAGE_SMALL_WIDTH;
        mImageView.getLayoutParams().height = App.IMAGE_SMALL_WIDTH;
    }

    public void setData(SimpleListItem data) {
        if (data != null) {
            String title = data.getTitle();
            mTextView.setText(title);
            mTextView.invalidate();

            ImageLoader.getInstance().displayImage(data.getImage(), mImageView);
        }
    }

    //-------------
    private static WeakReference<Bitmap> sBitmapWeakReference;

    private static Bitmap getBaseImage(Context context) {
        if (context == null) {
            return null;
        }
        Bitmap b = null;
        if (sBitmapWeakReference != null) {
            b = sBitmapWeakReference.get();
        }

        if (b == null) {
            b = BitmapFactory.decodeResource(context.getResources(), R.drawable.beach);
            b = Bitmap.createScaledBitmap(b, App.IMAGE_SMALL_WIDTH, App.IMAGE_SMALL_WIDTH, false);

            sBitmapWeakReference = new WeakReference<>(b);
        }
        return b;
    }
}
