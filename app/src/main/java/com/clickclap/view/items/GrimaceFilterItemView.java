package com.clickclap.view.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.view.CircleImageView;
import com.clickclap.view.CircleLayout;

import java.lang.ref.WeakReference;

/**
 * Created by Denis on 30.03.2014.
 */
public class GrimaceFilterItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTitleView;

    public GrimaceFilterItemView(Context context) {
        super(context);

        inflate(context, R.layout.item_grimace, this);

        mImageView = (CircleImageView) findViewById(R.id.filter_item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mImageView.setBorderSize(getResources().getDimensionPixelOffset(R.dimen.stream_item_small_border));
        mTitleView = (TextView) findViewById(R.id.filter_item_title);
    }


    public void setTitle(String title) {
        mTitleView.setText(title);
    }

    public void setImage(Bitmap bitmap) {
        int w = getW() - getResources().getDimensionPixelSize(R.dimen.stream_item_small_border) * 2;
        mImageView.getLayoutParams().width = w;
        mImageView.getLayoutParams().height = w;
        mImageView.setImageBitmapWithoutRounding(bitmap);
    }

    private static WeakReference<Bitmap> sBitmapWeakReference;

    private static synchronized Bitmap getBaseImage(Context context, String path, int camId, boolean isAlpha) {
        Bitmap b = null;
        if (sBitmapWeakReference != null) {
            b = sBitmapWeakReference.get();
        }

        if (b == null) {

        }

        return Bitmap.createBitmap(b);
    }
}
