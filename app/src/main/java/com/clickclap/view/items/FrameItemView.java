package com.clickclap.view.items;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.clickclap.R;
import com.clickclap.util.OnCacheUpdateListener;
import com.clickclap.view.CircleImageView;
import com.clickclap.view.CircleLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Владимир on 03.12.2014.
 */

public class FrameItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private String mData;
    private OnCacheUpdateListener mCacheListener;

    public FrameItemView(Context context) {
        super(context);
        View.inflate(context, R.layout.item_grimace, this);
        setGravity(Gravity.CENTER_HORIZONTAL);
        setOrientation(LinearLayout.VERTICAL);

        if (isInEditMode())
            return;

        mImageView = (CircleImageView) findViewById(R.id.item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mImageView.setBorderSize(getResources().getDimensionPixelOffset(R.dimen.stream_item_small_border));
    }


    public void setData(String data) {
        mData = data;


        ImageLoader.getInstance().displayImage("file://" + data,
                mImageView,
                DisplayImageOptions.createSimple());
    }


}
