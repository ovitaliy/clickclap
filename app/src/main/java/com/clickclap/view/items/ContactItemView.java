package com.clickclap.view.items;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.model.ContactItem;

/**
 * Created by Inteza23 on 17.12.2014.
 */
public class ContactItemView extends LinearLayout {

    private TextView name;
    private CheckBox check;
    private ContactItem contact;


    public ContactItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_item, this);

        name = (TextView) findViewById(R.id.txt_name);
        check = (CheckBox) findViewById(R.id.checkBox);
    }

    public void setData(ContactItem fr) {
        contact = fr;
        name.setText(contact.getName());
        check.setChecked(fr.isCheck());
    }

    public void setCheck(boolean val) {
        contact.setCheck(val);
        check.setChecked(val);
    }

    public boolean getCheck() {
        return contact.isCheck();
    }
}
