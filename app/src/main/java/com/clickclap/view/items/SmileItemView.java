package com.clickclap.view.items;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.enums.Emotion;
import com.clickclap.view.CircleLayout;

/**
 * Created by Владимир on 03.12.2014.
 */
public class SmileItemView extends CircleLayout.ItemWrapper {

    private ImageView mImageView;
    private TextView mTextView;
    private Emotion mData;

    public SmileItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_smile, this);

        mImageView = (ImageView) findViewById(R.id.smile_item_image);
        mTextView = (TextView) findViewById(R.id.smile_item_name);
    }

    public void setData(Emotion data) {
        mData = data;
        mImageView.setImageDrawable(mData.getDrawable());
        mTextView.setText(mData.getTitle());
    }
}
