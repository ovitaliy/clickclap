package com.clickclap.view;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clickclap.activities.ContactsActivity;
import com.clickclap.R;

/**
 * Created by Inteza23 on 17.12.2014.
 */
public class GroupItemView extends LinearLayout {

    private Context mContext;
    private TextView name;
    private CheckBox check;

    private ContactsActivity.Group mGroup;


    public GroupItemView(Context context) {
        super(context);
        mContext = context;
        inflate(mContext, R.layout.item_group, this);

        name = (TextView) findViewById(R.id.txt_name);
        check = (CheckBox) findViewById(R.id.checkBox);
    }

    public void setData(ContactsActivity.Group group) {
        mGroup = group;
        name.setText(group.name);
        check.setChecked(mGroup.checked);
    }

    public void setCheck(boolean val){
        mGroup.checked = val;
        check.setChecked(val);
    }
}
