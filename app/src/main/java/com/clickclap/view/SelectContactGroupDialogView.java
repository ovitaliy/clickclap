package com.clickclap.view;

import android.content.Context;
import android.widget.LinearLayout;

import com.clickclap.R;

/**
 * Created by michael on 25.11.14.
 */
public class SelectContactGroupDialogView extends LinearLayout {

    public SelectContactGroupDialogView(Context context) {
        super(context);
        inflate(context, R.layout.dialog_select_contact_group, this);
    }
}
