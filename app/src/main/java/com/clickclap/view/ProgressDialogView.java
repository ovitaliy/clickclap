package com.clickclap.view;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clickclap.R;


/**
 * Created by Владимир on 01.10.2014.
 */
public class ProgressDialogView extends RelativeLayout {
    private TextView mTitle;

    public ProgressDialogView(Context context) {
        super(context);
        inflate(context, R.layout.dialog_progress, this);
        mTitle = (TextView) findViewById(R.id.dialog_title);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void hideTitle() {
        mTitle.setVisibility(GONE);
    }

    public void showTitle() {
        mTitle.setVisibility(VISIBLE);
    }
}
