package com.clickclap.view;

import android.graphics.drawable.Drawable;

/**
 * Created by Denis on 05.03.2015.
 */
public interface CirclePickerItem extends BaseModel{

    Drawable getDrawable();

}