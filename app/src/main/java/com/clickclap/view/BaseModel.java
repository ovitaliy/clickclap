package com.clickclap.view;

import android.graphics.drawable.Drawable;

/**
 * Created by Denis on 05.03.2015.
 */
public interface BaseModel {
    int getId();

    String getTitle();
}