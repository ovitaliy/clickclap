package com.clickclap.view;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.clickclap.R;

/**
 * Created by ovitali on 17.02.2015.
 */
public class YellowProgressBar extends ProgressBar {
    public YellowProgressBar(Context context) {
        super(context);
    }

    public YellowProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public YellowProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        getIndeterminateDrawable().setColorFilter(getContext().getResources().getColor(R.color.yellow), PorterDuff.Mode.SRC_IN);
    }
}
