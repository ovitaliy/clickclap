package com.clickclap.view;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;

import com.clickclap.util.UiUtil;

import java.io.IOException;

/**
 * Created by ovitali on 25.11.2015.
 */
public class MediaFacade {

    private final VideoSurfaceView mSurfaceView;

    private IMediaEventListener mMediaEventListener;

    protected MediaPlayer mediaPlayer;

    protected String videoUrl;

    private boolean mLooping;

    public MediaFacade(final VideoSurfaceView surfaceView) {
        mSurfaceView = surfaceView;
    }

    public void setMediaEventListener(final IMediaEventListener mediaEventListener) {
        mMediaEventListener = mediaEventListener;
    }

    public void closeMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();

            mediaPlayer = null;
        }
    }

    public void replay() {
        play(videoUrl);
    }

    public void play(String videoUrl) {
        closeMediaPlayer();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setLooping(mLooping);
        this.videoUrl = videoUrl;

        mediaPlayer.setSurface(mSurfaceView.getHolder().getSurface());
        try {
            mediaPlayer.setDataSource(videoUrl);
            mediaPlayer.setOnPreparedListener(new OnPreparedListener());
            mediaPlayer.setOnCompletionListener(new OnCompletionListener());
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            Log.e(MediaFacade.class.getName(), videoUrl, e);
        }
    }

    private class OnPreparedListener implements MediaPlayer.OnPreparedListener {

        @Override
        public void onPrepared(final MediaPlayer mp) {
            View parent = (View) mSurfaceView.getParent();
            UiUtil.resizedToFitParent(
                    mSurfaceView,
                    parent,
                    mp.getVideoWidth(),
                    mp.getVideoHeight()
            );

            mp.start();
            if (mMediaEventListener != null) {
                mMediaEventListener.onVideoPlayingStarted(MediaFacade.this, mp);
            }

        }
    }

    public void setLooping(boolean looping) {
        mLooping = looping;
    }

    private class OnCompletionListener implements MediaPlayer.OnCompletionListener {

        @Override
        public void onCompletion(MediaPlayer mp) {
            if (!mp.isLooping()) {
                mp.setOnCompletionListener(null);
                closeMediaPlayer();

                if (mMediaEventListener != null) {
                    mMediaEventListener.onVideoPlayingCompleted(MediaFacade.this, mp);
                }
            }
        }
    }

}
