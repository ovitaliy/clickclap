package com.clickclap.view;

import com.clickclap.loaders.LocationDataLoader;
import com.clickclap.model.City;

import java.util.List;

/**
 * Created by ovitali on 15.12.2015.
 */
public interface CityListView {

    void setCityList(List<City> cities);

    void setSelectedPosition(int selectedPosition);

    void setLocationDataLoader(LocationDataLoader locationDataLoader);

    void clear();

}
