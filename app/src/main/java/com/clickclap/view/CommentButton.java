package com.clickclap.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.clickclap.R;

/**
 * Created by Владимир on 01.12.2014.
 */
public class CommentButton extends LinearLayout {

    private BubbleView mCountView;

    public CommentButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.view_comment_button, this);

        mCountView = (BubbleView) findViewById(R.id.comment_button_count_view);
    }

    public void setCount(int count) {
        mCountView.setCount(count);
    }

}
