package com.clickclap.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.clickclap.App;
import com.clickclap.R;

/**
 * Created by ovitali on 02.02.2015.
 */
public class CircleListWithPreview extends LinearLayout implements CircleLayout.OnItemSelectedListener {

    private CircleLayout mCircleLayout;
    private ImageView mImageView;

    private Object mSelectedItem;

    private CircleLayout.OnItemSelectedListener onItemSelectedListener;

    public CircleListWithPreview(Context context) {
        super(context);
        init(context);
    }

    public CircleListWithPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CircleListWithPreview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(Context context) {
        inflate(context, R.layout.circle_listview_with_preview, this);

        mCircleLayout = (CircleLayout) findViewById(R.id.horizontal_list_view);

        View view = findViewById(R.id.squareMarker);
        int margin = getResources().getDimensionPixelSize(R.dimen.mrg_big);
        view.getLayoutParams().width = App.WIDTH - margin * 2;
        view.getLayoutParams().height = App.WIDTH - margin * 2;

        mCircleLayout.setOnItemSelectedListener(this);
        mImageView = (ImageView) findViewById(R.id.preview_image);
        mImageView.setImageResource(R.drawable.beach_big);
    }

    public void setAdapter(BaseAdapter adapter) {
        mCircleLayout.setAdapter(adapter);
    }

    public void setOnItemSelectedListener(CircleLayout.OnItemSelectedListener listener) {
        onItemSelectedListener = listener;
    }

    @Override
    public void onItemSelected(Object data) {
        mSelectedItem = data;
        if (onItemSelectedListener != null)
            onItemSelectedListener.onItemSelected(data);
    }

    public void setSelectedItem(int position) {
        mCircleLayout.setSelectedItem(position);
    }


    public Object getSelectedItem() {
        return mSelectedItem;
    }

    public void refresh() {
        post(new Runnable() {
            @Override
            public void run() {
                mCircleLayout.invalidateAll();
                mCircleLayout.rotateButtons();
            }
        });
    }


}
