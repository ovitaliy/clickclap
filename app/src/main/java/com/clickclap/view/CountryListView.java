package com.clickclap.view;


import com.clickclap.model.Country;

import java.util.List;

/**
 * Created by ovitali on 15.12.2015.
 */
public interface CountryListView {
    void setCountryList(List<Country> countries);
}
