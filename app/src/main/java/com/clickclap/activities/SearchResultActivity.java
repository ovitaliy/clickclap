package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.clickclap.R;
import com.clickclap.fragment.SearchResultFragment;

/**
 * Created by Deni on 22.06.2015.
 */
public class SearchResultActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, SearchResultActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        startFragment(SearchResultFragment.newInstance(), false);
    }
}
