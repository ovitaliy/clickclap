package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.dialogs.ShareDialogFragment;
import com.clickclap.enums.VideoType;
import com.clickclap.fragment.video.AudioFragment;
import com.clickclap.fragment.video.FilterFragment;
import com.clickclap.fragment.video.FramePreviewFragment;
import com.clickclap.fragment.video.NewGrimaceFragment;
import com.clickclap.fragment.video.VideoPreviewFragment;
import com.clickclap.fragment.video.VideoRecordFragment;
import com.clickclap.jobs.SaveGrimaceToUploadJob;
import com.clickclap.jobs.SaveVideoToUploadJob;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.model.VideoInfo;
import com.clickclap.rest.model.AddGrimaceResponse;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.NetworkUtil;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.humanet.audio.MoveFilesJob;
import com.humanet.filters.videofilter.BaseFilter;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class CreateVideoActivity extends BaseActivity implements
        VideoRecordFragment.OnVideoRecordCompleteListener,
        VideoPreviewFragment.OnVideoCommandSelected {

    protected ProgressDialog mProgressDialog;

    public static Intent newInstance(Context context) {
        return new Intent(context, CreateVideoActivity.class);
    }

    /**
     * Information about video.
     */
    private VideoPreviewFragment mVideoPreviewFragment;

    protected void onCreate(Bundle savedInstanceState, VideoType videoType) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            NewVideoInfo.init(videoType);
        } else {
            VideoInfo video = (VideoInfo) savedInstanceState.get("video");
            NewVideoInfo.set(video);
        }

        initFfmpeg();

        MoveFilesJob.run(this, FilePathHelper.getAudioDirectory());
    }

    protected void openPreview() {
        mVideoPreviewFragment = VideoPreviewFragment.newInstance(VideoPreviewFragment.ACTION_CREATE);
        startFragment(mVideoPreviewFragment, true);
    }

    @Override
    public void onVideoRecordComplete(String videoPath, int cameraId) {
        NewVideoInfo.get().setAudioPath(FilePathHelper.getAudioStreamFile().getAbsolutePath());
        NewVideoInfo.get().setFilter(new BaseFilter());
        NewVideoInfo.get().setAudioApplied(NewVideoInfo.get().getAudioPath());
        NewVideoInfo.get().setImageFilterApplied(null);
        NewVideoInfo.get().setVideoFilterApplied(null);
        NewVideoInfo.get().setCameraId(cameraId);

        int framesCount = FilePathHelper.getVideoFrameFolder().listFiles().length;
        NewVideoInfo.get().setOriginalImagePath(FilePathHelper.getVideoFrameImagePath(Math.max(framesCount / 2 - 1, 0)));
        NewVideoInfo.get().setOriginalVideoPath(videoPath);
        VideoPreviewFragment.VideoProcessTask.getInstance().mPreviewBitmap = null;

        NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());

        openPreview();
    }


    @Override
    public void onOpenRecorder(int steps) {
        if (steps == 0)
            startFragment(VideoRecordFragment.newInstance(NewVideoInfo.get().getSmileId()), true);

        while (steps-- > 0)
            onBackPressed();

    }

    @Override
    public void onOpenFramePreview() {
        startFragment(FramePreviewFragment.newInstance(), true);
    }

    @Override
    public void onOpenTrack() {
        startFragment(AudioFragment.newInstance(), true);
    }

    @Override
    public void onOpenFilter() {
        startFragment(FilterFragment.newInstance(), true);
    }

    @Override
    public void onOpenGrimace() {
        startFragment(NewGrimaceFragment.newInstance(NewVideoInfo.get().getImagePath(), NewVideoInfo.get().getSmileId()), true);
    }

    @Override
    public void onComplete(int lastAction) {
        switch (lastAction) {
            case VideoPreviewFragment.ACTION_CREATE:
                if (NewVideoInfo.get().getSmileId() == -1) {
                    onOpenDescription();
                    break;
                }
            default:

                if (!NewVideoInfo.get().isGrimace()) {
                    Log.e("Test", "Not grimace");
                    sendVideo();
                } else {
                    sendGrimace();
                }

                if (NetworkUtil.isOfflineMode()) {
                    finish();
                }
        }
    }


    protected void sendGrimace() {
        AnalyticsHelper.trackEvent(this, AnalyticsHelper.GRIMACE_CREATE);
        SaveGrimaceToUploadJob.run(NewVideoInfo.get().clone());
    }


    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AddGrimaceResponse addGrimaceResponse) {

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        int id = addGrimaceResponse.getId();

        if (id != 0) {
            String url = getString(R.string.share_grimace_url, String.valueOf(id));
            new ShareDialogFragment.Builder()
                    .setCanBeClosed(true)
                    .setShareType(ShareDialogFragment.TYPE_GRIMACE)
                    .setVideoId(id)
                    .setLink(url)
                    .build()
                    .show(getSupportFragmentManager(), ShareDialogFragment.TAG);
        }
    }

    protected void sendVideo() {
        VideoInfo info = NewVideoInfo.get().clone();

        if (info.getSmileId() >= 0 && AppUser.get() != null) {
            AppUser.get().setPhoto(info.getImagePath());
        }

        if (info.getVideoType() != null) {
            switch (info.getVideoType()) {
                case EMOTICON_ALL:
                case EMOTICON_BEST:
                case EMOTICON_MY_CHOICE:
                    //TODO
                    AnalyticsHelper.trackEvent(this, AnalyticsHelper.CODE_REQUEST);
                    break;
                case CROWD_GIVE:
                case CROWD_ASK:
                    if (!info.isReply()) {
                        AnalyticsHelper.trackEvent(this, AnalyticsHelper.CROWD_CREATE);
                    }
                    break;
                default:
                    AnalyticsHelper.trackEvent(this, AnalyticsHelper.VIDEO_RECORDING);
                    break;
            }
        }

        SaveVideoToUploadJob.run(info);
    }

    protected void onOpenDescription() {
        startFragment(VideoPreviewFragment.newInstance(VideoPreviewFragment.ACTION_DESCRIPTION), true);
    }

    protected void onOpenPreview() {
        startFragment(VideoPreviewFragment.newInstance(VideoPreviewFragment.ACTION_VIEW), true);
    }

    protected void onSendVideo(int videoId, String res) {

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("video", NewVideoInfo.get());
    }

    private void initFfmpeg() {
        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                }

                @Override
                public void onFailure() {
                    Log.e("ffmpeg", "binary loading fails");
                }

                @Override
                public void onSuccess() {
                    Log.i("ffmpeg", "binary successful loaded");
                }

                @Override
                public void onFinish() {

                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }
    }

}
