package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.clickclap.R;
import com.clickclap.fragment.FeedbackChatFragment;
import com.clickclap.model.Feedback;

/**
 * Created by Deni on 06.07.2015.
 */
public class FeedbackActivity extends BaseActivity {

    public static Intent newIntent(Context context, int feedbackId, @Feedback.FeedbackType int type) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra("feedback_id", feedbackId);
        intent.putExtra("type", type);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        int id = getIntent().getIntExtra("feedback_id", -1);
        @Feedback.FeedbackType int type = getIntent().getIntExtra("type", 0);
        startFragment(FeedbackChatFragment.newInstance(id, type), false);
    }
}
