package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.clickclap.R;
import com.clickclap.fragment.GuessSmileFragment;
import com.clickclap.model.Video;

public class GuessSmileActivity extends BaseActivity {

    public static void startNewInstance(Context context, Video video) {
        Intent intent = new Intent(context, GuessSmileActivity.class);
        intent.putExtra("video", video);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_container);
        Video video = (Video) getIntent().getSerializableExtra("video");
        if (video.getSmileId() != -1) {
            startFragment(GuessSmileFragment.newInstance(video), false);
        }
    }
}
