package com.clickclap.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.dialogs.ErrorDialogFragment;
import com.clickclap.enums.Emotion;
import com.clickclap.enums.ServerError;
import com.clickclap.enums.VideoType;
import com.clickclap.events.ServerErrorEvent;
import com.clickclap.events.ShowProgressDialogEvent;
import com.clickclap.fragment.BaseFragment;
import com.clickclap.fragment.FlowListFragment;
import com.clickclap.model.Grimace;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.GetGrimacesCacheRequestListener;
import com.clickclap.rest.request.grimace.GetGrimacesRequest;
import com.clickclap.rest.service.AppFileRetrofitSpiceService;
import com.clickclap.rest.service.AppRetrofitSpiceService;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.PrefHelper;
import com.crashlytics.android.Crashlytics;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.CachedSpiceRequest;
import com.octo.android.robospice.request.listener.SpiceServiceListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by ovitali on 08.01.2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements SpiceContext {
    ErrorDialogFragment mErrorDialog;
    SpiceServiceListener mSpiceServiceListener;

    public static final int REQ_CODE_VIDEO_CREATED = 1231;
    public static final int REQ_CODE_AVATAR_CREATED = 1232;

    private SpiceManager mSpiceManager = new SpiceManager(AppRetrofitSpiceService.class);

    public void startFragment(BaseFragment f, boolean addToBackStack, boolean popBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        String tag = f.getClass().getSimpleName();

        if (popBackStack) {
            manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            if (manager.findFragmentByTag(tag) != null) {
                return;
            }
        }

        FragmentTransaction ft = manager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.container, f, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }

        ft.commitAllowingStateLoss();
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.actionbar_main);

            actionBar.setDisplayShowTitleEnabled(true);
        }
    }

    public void setActionBarTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            View view = actionBar.getCustomView();
            TextView titleView = (TextView) view.findViewById(R.id.title);
            if (!TextUtils.isEmpty(title)) {
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(title);
            } else {
                titleView.setVisibility(View.GONE);
            }
        }
    }

    public void setActionBarOfflineMode(boolean offlineMode) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            View view = actionBar.getCustomView();
            TextView offlineView = (TextView) view.findViewById(R.id.offline_mode);
            if (offlineMode) {
                offlineView.setVisibility(View.VISIBLE);
            } else {
                offlineView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        mSpiceServiceListener = new SpiceServiceListener() {
            @Override
            public void onRequestSucceeded(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(false);
            }

            @Override
            public void onRequestFailed(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(false);
            }

            @Override
            public void onRequestCancelled(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(false);
            }

            @Override
            public void onRequestProgressUpdated(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onRequestAdded(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(true);
            }

            @Override
            public void onRequestAggregated(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onRequestNotFound(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onRequestProcessed(CachedSpiceRequest<?> cachedSpiceRequest, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onServiceStopped() {
                showProgressDialog(false);
            }
        };
        mSpiceManager.addSpiceServiceListener(mSpiceServiceListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        mSpiceManager.removeSpiceServiceListener(mSpiceServiceListener);
    }

    private void showProgressDialog(boolean show) {
        EventBus.getDefault().post(new ShowProgressDialogEvent(show));
    }

    @Override
    protected void onStart() {
//        mSpiceManager = new SpiceManager(AppRetrofitSpiceService.class);
//        mFileSpiceManager = new SpiceManager(AppFileRetrofitSpiceService.class);
        mSpiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mSpiceManager.shouldStop();
        super.onStop();
    }

    public void logout() {
        mSpiceManager.cancelAllRequests();
        AppUser.clear();
        PrefHelper.setStringPref(Const.PREF_TOKEN, null);
        PrefHelper.setBooleanPref(Const.PREF_REG, false);
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerErrorEvent(ServerErrorEvent event) {
        if (mErrorDialog != null && mErrorDialog.isVisible()) {
            mErrorDialog.dismiss();
        }
        int[] codes = event.getCodes();
        String[] messages = event.getMessages();

        if (codes != null) {
            int errorCode = -1;
            String errorMessage = "";
            for (int i = 0; i < codes.length; i++) {
                int code = codes[i];
                if (code == ServerError.WRONG_ACCESS_TOKEN.getCode()) {
                    logout();
                    return;
                } else {
                    errorCode = code;
                    if (messages != null && messages.length > i) {
                        errorMessage = messages[i];
                    }
                }
            }
            mErrorDialog = ErrorDialogFragment.newInstance(errorCode, errorMessage);
            mErrorDialog.show(getSupportFragmentManager(), "error");
        }
    }

    public void startFragment(BaseFragment f, boolean addToBackStack) {
        startFragment(f, addToBackStack, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQ_CODE_VIDEO_CREATED:
                    if (data != null) {
                        openFlowList(data);
                    }
                    break;
            }
        }
    }

    private void openFlowList(Intent data) {
        String params = data.getStringExtra(Const.Params.PARAMS);
        Emotion emotion = (Emotion) data.getSerializableExtra(Const.Params.EMOTION);
        VideoType videoType = (VideoType) data.getSerializableExtra(Const.Params.TYPE);
        int replyId = data.getIntExtra(Const.Params.REPLY_ID, 0);
        FlowListFragment fragment = new FlowListFragment.Builder(videoType)
                .setParams(params)
                .setEmotion(emotion)
                .setReplyId(replyId)
                .build();

        startFragment(fragment, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (App.WIDTH == 0) {
            DisplayInfo displayInfo = new DisplayInfo(this);
            App.WIDTH = displayInfo.getScreenWidth();
            App.HEIGHT = displayInfo.getScreenHeight();

            App.MARGIN = getResources().getDimensionPixelSize(R.dimen.mrg_normal);
            App.WIDTH_WITHOUT_MARGINS = App.WIDTH - App.MARGIN * 2;

            App.IMAGE_BORDER = getResources().getDimensionPixelSize(R.dimen.stream_item_small_border);
            App.IMAGE_SMALL_WIDTH = (int) (App.WIDTH / 4.4 - App.IMAGE_BORDER * 4f);
        }

        initActionBar();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    public void navigateBack() {
        try {
            super.onBackPressed();
        } catch (Exception ex) {
            Crashlytics.log("crash on navigate back" + this.getClass().toString());
            throw ex;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (f != null && f instanceof BaseFragment) {
            ((BaseFragment) f).onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}
