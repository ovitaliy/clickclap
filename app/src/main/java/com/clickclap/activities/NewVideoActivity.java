package com.clickclap.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.dialogs.ShareDialogFragment;
import com.clickclap.enums.Emotion;
import com.clickclap.enums.VideoType;
import com.clickclap.fragment.registration.SelectEmoticonFragment;
import com.clickclap.fragment.video.VideoPreviewFragment;
import com.clickclap.fragment.video.VideoRecordFragment;
import com.clickclap.model.Category;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.model.Video;
import com.clickclap.rest.model.AddVideoResponse;
import com.clickclap.view.NewVideoView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by ovitali on 28.01.2015.
 */
public class NewVideoActivity extends CreateVideoActivity implements SelectEmoticonFragment.OnSelectSmileListener, NewVideoView, Const {

    public static void startNewInstance(Activity context) {
        Intent intent = new Intent(context, NewVideoActivity.class);
        context.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    public static void startNewInstance(Activity context, Video replyForVideo) {
        Intent intent = new Intent(context, NewVideoActivity.class);
        intent.putExtra(Params.REPLY_VIDEO, replyForVideo);
        context.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    public static void startNewInstance(Activity context, Emotion emotion, VideoType videoType) {
        Intent intent = new Intent(context, NewVideoActivity.class);
        intent.putExtra(Params.EMOTION, emotion);
        intent.putExtra(Params.TYPE, videoType);
        context.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    public static void startNewGrimaceInstance(Activity context, Emotion emotion) {
        Intent intent = new Intent(context, NewVideoActivity.class);
        intent.putExtra(Params.EMOTION, emotion);
        intent.putExtra(Params.IS_GRIMACE, true);
        context.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    public static void startNewInstance(Activity activity, VideoType videoType) {
        Intent intent = new Intent(activity, NewVideoActivity.class);
        intent.putExtra(Params.TYPE, videoType);
        activity.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    public static void startNewAvatarInstance(Activity activity) {
        Intent intent = new Intent(activity, NewVideoActivity.class);
        intent.putExtra(Params.TYPE, VideoType.EMOTICON_ALL);
        activity.startActivityForResult(intent, REQ_CODE_AVATAR_CREATED);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, getVideoType());
        setContentView(R.layout.activity_base_fragment_container);

        NewVideoInfo.get().setGrimace(getIntent().getBooleanExtra(Params.IS_GRIMACE, false));

        Video videoToReply = getVideoToReply();
        Emotion emotion = getEmotion();

        if (NewVideoInfo.isVideoRecorded()) {
            openPreview();
        } else {
            if (videoToReply != null) {
                NewVideoInfo.get().setReplyToVideoId(videoToReply.getVideoId());
                NewVideoInfo.get().setSmileId(videoToReply.getSmileId());
                NewVideoInfo.get().setVideoType(VideoType.getByCode(videoToReply.getType()));
                NewVideoInfo.get().setCategoryId(videoToReply.getCategoryId());
                startFragment(VideoRecordFragment.newInstance(NewVideoInfo.get().getSmileId()), false);
            } else if (emotion != null) {
                NewVideoInfo.get().setSmileId(emotion.getId());
                startFragment(VideoRecordFragment.newInstance(NewVideoInfo.get().getSmileId()), false);
            } else {
                VideoType type = getVideoType();
                if (type != null && type.equals(VideoType.EMOTICON_ALL)) {
                    startFragment(SelectEmoticonFragment.newInstance(), false);
                } else {
                    startFragment(VideoRecordFragment.newInstance(), false);
                }
            }
        }
    }

    @Override
    public void onSmileSelected(int smileId) {
        NewVideoInfo.get().setSmileId(smileId);
        onOpenRecorder(0);
    }

    @Override
    protected void onOpenPreview() {
        startFragment(VideoPreviewFragment.newInstance(VideoPreviewFragment.ACTION_VIEW), true);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AddVideoResponse addVideoResponse) {
        onSendVideo(addVideoResponse.getVideoId(), addVideoResponse.getShortUrl());
    }

    @Override
    protected void onSendVideo(int videoId, String res) {
        boolean isDialogShown = false;
        VideoType videoType = getVideoType();

        int shareType;
        if (VideoType.isGreetingType(videoType)) {
            shareType = ShareDialogFragment.TYPE_GREETING;
        } else if (VideoType.isEmoticonType(videoType)) {
            shareType = ShareDialogFragment.TYPE_EMOTICON;
        } else {
            shareType = ShareDialogFragment.TYPE_OTHER;
        }

        if (NewVideoInfo.get().getReplyToVideoId() == 0 && !NewVideoInfo.get().isGrimace()) {
            new ShareDialogFragment.Builder()
                    .setCanBeClosed(true)
                    .setShareType(shareType)
                    .setLink(res)
                    .setVideoId(videoId)
                    .build()
                    .show(getSupportFragmentManager(), ShareDialogFragment.TAG);
            isDialogShown = true;
        }

        setResultData();

        if (!isDialogShown) {
            finish();
        }
    }

    private void setResultData() {
        Intent intent = new Intent();
        Category category = Category.getFromDatabase(NewVideoInfo.get().getCategoryId(), NewVideoInfo.get().getVideoType());
        int smileId = NewVideoInfo.get().getSmileId();
        if (category != null) {
            intent.putExtra(Params.PARAMS, category.getParams());
        } else if (smileId > 0) {
            intent.putExtra(Params.EMOTION, Emotion.getById(smileId));
        }
        intent.putExtra(Params.TYPE, NewVideoInfo.get().getVideoType());
        intent.putExtra(Params.REPLY_ID, NewVideoInfo.get().getReplyToVideoId());
        setResult(RESULT_OK, intent);
    }

    private Video getVideoToReply() {
        Video video = null;
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Params.REPLY_VIDEO)) {
            video = (Video) intent.getSerializableExtra(Params.REPLY_VIDEO);
        }
        return video;
    }

    private Emotion getEmotion() {
        Emotion emotion = null;
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Params.EMOTION)) {
            emotion = (Emotion) intent.getSerializableExtra(Params.EMOTION);
        }
        return emotion;
    }

    @Override
    public VideoType getVideoType() {
        VideoType videoType = null;
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Params.TYPE)) {
            videoType = (VideoType) intent.getSerializableExtra(Params.TYPE);
        }
        return videoType;
    }

    @Override
    public boolean isGrimaceMode() {
        return (getVideoType() == null && getEmotion() != null);
    }
}
