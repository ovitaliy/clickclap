package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.enums.VideoType;
import com.clickclap.events.LoadedFileEvent;
import com.clickclap.events.OnGetFlowEvent;
import com.clickclap.jobs.VideoPreloadRunnable;
import com.clickclap.model.Video;
import com.clickclap.rest.listener.FlowRequestListener;
import com.clickclap.rest.request.video.GetFlowRequest;
import com.clickclap.util.BackgroundCutter;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.FilePathHelper;
import com.clickclap.view.square.SquareRelativeLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PlayFlowActivity extends BaseActivity implements View.OnTouchListener, Const, SurfaceHolder.Callback {

    ExecutorService mExecutorService = Executors.newSingleThreadExecutor();

    public static void startNewInstance(Context context, String params, int replyId, String title, ArrayList<Video> list, VideoType videoType) {
        Intent intent = new Intent(context, PlayFlowActivity.class);
        intent.putExtra(Params.VIDEO_LIST, list);
        intent.putExtra(Params.PARAMS, params);
        intent.putExtra(Params.REPLY_ID, replyId);
        intent.putExtra(Params.TITLE, title);
        intent.putExtra(Params.ACTION, Const.Action.PLAY_FLOW);
        intent.putExtra(Params.TYPE, videoType);
        context.startActivity(intent);
    }

    public static void startNewInstance(Context context, Video video) {
        Intent intent = new Intent(context, PlayFlowActivity.class);
        intent.putExtra(Const.Params.VIDEO_LIST, video);
        intent.putExtra(Const.Params.ACTION, Const.Action.PLAY_FLOW_SINGLE_VIDEO);
        context.startActivity(intent);
    }

    private ArrayList<Video> mVideos;
    private int mCurrentPosition = 0;

    private VideoView mVideoView;
    private ImageView mVideoPreview;
    private ProgressBar mProgressBar;

    private int mLoadedCount;
    private boolean mIsPlaying;

    private View mFadingView;

    private boolean mSurfaceIsAlive;

    private int mCurrentAction;

    private String mFlowRequestParams;

    private String mTitle;

    private int mReplyId;

    private VideoType mVideoType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_flow);

        mVideoView = (VideoView) findViewById(R.id.video_displaying_surface_view);
        mVideoPreview = (ImageView) findViewById(R.id.preview_image);
        mProgressBar = (ProgressBar) findViewById(R.id.video_loading_progress);
        mFadingView = findViewById(R.id.fading_view);

        mCurrentAction = getIntent().getIntExtra(Const.Params.ACTION, 0);
        if (mCurrentAction == Const.Action.PLAY_FLOW) {
            mFlowRequestParams = getIntent().getStringExtra(Params.PARAMS);
            mTitle = getIntent().getStringExtra(Params.TITLE);
            mReplyId = getIntent().getIntExtra(Params.REPLY_ID, 0);
            mVideos = (ArrayList<Video>) getIntent().getSerializableExtra(Params.VIDEO_LIST);
            mFadingView.setOnTouchListener(this);
            mVideoView.setOnTouchListener(this);
        } else {
            Video video = (Video) getIntent().getSerializableExtra(Params.VIDEO_LIST);
            mVideos = new ArrayList<>(1);
            mVideoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateBack();
                }
            });
            mVideos.add(video);
        }

        if (getIntent().hasExtra(Params.TYPE)) {
            mVideoType = (VideoType) getIntent().getSerializableExtra(Params.TYPE);
        }

        if (mVideos != null && mVideos.size() > 0) {
            Video video = mVideos.get(0);
            ImageLoader.getInstance().displayImage(video.getThumbUrl(), mVideoPreview);
        }

        initBackgroundImage();
    }

    public void initBackgroundImage() {
        DisplayInfo info = new DisplayInfo(this);
        final int mDisplayWidth = info.getScreenWidth();
        final int mDisplayHeight = info.getContentHeight();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                RelativeLayout videoContainer = (RelativeLayout) findViewById(R.id.video_displaying_container);
                SquareRelativeLayout squareRelativeLayout = (SquareRelativeLayout) findViewById(R.id.squareMarker);
                final BackgroundCutter cutter = new BackgroundCutter();
                Bitmap background = cutter.cutBitmapColor(R.color.screen_background, mDisplayWidth, mDisplayHeight, squareRelativeLayout);
                if (background == null) {
                    handler.postDelayed(this, 100);
                } else {
                    videoContainer.setBackgroundDrawable(new BitmapDrawable(getResources(), background));

                    int w = squareRelativeLayout.getWidth();

                    mVideoView.getLayoutParams().width = w;
                    mVideoView.getLayoutParams().height = w;

                    int squareCenterX = squareRelativeLayout.getLeft() + squareRelativeLayout.getWidth() / 2;
                    int squareCenterY = squareRelativeLayout.getTop() + squareRelativeLayout.getHeight() / 2;

                    mVideoView.setX(squareCenterX - w / 2);
                    mVideoView.setY(squareCenterY - w / 2);
                }

            }
        }, 100);
    }

    public void nextStep() {
        if (mCurrentAction == Const.Action.PLAY_FLOW) {
            play();
        } else {
            finish();
        }
    }

    private boolean play() {

        if (!mSurfaceIsAlive) {
            mIsPlaying = false;
            Log.e("PLAY FLOW", "surface isn't alive");
            return false;
        }
        if (!mVideoView.getHolder().getSurface().isValid()) {
            mIsPlaying = false;
            Log.e("PLAY FLOW", "surface isn't valid");
            return false;
        }

        if (mCurrentPosition >= mVideos.size()) {
            //redirect to video list
            openFlowScreen();
            mIsPlaying = false;
            Log.e("PLAY FLOW", "cur pos=" + mCurrentPosition + ", size:" + mVideos.size());
            return false;
        }

        String videoUrl = mVideos.get(mCurrentPosition).getMedia();
        videoUrl = videoUrl.substring(videoUrl.lastIndexOf("/") + 1);
        File file = new File(FilePathHelper.getVideoCacheDirectory(), videoUrl);

        if (!file.exists()) {
            if (mCurrentPosition + 1 < mVideos.size()) {
                Video video = mVideos.get(mCurrentPosition + 1);
                ImageLoader.getInstance().displayImage(video.getThumbUrl(), mVideoPreview);
            } else {
                mCurrentPosition++;
                nextStep();
            }
            mIsPlaying = false;
        } else {
            mVideoView.setVideoPath(file.getAbsolutePath());
            mVideoPreview.setVisibility(View.GONE);
            mFadingView.setVisibility(View.GONE);

            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                    mCurrentPosition++;
                    nextStep();
                }
            });

            mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(final MediaPlayer mp, int what, int extra) {
                    Log.e("PLAY FLOW", "error " + what + ", extra:" + extra);
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mIsPlaying = false;
                            mp.reset();
                            mCurrentPosition++;
                            nextStep();
                        }
                    }, 50);

                    return true;
                }
            });
        }
        try {
            mVideoView.start();
            mIsPlaying = true;
        } catch (Exception ex) {
            Log.e("PLAY FLOW", "exception " + ex.toString());
            ex.printStackTrace();
            mIsPlaying = false;
            return false;
        }

        int duration = mVideoView.getDuration();

        int stepDuration;
        if (duration <= 2000) {
            stepDuration = 200;
        } else if (duration <= 4000) {
            stepDuration = 400;
        } else {
            stepDuration = 600;
        }

        Handler handler = new Handler();
        int steps = stepDuration / 40;
        for (int i = 0; i < steps + 1; i++) {
            float delta = 1.0f / ((float) steps) * i;
            handler.postDelayed(new VolumeChangeRunnable(delta), i * 40);

            Log.i("PlayFlowActivity", duration + "  " + (i * 40) + "  " + "  " + delta);
        }

        for (int i = 0; i < steps; i++) {
            float delta = 1.0f / ((float) steps) * ((float) (steps - i));
            int d = duration - stepDuration + i * 40;
            handler.postDelayed(new VolumeChangeRunnable(delta), d);

            Log.i("PlayFlowActivity", duration + "  " + d + "  " + "  " + delta);
        }
//        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        mVideoView.getHolder().addCallback(this);

        if (mIsPlaying) {
            if (!play()) {
                openFlowScreen();
            } else {
                runPreloader();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mVideoView != null) {
            mVideoView.stopPlayback();
            mVideoView = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void runPreloader() {
        if (mLoadedCount < mVideos.size()) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(mIsPlaying ? View.GONE : View.VISIBLE);
                }
            });

            mExecutorService.submit(new VideoPreloadRunnable(mVideos.get(mLoadedCount).getMedia()));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadedFileEvent(LoadedFileEvent event) {
        mLoadedCount++;
        if (mLoadedCount < mVideos.size())
            runPreloader();

        if (!TextUtils.isEmpty(event.getPath())) {
            if (!mIsPlaying) {
                play();
            }
        } else {
            if (mVideoView != null) {
                mVideoView.stopPlayback();
            }
            mIsPlaying = false;
            mCurrentPosition++;
            nextStep();
        }

//        if (mLoadedCount >= mVideos.size() - 2) {
//            if (mCurrentAction == Const.Action.PLAY_FLOW)
//                loadNextPage();
//        }
        if (mProgressBar != null) {
            mProgressBar.setVisibility(mIsPlaying ? View.GONE : View.VISIBLE);
        }
    }

    public void loadNextPage() {
        GetFlowRequest request = new GetFlowRequest(mFlowRequestParams);

        long lastId = mVideos.get(mVideos.size() - 1).getVideoId();

        request.setLast(lastId);

        if (mReplyId != 0) {
            request.setReplyId(mReplyId);
        }

        SpiceManager manager = getSpiceManager();
        if (manager != null) {
            manager.execute(request, new FlowRequestListener(1, lastId > 0));
        }
    }

    @Subscribe
    public void onEvent(OnGetFlowEvent event) {
        ArrayList<Video> list = event.getVideos();
        mVideos.addAll(list);
    }

    private void openFlowScreen() {
        Intent intent = new Intent(this, MainActivity.class);

        intent.putExtra(Params.ACTION, Const.Action.PLAY_FLOW);
        intent.putExtra(Params.PARAMS, mFlowRequestParams);
        intent.putExtra(Params.TITLE, mTitle);
        intent.putExtra(Params.REPLY_ID, mReplyId);
        intent.putExtra(Params.POSITION, mCurrentPosition - 1);
        intent.putExtra(Params.TYPE, mVideoType);

        startActivity(intent);

        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            openFlowScreen();
        }
        return false;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mSurfaceIsAlive = true;
        runPreloader();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mSurfaceIsAlive = false;
    }

    private class VolumeChangeRunnable implements Runnable {

        private float mVolume;

        public VolumeChangeRunnable(float volume) {
            mVolume = volume;
        }

        @Override
        public void run() {
            if (mVideoView == null)
                return;

            //mVideoView.setVolume(mVolume, mVolume);//TODO
            mFadingView.setAlpha(1.0f - mVolume);
        }
    }
}
