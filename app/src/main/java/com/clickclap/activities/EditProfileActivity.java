package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.events.ShowProgressDialogEvent;
import com.clickclap.fragment.registration.BaseProfileFragment;
import com.clickclap.fragment.registration.ProfileAboutFragment;
import com.clickclap.fragment.registration.ProfileInfoFragment;
import com.clickclap.model.UserInfo;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.request.user.EditUserInfoRequest;
import com.clickclap.view.TabView;

import org.greenrobot.eventbus.EventBus;

public class EditProfileActivity extends BaseActivity implements BaseProfileFragment.OnProfileFillListener, TabView.OnTabSelectListener {
    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        TabView tabView = (TabView) findViewById(R.id.tabhost);
        tabView.setOnTabSelectListener(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back);
        getSupportActionBar().setTitle(getString(R.string.profile_extended_title));

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFillProfile(AppUser.get());
            }
        });

        onTabSelected(null, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && data.hasExtra(Const.Params.ACTION)) {
        }
    }

    @Override
    public void onFillProfile(UserInfo userInfo) {
        saveProfile(userInfo);
    }

    public void saveProfile(final UserInfo userInfo) {
        AppUser.set(userInfo);
        EventBus.getDefault().post(new ShowProgressDialogEvent(true));

        getSpiceManager().execute(new EditUserInfoRequest(userInfo), new BaseRequestListener() {
            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                super.onRequestSuccess(baseResponse);
                Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                intent.putExtra(Const.Params.ACTION, Const.Action.SHOW_PROFILE);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onTabSelected(View view, int position) {
        switch (position) {
            case 0:
                startFragment(ProfileAboutFragment.newInstance(), false);
                break;
            case 1:
                startFragment(ProfileInfoFragment.newInstance(), false);
                break;
        }
    }
}
