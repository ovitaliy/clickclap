package com.clickclap.activities;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.GroupMembership;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.Groups;
import android.provider.Telephony;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.adapter.ContactsListAdapter;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.dialogs.SelectContactsTypeDialog;
import com.clickclap.dialogs.SelectPhoneNumberDialogFragment;
import com.clickclap.dialogs.ShareDialogFragment;
import com.clickclap.listener.NumberSelectedListener;
import com.clickclap.model.ContactItem;
import com.clickclap.model.FriendItem;
import com.clickclap.model.UserInfo;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.request.user.SendTokenRequest;
import com.clickclap.rest.request.video.ShareGrimaceRequest;
import com.clickclap.rest.request.video.ShareVideoRequest;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.view.GroupItemView;
import com.clickclap.view.items.ContactItemView;

import java.util.ArrayList;

public class ContactsActivity extends BaseActivity implements NumberSelectedListener {
    FriendItem mItem;
    private boolean mSingleMode;

    public static Intent newShareInstance(Context context, String shareLink, int videoId, @ShareDialogFragment.ShareType int shareType) {
        Intent intent = new Intent(context, ContactsActivity.class);
        intent.putExtra(Const.Params.ACTION, Const.Action.SHARE);
        intent.putExtra(Const.Params.LINK, shareLink);
        intent.putExtra(Const.Params.VIDEO_ID, videoId);
        intent.putExtra(Const.Params.TYPE, shareType);
        return intent;
    }

    public static Intent newPickInstance(Context context, int relationship) {
        Intent intent = new Intent(context, ContactsActivity.class);
        intent.putExtra(Const.Params.ACTION, Const.Action.GET_CONTACTS);
        intent.putExtra(Const.Params.RELATIONSHIP, relationship);
        return intent;
    }

    public static Intent newTokenInstance(Context context) {
        Intent intent = new Intent(context, ContactsActivity.class);
        intent.putExtra(Const.Params.ACTION, Const.Action.TOKEN);
        intent.putExtra("single_mode", true);
        return intent;
    }


    private static final String TAG = ContactsActivity.class.getSimpleName();

    private EditText lEdText;
    private ExpandableListView frListView;
    private ContactsListAdapter mAdapter;

    private ArrayList<Group> mGroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contacts);

        mSingleMode = getIntent().getBooleanExtra("single_mode", false);

        mGroups = new ArrayList<>();

        lEdText = (EditText) findViewById(R.id.search_field);

        lEdText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateList(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        int dummyHeight = getResources().getDimensionPixelOffset(R.dimen.round_button_size) + getResources().getDimensionPixelOffset(R.dimen.mrg_normal) * 2;
        View dummyView = new View(this);
        dummyView.setLayoutParams(new AbsListView.LayoutParams(dummyHeight, dummyHeight));

        frListView = (ExpandableListView) findViewById(R.id.friend_list);
        frListView.addFooterView(dummyView, null, false);

        mAdapter = new ContactsListAdapter(this, mGroups, frListView);
        frListView.setAdapter(mAdapter);

        frListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (mSingleMode) {
                    unselectAll();
                }
                ContactItemView friend = (ContactItemView) v;
                friend.setCheck(!friend.getCheck());
                return true;
            }
        });
        frListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSingleMode) {
                    unselectAll();
                }
                ContactItemView friend = (ContactItemView) view;
                friend.setCheck(!friend.getCheck());
            }
        });

        frListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (!mSingleMode) {
                    boolean val = !mAdapter.getGroup(groupPosition).checked;
                    ((GroupItemView) v).setCheck(val);

                    int childCount = mAdapter.getChildrenCount(groupPosition);
                    for (int i = 0; i < childCount; i++) {
                        mAdapter.getChild(groupPosition, i).setCheck(val);
                    }
                    mAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });


        new SelectContactsTypeDialog(this).setCallback(mContactLoader).show();

        findViewById(R.id.btn_nav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int action = getIntent().getIntExtra(Const.Params.ACTION, 0);
                switch (action) {
                    case Const.Action.SHARE:
                        share();
                        break;
                    case Const.Action.GET_CONTACTS:
                        getContacts();
                        break;
                    case Const.Action.TOKEN:
                        token();
                        break;
                }


            }
        });
    }

    private void unselectAll() {
        int groupCount = mAdapter.getGroupCount();
        for (int i = 0; i < groupCount; i++) {
            int childCount = mAdapter.getChildrenCount(i);
            for (int j = 0; j < childCount; j++) {
                mAdapter.getChild(i, j).setCheck(false);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private ArrayList<FriendItem> getSelectedContactsWithPhones() {
        ArrayList<Long> ids = new ArrayList<>();
        for (int i = 0; i < mGroups.size(); i++) {
            ArrayList<ContactItem> originalFriendItems = mGroups.get(i).list;
            for (int j = 0; j < originalFriendItems.size(); j++) {
                ContactItem fr = originalFriendItems.get(j);
                if (fr.isCheck()) {
                    ids.add(fr.getId());
                }
            }
        }

        Cursor cursor = getContentResolver().query(Phone.CONTENT_URI,
                new String[]{Phone.CONTACT_ID, Phone.NUMBER, Phone.DISPLAY_NAME, Phone.TYPE},
                Phone.CONTACT_ID + " IN (" + TextUtils.join(", ", ids) + ") " +
                        "AND " +
                        Phone.TYPE + " IN (" + TextUtils.join(", ", new Integer[]{Phone.TYPE_MOBILE, Phone.TYPE_WORK_MOBILE, Phone.TYPE_WORK, Phone.TYPE_HOME}) + ")",
                null,
                Phone.CONTACT_ID + " ASC");

        ArrayList<FriendItem> items = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                long curId = 0;
                FriendItem item = null;
                do {
                    long id = cursor.getLong(0);
                    if (id != curId) {
                        if (item != null) {
                            items.add(item);
                        }
                        item = new FriendItem();
                        String name = cursor.getString(2);
                        item.setName(name);
                    }
                    String number = cursor.getString(1);
                    item.addPhoneNumber(number);
                    curId = id;

                } while (cursor.moveToNext());

                if (item != null && !items.contains(item)) {
                    items.add(item);
                }
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return items;

    }

    public void getContacts() {
        ArrayList<FriendItem> list = getSelectedContactsWithPhones();

        Intent intent = new Intent();
        intent.putExtra(Const.Params.ACTION, Const.Action.GET_CONTACTS);
        intent.putExtra(Const.Params.RELATIONSHIP, getIntent().getIntExtra(Const.Params.RELATIONSHIP, 0));
        intent.putParcelableArrayListExtra(Const.Params.CONTACTS, list);

        setResult(RESULT_OK, intent);
        finish();
    }

    public void sendTokenToNumber(String number) {
        if (mItem != null) {
            getSpiceManager().execute(new SendTokenRequest(mItem.getName(), number), new BaseRequestListener() {
                @Override
                public void onRequestSuccess(BaseResponse baseResponse) {
                    super.onRequestSuccess(baseResponse);
                }
            });
            UserInfo info = AppUser.get();
            info.setToken(info.getToken() - 1);
            AppUser.set(info);
            finish();
        }
    }

    public void token() {
        ArrayList<FriendItem> items = getSelectedContactsWithPhones();
        if (items.size() > 0) {
            mItem = items.get(0);
            ArrayList<String> phones = mItem.getTrimmedPhones();
            if (phones.size() > 1) {
                SelectPhoneNumberDialogFragment.newInstance(mItem.getName(), phones).show(getSupportFragmentManager(), "select_number");
            } else {
                AnalyticsHelper.trackEvent(this, AnalyticsHelper.TOKEN_SEND);
                sendTokenToNumber(phones.get(0));
                Toast.makeText(this, R.string.token_invitation_sent, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void share() {
        String shareLink = getIntent().getStringExtra(Const.Params.LINK);
        int videoId = getIntent().getIntExtra(Const.Params.VIDEO_ID, 0);
        int shareType = getIntent().getIntExtra(Const.Params.TYPE, ShareDialogFragment.TYPE_SHOT);

        ArrayList<FriendItem> items = getSelectedContactsWithPhones();

        setResult(RESULT_OK);
        finish();

        if (items.size() == 0)
            return;

        String message = shareLink;
        StringBuilder toContact = new StringBuilder();
        StringBuilder trimmedContacts = new StringBuilder();
        for (FriendItem item : items) {
            ArrayList<String> phoneNumbers = item.getPhoneNumbers();
            ArrayList<String> trimmedNumbers = item.getTrimmedPhones();
            if (phoneNumbers != null && phoneNumbers.size() > 0) {
                String phoneNumber = phoneNumbers.get(0);
                toContact.append(phoneNumber);
                toContact.append(";");
            }
            if (trimmedNumbers != null && trimmedNumbers.size() > 0) {
                String phoneNumber = trimmedNumbers.get(0);
                trimmedContacts.append(phoneNumber);
                trimmedContacts.append(",");
            }
        }

        Intent smsIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // Android 4.4 and up
        {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this);
            smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + Uri.encode(toContact.toString())));
            smsIntent.putExtra("sms_body", message);
            if (defaultSmsPackageName != null) // Can be null in case that there is no default, then the user would be able to choose any app that supports this intent.
            {
                smsIntent.setPackage(defaultSmsPackageName);
            }
        } else {
            smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address", toContact.toString());
            smsIntent.putExtra("sms_body", message);
        }

        if (videoId != 0) {
            String phones = trimmedContacts.toString().substring(0, trimmedContacts.length() - 1);
            if (shareType == ShareDialogFragment.TYPE_GRIMACE) {
                getSpiceManager().execute(new ShareGrimaceRequest(videoId, phones), new BaseRequestListener());
            } else {
                getSpiceManager().execute(new ShareVideoRequest(videoId, phones), new BaseRequestListener());
            }
        }

        startActivity(smsIntent);
    }


    public ArrayList<Group> loadAll() {
        ArrayList<Group> list = new ArrayList<>(1);
        Group group = new Group();
        group.name = getString(R.string.dialog_share_sms_group_all);
        group.list = getUsersWhere(null);
        list.add(group);
        return list;
    }

    public ArrayList<Group> loadFavorites() {
        ArrayList<Group> list = new ArrayList<>(1);
        Group group = new Group();
        group.name = getString(R.string.dialog_share_sms_group_favorite);
        group.list = getUsersWhere(ContactsContract.Contacts.STARRED + "='1'");

        list.add(group);
        return list;
    }

    private AsyncTask<Integer, Void, ArrayList<Group>> mContactLoader = new AsyncTask<Integer, Void, ArrayList<Group>>() {
        ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new ProgressDialog(ContactsActivity.this);
            mDialog.setTitle(R.string.loading);
            mDialog.show();
        }

        @Override
        protected ArrayList<Group> doInBackground(Integer[] params) {
            int id = params[0];
            switch (id) {
                case R.id.rb_fav:
                    return loadFavorites();
                case R.id.rb_all:
                    return loadAll();
                case R.id.rb_group:
                    return loadGroups();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Group> groups) {
            super.onPostExecute(groups);
            mGroups = groups;
            updateList("");
            if (mDialog != null && !mDialog.isDismissed()) {
                mDialog.dismiss();
            }
        }
    };

    private ArrayList<Group> filter(String text) {
        text = text.toUpperCase();

        int n = mGroups != null ? mGroups.size() : 0;

        ArrayList<Group> groups = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            ArrayList<ContactItem> originalFriendItems = mGroups.get(i).list;
            ArrayList<ContactItem> friendItems = new ArrayList<>();
            for (int j = 0; j < originalFriendItems.size(); j++) {
                ContactItem fr = originalFriendItems.get(j);
                String name = fr.getName();
                if (!TextUtils.isEmpty(name)) {
                    if (name.toUpperCase().startsWith(text)) {
                        friendItems.add(fr);
                    }
                }
            }

            if (friendItems.size() > 0) {
                Group group = new Group();
                group.name = mGroups.get(i).name;
                group.list = friendItems;
                groups.add(group);
            }
        }

        return groups;
    }


    public ArrayList<Group> loadGroups() {
        ArrayList<Group> groups = new ArrayList<>();
        final String[] GROUP_PROJECTION = new String[]{
                ContactsContract.Groups._ID,
                ContactsContract.Groups.TITLE
        };

        Cursor c = getContentResolver().query(
                Groups.CONTENT_URI,
                GROUP_PROJECTION,
                null,
                null,
                null);
        final int IDX_ID = c.getColumnIndex(ContactsContract.Groups._ID);
        final int IDX_TITLE = c.getColumnIndex(ContactsContract.Groups.TITLE);


        while (c.moveToNext()) {
            Group g = new Group();
            g.id = c.getString(IDX_ID);
            g.name = c.getString(IDX_TITLE);
            g.list = fetchGroupMembers(g.id);
            groups.add(g);
        }
        c.close();

        Log.i(TAG, mGroups.toString() + mGroups.size());
        return groups;
    }

    public ArrayList<ContactItem> getUsersWhere(String where) {
        ArrayList<ContactItem> friendItems = new ArrayList<>();

        if (!TextUtils.isEmpty(where)) {
            where += " AND ";
        } else {
            where = "";
        }

        where += Data.HAS_PHONE_NUMBER + " = 1";

        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME},
                where,
                null,
                ContactsContract.Data.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
        if (cursor.moveToFirst()) {
            do {
                int contactId =
                        cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                ContactItem item = new ContactItem(contactId, name);
                friendItems.add(item);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return friendItems;
    }

    private void updateList(String text) {
        mAdapter.replace(filter(text));
    }

    private ArrayList<ContactItem> fetchGroupMembers(String groupId) {
        ArrayList<ContactItem> groupMembers = new ArrayList<>();
        String where = CommonDataKinds.GroupMembership.GROUP_ROW_ID + "=" + groupId
                + " AND "
                + CommonDataKinds.GroupMembership.MIMETYPE + "='"
                + CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE + "'"
                + " AND "
                + Data.HAS_PHONE_NUMBER + " = 1";
        String[] projection = new String[]{GroupMembership.RAW_CONTACT_ID, Data.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(Data.CONTENT_URI, projection, where, null,
                Data.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(GroupMembership.RAW_CONTACT_ID));
            String name = cursor.getString(cursor.getColumnIndex(Data.DISPLAY_NAME));
            ContactItem item = new ContactItem(id, name);
            groupMembers.add(item);
        }
        cursor.close();
        return groupMembers;
    }

    public String fetchPhone(int userId) {
        String phone = null;
        Cursor phoneFetchCursor = getContentResolver().query(Phone.CONTENT_URI,
                new String[]{Phone.NUMBER, Phone.DISPLAY_NAME, Phone.TYPE},
                Phone.CONTACT_ID + "=" + userId, null, null);
        while (phoneFetchCursor.moveToNext()) {
            phone = phoneFetchCursor.getString(phoneFetchCursor.getColumnIndex(Phone.NUMBER));
        }
        phoneFetchCursor.close();

        return phone;
    }


    public static class Group {
        public String id;
        public String name;
        public boolean checked;
        public ArrayList<ContactItem> list = new ArrayList<>();
    }

    @Override
    public void onNumberSelected(String number) {
        sendTokenToNumber(number);
    }
}
