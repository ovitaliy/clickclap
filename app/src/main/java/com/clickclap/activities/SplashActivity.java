package com.clickclap.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.clickclap.Const;
import com.clickclap.jobs.UploadGrimaceJob;
import com.clickclap.jobs.UploadVideoJob;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.util.NetworkUtil;
import com.clickclap.util.PrefHelper;

/**
 * Created by michael on 20.11.14.
 */
public class SplashActivity extends AppCompatActivity implements Const {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AnalyticsHelper.init(this);

        if (PrefHelper.getBooleanPref(PREF_REG)) {
            MainActivity.startNewInstance(SplashActivity.this);
        } else {
            if (!NetworkUtil.isOfflineMode()) {
                startActivity(new Intent(SplashActivity.this, RegistrationActivity.class));
            } else {
                Toast.makeText(this, "No Internet connection", Toast.LENGTH_LONG).show();
            }
        }

        AnalyticsHelper.trackEvent(this, AnalyticsHelper.ENTER_IN_APP);

        //TODO send gcm_token/null to server if client allowed/not allowed gcm notifications

        finish();

        UploadGrimaceJob.startUploader();
        UploadVideoJob.startUploader();
    }

}
