package com.clickclap.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.clickclap.R;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.rest.listener.AddVoteRequestListener;
import com.clickclap.rest.model.AddVoteResponse;
import com.clickclap.rest.request.vote.AddVoteRequest;
import com.clickclap.util.AnalyticsHelper;

import java.util.ArrayList;

/**
 * Created by Denis on 29.04.2015.
 */
public class NewVoteActivity extends BaseActivity implements View.OnClickListener {
    EditText mNewVote;
    EditText mNewOption1;
    EditText mNewOption2;
    EditText mNewOption3;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_vote);

        setActionBarTitle(getString(R.string.new_vote_hint));

        mNewVote = (EditText) findViewById(R.id.new_vote);

        mNewOption1 = (EditText) findViewById(R.id.new_option_1);
        mNewOption2 = (EditText) findViewById(R.id.new_option_2);
        mNewOption3 = (EditText) findViewById(R.id.new_option_3);

        findViewById(R.id.btn_save).setOnClickListener(this);
    }

    private boolean validate() {
        boolean success = true;
        String newVote = mNewVote.getText().toString();
        if (TextUtils.isEmpty(newVote)) {
            mNewVote.setError(getString(R.string.field_required));
            success = false;
        }

        String newOption1 = mNewOption1.getText().toString();
        if (TextUtils.isEmpty(newOption1)) {
            mNewOption1.setError(getString(R.string.field_required));
            success = false;
        }

        String newOption2 = mNewOption2.getText().toString();
        if (TextUtils.isEmpty(newOption2)) {
            mNewOption2.setError(getString(R.string.field_required));
            success = false;
        }
        return success;
    }

    @Override
    public void onClick(View view) {
        if (validate()) {
            if (mProgressDialog == null || !mProgressDialog.isShowing()) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.show();
            }

            String vote = mNewVote.getText().toString();

            ArrayList<String> optionsList = new ArrayList<>();

            String option1 = mNewOption1.getText().toString();
            if (!TextUtils.isEmpty(option1)) {
                optionsList.add(option1);
            }

            String option2 = mNewOption2.getText().toString();
            if (!TextUtils.isEmpty(option2)) {
                optionsList.add(option2);
            }

            String option3 = mNewOption3.getText().toString();
            if (!TextUtils.isEmpty(option3)) {
                optionsList.add(option3);
            }

            AnalyticsHelper.trackEvent(this, AnalyticsHelper.POLL_CREATE);

            getSpiceManager().execute(
                    new AddVoteRequest(vote, optionsList.toArray(new String[optionsList.size()])),
                    new AddVoteRequestListener(vote, optionsList) {
                        @Override
                        public void onRequestSuccess(AddVoteResponse response) {
                            super.onRequestSuccess(response);
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                            finish();
                        }
                    });
        }
    }
}
