package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.model.UserInfo;

/**
 * Created by Deni on 27.08.2015.
 */
public class TokenActivity extends BaseActivity implements View.OnClickListener {
    TextView mMessage;
    Button mSendToken;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, TokenActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.token);

        mMessage = (TextView) findViewById(R.id.message);

        mSendToken = (Button) findViewById(R.id.send_token);
        mSendToken.setOnClickListener(this);

        setActionBarTitle(getString(R.string.token_title));
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.btn_back));
    }

    @Override
    protected void onResume() {
        super.onResume();

        UserInfo userInfo = AppUser.get();
        mMessage.setText(Html.fromHtml(getString(R.string.token_message_remains, userInfo.getToken())));

        if (userInfo.getToken() == 0) {
            mSendToken.setEnabled(false);
        } else {
            mSendToken.setEnabled(true);
        }
    }

    private void sendToken() {
        startActivity(ContactsActivity.newTokenInstance(this));
    }

    @Override
    public void onClick(View v) {
        sendToken();
    }
}
