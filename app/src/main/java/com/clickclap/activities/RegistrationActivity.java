package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.dialogs.DialogBuilder;
import com.clickclap.dialogs.ProgressDialog;
import com.clickclap.enums.VideoType;
import com.clickclap.fragment.registration.BaseProfileFragment;
import com.clickclap.fragment.registration.OkFragment;
import com.clickclap.fragment.registration.ProfileInfoFragment;
import com.clickclap.fragment.registration.SelectEmoticonFragment;
import com.clickclap.fragment.registration.VerificationFragment;
import com.clickclap.model.NewVideoInfo;
import com.clickclap.model.UserInfo;
import com.clickclap.receivers.SmsReceiver;
import com.clickclap.rest.listener.AuthRequestListener;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.listener.CountriesRequestListener;
import com.clickclap.rest.model.AuthResponse;
import com.clickclap.rest.model.BaseResponse;
import com.clickclap.rest.request.GetCountriesRequest;
import com.clickclap.rest.request.user.AuthRequest;
import com.clickclap.rest.request.user.EditUserInfoRequest;
import com.clickclap.util.PrefHelper;
import com.clickclap.util.Utils;
import com.clickclap.view.NewVideoView;


public class RegistrationActivity extends CreateVideoActivity implements Const,
        SelectEmoticonFragment.OnSelectSmileListener,
        OkFragment.OnCongratulationListener,
        VerificationFragment.OnVerifyListener,
        NewVideoView,
        BaseProfileFragment.OnProfileFillListener, SmsReceiver.SmsReceiverListener {

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, RegistrationActivity.class);
        context.startActivity(intent);
    }

    public static void startNewInstance(Context context, int action) {
        Intent intent = new Intent(context, RegistrationActivity.class);
        intent.putExtra(Params.ACTION, Action.FILL_PROFILE);
        context.startActivity(intent);
    }

    private VerificationFragment mVerificationFragment;

    private WakefulBroadcastReceiver mSmsBroadcastReceiver;

    /*
    Sync with server that performed once after retrieving auth_token
     */
    private void initSync() {
        getSpiceManager().execute(new GetCountriesRequest(), new CountriesRequestListener());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, getVideoType());
        setContentView(R.layout.activity_base_fragment_container);

        if (TextUtils.isEmpty(PrefHelper.getToken())) {
            AuthRequestListener authRequestListener = new AuthRequestListener() {
                @Override
                public void onRequestSuccess(AuthResponse response) {
                    super.onRequestSuccess(response);
                    initSync();
                }
            };
            getSpiceManager().execute(new AuthRequest(), authRequestListener);
        } else {
            initSync();
            Log.i(getClass().getSimpleName(), "user token " + PrefHelper.getToken());
        }

        if (getIntent().hasExtra(Params.ACTION) && getIntent().getIntExtra(Params.ACTION, 0) == Action.FILL_PROFILE) {
            startFragment(ProfileInfoFragment.newInstance(), false);
        } else if (NewVideoInfo.isVideoRecorded()) {
            openPreview();
        } else {
            startFragment(SelectEmoticonFragment.newInstance(), false);
        }


        if (Utils.isAndroidEmulator()) {
            onOpenVerification();
        }


    }


    @Override
    protected void onStart() {
        super.onStart();

        mSmsBroadcastReceiver = new SmsReceiver(this);
        registerReceiver(mSmsBroadcastReceiver, new IntentFilter(SmsReceiver.SMS_RECEIVED));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mSmsBroadcastReceiver);
    }


    @Override
    protected void sendVideo() {
        super.sendVideo();

        startFragment(OkFragment.newInstance(Const.EMOTICON_OK), true);
    }

    public void sendVerificationCode(CharSequence code) {
        if (mVerificationFragment == null)
            return;

        mVerificationFragment.sendVerificationCode(code);
    }


    @Override
    public void onSmileSelected(int smileId) {
        NewVideoInfo.get().setSmileId(smileId);
        onOpenRecorder(0);
    }

    public void openCongratulationsWindow() {
        startFragment(OkFragment.newInstance(Const.VIDEO_OK), true);
    }

    public void openSaveProfileCongratulationsWindow() {
        startFragment(OkFragment.newInstance(Const.PROFILE_OK), true);
    }

    @Override
    public void onOpenVerification() {
        mVerificationFragment = VerificationFragment.newInstance();
        startFragment(mVerificationFragment, true);
    }

    @Override
    public void onVerify(boolean isNewUser) {
        //isNewUser = true;
        if (isNewUser) {
            openConfirmVerification();
        } else {
            openMainActivity();
        }
    }

    private void openConfirmVerification() {
        startFragment(OkFragment.newInstance(VERIFY_OK), true);
    }

    public void onOpenFillProfile() {
        startFragment(ProfileInfoFragment.newInstance(), true);
    }

    @Override
    public void onRegistrationProcedureComplete() {
        openMainActivity();
    }

    public void openMainActivity() {
        PrefHelper.setBooleanPref(PREF_REG, true);
        finish();

        AppUser.clear();

        MainActivity.startNewInstance(this);
    }


    @Override
    public void onFillProfile(UserInfo userInfo) {
        saveProfile(userInfo);
    }

    public void saveProfile(UserInfo userInfo) {
        AppUser.set(userInfo);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.saving);
        progressDialog.show();
        getSpiceManager().execute(new EditUserInfoRequest(userInfo), new BaseRequestListener() {
            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                super.onRequestSuccess(baseResponse);
                progressDialog.dismiss();
                if (baseResponse.isSuccess()) {
                    openSaveProfileCongratulationsWindow();
                } else {
                    new DialogBuilder(RegistrationActivity.this).setMessage(baseResponse.getErrorsMessage()).create();
                }
            }
        });
    }

    @Override
    public VideoType getVideoType() {
        return VideoType.EMOTICON_ALL;
    }

    @Override
    public boolean isGrimaceMode() {
        return false;
    }


    @Override
    public void onGetSmsCode(CharSequence code) {
        sendVerificationCode(code);
    }
}
