package com.clickclap.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;

import com.clickclap.R;
import com.clickclap.fragment.InfoFragment;

/**
 * Created by Denis on 03.03.2015.
 */
public class AgreementActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        startFragment(InfoFragment.newInstance(), false);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back);
    }
}
