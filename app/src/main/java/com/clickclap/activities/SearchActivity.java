package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.clickclap.R;
import com.clickclap.fragment.SearchFragment;
import com.clickclap.fragment.SearchGrimaceFragment;

/**
 * Created by Deni on 18.06.2015.
 */
public class SearchActivity extends BaseActivity {

    public static Intent newIntent(Context context, boolean grimaceSearch) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra("grimace", grimaceSearch);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        if (getIntent().getBooleanExtra("grimace", false)) {
            startFragment(new SearchGrimaceFragment(), false);
        } else {
            startFragment(new SearchFragment(), false);
        }
    }
}