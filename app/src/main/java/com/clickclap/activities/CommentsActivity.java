package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.clickclap.R;
import com.clickclap.dialogs.MoreDialogFragment;
import com.clickclap.fragment.CommentsFragment;
import com.clickclap.fragment.ProfileFragment;
import com.clickclap.listener.OnViewProfileListener;
import com.clickclap.model.Video;
import com.clickclap.view.items.VideoItemView;

public class CommentsActivity extends BaseActivity implements
        VideoItemView.OnVideoChooseListener,
        VideoItemView.OnCommentListener,
        VideoItemView.OnMoreListener, OnViewProfileListener {

    public static void startNewInstance(Context context, Video video) {
        Intent intent = new Intent(context, CommentsActivity.class);
        intent.putExtra("video", video);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_container);
        Video video = (Video) getIntent().getSerializableExtra("video");
        startFragment(CommentsFragment.newInstance(video), false);
    }

    @Override
    public void onMore(Video video, Video videoParent) {
        MoreDialogFragment.newInstance(video, videoParent).show(getSupportFragmentManager(), MoreDialogFragment.TAG);
    }

    @Override
    public void onVideoChosen(Video video) {
        PlayFlowActivity.startNewInstance(this, video);
    }

    @Override
    public void onSmileGuessingStart(Video video) {
        GuessSmileActivity.startNewInstance(this, video);
    }

    @Override
    public void onViewProfile(int uid) {
        startFragment(ProfileFragment.newInstance(uid), true);
    }

    @Override
    public void onComment(Video video, String comment) {

    }

    @Override
    public void onShowComment(Video video) {
        startFragment(CommentsFragment.newInstance(video), false);
    }
}
