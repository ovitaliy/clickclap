package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.dialogs.MoreDialogFragment;
import com.clickclap.enums.Emotion;
import com.clickclap.enums.VideoType;
import com.clickclap.fragment.BaseFragment;
import com.clickclap.fragment.EmoticonsFragment;
import com.clickclap.fragment.FlowListFragment;
import com.clickclap.fragment.GrimaceFragment;
import com.clickclap.fragment.MenuFragment;
import com.clickclap.fragment.NavigationInfoFragment;
import com.clickclap.fragment.NavigationMarketplaceFragment;
import com.clickclap.fragment.ProfileFragment;
import com.clickclap.fragment.VistoryFragment;
import com.clickclap.listener.OnPlayFlowListener;
import com.clickclap.listener.OnUpdateVideoListener;
import com.clickclap.listener.OnViewProfileListener;
import com.clickclap.model.Video;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.listener.PayForVideoRequestListener;
import com.clickclap.rest.model.BaseCoinsResponse;
import com.clickclap.rest.request.video.MarkVideoRequest;
import com.clickclap.rest.request.video.PayForVideoRequest;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.util.PrefHelper;
import com.clickclap.view.items.VideoItemView;

import java.util.ArrayList;

/**
 * Created by michael on 20.11.14.
 */
public class MainActivity extends BaseActivity implements
        OnClickListener,
        MenuFragment.OnMenuListener,
        OnPlayFlowListener,
        VideoItemView.OnCommentListener,
        VideoItemView.OnVideoChooseListener,
        VideoItemView.OnMoreListener,
        OnViewProfileListener,
        OnUpdateVideoListener {

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    Toast mToast;

    private BaseFragment /*mFragment,*/ mPrevFragment;


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnalyticsHelper.identifyUser();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.btn_menu,  /* nav drawer icon to replace 'Up' caret */
                R.string.app_name,  /* "open drawer" description */
                R.string.app_name  /* "close drawer" description */
        );

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        onOpenEmoticons();

        App.syncGrimaces();

        //RegistrationActivity.startNewInstance(this, Constants.ACTION.FILL_PROFILE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PrefHelper.getIntPref("create_grimace") > 0) {
            //app is opened from widget to create new grimace
            NewVideoActivity.startNewGrimaceInstance(this, Emotion.getById(PrefHelper.getIntPref("create_grimace")));
            PrefHelper.setIntPref("create_grimace", -1);
            finish();
        }

        AppsFlyerLib.onActivityResume(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppsFlyerLib.onActivityPause(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void openSearch() {
        String grimaceFragmentTag = GrimaceFragment.class.getSimpleName();
        Fragment myFragment = getSupportFragmentManager().findFragmentByTag(grimaceFragmentTag);
        if (myFragment != null && myFragment.isVisible()) {
            startActivity(SearchActivity.newIntent(this, true));
        } else {
            startActivity(SearchActivity.newIntent(this, false));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                break;
        }
        return false;
    }

    public void toggleDrawerMenu() {
        if (!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        } else {
            mDrawerLayout.closeDrawers();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
           /* case R.id.btn_menu:
                toggleMenu();
                break;
            case R.id.btn_search:
                break;
            case R.id.btn_video:
                startFragment(TypeVideoFragment.newShareInstance(), true);
                break;*/
        }
    }

    @Override
    public void onOpenMarketplace() {
        mDrawerLayout.closeDrawers();
        startFragment(new NavigationMarketplaceFragment(), false, true);
    }

    @Override
    public void onOpenInfo() {
        mDrawerLayout.closeDrawers();
        startFragment(new NavigationInfoFragment(), false, true);
    }

    @Override
    public void onOpenGrimaces() {
        mDrawerLayout.closeDrawers();
        startFragment(GrimaceFragment.newInstance(), false, true);
    }

    @Override
    public void onOpenVistory() {
        mDrawerLayout.closeDrawers();
        startFragment(VistoryFragment.newInstance(), false, true);
    }

    @Override
    public void onOpenEmoticons() {
        mDrawerLayout.closeDrawers();
        startFragment(EmoticonsFragment.newInstance(), false, true);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.hasExtra(Const.Params.ACTION)) {
            if (intent.getIntExtra(Const.Params.ACTION, 0) == Const.Action.PLAY_FLOW) {
                int currentVideoPosition = intent.getIntExtra(Const.Params.CURRENT_VIDEO, 0);
                String params = intent.getStringExtra(Const.Params.PARAMS);
                int replyId = intent.getIntExtra(Const.Params.REPLY_ID, 0);
                String title = intent.getStringExtra(Const.Params.TITLE);
                VideoType videoType = (VideoType) intent.getSerializableExtra(Const.Params.TYPE);
                FlowListFragment fragment = new FlowListFragment.Builder(null)
                        .setParams(params)
                        .setReplyId(replyId)
                        .setVideoType(videoType)
                        .setTitle(title)
                        .setCurrentPosition(currentVideoPosition)
                        .build();
                startFragment(fragment, true, true);
            } else if (intent.getIntExtra(Const.Params.ACTION, 0) == Const.Action.SHOW_PROFILE) {
                startFragment(ProfileFragment.newInstance(AppUser.getUid(), 1), true, true);
            }
        }
    }

    @Override
    public void onComment(Video video, String comment) {
    }

    @Override
    public void onShowComment(Video video) {
        CommentsActivity.startNewInstance(this, video);
    }

    @Override
    public void onMore(Video video, Video videoParent) {
        MoreDialogFragment.newInstance(video, videoParent).show(getSupportFragmentManager(), MoreDialogFragment.TAG);
    }

    @Override
    public void onVideoChosen(Video video) {
        PlayFlowActivity.startNewInstance(this, video);
    }

    @Override
    public void onSmileGuessingStart(Video video) {
        if (video.getSmileId() != -1) {
            GuessSmileActivity.startNewInstance(this, video);
        }
    }

    @Override
    public void onViewProfile(int uid) {
        mDrawerLayout.closeDrawers();
        startFragment(ProfileFragment.newInstance(uid), true);
    }

    @Override
    public void onPlayFlow(String params, int replyId, String title, ArrayList<Video> mVideoList, VideoType videoType) {
        PlayFlowActivity.startNewInstance(this, params, replyId, title, mVideoList, videoType);
    }

    public void showPayMessage(VideoType type, int amount) {
        String text = "";
        switch (type) {
            case CROWD_GIVE:
                text = getString(R.string.message_crowd_share);
                break;
            case CROWD_ASK:
                text = getString(R.string.message_crowd, String.valueOf(Math.abs(amount)));
                break;
        }
        if (mToast == null || mToast.getView().getWindowVisibility() != View.VISIBLE) {
            mToast = Toast.makeText(getApplicationContext(),
                    text, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    @Override
    public void onVideoPay(final VideoType videoType, int videoId) {
        if (videoType != null) {
            switch (videoType) {
                case CROWD_ASK:
                case CROWD_GIVE:
                    AnalyticsHelper.trackEvent(this, AnalyticsHelper.CROWD_DONATE);
                    break;
            }
        }
        getSpiceManager().execute(
                new PayForVideoRequest(videoId),
                new PayForVideoRequestListener(videoType, videoId) {
                    @Override
                    public void onRequestSuccess(BaseCoinsResponse response) {
                        super.onRequestSuccess(response);
                        if (response != null && response.isSuccess()) {
                            showPayMessage(videoType, response.getCoinsChange());
                        }
                    }
                });
    }

    @Override
    public void onVideoMark(int videoId, int mark) {
        getSpiceManager().execute(
                new MarkVideoRequest(videoId, mark),
                new BaseRequestListener());
    }
}
