package com.clickclap.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.clickclap.App;

/**
 * Created by michael on 07.11.14.
 * Changed by Denis on 25.02.15
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "clickclap.db";
    private static final int DB_VERSION = 23;

    private static DBHelper instance;

    public static DBHelper getInstance() {
        if (instance == null) {
            instance = new DBHelper(App.getInstance());
        }
        return instance;
    }

    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    private DBHelper(Context c) {
        super(c, DB_NAME, null, DB_VERSION);
    }

    private void createTables(SQLiteDatabase db) {
        String createTableQuery = "CREATE TABLE " + ContentDescriptor.Videos.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Videos.Cols.ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.VIDEO_URL + " TEXT,"
                + ContentDescriptor.Videos.Cols.VIDEO_SHORT_URL + " TEXT,"
                + ContentDescriptor.Videos.Cols.THUMB_URL + " TEXT,"
                + ContentDescriptor.Videos.Cols.CREATED_AT + " INTEGER,"
                + ContentDescriptor.Videos.Cols.AUTHOR_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.AUTHOR_NAME + " TEXT,"
                + ContentDescriptor.Videos.Cols.VIEWS + " INTEGER,"
                + ContentDescriptor.Videos.Cols.LIKES + " INTEGER,"
                + ContentDescriptor.Videos.Cols.REPLIES + " INTEGER,"
                + ContentDescriptor.Videos.Cols.REPLAY_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.COMMENTS_COUNT + " INTEGER,"
                + ContentDescriptor.Videos.Cols.SMILE_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.LENGTH + " REAL,"
                + ContentDescriptor.Videos.Cols.MY_MARK + " INTEGER,"
                + ContentDescriptor.Videos.Cols.TAGS + " TEXT, "
                + ContentDescriptor.Videos.Cols.GUESSED + " INTEGER,"
                + ContentDescriptor.Videos.Cols.CITY_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.CITY_ENG + " TEXT,"
                + ContentDescriptor.Videos.Cols.CITY_ESP + " TEXT,"
                + ContentDescriptor.Videos.Cols.CITY_RUS + " TEXT,"
                + ContentDescriptor.Videos.Cols.COUNTRY_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.COUNTRY_ENG + " TEXT,"
                + ContentDescriptor.Videos.Cols.COUNTRY_RUS + " TEXT,"
                + ContentDescriptor.Videos.Cols.COUNTRY_ESP + " TEXT,"
                + ContentDescriptor.Videos.Cols.COST + " INTEGER,"
                + ContentDescriptor.Videos.Cols.AVAILABLE + " INTEGER,"
                + ContentDescriptor.Videos.Cols.EARN + " INTEGER,"
                + ContentDescriptor.Videos.Cols.UPLOADING_STATUS + " INTEGER,"
                + ContentDescriptor.Videos.Cols.UPLOADED_VIDEO + " TEXT,"
                + ContentDescriptor.Videos.Cols.UPLOADED_IMAGE + " TEXT,"
                + ContentDescriptor.Videos.Cols.PRODUCT_TYPE + " INTEGER,"
                + ContentDescriptor.Videos.Cols.GAME_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Videos.Cols.CATEGORY_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.IS_AVATAR + " INTEGER DEFAULT 0,"
                + ContentDescriptor.Videos.Cols.SCREEN + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Smiles.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Smiles.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Smiles.Cols.NAME + " TEXT,"
                + ContentDescriptor.Smiles.Cols.PATH + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Categories.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Categories.Cols.ID + " INTEGER,"
                + ContentDescriptor.Categories.Cols.NAME_ENG + " TEXT, "
                + ContentDescriptor.Categories.Cols.NAME_ESP + " TEXT, "
                + ContentDescriptor.Categories.Cols.NAME_RUS + " TEXT, "
                + ContentDescriptor.Categories.Cols.VIDEO_TYPE + " INTEGER, "
                + ContentDescriptor.Categories.Cols.TECH + " INTEGER, "
                + ContentDescriptor.Categories.Cols.PARAMS + " TEXT, "
                + ContentDescriptor.Categories.Cols.THUMBNAIL + " TEXT, "
                + ContentDescriptor.Categories.Cols.MAIN + " INTEGER, "
                + ContentDescriptor.Categories.Cols.VIDEO_COUNT + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Tags.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Tags.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Tags.Cols.NAME + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Balances.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Balances.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Balances.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Balances.Cols.CHANGE + " INTEGER,"
                + ContentDescriptor.Balances.Cols.OBJECT_ID + " INTEGER,"
                + ContentDescriptor.Balances.Cols.OBJECT_IMG + " TEXT,"
                + ContentDescriptor.Balances.Cols.CREATED_AT + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Countries.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Countries.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Countries.Cols.NAME_ENG + " TEXT,"
                + ContentDescriptor.Countries.Cols.NAME_RUS + " TEXT,"
                + ContentDescriptor.Countries.Cols.NAME_ESP + " TEXT,"
                + ContentDescriptor.Countries.Cols.PHONE_CODE + " INTEGER,"
                + ContentDescriptor.Countries.Cols.PHONE_LENGHT + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Cities.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Cities.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Cities.Cols.COUNTRY_ID + " INTEGER,"
                + ContentDescriptor.Cities.Cols.NAME_ENG + " TEXT,"
                + ContentDescriptor.Cities.Cols.NAME_RUS + " TEXT,"
                + ContentDescriptor.Cities.Cols.NAME_ESP + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Grimaces.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Grimaces.Cols.ID + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.ID_AUTHOR + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.ID_SMILE + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.BOUGHT + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.SELLING + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.URL + " TEXT,"
                + ContentDescriptor.Grimaces.Cols.EFFECT + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.TYPE + " INTEGER, "
                + ContentDescriptor.Grimaces.Cols.TO_UPLOAD + " INTEGER "
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Votes.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Votes.Cols.ID + " INTEGER,"
                + ContentDescriptor.Votes.Cols.TITLE_ENG + " TEXT,"
                + ContentDescriptor.Votes.Cols.TITLE_RUS + " TEXT,"
                + ContentDescriptor.Votes.Cols.TITLE_ESP + " TEXT,"
                + ContentDescriptor.Votes.Cols.DESCR_ENG + " TEXT,"
                + ContentDescriptor.Votes.Cols.DESCR_RUS + " TEXT,"
                + ContentDescriptor.Votes.Cols.DESCR_ESP + " TEXT,"
                + ContentDescriptor.Votes.Cols.STATUS + " INTEGER,"
                + ContentDescriptor.Votes.Cols.MY_VOTE + " INTEGER,"
                + ContentDescriptor.Votes.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Votes.Cols.VOTES_TOTAL + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.VoteOptions.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.VoteOptions.Cols.OPTION_ID + " INTEGER,"
                + ContentDescriptor.VoteOptions.Cols.VOTE_ID + " INTEGER,"
                + ContentDescriptor.VoteOptions.Cols.VOTES + " INTEGER,"
                + ContentDescriptor.VoteOptions.Cols.TITLE_ENG + " TEXT,"
                + ContentDescriptor.VoteOptions.Cols.TITLE_RUS + " TEXT,"
                + ContentDescriptor.VoteOptions.Cols.TITLE_ESP + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Info.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Info.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Info.Cols.TEXT + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.GrimaceStatistic.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.GrimaceStatistic.Cols.GRIMACE_ID + " INTEGER,"
                + ContentDescriptor.GrimaceStatistic.Cols.USE_COUNT + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Users.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Users.Cols.ID + " INTEGER,"
                + ContentDescriptor.Users.Cols.FIRST_NAME + " TEXT,"
                + ContentDescriptor.Users.Cols.LAST_NAME + " TEXT,"
                + ContentDescriptor.Users.Cols.PHOTO + " TEXT,"
                + ContentDescriptor.Users.Cols.PHOTO_100 + " TEXT,"
                + ContentDescriptor.Users.Cols.ID_CITY + " TEXT,"
                + ContentDescriptor.Users.Cols.ID_COUNTRY + " TEXT,"
                + ContentDescriptor.Users.Cols.LANG + " TEXT,"
                + ContentDescriptor.Users.Cols.HOBBY + " TEXT,"
                + ContentDescriptor.Users.Cols.SPORT + " TEXT,"
                + ContentDescriptor.Users.Cols.PETS + " TEXT,"
                + ContentDescriptor.Users.Cols.INTEREST + " TEXT,"
                + ContentDescriptor.Users.Cols.RELIGION + " TEXT,"
                + ContentDescriptor.Users.Cols.CRAFT + " TEXT,"
                + ContentDescriptor.Users.Cols.TAGS + " TEXT,"
                + ContentDescriptor.Users.Cols.RATING + " INTEGER,"
                + ContentDescriptor.Users.Cols.RECORD_TYPE + " INTEGER,"
                + ContentDescriptor.Users.Cols.CITY_ENG + " TEXT,"
                + ContentDescriptor.Users.Cols.CITY_ESP + " TEXT,"
                + ContentDescriptor.Users.Cols.CITY_RUS + " TEXT,"
                + ContentDescriptor.Users.Cols.COUNTRY_ENG + " TEXT,"
                + ContentDescriptor.Users.Cols.COUNTRY_ESP + " TEXT,"
                + ContentDescriptor.Users.Cols.COUNTRY_RUS + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Feedback.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Feedback.Cols.ID + " INTEGER,"
                + ContentDescriptor.Feedback.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Feedback.Cols.LAST_MESSAGE + " TEXT,"
                + ContentDescriptor.Feedback.Cols.NEW + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.FeedbackMessages.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.FeedbackMessages.Cols.ID_USER + " INTEGER,"
                + ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK + " INTEGER,"
                + ContentDescriptor.FeedbackMessages.Cols.MESSAGE + " TEXT,"
                + ContentDescriptor.FeedbackMessages.Cols.TIME + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        /*createTableQuery = "CREATE TABLE " + ContentDescriptor.AudioTracks.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.AudioTracks.Cols.TITLE + " INTEGER,"
                + ContentDescriptor.AudioTracks.Cols.ARTIST + " INTEGER,"
                + ContentDescriptor.AudioTracks.Cols.PATH + " TEXT"
                + ");";*/
//        db.execSQL(createTableQuery);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    private void dropTables(SQLiteDatabase db) {
        dropTable(db, ContentDescriptor.Videos.TABLE_NAME);
        dropTable(db, ContentDescriptor.Smiles.TABLE_NAME);
        dropTable(db, ContentDescriptor.Categories.TABLE_NAME);
        dropTable(db, ContentDescriptor.Tags.TABLE_NAME);
        dropTable(db, ContentDescriptor.Countries.TABLE_NAME);
        dropTable(db, ContentDescriptor.Cities.TABLE_NAME);
        dropTable(db, ContentDescriptor.Balances.TABLE_NAME);
        dropTable(db, ContentDescriptor.Grimaces.TABLE_NAME);
        dropTable(db, ContentDescriptor.Votes.TABLE_NAME);
        dropTable(db, ContentDescriptor.VoteOptions.TABLE_NAME);
        dropTable(db, ContentDescriptor.Info.TABLE_NAME);
        dropTable(db, ContentDescriptor.GrimaceStatistic.TABLE_NAME);
        dropTable(db, ContentDescriptor.Users.TABLE_NAME);
        dropTable(db, ContentDescriptor.Feedback.TABLE_NAME);
        dropTable(db, ContentDescriptor.FeedbackMessages.TABLE_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    public void dropTable(SQLiteDatabase db, String table) {
        db.execSQL("DROP TABLE IF EXISTS " + table);
    }

}
