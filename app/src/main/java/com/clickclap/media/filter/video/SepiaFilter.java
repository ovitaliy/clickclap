package com.clickclap.media.filter.video;

import android.graphics.Color;

import com.clickclap.App;
import com.clickclap.R;

public class SepiaFilter extends BaseVideoFilter
{
	final int depth = 20;
	final int sepiaIntensity = 20;

    public SepiaFilter() {
        setVideoFilterName(
                "colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131");
        setFilterType(SEPIA);
    }

    @Override
	protected int getNewPixelColor(int pixelColor)
	{
		pixelRed = Color.red(pixelColor);
		pixelGreen = Color.green(pixelColor);
		pixelBlue = Color.blue(pixelColor);

        int gry = (pixelRed + pixelGreen + pixelBlue) / 3;
        pixelRed = pixelBlue = pixelGreen = gry;

		newRed = pixelRed + (depth * 2);
		newGreen = pixelGreen + depth;

        //normalize new colors
        if(newRed > 255) newRed = 255;
        if(newGreen > 255) newGreen = 255;

        //darker blue color for increase sepia effect
        pixelBlue -= sepiaIntensity;

        //normalize blue pixel
        if(pixelBlue > 255) pixelBlue = 255;
        else if(pixelBlue < 0) pixelBlue = 0;

        return Color.rgb(newRed, newGreen, pixelBlue);
	}

    @Override
    public String getTitle() {
        return App.getInstance().getString(R.string.video_filter_sepia);
    }
}
