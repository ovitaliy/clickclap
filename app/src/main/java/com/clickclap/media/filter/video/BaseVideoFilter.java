package com.clickclap.media.filter.video;

import android.graphics.Bitmap;
import android.support.annotation.IntDef;

import com.clickclap.media.filter.Filter;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class BaseVideoFilter implements Serializable, Filter {
    public static final int NEGATIVE = 1;
    public static final int GRAY = 2;
    public static final int COOL = 3;
    public static final int SEPIA = 4;
    public static final int CINEMATIC = 5;
    public static final int CINEMATIC2 = 6;
    public static final int CINEMATIC3 = 7;
    public static final int GRITTY = 8;
    public static final int WARM = 9;

    @IntDef({NEGATIVE, GRAY, COOL, SEPIA, CINEMATIC, CINEMATIC2, CINEMATIC3, GRITTY, WARM})
    @Retention(RetentionPolicy.SOURCE)
    @interface FilterType {
    }

    /**
     * The formatted string that contains name of FFMPEG filter
     * and some additional parameters, if needed.
     */
    protected String mVideoFilterName;

    protected
    @FilterType
    int mFilterType;

    /**
     * The width of source bitmap.
     */
    protected int width;

    /**
     * The height of source bitmap;
     */
    protected int height;

    /**
     * Old and new values of each RGB-channels of bitmap.
     * We declare them here for avoid allocation if getNewPixelColor(),
     * that calls often.
     */
    protected int pixelRed, pixelGreen, pixelBlue;
    protected int newRed, newGreen, newBlue;

    /**
     * Gets pixel color and does some change with it.
     *
     * @param pixelColor The pixel color of source image.
     * @return The new values of pixel.
     */
    protected abstract int getNewPixelColor(int pixelColor);

    /**
     * Go through all pixel of source bitmap
     * and transform each of them.
     * Call getNewPixelColor() for each pixel.
     * You can override this method in child classes,
     * for example, if you need a few passes through pixels for your effect.
     *
     * @param source The source bitmap.
     * @return The filtered bitmap.
     */
    public Bitmap setEffect(Bitmap source) {
        width = source.getWidth();
        height = source.getHeight();
        Bitmap result = Bitmap.createBitmap(width, height, source.getConfig());

        int pixelColor;
        for (int x = 0; x < width; x++) {
            if (Thread.interrupted()) {
                break;
            }
            for (int y = 0; y < height; y++) {
                if (Thread.interrupted()) {
                    break;
                }
                pixelColor = source.getPixel(x, y);
                result.setPixel(x, y, getNewPixelColor(pixelColor));
            }
        }
        return result;
    }

    protected void clipNewPixels() {
        //clip red
        if (newRed > 255) {
            newRed = 255;
        } else if (newRed < 0) {
            newRed = 0;
        }

        //clip green
        if (newGreen > 255) {
            newGreen = 255;
        } else if (newGreen < 0) {
            newGreen = 0;
        }

        //clip blue
        if (newBlue > 255) {
            newBlue = 255;
        } else if (newBlue < 0) {
            newBlue = 0;
        }
    }

    public String getVideoFilterName() {
        return mVideoFilterName;
    }

    public void setVideoFilterName(String videoFilterName) {
        mVideoFilterName = videoFilterName;
    }

    public
    @FilterType
    int getFilterType() {
        return mFilterType;
    }

    public void setFilterType(@FilterType int filterType) {
        mFilterType = filterType;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;

        return mVideoFilterName.equals(((BaseVideoFilter) o).getVideoFilterName());
    }
}







