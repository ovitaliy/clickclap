package com.clickclap.media.filter;

/**
 * Created by Denis on 30.03.2015.
 */
public interface Filter {
    String getTitle();
}
