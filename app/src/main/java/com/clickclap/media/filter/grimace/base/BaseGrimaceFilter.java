package com.clickclap.media.filter.grimace.base;

import android.graphics.Bitmap;

import com.clickclap.media.filter.Filter;
import com.clickclap.media.AutoContrast;
import com.clickclap.media.filter.grimace.AnimeFilter;
import com.clickclap.media.filter.grimace.CheFilter;
import com.clickclap.media.filter.grimace.PopArtFilter;
import com.clickclap.media.filter.grimace.util.AndroidUtils;

import java.io.Serializable;

/**
 * Created by Denis on 18.05.2015.
 */
public abstract class BaseGrimaceFilter implements Filter, Serializable {

    protected enum ImageSize {
        LARGE, SMALL
    }

    protected ImageSize getImageSize(int width, int height) {
        if (width > 1000) {
            return ImageSize.LARGE;
        } else {
            return ImageSize.SMALL;
        }
    }

    public Bitmap apply(Bitmap source) {

        int[] inputColors = AndroidUtils.bitmapToIntArray(source);

        int[] src;
        if (this instanceof PopArtFilter || this instanceof CheFilter || this instanceof AnimeFilter) {
            src = new AutoContrast().apply(inputColors);
        } else {
            src = inputColors;
        }

        int width = source.getWidth();
        int height = source.getHeight();

        int[] outputColors = filter(src, width, height);
        return Bitmap.createBitmap(outputColors, 0, width, width, height, Bitmap.Config.ARGB_8888);
    }

    public abstract int[] filter(int[] src, int W, int H);

    public abstract int getId();
}
