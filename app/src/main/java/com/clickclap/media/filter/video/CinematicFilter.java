package com.clickclap.media.filter.video;

import android.graphics.Color;

import com.clickclap.media.Contrast;

/**
 * Created by Владимир on 24.12.2014.
 */
public class CinematicFilter extends BaseVideoFilter {

    /**
     * Value that we'll use for calculate contrast factor.
     */
    private static final double CONTRAST_VALUE = 20.0;

    /**
     * Factor that we'll use for calculate value of contrasted pixel.
     */
    private static final double CONTRAST_FACTOR = Contrast.getContrastFactor(CONTRAST_VALUE);


    private static final double RED_FACTOR = 1.2;
    private static final double GREEN_FACTOR = 1.2;
    private static final double BLUE_FACTOR = 1.5;


    public CinematicFilter() {
        setVideoFilterName(getFormattedFilterName());
        setFilterType(CINEMATIC2);
    }


    /*@Override
    public Bitmap setEffect(Bitmap source) {
        mAutoContrast = new AutoContrast();
        return mAutoContrast.apply(source);
    }*/

    @Override
    protected int getNewPixelColor(int pixelColor) {
        pixelRed = Color.red(pixelColor);
        pixelGreen = Color.green(pixelColor);
        pixelBlue = Color.blue(pixelColor);

        //apply contrast for every channel
        newRed = (int) (Contrast.getContrastedPixel(pixelRed, CONTRAST_FACTOR) * RED_FACTOR);
        newGreen = (int) (Contrast.getContrastedPixel(pixelGreen, CONTRAST_FACTOR) * GREEN_FACTOR);
        newBlue = (int) (Contrast.getContrastedPixel(pixelBlue, CONTRAST_FACTOR) * BLUE_FACTOR);

        //normalize colors
        clipNewPixels();

        return Color.rgb(newRed, newGreen, newBlue);
    }

    private String getFormattedFilterName() {
        return "colorbalance=bh=.25:bm=.20:bs=.10:gm=0.1:gh=0.2," +
                "mp=eq2=1:" +//gamma (0.1-10, def:1.0)
                "1.40:" +//contrast (-2-2, def:1.0)
                "0.03"; //brightness (-1-1, def:0.0)
    }

    @Override
    public String getTitle() {
        return "Cinematic";
    }
}
