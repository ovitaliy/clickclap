package com.clickclap.media.filter.video;

import android.graphics.Color;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.media.Contrast;

/**
 * Created by Владимир on 24.12.2014.
 */
public class GrittyFilter extends BaseVideoFilter {

    /**
     * Value that we'll use for calculate contrast factor.
     * This value should give us contrast comparable with FFMPEG "curvess=preset=strong_contrast"
     */
    private final double CONTRAST_VALUE = 30.0;

    /**
     * Factor that we'll use for calculate value of contrasted pixel.
     */
    private final double CONTRAST_FACTOR = Contrast.getContrastFactor(CONTRAST_VALUE);


    private static final double RED_FACTOR = 1;
    private static final double GREEN_FACTOR = 1.2;
    private static final double BLUE_FACTOR = 1;


    public GrittyFilter() {
        //add contrast and increase a bit colors
        setVideoFilterName(getFormattedFilterName());
        setFilterType(GRITTY);
    }

    @Override
    protected int getNewPixelColor(int pixelColor) {
        pixelRed = Color.red(pixelColor);
        pixelGreen = Color.green(pixelColor);
        pixelBlue = Color.blue(pixelColor);

        //apply contrast for every channel
        newRed = (int) (Contrast.getContrastedPixel(pixelRed, CONTRAST_FACTOR) * RED_FACTOR);
        newGreen = (int) (Contrast.getContrastedPixel(pixelGreen, CONTRAST_FACTOR) * GREEN_FACTOR);
        newBlue = (int) (Contrast.getContrastedPixel(pixelBlue, CONTRAST_FACTOR) * BLUE_FACTOR);

        //normalize colors
        clipNewPixels();

        return Color.rgb(newRed, newGreen, newBlue);
    }

    @Override
    public String getTitle() {
        return App.getInstance().getString(R.string.video_filter_gritty);
    }

    private String getFormattedFilterName() {
        return "hue=s=0.85," +
                "colorbalance=gm=.15:gh=.15:gs=.15:bm=-.25," +
                "mp=eq2=1:" +//gamma (0.1-10, def:1.0)
                "1.7:" +//contrast (-2-2, def:1.0)
                "-0.12";
//        String filterName = new String();
//
//        //first apply curves filter for contrast
//        filterName += "curves=preset=strong_contrast,";
//
//        //the we use lutrgb filter for increase colors
//        filterName += "lutrgb=";
//
//        //red color
//        filterName += "r=" + RED_FACTOR + "*val:";
//
//        //green color
//        filterName += "g=" + GREEN_FACTOR + "*val:";
//
//        //blue color
//        filterName += "b=" + BLUE_FACTOR + "*val";
//
//        return filterName;
    }
}


