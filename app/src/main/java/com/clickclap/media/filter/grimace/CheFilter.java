/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.clickclap.media.filter.grimace;


import com.clickclap.media.filter.grimace.base.PointFilter;
import com.clickclap.media.filter.grimace.util.ImageMath;
import com.clickclap.media.filter.grimace.util.PixelUtils;

/**
 * A filter which performs a threshold operation on an image.
 */
public class CheFilter extends PointFilter {

    private int lowerThreshold;
    private int upperThreshold;
    private int white = 0xffffff;
    private int black = 0x000000;

    public int filterRGB(int x, int y, int rgb) {
        int v = PixelUtils.brightness(rgb);
        float f = ImageMath.smoothStep(lowerThreshold, upperThreshold, v);
        return (rgb & 0xff000000) | (ImageMath.mixColors(f, black, white) & 0xffffff);
    }

    @Override
    public String getTitle() {
        return "Che";
    }

    @Override
    public int getId() {
        return 2;
    }

    @Override
    public int[] filter(int[] src, int w, int h) {
        switch (getImageSize(w, h)) {
            case SMALL:
                lowerThreshold = 85;
                upperThreshold = 85;
                break;
            default:
                lowerThreshold = 95;
                upperThreshold = 95;
                break;
        }
        return super.filter(src, w, h);
    }
}
