package com.clickclap.media.filter.video;

import android.graphics.Color;

import com.clickclap.App;
import com.clickclap.R;

public class GrayFilter extends BaseVideoFilter
{
    public GrayFilter() {
        setVideoFilterName("lutyuv=u=128:v=128");
        setFilterType(GRAY);
    }

    @Override
	protected int getNewPixelColor(int pixelColor)
	{
		int pixelRed = Color.red(pixelColor);
		int pixelGreen = Color.green(pixelColor);
		int pixelBlue = Color.blue(pixelColor);
		int gray = (int) (0.3 * (double)pixelRed + 0.59 * (double)pixelGreen + 0.11 * (double)pixelBlue);
        return Color.rgb(gray, gray, gray);
	}

	@Override
	public String getTitle() {
		return App.getInstance().getString(R.string.video_filter_gray);
	}
}
