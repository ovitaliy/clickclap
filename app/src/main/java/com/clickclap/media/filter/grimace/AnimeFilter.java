package com.clickclap.media.filter.grimace;

import com.clickclap.media.filter.grimace.base.HSBAdjustFilter;
import com.clickclap.media.filter.grimace.base.BaseGrimaceFilter;
import com.clickclap.media.filter.grimace.base.ContrastFilter;
import com.clickclap.media.filter.grimace.base.LevelsFilter;
import com.clickclap.media.filter.grimace.base.PosterizeFilter;

/**
 * Created by Denis on 24.05.2015.
 */
public class AnimeFilter extends BaseGrimaceFilter {
    @Override
    public int[] filter(int[] src, int W, int H) {
        int numLevels;
        int hFactor;
        switch (getImageSize(W, H)) {
            case SMALL:
                hFactor = 15;
                numLevels = 13;
                break;
            default:
                hFactor = 17;
                numLevels = 12;
                break;
        }

        ContrastFilter contrastFilter = new ContrastFilter();
        contrastFilter.setContrast(1.7f);
        contrastFilter.setBrightness(0.90f);
        src = contrastFilter.filter(src, W, H);

        HSBAdjustFilter hsbAdjustFilter = new HSBAdjustFilter();
        hsbAdjustFilter.setHFactor(hFactor);
        hsbAdjustFilter.setSFactor(0.1f);
        hsbAdjustFilter.setBFactor(-0.05f);
        src = hsbAdjustFilter.filter(src, W, H);

        LevelsFilter levelsFilter = new LevelsFilter();
        levelsFilter.setHighOutputLevel(1);
        levelsFilter.setLowLevel(0);
        levelsFilter.setHighLevel(0.8f);
        levelsFilter.setLowOutputLevel(0);

        PosterizeFilter filter = new PosterizeFilter();
        filter.setNumLevels(numLevels);
        src = filter.filter(src, W, H);
        return src;
    }

    @Override
    public String getTitle() {
        return "Anime";
    }

    @Override
    public int getId() {
        return 5;
    }
}
