package com.clickclap.media.filter.grimace;

import com.clickclap.media.filter.grimace.base.BaseGrimaceFilter;
import com.clickclap.media.filter.grimace.base.ContrastFilter;
import com.clickclap.media.filter.grimace.base.Curve;
import com.clickclap.media.filter.grimace.base.CurvesFilter;
import com.clickclap.media.filter.grimace.util.PixelUtils;

/**
 * Created by Denis on 26.05.2015.
 */
public class PopArtFilter extends BaseGrimaceFilter {

    @Override
    public int[] filter(int[] src, int W, int H) {
        ContrastFilter contrastFilter = new ContrastFilter();
        contrastFilter.setContrast(1.6f);
        contrastFilter.setBrightness(0.85f);
        src = contrastFilter.filter(src, W, H);

        CurvesFilter filter = new CurvesFilter();
        Curve curve = new Curve();
        curve.addKnot(0.34f, 0);
        filter.setCurve(curve);
        src = filter.filter(src, W, H);

        src = PixelUtils.hue(src, 174, W, H);

        return src;
    }

    @Override
    public int getId() {
        return 6;
    }

    @Override
    public String getTitle() {
        return "Pop-Art";
    }
}
