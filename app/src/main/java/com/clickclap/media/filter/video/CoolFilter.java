package com.clickclap.media.filter.video;

import android.graphics.Color;

import com.clickclap.App;
import com.clickclap.R;

/**
 * Created by Владимир on 24.12.2014.
 */
public class CoolFilter extends BaseVideoFilter {

    private static final double RED_FACTOR = 0.8;
    private static final double GREEN_FACTOR = 1.0;
    private static final double BLUE_FACTOR = 1.2;

    public CoolFilter() {
        setVideoFilterName(getFormattedFilterName());
        setFilterType(COOL);
    }

    @Override
    protected int getNewPixelColor(int pixelColor) {

        //get channels value
        pixelRed = Color.red(pixelColor);
        pixelGreen = Color.green(pixelColor);
        pixelBlue = Color.blue(pixelColor);

        //just increase color values
        newRed = (int) (pixelRed * RED_FACTOR);
        newGreen = (int) (pixelGreen * GREEN_FACTOR);
        newBlue = (int) (pixelBlue * BLUE_FACTOR);

        clipNewPixels();

        return Color.rgb(newRed, newGreen, newBlue);
    }

    @Override
    public String getTitle() {
        return App.getInstance().getString(R.string.video_filter_cool);
    }

    /**
     * Create a string that will be parameter for
     * Ffmpeg filter.
     *
     * @return Formatted string, ready for send to Ffmpeg.
     */
    private String getFormattedFilterName() {
        return "hue=h=193:s=-0.6," +
                "lutrgb=b=val*1.4:g=val*1.045:r=val*1.02," +
                "mp=eq2=1:" +//gamma (0.1-10, def:1.0)
                "1.5:" +//contrast (-2-2, def:1.0)
                "";
    }
}
