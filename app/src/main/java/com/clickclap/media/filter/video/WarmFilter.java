package com.clickclap.media.filter.video;

import android.graphics.Color;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.media.Contrast;

/**
 * Created by Denis on 15.05.2015.
 */
public class WarmFilter extends BaseVideoFilter {

    /**
     * Value that we'll use for calculate contrast factor.
     */
    private final double CONTRAST_VALUE = 20.0;

    /**
     * Factor that we'll use for calculate value of contrasted pixel.
     */
    private final double CONTRAST_FACTOR = Contrast.getContrastFactor(CONTRAST_VALUE);


    private static final double RED_FACTOR = 1.2;
    private static final double GREEN_FACTOR = 0.9;
    private static final double BLUE_FACTOR = 1.1;


    public WarmFilter() {
        setVideoFilterName(getFormattedFilterName());
        setFilterType(WARM);
    }

    @Override
    protected int getNewPixelColor(int pixelColor) {
        pixelRed = Color.red(pixelColor);
        pixelGreen = Color.green(pixelColor);
        pixelBlue = Color.blue(pixelColor);

        //just increase colors
        newRed = (int) (Contrast.getContrastedPixel(pixelRed, CONTRAST_FACTOR) * RED_FACTOR);
        newGreen = (int) (Contrast.getContrastedPixel(pixelGreen, CONTRAST_FACTOR) * GREEN_FACTOR);
        newBlue = (int) (Contrast.getContrastedPixel(pixelBlue, CONTRAST_FACTOR) * BLUE_FACTOR);

        //normalize colors
        clipNewPixels();

        return Color.rgb(newRed, newGreen, newBlue);
    }

    @Override
    public String getTitle() {
        return App.getInstance().getString(R.string.video_filter_warm);
    }

    private String getFormattedFilterName() {
        String filterName = "colorbalance=rm=.20:rh=.15:rs=.10" +
                ":bs=.05:bm=.15," +
                "mp=eq2=1:" +//gamma (0.1-10, def:1.0)
                "1.50:" +//contrast (-2-2, def:1.0)
                "0.02";//brightness (-1-1, def:0.0)

        return filterName;
    }
}
