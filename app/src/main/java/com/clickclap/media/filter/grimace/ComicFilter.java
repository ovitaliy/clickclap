package com.clickclap.media.filter.grimace;

import com.clickclap.media.filter.grimace.base.BaseGrimaceFilter;
import com.clickclap.media.filter.grimace.base.ContrastFilter;
import com.clickclap.media.filter.grimace.base.GaussianFilter;
import com.clickclap.media.filter.grimace.base.LevelsFilter;
import com.clickclap.media.filter.grimace.base.PosterizeFilter;
import com.clickclap.media.filter.grimace.base.RGBAdjustFilter;

/**
 * Created by Denis on 21.05.2015.
 */
public class ComicFilter extends BaseGrimaceFilter {

    @Override
    public int[] filter(int[] src, int W, int H) {

        int numLevels;
        switch (getImageSize(W, H)) {
            case SMALL:
                numLevels = 12;
                break;
            default:
                numLevels = 9;
                break;
        }

        RGBAdjustFilter rgbAdjustFilter = new RGBAdjustFilter(0.08f, 0.04f, -0.07f);
        src = rgbAdjustFilter.filter(src, W, H);

        GaussianFilter gaussianFilter = new GaussianFilter(3);
        src = gaussianFilter.filter(src, W, H);

        LevelsFilter levelsFilter = new LevelsFilter();
        levelsFilter.setLowLevel(0.10f);
        levelsFilter.setHighLevel(0.9f);
        levelsFilter.setLowOutputLevel(0.0f);
        levelsFilter.setHighOutputLevel(1f);
        src = levelsFilter.filter(src, W, H);

        PosterizeFilter posterizeFilter = new PosterizeFilter();
        posterizeFilter.setNumLevels(numLevels);
        src = posterizeFilter.filter(src, W, H);

        ContrastFilter contrastFilter = new ContrastFilter();
        contrastFilter.setBrightness(1f);
        contrastFilter.setContrast(0.80f);
        src = contrastFilter.filter(src, W, H);
        return src;
    }

    @Override
    public String getTitle() {
        return "Comic";
    }

    @Override
    public int getId() {
        return 4;
    }
}
