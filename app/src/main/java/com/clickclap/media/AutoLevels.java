package com.clickclap.media;

import android.graphics.Bitmap;

import java.util.Arrays;

/**
 * Created by Владимир on 26.12.2014.
 *
 * Class for apply auto levels effect to bitmap.
 *
 * Difference between this filter and {@link net.inteza.imonandroid.media.AutoContrast}
 * is that auto levels work with each RGB channel separately.
 *
 * You can just call apply() with your bitmap and you'll get processed image with applied auto levels.
 */
public class AutoLevels extends AutoFilter {

    /**
     * The array of bitmap pixels.
     */
    private int [] mPixels;

    /**
     * The array of luminance of red values of pixels.
     */
    private int [] mRedPixels = new int[256];

    /**
     * The array of luminance of green values of pixels.
     */
    private int [] mGreenPixels = new int[256];

    /**
     * The array of luminance of blue values of pixels.
     */
    private int [] mBluePixels = new int[256];

    /**
     * The value we'll use for seeking maximum and minimum values.
     */
    private static final double CLIPPING = 0.01;


    public Bitmap apply(Bitmap source) {
        int width = source.getWidth();
        int height = source.getHeight();

        //init pixels array
        mPixels = new int[width * height];
        source.getPixels(mPixels, 0, width, 0, 0, width, height);

        //apply auto levels
        autoLevels();

        //fill result bitmap new pixels and return it
        Bitmap result = Bitmap.createBitmap(width, height, source.getConfig());
        result.setPixels(mPixels, 0, width, 0, 0, width, height);
        return result;
    }

    /**
     * Adjust the auto levels to the image.
     */
    private void autoLevels() {
        createArrays();

        int clipNumber = round(mPixels.length * CLIPPING);
        double [] factors = new double[6];

        //get red max and min
        int max = getMaximum(clipNumber, mRedPixels);
        int min = getMinimum(clipNumber, mRedPixels);

        factors[0] = 255.0 / (max - min); //contrast of red
        factors[1] = 127.5 - ( (max + min) / 2.0); //brightness of red

        max = getMaximum(clipNumber, mGreenPixels);
        min = getMinimum(clipNumber, mGreenPixels);

        factors[2] = 255.0 / (max - min); //contrast of green
        factors[3] = 127.5 - ( (max + min) / 2.0); //brightness of green

        max = getMaximum(clipNumber, mBluePixels);
        min = getMinimum(clipNumber, mBluePixels);

        factors[4] = 255.0 / (max - min); //contrast of blue
        factors[5] = 127.5 - ( (max + min) / 2.0); //contrast of blue

        changeLevels(factors);
    }

    /**
     * Change contrasts and brightness of each pixel.
     *
     * @param factors The array of values of contrast and brightness.
     */
    private void changeLevels(double [] factors) {
        int alpha;
        int red;
        int green;
        int blue;
        int pixel;
        int [] clipped;

        //go through all pixels and change them
        for(int i=0; i<mPixels.length; i++) {
            pixel = mPixels[i];

            alpha = (pixel >> 24) & 0xff;
            red = (pixel >> 16) & 0xff;
            green = (pixel >> 8) & 0xff;
            blue = pixel & 0xff;

            //change contrast and brightness
            red = (int) (factors[0] * (red + factors[1] - 127.5) + 127.5);
            green = (int) (factors[2] * (green + factors[3] - 127.5) + 127.5);
            blue = (int) (factors[4] * (blue + factors[5] - 127.5) + 127.5);

            clipped = clipping(red, green, blue);
            mPixels[i] = (alpha << 24) | (clipped[0] << 16) | (clipped[1] << 8) | clipped[2];
        }
    }

    /**
     * Fill histograms of colors.
     */
    private void createArrays() {
        Arrays.fill(mRedPixels, 0);
        Arrays.fill(mGreenPixels, 0);
        Arrays.fill(mBluePixels, 0);

        int red;
        int green;
        int blue;

        for (int pixel : mPixels) {
            red = (pixel >> 16) & 0xff;
            green = (pixel >> 8) & 0xff;
            blue = pixel & 0xff;

            mRedPixels[red]++;
            mGreenPixels[green]++;
            mBluePixels[blue]++;
        }
    }

    @Override
    public int[] apply(int[] source) {
        return new int[0];
    }
}
