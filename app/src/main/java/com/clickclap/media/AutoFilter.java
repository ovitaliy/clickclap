package com.clickclap.media;

/**
 * Created by Владимир on 27.12.2014.
 *
 * The abstract class that provide some functions,
 * which can be needed for some filters, like {@link net.inteza.imonandroid.media.AutoContrast} or {@link net.inteza.imonandroid.media.AutoLevels}.
 * Child classes have to implement apply() function.
 */
public abstract class AutoFilter {

    /**
     * Apply filters to bitmap.
     * Have to return new image with applied effect.
     *
     * @param source The source bitmap that we'll process.
     * @return The processed bitmap.
     */
    public abstract int[] apply(int[] source);

    /**
     * Normalize values of channels.
     *
     * @param red The value of red channel.
     * @param green The value of green channel.
     * @param blue The value of blue channel;
     * @return The array of int with clipped values.
     */
    protected int [] clipping(int red, int green, int blue) {
        if(red > 255) {
            red = 255;
        } else if(red < 0) {
            red = 0;
        }

        if(green > 255) {
            green = 255;
        } else if(green < 0) {
            green = 0;
        }

        if(blue > 255) {
            blue = 255;
        } else if(blue < 0) {
            blue = 0;
        }

        int [] clipped = {red, green, blue};
        return clipped;
    }

    /**
     * Rounds double value of luminance to int.
     *
     * @param lum The double value of luminance.
     * @return Rounded value of luminance.
     */
    protected int round(double lum) {
        int result;
        if(lum >= 0) {
            result = (int) (lum + 0.5);
        } else {
            result = (int) (lum - 0.5);
        }
        return result;
    }

    /**
     * Searches for the maximum in the given array.
     *
     * @param clipNumber The value that we use for found maximum.
     * @param array The array where we'll search maximum.
     * @return The index of maximum in the given array.
     */
    protected int getMaximum(int clipNumber, int [] array) {
        int max = 0;
        int i = 255;

        while(max < clipNumber) {
            max += array[i];
            i--;
        }
        i++;
        max = i;
        return max;
    }

    /**
     * Searches for the minimum in the given array.
     *
     * @param clipNumber The value that we use for found minimum.
     * @param array The array where we'll search minimum.
     * @return The index of the minimum in the given array.
     */
    protected int getMinimum(int clipNumber, int [] array) {
        int min = 0;
        int i = 0;

        while(min < clipNumber) {
            min += array[i];
            i++;
        }
        i--;
        min = i;
        return min;
    }
}
