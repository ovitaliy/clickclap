package com.clickclap.media;

/**
 * Created by Владимир on 25.12.2014.
 */
public class Contrast {

    /**
     * Calculate contrast factor for using it in future.
     *
     * @param contrastValue The contrast value. 30.0 using for {@link net.inteza.imonandroid.media.filter.GrittyFilter}
     *                      and give strong contrast.
     *                      Range: from -100.0 to 100.0.
     * @return The contrast factor that we will use for getting contrasted pixels.
     */
    public static double getContrastFactor(double contrastValue) {
        return Math.pow( (100 + contrastValue) / 100, 2);
    }

    /**
     * Calculate value of pixel using contrastFactor.
     * You can give pixel to this method as value of one of the RGB channels of pixel.
     * As result you will get contrasted value of this pixel and you can just set it value to new pixel.
     *
     * @param pixel The value of pixel. It is can be value of one of the RGB channels.
     * @param contrastFactor The contrast factor that we will use for calculate value of contrasted pixel.
     * @return The value of contrasted pixel.
     */
    public static int getContrastedPixel(int pixel, double contrastFactor) {
        return (int) (((((pixel / 255.0) - 0.5) * contrastFactor) + 0.5) * 255.0);
    }
}
