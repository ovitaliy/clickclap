package com.clickclap.media;

import java.util.Arrays;

/**
 * Created by Владимир on 25.12.2014.
 *
 * Class for apply auto contrast effect to bitmap.
 * You can just call apply() with your bitmap and you'll get processed image with applied auto contrast.
 */
public class AutoContrast extends AutoFilter {

    /**
     * Array of pixels.
     */
    private int [] mPixels;

    /**
     * Histogram of values of pixels luminance.
     */
    private final int [] mLumPixels = new int[256];

    /**
     * This value need for seeking max and min values.
     */
    private static final double CLIPPING = 0.01;

    public AutoContrast() {
    }

    @Override
    public int[] apply(int[] source) {
        mPixels = source;
        autoContrast();
        return mPixels;
    }

    /**
     * Adjust the auto contrast to the image.
     */
    private void autoContrast() {
        createLuminanceArray();
        int clipNumber = round(mPixels.length * CLIPPING);

        //get max and min values
        int max = getMaximum(clipNumber, mLumPixels);
        int min = getMinimum(clipNumber, mLumPixels);

        //calculate contrast and brightness
        double contrast = 255.0 / (max - min);
        double brightness = 127.5 - ((max + min) / 2.0);

        //apply our new contrast and brightness values
        changeContrastBrightness(contrast, brightness);
    }

    /**
     * Change contrast and brightness.
     *
     * @param contrast The new contrast value.
     * @param brightness The new brightness value.
     */
    private void changeContrastBrightness(double contrast, double brightness) {
        for(int i=0; i<mPixels.length; i++) {
            int color = mPixels[i];

            int alpha = (color >> 24) & 0xff;
            int red = (color >> 16) & 0xff;
            int green = (color >> 8) & 0xff;
            int blue = color & 0xff;

            //calculate YCbCr values
            double Y = 0.299 * red + 0.587 * green + 0.114 * blue;
            double Cb = -0.168736 * red - 0.331264 * green + 0.5 * blue;
            double Cr = 0.5 * red - 0.418688 * green - 0.081312 * blue;

            Y = contrast * (Y + brightness - 127.5) + 127.5;

            red = (int) (Y + 1.402 * Cr + 0.5);
            green = (int) (Y - 0.3441 * Cb - 0.7141 * Cr + 0.5);
            blue = (int) (Y + 1.772 * Cb + 0.5);

            int [] clipped = clipping(red, green, blue);
            mPixels[i] = (alpha << 24) | (clipped[0] << 16) | (clipped[1] << 8) | clipped[2];
        }
    }

    /**
     * Calculates the luminance values and counts them.
     */
    private void createLuminanceArray() {
        //clear lum pixels
        Arrays.fill(mLumPixels, 0);

        for (int color : mPixels) {
            //get our channels values
            int red = (color >> 16) & 0xff;
            int green = (color >> 8) & 0xff;
            int blue = color & 0xff;

            //calculate luminance
            double lum = 0.299 * red + 0.587 * green + 0.114 * blue;
            int roundedLum = round(lum);
            mLumPixels[roundedLum]++;
        }
    }

}
