package com.clickclap.listener;

import com.clickclap.enums.VideoType;
import com.clickclap.model.Video;

import java.util.ArrayList;

/**
 * Created by Deni on 13.07.2015.
 */
public interface OnPlayFlowListener {
    void onPlayFlow(String params, int replyId, String title, ArrayList<Video> mVideoList, VideoType videoType);
}
