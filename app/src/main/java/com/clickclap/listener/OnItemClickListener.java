package com.clickclap.listener;

import android.view.View;

/**
 * Created by denisvasilenko on 09.12.15.
 */
public interface OnItemClickListener<I> {
    void onItemClick(I item, View v);
}