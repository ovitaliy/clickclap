package com.clickclap.listener;

import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ovitali on 30.01.2015.
 */
public interface OnContactSelectListener {
    void onContactsSelected(int relationship, ArrayList<Parcelable> contacts);
}
