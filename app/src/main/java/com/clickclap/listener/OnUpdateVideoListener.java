package com.clickclap.listener;

import com.clickclap.enums.VideoType;

/**
 * Created by denisvasilenko on 21.09.2015.
 */
public interface OnUpdateVideoListener {
    void onVideoPay(VideoType videoType, int videoId);

    void onVideoMark(int videoId, int mark);
}
