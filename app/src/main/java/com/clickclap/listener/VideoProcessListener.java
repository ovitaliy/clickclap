package com.clickclap.listener;

import com.clickclap.fragment.video.BaseVideoCreationFragment;

/**
 * Created by ovitali on 17.02.2015.
 */
public interface VideoProcessListener {
    void onVideoProcessStatus(BaseVideoCreationFragment.Status status);
}
