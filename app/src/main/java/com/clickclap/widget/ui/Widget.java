package com.clickclap.widget.ui;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.clickclap.App;
import com.clickclap.R;
import com.clickclap.activities.SplashActivity;
import com.clickclap.db.ContentDescriptor;
import com.clickclap.db.DBHelper;
import com.clickclap.enums.Emotion;
import com.clickclap.model.Grimace;
import com.clickclap.util.DisplayInfo;
import com.clickclap.util.FilePathHelper;
import com.clickclap.util.Utils;
import com.clickclap.view.CircleImageView;
import com.clickclap.view.GrimaceCircleView;
import com.clickclap.view.SmartViewPager;
import com.clickclap.widget.enums.Messenger;
import com.clickclap.widget.helper.PrefHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Denis on 03.06.2015.
 */
public enum Widget {
    INSTANCE;

    private static final String TAG = Widget.class.getSimpleName();

    public Button button;
    public Picker picker;

    private View mRootView;

    private int mAvailableAreaHeight;
    private int mAvailableAreaWidth;

    private final Context mContext;

    Widget() {
        mContext = App.getInstance();
        init();
    }

    public void init() {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View container = layoutInflater.inflate(R.layout.widget_container, null);
        mRootView = container.findViewById(R.id.root);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                0 | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        wm.addView(container, params);

        mAvailableAreaHeight = DisplayInfo.getScreenHeight(App.getInstance());
        mAvailableAreaWidth = DisplayInfo.getScreenWidth(App.getInstance());

        View pickerView = layoutInflater.inflate(R.layout.widget_picker, null);
        View buttonView = layoutInflater.inflate(R.layout.widget_button, null);

        button = new Button(mContext, buttonView);
        picker = new Picker(mContext, pickerView);
    }

    public void hide() {
        button.hide();
        picker.hide();
    }

    private int getOrientation() {
        if (mAvailableAreaHeight > mAvailableAreaWidth) {
            return Configuration.ORIENTATION_PORTRAIT;
        } else {
            return Configuration.ORIENTATION_LANDSCAPE;
        }
    }

    public void showButton() {
        button.show();
    }

    public void hideButton() {
        button.hide();
    }

    public void openPicker() {
        picker.show();
    }

    public void hidePicker() {
        picker.hide();
    }

    public boolean isPickerVisible() {
        return picker.isShowing();
    }

    public class Picker extends PopupWindow {
        private final int mHeight;
        private final int mWidth;

        private final int mViewPagerWidth;

        private boolean mVisible;
        private final int mCellSize;

        private final int MARGIN = Utils.convertDpToPixel(5, App.getInstance());

        private final Column mColumn1;
        private final Column mColumn2;

        private final ScrollView mScrollView;

        private static final int COLUMNS_PER_SCREEN_PORTRAIT = 2;
        private static final int EMOTIONS_PER_PAGE_PORTRAIT = 4;
        private static final int GRIMACES_PER_PAGE_PORTRAIT = 2;

        private static final int COLUMNS_PER_SCREEN_LANDSCAPE = 3;
        private static final int EMOTIONS_PER_PAGE_LANDSCAPE = 3;
        private static final int GRIMACES_PER_PAGE_LANDSCAPE = 3;

        private final Context mContext;

        private final int MAX_EMOTIONS_COUNT = 10;

        private int getColumnsPerScreen() {
            switch (getOrientation()) {
                case Configuration.ORIENTATION_LANDSCAPE:
                    return COLUMNS_PER_SCREEN_LANDSCAPE;
                default:
                    return COLUMNS_PER_SCREEN_PORTRAIT;
            }
        }

        private int getEmotionsPerPage() {
            switch (getOrientation()) {
                case Configuration.ORIENTATION_LANDSCAPE:
                    return EMOTIONS_PER_PAGE_LANDSCAPE;
                default:
                    return EMOTIONS_PER_PAGE_PORTRAIT;
            }
        }

        private int getGrimacesPerPage() {
            switch (getOrientation()) {
                case Configuration.ORIENTATION_LANDSCAPE:
                    return GRIMACES_PER_PAGE_LANDSCAPE;
                default:
                    return GRIMACES_PER_PAGE_PORTRAIT;
            }
        }

        public Picker(Context context, View view) {
            super(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            mContext = context;

            int displayWidht = DisplayInfo.getScreenWidth(App.getInstance());

            mWidth = displayWidht;

            mCellSize = (mWidth / getColumnsPerScreen()) / (getGrimacesPerPage() + 1);

            mHeight = mCellSize * getEmotionsPerPage();

            mViewPagerWidth = mWidth / getColumnsPerScreen() - mCellSize;

            LinearLayout.LayoutParams screenParams = new LinearLayout.LayoutParams(mWidth, mHeight);
            mScrollView = (ScrollView) view.findViewById(R.id.container);
            mScrollView.setLayoutParams(screenParams);

            LinearLayout emotionsContainer1 = (LinearLayout) view.findViewById(R.id.emotions_container_1);
            LinearLayout emotionsContainer2 = (LinearLayout) view.findViewById(R.id.emotions_container_2);

            SmartViewPager viewPager1 = (SmartViewPager) view.findViewById(R.id.grimaces_pager_1);
            viewPager1.getLayoutParams().width = mViewPagerWidth;

            SmartViewPager viewPager2 = (SmartViewPager) view.findViewById(R.id.grimaces_pager_2);
            viewPager2.getLayoutParams().width = mViewPagerWidth;

            mColumn1 = new Column(emotionsContainer1, viewPager1);
            mColumn2 = new Column(emotionsContainer2, viewPager2);

            setBackgroundDrawable(new BitmapDrawable());
            setOutsideTouchable(true);
        }

        private void initEmotionsAndGrimaces(ArrayList<Integer> emotions, ArrayList<Grimace> grimaces) {
            String sortBy = ContentDescriptor.Grimaces.Cols.ID_SMILE + ", " + ContentDescriptor.GrimaceStatistic.Cols.USE_COUNT + " DESC";
            String selection = ContentDescriptor.Grimaces.Cols.TYPE + " = " + Grimace.TYPE_COLLECTION;

            String grimaceTable = ContentDescriptor.Grimaces.TABLE_NAME;
            String statisticTable = ContentDescriptor.GrimaceStatistic.TABLE_NAME;

            String query = "SELECT * FROM " + grimaceTable +
                    " LEFT OUTER JOIN " + statisticTable +
                    " ON " + ContentDescriptor.Grimaces.Cols.ID + " = " + ContentDescriptor.GrimaceStatistic.Cols.GRIMACE_ID +
                    " WHERE " + selection +
                    " ORDER BY " + sortBy;

            Cursor cursor = DBHelper.getInstance().getWritableDatabase().rawQuery(query,
                    null);

            ArrayList<Grimace> unsortedGrimaces = new ArrayList<>();

            try {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        Grimace grimace = Grimace.fromCursor(cursor);
                        unsortedGrimaces.add(grimace);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }

            LinkedHashMap<Integer, Integer> sortMap = new LinkedHashMap<>();

            int prevEmotion = 0;
            int prevUsedSum = 0;
            for (Grimace grimace : unsortedGrimaces) {
                if (prevEmotion != 0 && grimace.getSmileId() != prevEmotion) {
                    sortMap.put(prevEmotion, prevUsedSum);
                    prevUsedSum = 0;
                }
                prevEmotion = grimace.getSmileId();
                prevUsedSum += grimace.useCount;
            }
            sortMap.put(prevEmotion, prevUsedSum);

            ArrayList<Map.Entry<Integer, Integer>> entries = new ArrayList<>(sortMap.entrySet());
            Collections.sort(entries, new Comparator<Map.Entry<Integer, Integer>>() {
                public int compare(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
                    return b.getValue().compareTo(a.getValue());
                }
            });

            sortMap.clear();
            for (Map.Entry<Integer, Integer> entry : entries) {
                sortMap.put(entry.getKey(), entry.getValue());
            }

            for (Map.Entry<Integer, Integer> entry : sortMap.entrySet()) {
                int curSmileId = entry.getKey();
                for (Grimace grimace : unsortedGrimaces) {
                    if (grimace.getSmileId() == curSmileId) {
                        grimaces.add(grimace);
                    }
                }
            }

            for (Grimace grimace : grimaces) {
                if (!emotions.contains(grimace.getSmileId())) {
                    emotions.add(grimace.getSmileId());
                }
            }

            //If grimaces in db less than minimum set, add missing emotions
            for (Emotion emotion : Emotion.values()) {
                int emotionId = emotion.getId();
                if (!emotions.contains(emotionId)) {
                    emotions.add(emotionId);
                }
            }

            if (emotions.size() < MAX_EMOTIONS_COUNT) {
                int count = MAX_EMOTIONS_COUNT - emotions.size();
                for (int i = 0; i < count; i++) {
                    emotions.add(-1);
                }
            }
        }

        private void loadGrimaces() {
            final ArrayList<Grimace> grimaces = new ArrayList<>();
            final ArrayList<Integer> emotions = new ArrayList<>();

            initEmotionsAndGrimaces(emotions, grimaces);

            mColumn1.clear();
            mColumn2.clear();
            Column column = mColumn1;
            for (int i = 0; i < emotions.size(); i++) {
                if (i < getEmotionsPerPage() * getColumnsPerScreen()) {
                    if (i > 0 && i % getEmotionsPerPage() == 0) {
                        column = column == mColumn1 ? mColumn2 : mColumn1;
                    }
                } else {
                    column = column == mColumn1 ? mColumn2 : mColumn1;
                }
                column.emotions.add(emotions.get(i));
            }

            initEmotionsViewContainers(mColumn1);
            initEmotionsViewContainers(mColumn2);

            int positionOnCurPage = 0;
            int prevSmileId = 0;
            int pageNum = 0;

            column = mColumn1;
            int emotionNum = 0;
            for (int i = 0; i < grimaces.size(); i++) {
                Grimace grimace = grimaces.get(i);
                int curSmileId = grimace.getSmileId();

                if (curSmileId != prevSmileId) {
                    //new smile = new row on page
                    pageNum = 0;
                    positionOnCurPage = 0;

                    //if emotion placed on area that visible without scrolling, than
                    //pages should be filled in order - when first will be full, than second
                    //will started filling
                    //if emotion placed on area that will be visible only after scrolling,
                    //than pages should be filled alternatively - 1 emotion to first page,
                    //1 to second, 1 to first, 1 to second... etc.
                    if (emotionNum < getEmotionsPerPage() * getColumnsPerScreen()) {
                        if (emotionNum > 0 && emotionNum % getEmotionsPerPage() == 0) {
                            column = column == mColumn1 ? mColumn2 : mColumn1;
                        }
                    } else {
                        column = column == mColumn1 ? mColumn2 : mColumn1;
                    }
                    emotionNum++;
                } else if (positionOnCurPage >= getGrimacesPerPage()) {
                    pageNum++;
                    positionOnCurPage = 0;
                } else {
                    if (prevSmileId < 0) {
                        column = column == mColumn1 ? mColumn2 : mColumn1;
                    }
                }

                if (pageNum > column.pages.size() - 1) {
                    Page page = new Page();
                    for (int j = 0; j < column.emotions.size(); j++) {
                        int emotion = column.emotions.get(j);
                        page.put(emotion, new Grimace[getGrimacesPerPage()]);
                    }
                    positionOnCurPage = 0;
                    column.pages.add(page);
                }

                Page page = column.pages.get(pageNum);

                Grimace[] grimacesOnPage = page.get(curSmileId);
                grimacesOnPage[positionOnCurPage] = grimace;

                prevSmileId = curSmileId;
                positionOnCurPage++;
            }

            if (mColumn1.pages.size() == 0) {
                Page page = new Page();
                for (int j = 0; j < mColumn1.emotions.size(); j++) {
                    int emotion = mColumn1.emotions.get(j);
                    page.put(emotion, new Grimace[getGrimacesPerPage()]);
                }
                mColumn1.pages.add(page);
            }

            if (mColumn1.pages.size() == 0) {
                Page page = new Page();
                for (int j = 0; j < mColumn2.emotions.size(); j++) {
                    int emotion = mColumn2.emotions.get(j);
                    page.put(emotion, new Grimace[getGrimacesPerPage()]);
                }
                mColumn2.pages.add(page);
            }
        }

        private void initEmotionsViewContainers(Column column) {
            column.emotionsContentView.removeAllViews();
            for (int i = 0; i < column.emotions.size(); i++) {
                final int emotionId = column.emotions.get(i);
                ImageView imageView = new ImageView(mContext);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mCellSize - (MARGIN * 2), mCellSize - (MARGIN * 2));
                params.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);
                imageView.setLayoutParams(params);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setTag(emotionId);

                if (emotionId >= 0) {
                    imageView.setImageResource(Emotion.getById(emotionId).getResId(mContext, "drawable"));
                } else {
                    imageView.setImageResource(R.drawable.ic_buy);
                }

                column.emotionsContentView.addView(imageView);

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createEmoticon(emotionId);
                    }
                });
            }

            column.grimacesContentView.getLayoutParams().height = mCellSize * column.emotions.size();
        }

        private void createEmoticon(int emotionId) {
            if (emotionId > 0) {
                com.clickclap.util.PrefHelper.setIntPref("create_grimace", emotionId);
                Intent intent = new Intent(mContext, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        }

        public void show() {
            mVisible = true;

            mScrollView.post(new Runnable() {
                public void run() {
                    mScrollView.scrollTo(0, mScrollView.getTop());
                }
            });

            if (mRootView != null) {
                mRootView.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        loadGrimaces();

                        mColumn1.grimacesContentView.setAdapter(new GrimacesPagerAdapter(mColumn1));

                        mColumn2.grimacesContentView.setAdapter(new GrimacesPagerAdapter(mColumn2));

                        int mCurrentY = DisplayInfo.getScreenHeight(App.getInstance()) - mHeight;
                        showAtLocation(mRootView, Gravity.NO_GRAVITY, 0, mCurrentY);
                    }
                }, 100);
            }
        }

        public void hide() {
            mVisible = false;
            if (picker.isShowing()) {
                dismiss();
            }
        }

        public boolean isVisible() {
            return mVisible;
        }

        private class GrimacesPagerAdapter extends PagerAdapter {
            private Column mColumn;

            GrimacesPagerAdapter(Column column) {
                mColumn = column;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {

                Context context = App.getInstance();

                int margin = Utils.convertDpToPixel(3, context);
                int cellSize = mCellSize - (margin * 2);

                Page page = mColumn.pages.get(position);

                TableLayout tableView = new TableLayout(context);

                for (Map.Entry<Integer, Grimace[]> entry : page.entrySet()) {
                    Grimace[] grimaces = entry.getValue();
                    final int emotion = entry.getKey();

                    TableRow row = new TableRow(context);
                    row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

                    for (int i = 0; i < getGrimacesPerPage(); i++) {
                        final Grimace grimace = grimaces[i];

                        View cellView;
                        if (grimace != null) {
                            cellView = new GrimaceCircleView(context);
                            ((GrimaceCircleView) cellView).setGrimace(grimace);
                        } else {
                            cellView = new CircleImageView(mContext);
                            ((CircleImageView) cellView).setBorderColor(R.color.yellow);
                            ((CircleImageView) cellView).setBorderSize(3);
                            ((CircleImageView) cellView).setImageResource(R.drawable.circle_plus);
                        }

                        TableRow.LayoutParams params = new TableRow.LayoutParams(cellSize, cellSize);
                        params.setMargins(margin, margin, margin, margin);
                        cellView.setLayoutParams(params);

                        cellView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (grimace != null) {
                                    sendGrimace(grimace);
                                    hide();
                                } else {
                                    createEmoticon(emotion);
                                }
                            }
                        });

                        row.addView(cellView);
                    }
                    tableView.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }

                container.addView(tableView);
                return tableView;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public int getCount() {
                return mColumn.pages.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

        }

        /**
         * Container for viewpager pages
         *
         * @link COLUMNS_PER_SCREEN - how much columns (viewpagers) will be showed
         */
        private class Column {
            ArrayList<Integer> emotions = new ArrayList<>();
            ArrayList<Page> pages = new ArrayList<>();

            ViewGroup emotionsContentView;
            SmartViewPager grimacesContentView;

            public void clear() {
                emotions.clear();
                pages.clear();
            }

            Column(ViewGroup emotionsContentView, SmartViewPager grimacesContentView) {
                this.emotionsContentView = emotionsContentView;
                this.grimacesContentView = grimacesContentView;
            }
        }

        /**
         * Contains grimaces and emotions inside page that will be used in viewpager
         */
        private class Page extends LinkedHashMap<Integer, Grimace[]> {

        }

    }

    public class Button extends PopupWindow implements View.OnTouchListener {

        public static final String POSITION_X = "x";
        public static final String POSITION_Y = "y";

        private int mInitialPositionY;
        private int mInitialPositionX;

        private final int mHideAreaHeight;

        private final int mButtonSize;

        private boolean mVisible;

        private float mInitialDx;
        private float mInitialDy;

        private int mCurrentX;
        private int mCurrentY;

        boolean mMoved;

        private final int mMoveStep;

        public void hide() {
            mVisible = false;
            if (isShowing()) {
                dismiss();
            }
        }

        public void show() {
            mVisible = true;
            mRootView.post(new Runnable() {
                @Override
                public void run() {
                    initPosition();
                    mCurrentY = mInitialPositionY;
                    mCurrentX = mInitialPositionX;
                    showAtLocation(mRootView, Gravity.NO_GRAVITY, mCurrentX, mCurrentY);
                }
            });
        }

        private void initPosition() {
            if (PrefHelper.getFloatPreference(POSITION_X) == 0) {
                mInitialPositionX = (mAvailableAreaWidth - mButtonSize) / 2;
            } else {
                mInitialPositionX = (int) PrefHelper.getFloatPreference(POSITION_X);
            }

            if (PrefHelper.getFloatPreference(POSITION_Y) == 0) {
                mInitialPositionY = (int) (mAvailableAreaHeight - mButtonSize - App.getInstance().getResources().getDimension(R.dimen.button_size_bottom_offset));
            } else {
                mInitialPositionY = (int) PrefHelper.getFloatPreference(POSITION_Y);
            }
        }

        public Button(Context context, View view) {
            super(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            mButtonSize = (int) context.getResources().getDimension(R.dimen.button_size);

            view.setOnTouchListener(this);
            mHideAreaHeight = (int) (mButtonSize * 1.1);
            initPosition();

            mMoveStep = mAvailableAreaWidth / 7;
        }

        private void checkHideArea() {
            if (mCurrentY + mButtonSize < mHideAreaHeight) {
                PrefHelper.setFloatPreference(POSITION_X, 0);
                PrefHelper.setFloatPreference(POSITION_Y, 0);
                hideButton();
                PrefHelper.setHided(true);
            }
        }

        public int getButtonSize() {
            return mButtonSize;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_DOWN) {
                mMoved = false;
                mInitialDx = mCurrentX - event.getRawX();
                mInitialDy = mCurrentY - event.getRawY();
            } else if (action == MotionEvent.ACTION_MOVE) {
                int newY = (int) (event.getRawY() + mInitialDy);
                int newX = (int) (event.getRawX() + mInitialDx);

                int diffX = newX - mCurrentX;
                int diffY = newY - mCurrentY;

                int cellCountY = Math.abs(diffY) / mMoveStep;
                int cellCountX = Math.abs(diffX) / mMoveStep;

                if (cellCountX > 0 || cellCountY > 0) {
                    if (cellCountX > cellCountY) {
                        mCurrentX = mCurrentX + ((cellCountX * mMoveStep) * (diffX > 0 ? 1 : -1));
                    } else {
                        mCurrentY = mCurrentY + ((cellCountY * mMoveStep) * (diffY > 0 ? 1 : -1));
                    }
                    mMoved = true;
                    button.update(mCurrentX, mCurrentY, -1, -1);
                    PrefHelper.setFloatPreference(POSITION_X, mCurrentX);
                    PrefHelper.setFloatPreference(POSITION_Y, mCurrentY);
                    checkHideArea();
                }
            } else if (action == MotionEvent.ACTION_UP) {
                if (!mMoved) {
                    openPicker();
                }
            }
            return true;
        }

        public boolean isVisible() {
            return mVisible;
        }
    }

    private void updateGrimaceUsingStatistics(final Grimace grimace) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int grimaceId = grimace.getId();
                String selection = ContentDescriptor.GrimaceStatistic.Cols.GRIMACE_ID + " = " + grimaceId;

                ContentResolver resolver = mContext.getContentResolver();
                Cursor cursor = resolver.query(ContentDescriptor.GrimaceStatistic.URI,
                        new String[]{ContentDescriptor.GrimaceStatistic.Cols.USE_COUNT},
                        selection,
                        null,
                        null);

                int useCount = 0;
                try {
                    if (cursor != null && cursor.moveToFirst()) {
                        useCount = cursor.getInt(0);
                    }
                } finally {
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                    }
                }

                ContentValues values = new ContentValues(2);
                values.put(ContentDescriptor.GrimaceStatistic.Cols.USE_COUNT, ++useCount);
                values.put(ContentDescriptor.GrimaceStatistic.Cols.GRIMACE_ID, grimaceId);

                if (useCount == 1) {
                    resolver.insert(ContentDescriptor.GrimaceStatistic.URI, values);
                } else {
                    int updated = resolver.update(ContentDescriptor.GrimaceStatistic.URI, values, selection, null);
                    Log.i("UPDATED", String.valueOf(updated));
                }
            }
        }).start();
    }

    private void sendGrimace(Grimace grimace) {

        updateGrimaceUsingStatistics(grimace);

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.setType("image/png");

        String type = com.clickclap.widget.Utils.getCurLaunchedAppPackageName(mContext);
        File file = FilePathHelper.getGrimaceFile(grimace.getUrl());
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));

        if (Messenger.getByPackageName(type) != null) {
            // gets the list of intents that can be loaded.
            List<ResolveInfo> resInfo = mContext.getPackageManager().queryIntentActivities(shareIntent, 0);
            if (!resInfo.isEmpty()) {
                for (ResolveInfo info : resInfo) {
                    if (info.activityInfo.packageName.toLowerCase().contains(type) || info.activityInfo.name.toLowerCase().contains(type)) {
                        shareIntent.setPackage(info.activityInfo.packageName);
                        break;
                    }
                }
            }
        }

        mContext.startActivity(shareIntent);
    }
}
