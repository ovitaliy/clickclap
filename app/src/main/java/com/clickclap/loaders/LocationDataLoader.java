package com.clickclap.loaders;

import android.view.View;
import android.widget.AdapterView;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.events.SimpleOnItemSelectedListener;
import com.clickclap.model.City;
import com.clickclap.model.Country;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.model.CitiesListResponse;
import com.clickclap.rest.model.CountriesListResponse;
import com.clickclap.rest.request.GetCitiesRequest;
import com.clickclap.rest.request.GetCountriesRequest;
import com.clickclap.view.CityAutoCompleteTextView;
import com.clickclap.view.CountrySpinnerView;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LocationDataLoader {

    private SpiceManager mSpiceManager;
    private CityAutoCompleteTextView mCityAutoCompleteView;
    private CountrySpinnerView mCountrySpinnerView;

    private GetCitiesRequest mCitiesRequest;

    private int mUserCountryId = -1;
    private String mUserCityName = null;

    private int mCountryId = -1;

    private LocationSelectListener mLocationSelectListener = null;

    public LocationDataLoader(SpiceManager spiceManager, CityAutoCompleteTextView cityListView, CountrySpinnerView countryListView, String userCityName) {
        mSpiceManager = spiceManager;
        mCityAutoCompleteView = cityListView;
        mCityAutoCompleteView.setLocationDataLoader(this);
        mCountrySpinnerView = countryListView;

        mCityAutoCompleteView.setOnItemSelectedListener(new SimpleOnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mLocationSelectListener != null)
                    mLocationSelectListener.onCitySelected((int) id);
            }
        });


        mCountrySpinnerView.setOnItemSelectedListener(new SimpleOnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLocationSelectListener != null)
                    mLocationSelectListener.onCountrySelected((int) id);

                if (position == 0) {
                    mCityAutoCompleteView.setVisibility(View.GONE);
                } else {
                    if (mUserCityName != null) {
                        mCountryId = (int) id;
                        loadCities(mCountryId, mUserCityName);
                    } else {
                        loadCities((int) id);
                    }
                }
            }
        });

        if (AppUser.get() != null) { // on new profile User is null
            mUserCountryId = AppUser.get().getCountryId();
        }

        mUserCityName = userCityName;

        loadCountries();
    }

    private void loadCities(final int countryId) {
        if (countryId != mCountryId) {
            mCityAutoCompleteView.clear();
        }
        mCountryId = countryId;
        loadCities(countryId, null);
    }

    public void loadCities(String typed) {
        loadCities(mCountryId, typed);
    }

    private void loadCities(final int countryId, String typed) {
        if (mCitiesRequest != null) {
            mSpiceManager.cancel(mCitiesRequest);
        }
        mCitiesRequest = new GetCitiesRequest(countryId, typed);

        mSpiceManager.execute(mCitiesRequest, new BaseRequestListener<CitiesListResponse>() {
            @Override
            public void onRequestSuccess(CitiesListResponse response) {
                super.onRequestSuccess(response);
                City[] cities = response.getCities();
                if (cities == null) {
                    return;
                }
                mCityAutoCompleteView.setCityList(Arrays.asList(cities));
                if (mUserCityName != null) {
                    for (int i = 0; i < cities.length; i++) {
                        if (mUserCityName.equals(cities[i].getTitle())) {
                            mCityAutoCompleteView.setSelectedPosition(i);
                            break;
                        }
                    }
                    mUserCityName = null;
                }

                mCitiesRequest = null;
            }
        });
    }

    public void loadCountries() {
        mSpiceManager.execute(new GetCountriesRequest(), new BaseRequestListener<CountriesListResponse>() {
            @Override
            public void onRequestSuccess(final CountriesListResponse response) {
                super.onRequestSuccess(response);

                Country[] countries = response.getCountries();
                List<Country> countryList = new ArrayList<>(countries.length + 1);
                countryList.add(new Country(App.getInstance().getString(R.string.choose_country)));
                Collections.addAll(countryList, countries);
                mCountrySpinnerView.setCountryList(countryList);

                if (mUserCountryId >= 0) {
                    for (int i = 0; i < countryList.size(); i++) {
                        int id = countryList.get(i).getId();
                        if (id == mUserCountryId) {
                            mCountrySpinnerView.setSelection(i);
                            break;
                        }
                    }
                }

                // get country id by phone code which was entered in verification
                /* else {



                    String countryCode = PrefHelper.getStringPref(Const.PREF.COUNTRY_CODE);
                    if (countryCode != null) {
                        for (int i = 0; i < countryList.size(); i++) {
                            Country country = countryList.get(i);
                            if (countryCode.equalsIgnoreCase(country.getCode())) {
                                mCountrySpinnerView.setSelection(i);
                                break;
                            }
                        }
                    }
                }*/

            }
        });

    }

    public void setLocationSelectListener(LocationSelectListener locationSelectListener) {
        mLocationSelectListener = locationSelectListener;
    }

    public int getSelectedCountryId() {
        return (int) mCountrySpinnerView.getSelectedItemId();
    }

    public String getSelectedCountryName() {
        return ((Country) mCountrySpinnerView.getSelectedItem()).getTitle();
    }

    public int getSelectedCityId() {
        return mCityAutoCompleteView.getSelectedItemId();
    }

    public String getSelectedCityName() {
        return mCityAutoCompleteView.getSelectedItem().getTitle();
    }


    public interface LocationSelectListener {

        void onCountrySelected(int countryId);

        void onCitySelected(int cityId);

    }

}
