package com.clickclap.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.events.VideoUploadedEvent;
import com.clickclap.rest.CountingFileRequestBody;
import com.clickclap.util.FilePathHelper;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Deni on 03.07.2015.
 */
public class UploadMediaService extends IntentService {
    private static final String TAG = "UploadMediaService";
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;

    public UploadMediaService() {
        super("UploadMediaService");
    }

    public static Intent newIntent(Context context, int videoId, String videoPath, String imagePath, boolean clearCacheAfterUpload) {
        Intent intent = new Intent(context, UploadMediaService.class);
        intent.putExtra("video_path", videoPath);
        intent.putExtra("image_path", imagePath);
        intent.putExtra("clear_cache", clearCacheAfterUpload);
        intent.putExtra("id", videoId);
        return intent;
    }

    private void updateProgress(int progress) {
        if (progress < 100) {
            mBuilder.setProgress(100, progress, false);
            mBuilder.setContentText("Progress: " + progress + "%");
            mNotifyManager.notify(1, mBuilder.build());
        } else {
            mNotifyManager.cancel(1);
        }
    }

    public void upload(int id, String videoPath, String imagePath, boolean clearCacheAfterUpload) {
        File videoFile = new File(videoPath);
        File imgFile = new File(imagePath);

        Log.i(TAG, "start uploading:" + new Date());

        final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
        final MediaType MEDIA_TYPE_MP4 = MediaType.parse("video/mp4");
        final MediaType MEDIA_TYPE_TEXT = MediaType.parse("text/plain");

        final OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"id_video\""),
                        RequestBody.create(MEDIA_TYPE_TEXT, String.valueOf(id))
                )
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"thumb\"; filename=\"" + imgFile.getName() + "\""),
                        RequestBody.create(MEDIA_TYPE_PNG, imgFile))
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"" + videoFile.getName() + "\""),
                        new CountingFileRequestBody(RequestBody.create(MEDIA_TYPE_MP4, videoFile), new CountingFileRequestBody.Listener() {
                            int mLastProgress = -1;

                            @Override
                            public void onRequestProgress(long bytesWritten, long contentLength) {
                                int progress = (int) ((bytesWritten / (float) contentLength) * 100);
                                if (progress > mLastProgress) {
                                    updateProgress(progress);
                                    Log.d(TAG, "uploaded:" + progress + "%");
                                    mLastProgress = progress;
                                }
                            }
                        })
                )
                .build();

        Request request = new Request.Builder()
                .url(Const.MEDIA_URL + "/media/")
                .post(requestBody)
                .build();

        try {
            client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i(TAG, "end uploading:" + new Date());

        EventBus.getDefault().post(new VideoUploadedEvent());

        if (clearCacheAfterUpload) {
            File file = FilePathHelper.getVideoTmpFile();
            File file2 = FilePathHelper.getVideoTmpFile2();

            if (file != null) {
                file.delete();
            }

            if (file2 != null) {
                file2.delete();
            }

            videoFile.delete();
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle("Video Upload")
                .setSmallIcon(R.drawable.ic_launcher);
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
        mBuilder.setLargeIcon(bm);

        String videoPath = intent.getStringExtra("video_path");
        String imagePath = intent.getStringExtra("image_path");
        boolean clearCache = intent.getBooleanExtra("clear_cache", false);
        int id = intent.getIntExtra("id", 0);
        if (!TextUtils.isEmpty(videoPath) && !TextUtils.isEmpty(imagePath) && id > 0) {
            upload(id, videoPath, imagePath, clearCache);
        } else {
            String log = String.format("id %d, videoPath: %s, imagePath: %s", id, videoPath, imagePath);
            throw new IllegalArgumentException("Video info required");
        }
    }
}
