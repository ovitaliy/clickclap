package com.clickclap;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.text.TextUtils;

import com.clickclap.db.ContentDescriptor;
import com.clickclap.enums.Language;
import com.clickclap.model.Grimace;
import com.clickclap.rest.listener.GetGrimacesCacheRequestListener;
import com.clickclap.rest.request.grimace.GetGrimacesRequest;
import com.clickclap.rest.service.AppFileRetrofitSpiceService;
import com.clickclap.rest.service.AppRetrofitSpiceService;
import com.clickclap.util.PrefHelper;
import com.clickclap.widget.service.BackgroundService;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.octo.android.robospice.SpiceManager;

import io.fabric.sdk.android.Fabric;

public class App extends Application {
    public static String API_APP_NAME = "cc";

    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static int WIDTH;
    public static int WIDTH_WITHOUT_MARGINS;
    public static int HEIGHT;
    public static int MARGIN;
    public static int IMAGE_BORDER;
    public static int IMAGE_SMALL_WIDTH;

    private void initLang() {
        if (TextUtils.isEmpty(PrefHelper.getStringPref(Const.PREF_SUFFIX))) {
            PrefHelper.setStringPref(Const.PREF_SUFFIX, Language.getSystem().getSuffix());
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();

        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());

        instance = this;
        PrefHelper.init(this);

        initImageLoader();
        initLang();

        startWidget();

//        Ubertesters.initialize(this);
    }

    private void startWidget() {
        /*if (!BuildConfig.DEBUG) {*/
        if (checkWidgetWindowPermissionPermission(this)) {
            Intent serviceIntent = new Intent(this, BackgroundService.class);
            startService(serviceIntent);
            PendingIntent pintent = PendingIntent.getService(this, 0, serviceIntent, 0);
            AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarm.cancel(pintent);
            alarm.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 60000, pintent);
        }
    }

    private void initImageLoader() {
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(1)
                .defaultDisplayImageOptions(displayImageOptions)
                //.writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }

    //----
    private static SpiceManager sFileSpiceManager;

    public static SpiceManager getFileSpiceManager() {
        if (sFileSpiceManager == null || !sFileSpiceManager.isStarted()) {
            sFileSpiceManager = new SpiceManager(AppFileRetrofitSpiceService.class);
            sFileSpiceManager.start(instance);
        }
        return sFileSpiceManager;
    }

    private static SpiceManager sSpiceManager;

    public static SpiceManager getSpiceManager() {
        if (sSpiceManager == null || !sSpiceManager.isStarted()) {
            sSpiceManager = new SpiceManager(AppRetrofitSpiceService.class);
            sSpiceManager.start(instance);
        }
        return sSpiceManager;
    }

    private boolean checkWidgetWindowPermissionPermission(Context context) {
        String permission = "android.permission.SYSTEM_ALERT_WINDOW";
        int res = context.checkCallingPermission(permission);
        return res == PackageManager.PERMISSION_GRANTED;
    }

    public static void syncGrimaces() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor grimaceCursor = App.getInstance().getContentResolver().query(ContentDescriptor.Grimaces.URI,
                        null,
                        ContentDescriptor.Grimaces.Cols.TO_UPLOAD + " = 1",
                        null,
                        null);

                if (grimaceCursor == null || grimaceCursor.getCount() == 0) {
                    getSpiceManager().execute(
                            new GetGrimacesRequest(Grimace.TYPE_COLLECTION),
                            new GetGrimacesCacheRequestListener());
                }

                if (grimaceCursor != null && !grimaceCursor.isClosed()) {
                    grimaceCursor.close();
                }
            }
        }).start();
    }

}
