package com.clickclap;

import java.text.SimpleDateFormat;
import java.util.Locale;

public interface Const {
    String EXTRA_LAYOUT_ID = "layout_id";
    String EXTRA_ACTION = "action";
    String EXTRA_SMILE = "smile";

    SimpleDateFormat DAY_MONTH_YEAR_FORMAT = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());
    SimpleDateFormat HOUR_MINUTE_FORMAT = new SimpleDateFormat("hh:mm", Locale.getDefault());
    SimpleDateFormat MINUTE_SECOND_FORMAT = new SimpleDateFormat("mm:ss", Locale.getDefault());

    String VIDEO_EXTENTION = "mp4";
    String IMAGE_EXTENTION = "png";
    String AUDIO_EXTENTION = "mp3";

    String RES_TYPE_DRAWABLE = "drawable";
    String RES_TYPE_STRING = "string";

    final class Action {
        public static final int TOKEN = 1;
        public static final int SHARE = 2;
        public static final int GET_CONTACTS = 3;

        public static final int PLAY_FLOW = 4;
        public static final int PLAY_FLOW_SINGLE_VIDEO = 5;

        public static final int FILL_PROFILE = 6;
        public static final int SHOW_PROFILE = 7;
    }

    final class Params {
        public static final String ACTION = "action";
        public static final String LINK = "link";
        public static final String VIDEO_ID = "video_id";
        public static final String VIDEO_LIST = "videoList";
        public static final String CURRENT_VIDEO = "currentVideo";
        public static final String TYPE = "type";
        public static final String PARAMS = "params";
        public static final String TITLE = "title";
        public static final String POSITION = "position";
        public static final String REPLY_ID = "reply_id";
        public static final String EMOTION = "emotion";
        public static final String REPLY_VIDEO = "reply_video";
        public static final String CATEGORY = "category";
        public static final String USER_ID = "userId";
        public static final String RELATIONSHIP = "relationship";
        public static final String CONTACTS = "contacts";
        public static final String IS_GRIMACE = "is_grimace";

        public static final String VIDEO = "video";
    }

    String PREF_PHONE = "phone_number";
    String PREF_REG = "registered";
    String PREF_TOKEN = "auth_token";
    String PREF_SUFFIX = "suffix";

    //Directories for save files
    String DIR_BASE = "clikclap";
    String DIR_AUDIO = "music";

    String BASE_API_URL = "http://api.thehumanet.com/cc";
    String MEDIA_URL = "http://api.thehumanet.com/";

    int EMOTICON_OK = 1;
    int VIDEO_OK = 2;
    int VERIFY_OK = 3;
    int PROFILE_OK = 4;

    int LIMIT = 20;

    int CROWD_SINGLE_AMOUNT = 10;

    int USER_MIN_AGE = 12;
}
