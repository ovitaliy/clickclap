package com.clickclap.events;

/**
 * Created by Denis on 24.05.2015.
 */
public class ServerErrorEvent {
    int[] mCodes;
    String[] mMessages;

    public ServerErrorEvent(int codes[], String[] messages) {
        mCodes = codes;
        mMessages = messages;
    }

    public int[] getCodes() {
        return mCodes;
    }

    public String[] getMessages() {
        return mMessages;
    }
}
