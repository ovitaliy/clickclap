package com.clickclap.events;

/**
 * Created by Deni on 06.08.2015.
 */
public class ShowProgressDialogEvent {
    private boolean mShow;

    public ShowProgressDialogEvent(boolean show) {
        mShow = show;
    }

    public boolean isShow() {
        return mShow;
    }
}
