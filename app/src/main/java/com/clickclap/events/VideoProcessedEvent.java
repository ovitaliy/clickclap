package com.clickclap.events;

/**
 * Created by Denis on 17.04.2015.
 */
public class VideoProcessedEvent {
    private boolean mSuccess;

    public VideoProcessedEvent(boolean success) {
        mSuccess = success;
    }

    public boolean isSuccess() {
        return mSuccess;
    }
}
