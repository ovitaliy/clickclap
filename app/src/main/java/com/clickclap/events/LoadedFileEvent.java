package com.clickclap.events;

/**
 * Created by denisvasilenko on 15.02.16.
 */
public class LoadedFileEvent {
    String mPath;

    public LoadedFileEvent(String path) {
        mPath = path;
    }

    public String getPath() {
        return mPath;
    }
}
