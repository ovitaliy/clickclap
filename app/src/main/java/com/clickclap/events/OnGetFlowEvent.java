package com.clickclap.events;

import com.clickclap.model.Video;

import java.util.ArrayList;

/**
 * Created by Deni on 13.02.2016.
 */
public class OnGetFlowEvent {
    ArrayList<Video> mVideos = new ArrayList<>();

    public OnGetFlowEvent(ArrayList<Video> list) {
        mVideos.clear();
        mVideos.addAll(list);
    }

    public ArrayList<Video> getVideos() {
        return mVideos;
    }
}