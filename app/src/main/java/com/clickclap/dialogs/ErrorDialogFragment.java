package com.clickclap.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.clickclap.enums.ServerError;

/**
 * Created by Denis on 24.05.2015.
 */
public class ErrorDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public static ErrorDialogFragment newInstance(int code, String message) {
        ErrorDialogFragment errorDialog = new ErrorDialogFragment();
        Bundle args = new Bundle(2);
        args.putInt("code", code);
        args.putString("msg", message);
        errorDialog.setArguments(args);
        return errorDialog;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int code = getArguments().getInt("code");
        String msg = getArguments().getString("msg");
        if (code > 0) {
            ServerError error = ServerError.getByCode(code);
            if (error != null) {
                msg = error.getMessage();
            }
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle("Error")
                .setNeutralButton(android.R.string.ok, this)
                .setMessage(msg);
        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();
    }
}
