package com.clickclap.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.clickclap.Const;
import com.clickclap.activities.ContactsActivity;
import com.clickclap.R;
import com.clickclap.view.SelectContactGroupDialogView;

import java.util.ArrayList;

/**
 * Created by michael on 25.11.14.
 */
public class SelectContactsTypeDialog implements Const {
    private Context mContext;
    private Dialog mDialog;
    private View mView;

    private AsyncTask<Integer, Void, ArrayList<ContactsActivity.Group>> mTask;

    private int mSelectedGroup;

    public SelectContactsTypeDialog(Context context) {
        mContext = context;
        mDialog = create();
        setupView();
    }

    public SelectContactsTypeDialog setCallback(AsyncTask<Integer, Void, ArrayList<ContactsActivity.Group>> task) {
        mTask = task;
        return this;
    }

    public SelectContactsTypeDialog show() {
        mDialog.show();
        return this;
    }

    /*public void dismiss() {
        mDialog.dismiss();
    }*/

    private Dialog create() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mView = new SelectContactGroupDialogView(mContext);
        dialog.setContentView(mView);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    private void setupView() {
        RadioGroup rg = (RadioGroup) mView.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedGroup = checkedId;
            }
        });
        mView.findViewById(R.id.btn_nav).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mTask.execute(mSelectedGroup);
                dismiss();
            }
        });
        rg.check(R.id.rb_fav);
    }

    private void dismiss() {
        mDialog.dismiss();
    }
}
