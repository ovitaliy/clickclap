package com.clickclap.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.clickclap.listener.NumberSelectedListener;

import java.util.ArrayList;

/**
 * Created by Deni on 26.08.15.
 */
public class SelectPhoneNumberDialogFragment extends DialogFragment {
    NumberSelectedListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (NumberSelectedListener) activity;
    }

    public static SelectPhoneNumberDialogFragment newInstance(String name, ArrayList<String> phones) {
        SelectPhoneNumberDialogFragment fragment = new SelectPhoneNumberDialogFragment();
        Bundle args = new Bundle(1);
        args.putSerializable("phones", phones);
        args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        ArrayList<String> phonesList = (ArrayList<String>) getArguments().getSerializable("phones");
        String name = getArguments().getString("name");

        final String[] phones = phonesList.toArray(new String[phonesList.size()]);

        return new AlertDialog.Builder(getActivity())
                .setItems(phones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String phone = phones[which];
                        mListener.onNumberSelected(phone);
                        dismiss();
                    }
                })
                .setTitle(name)
                .setCancelable(true)
                .show();
    }

}
