package com.clickclap.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.adapter.TipsAdapter;

/**
 * Created by Denis on 24.05.2015.
 */
public class TipsDialogFragment extends DialogFragment implements DialogInterface.OnClickListener, AbsListView.OnScrollListener, View.OnClickListener {
    TextView mTitleView;
    String[] mTitles;
    Button mButtonClose;

    int mPrevVisibleItem = -1;

    public static TipsDialogFragment newInstance() {
        TipsDialogFragment dialog = new TipsDialogFragment();
        return dialog;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_tips, new LinearLayout(getActivity()));
        ListView listView = (ListView) view.findViewById(R.id.tips_list);
        mButtonClose = (Button) view.findViewById(R.id.btn_close);

        String[] tips = getResources().getStringArray(R.array.tips);
        TipsAdapter adapter = new TipsAdapter(getActivity(), tips);

        mTitles = new String[tips.length];
        for (int i = 0; i < tips.length; i++) {
            String tip = tips[i];
            mTitles[i] = tip.split("<br>")[0];
        }

        listView.setAdapter(adapter);
        mTitleView = (TextView) view.findViewById(R.id.title);
        listView.setOnScrollListener(this);

        mButtonClose.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(view);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem != mPrevVisibleItem) {
            if (mTitles.length > 0) {
                String title = mTitles[firstVisibleItem];
                mTitleView.setText(title);
                mPrevVisibleItem = firstVisibleItem;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_close:
                dismiss();
                break;
        }
    }
}
