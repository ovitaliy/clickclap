package com.clickclap.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.clickclap.App;
import com.clickclap.AppUser;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.MainActivity;
import com.clickclap.activities.NewVideoActivity;
import com.clickclap.enums.GameType;
import com.clickclap.enums.VideoUpdateAction;
import com.clickclap.fragment.FlowListFragment;
import com.clickclap.model.Video;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.request.video.ComplaintVideoRequest;
import com.clickclap.rest.request.video.UpdateVideoRequest;
import com.clickclap.view.dialogViews.MoreDialogView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by michael on 25.11.14.
 */
public class MoreDialogFragment extends DialogFragment implements Const {
    public static final String TAG = "MoreDialog";

    private MoreDialogView mView;
    private Video mVideo;
    private Video mVideoParent;
    private int mAction = -1;
    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args.containsKey("video")) {
            mVideo = (Video) args.getSerializable("video");
        }

        if (args.containsKey("video_parent")) {
            mVideoParent = (Video) args.getSerializable("video_parent");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mView = new MoreDialogView(getActivity());

        RadioButton responsesButton = (RadioButton) mView.findViewById(R.id.rb_watch_responses);
        if (mVideo.getReplays() > 0) {
            responsesButton.setVisibility(View.VISIBLE);
            responsesButton.setText(App.getInstance().getString(R.string.record_answer_watch, mVideo.getReplays()));
        } else {
            responsesButton.setVisibility(View.GONE);
        }

        RadioButton finishGameButton = (RadioButton) mView.findViewById(R.id.rb_finish_game);
        if (mVideo.getGameId() == GameType.WHAT_WHERE_WHEN.getId() && mVideo.getReplyId() == 0 && mVideo.getAuthorId() == AppUser.getUid()) {
            finishGameButton.setVisibility(View.VISIBLE);
        } else {
            finishGameButton.setVisibility(View.GONE);
        }

        RadioButton setWinnerButton = (RadioButton) mView.findViewById(R.id.rb_select_winner);
        if (mVideoParent != null && mVideoParent.getGameId() == GameType.WHAT_WHERE_WHEN.getId() && mVideoParent.getAuthorId() == AppUser.getUid()) {
            setWinnerButton.setVisibility(View.VISIBLE);
        } else {
            setWinnerButton.setVisibility(View.GONE);
        }

        RadioGroup rg = (RadioGroup) mView.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mAction = checkedId;
                switch (mAction) {
                    case R.id.rb_share:
                        int shareType;
                        int smileId = mVideo.getSmileId();
                        if (smileId > 0) {
                            shareType = ShareDialogFragment.TYPE_EMOTICON;
                        } else {
                            shareType = ShareDialogFragment.TYPE_SHOT;
                        }
                        new ShareDialogFragment.Builder()
                                .setShareType(shareType)
                                .setLink(mVideo.getShortUrl())
                                .setVideoId(mVideo.getVideoId())
                                .setCanBeClosed(true)
                                .build()
                                .show(getFragmentManager(), ShareDialogFragment.TAG);
                        dismiss();
                        break;
                    case R.id.rb_complaint:
                        mSpiceManager.execute(new ComplaintVideoRequest(mVideo.getVideoId()), new BaseRequestListener());
                        dismiss();
                        break;
                    case R.id.rb_answer:
                        recordAnswer();
                        dismiss();
                        break;
                    case R.id.rb_watch_responses:
                        watchResponses();
                        break;
                    case R.id.rb_finish_game:
                        finishGame();
                        break;
                    case R.id.rb_select_winner:
                        setWinner();
                        break;
                    default:
                        dismiss();
                        break;
                }
            }
        });

        Dialog dialog = new AlertDialog.Builder(getActivity())
                .setView(mView)
                .show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.CENTER);
        return dialog;
    }

    public static MoreDialogFragment newInstance(Video video, Video videoParent) {
        MoreDialogFragment fragment = new MoreDialogFragment();
        Bundle args = new Bundle(2);
        args.putSerializable("video", video);
        args.putSerializable("video_parent", videoParent);
        fragment.setArguments(args);
        return fragment;
    }

    private void recordAnswer() {
        NewVideoActivity.startNewInstance(getActivity(), mVideo);
    }

    private void watchResponses() {
        FlowListFragment fragment = new FlowListFragment.Builder(null)
                .setReplyId(mVideo.getVideoId())
                .setTitle(getString(R.string.dialog_responses_watch))
                .build();

        ((MainActivity) getActivity()).startFragment(fragment, true);
        dismiss();
    }

    private void finishGame() {
        dismiss();
        mSpiceManager.execute(new UpdateVideoRequest(mVideo.getVideoId(), VideoUpdateAction.END), new BaseRequestListener());
    }

    private void setWinner() {
        dismiss();
        mSpiceManager.execute(new UpdateVideoRequest(mVideo.getVideoId(), VideoUpdateAction.WINNER), new BaseRequestListener());
    }

}
