package com.clickclap.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;

import com.clickclap.view.ProgressDialogView;

public class ProgressDialog {
    private static final String TAG = "ProgressDialog";

    private Context mContext;
    private Dialog mDialog;
    private ProgressDialogView mView;

    public ProgressDialog(Context context) {
        mContext = context;
        mDialog = create();
    }

    public void setTitle(String title) {
        mView.setTitle(title);
    }

    public void setTitle(int title) {
        mView.setTitle(mContext.getResources().getString(title));
    }

    public void hideTitle() {
        mView.hideTitle();
    }

    public void showTitle() {
        mView.showTitle();
    }

    public void setCancelable(boolean isCancelable) {
        if (mDialog != null) {
            mDialog.setCancelable(isCancelable);
        }
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener listener) {
        mDialog.setOnCancelListener(listener);
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
            Log.i(TAG, "show");
        }
    }

    public void hide() {
        mDialog.hide();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            Log.i(TAG, "dissmis");
        }
    }

    private Dialog create() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mView = new ProgressDialogView(mContext);
        dialog.setContentView(mView);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public boolean isShowing() {
        if (mDialog == null) {
            return false;
        }
        return mDialog.isShowing();
    }

    public boolean isDismissed() {
        if (mDialog == null) {
            return true;
        } else {
            return !mDialog.isShowing();
        }
    }
}
