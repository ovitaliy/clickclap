package com.clickclap.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.clickclap.App;
import com.clickclap.Const;
import com.clickclap.R;
import com.clickclap.activities.ContactsActivity;
import com.clickclap.activities.NewVideoActivity;
import com.clickclap.activities.RegistrationActivity;
import com.clickclap.rest.SpiceContext;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.request.video.ShareGrimaceRequest;
import com.clickclap.rest.request.video.ShareVideoRequest;
import com.clickclap.util.AnalyticsHelper;
import com.clickclap.view.dialogViews.ShareDialogView;
import com.octo.android.robospice.SpiceManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ShareDialogFragment extends DialogFragment implements Const, View.OnClickListener {

    public static final String TAG = "ShareDialog";

    private String mLink;
    private int mVideoId;
    private int mShareType;
    private boolean mCanBeClosed;

    SpiceManager mSpiceManager;

    View mCloseButton;

    private int mSelectedShareMethod;

    @IntDef({TYPE_EMOTICON, TYPE_SHOT, TYPE_GRIMACE, TYPE_GAME, TYPE_GREETING, TYPE_OTHER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ShareType {
    }

    public static final int TYPE_EMOTICON = 1;
    public static final int TYPE_SHOT = 2;
    public static final int TYPE_GRIMACE = 3;
    public static final int TYPE_GAME = 4;
    public static final int TYPE_GREETING = 5;
    public static final int TYPE_OTHER = 6;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args.containsKey("video_id")) {
            mVideoId = args.getInt("video_id");
        }

        if (args.containsKey("type")) {
            mShareType = args.getInt("type");
        }

        if (args.containsKey("closeable")) {
            mCanBeClosed = getArguments().getBoolean("closeable");
        }

        if (args.containsKey("link")) {
            mLink = getArguments().getString("link");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = new ShareDialogView(getActivity());

        RadioGroup rg = (RadioGroup) view.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedShareMethod = checkedId;
            }
        });

        view.findViewById(R.id.btn_nav).setOnClickListener(this);
        mCloseButton = view.findViewById(R.id.btn_close);
        mCloseButton.setOnClickListener(this);
        if (isCanBeClosed()) {
            mCloseButton.setVisibility(View.VISIBLE);
        } else {
            mCloseButton.setVisibility(View.GONE);
        }

        AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.Dialog_No_Border)
                .setView(view)
                .setCancelable(false)
                .show();
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() instanceof RegistrationActivity) {
            RegistrationActivity activity = (RegistrationActivity) getActivity();
            activity.openCongratulationsWindow();
        } else if (getActivity() instanceof NewVideoActivity) {
            getActivity().finish();
        }
    }

    private String getText() {
        switch (mShareType) {
            case TYPE_SHOT:
                return App.getInstance().getString(R.string.share_shot);
            case TYPE_EMOTICON:
                return App.getInstance().getString(R.string.share_emoticon);
            case TYPE_GRIMACE:
                return App.getInstance().getString(R.string.share_grimace);
            case TYPE_GAME:
                return App.getInstance().getString(R.string.share_game);
            case TYPE_GREETING:
                return App.getInstance().getString(R.string.share_greeting);
            case TYPE_OTHER:
                return App.getInstance().getString(R.string.share_other);
            default:
                throw new IllegalArgumentException("Unknown share type!");
        }
    }

    private void share(int shareMethod) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.SHARE);
        String text = getText();
        switch (shareMethod) {
            case R.id.rb_sms:
                getActivity().startActivity(ContactsActivity.newShareInstance(getActivity(), text + " " + mLink, mVideoId, mShareType));
                break;
            case R.id.rb_msg:
                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.setType("text/plain");
                msg.putExtra(Intent.EXTRA_SUBJECT, text);
                msg.putExtra(Intent.EXTRA_TEXT, mLink);
                getActivity().startActivity(Intent.createChooser(msg, App.getInstance().getString(R.string.ch_msg)));
                if (mShareType == TYPE_GRIMACE) {
                    mSpiceManager.execute(new ShareGrimaceRequest(mVideoId, ""), new BaseRequestListener());
                } else {
                    mSpiceManager.execute(new ShareVideoRequest(mVideoId, "messenger"), new BaseRequestListener());
                }
                break;
            default:
                break;
        }

        //we should make close button available immediatly after sharing
        if (mCloseButton.getVisibility() != View.VISIBLE && shareMethod > 0) {
            mCloseButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_nav:
                share(mSelectedShareMethod);
                break;
            case R.id.btn_close:
                dismiss();
                break;
        }
    }

    public boolean isCanBeClosed() {
        return mCanBeClosed;
    }

    public static class Builder {
        Bundle mArgs;

        public Builder() {
            mArgs = new Bundle(4);
        }

        public ShareDialogFragment build() {
            ShareDialogFragment fragment = new ShareDialogFragment();
            fragment.setArguments(mArgs);
            return fragment;
        }

        public Builder setLink(String link) {
            mArgs.putString("link", link);
            return this;
        }

        public Builder setVideoId(int videoId) {
            mArgs.putInt("video_id", videoId);
            return this;
        }

        public Builder setShareType(@ShareType int shareType) {
            mArgs.putInt("type", shareType);
            return this;
        }

        public Builder setCanBeClosed(boolean canBeClosed) {
            mArgs.putBoolean("closeable", canBeClosed);
            return this;
        }
    }
}
