package com.clickclap.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.clickclap.AppUser;
import com.clickclap.R;
import com.clickclap.rest.SpiceContext;
import com.clickclap.model.UserInfo;
import com.clickclap.rest.listener.BaseRequestListener;
import com.clickclap.rest.request.user.EditUserInfoRequest;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by michael on 25.11.14.
 */
public class ActivateWidgetDialogFragment extends DialogFragment implements View.OnClickListener {

    SpiceManager mSpiceManager;

    public static ActivateWidgetDialogFragment newInstance() {
        ActivateWidgetDialogFragment fragment = new ActivateWidgetDialogFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        View view = View.inflate(getActivity(), R.layout.fragment_activate_widget, new RelativeLayout(getActivity()));
        Button nextButton = (Button) view.findViewById(R.id.btn_nav);
        nextButton.setOnClickListener(this);
        return new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(view)
                .show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_nav:
                UserInfo user = AppUser.get();
                user.setWidget(true);
                AppUser.set(user);
                mSpiceManager.execute(new EditUserInfoRequest(user), new BaseRequestListener());
                dismiss();
                break;
        }
    }
}
