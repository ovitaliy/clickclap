package com.clickclap.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.clickclap.R;
import com.clickclap.enums.GameType;

/**
 * Created by michael on 25.11.14.
 */
public class NewQuestionDialogFragment extends DialogFragment implements View.OnClickListener {

    public static NewQuestionDialogFragment newInstance(GameType gameType) {
        NewQuestionDialogFragment fragment = new NewQuestionDialogFragment();
        Bundle args = new Bundle(1);
        args.putSerializable("game_type", gameType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        View view = View.inflate(getActivity(), R.layout.fragment_new_question_dialog, new RelativeLayout(getActivity()));
        Button closeButton = (Button) view.findViewById(R.id.btn_close);
        Button nextButton = (Button) view.findViewById(R.id.btn_nav);
        nextButton.setOnClickListener(this);
        closeButton.setOnClickListener(this);
        return new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(view)
                .show();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_close:
                dismiss();
                break;
            case R.id.btn_nav:
                //NewVideoActivity.startNewInstance(getActivity(), VideoType.GAMES, (GameType) getArguments().get("game_type"));//TODO
                dismiss();
                break;
        }
    }
}
